﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ExampleGUI::OnGUI()
extern void ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514 (void);
// 0x00000002 System.Void ExampleGUI::ShowGUI(System.Int32)
extern void ExampleGUI_ShowGUI_m3A21CF5132CCE304624990EEB9311B27FC974279 (void);
// 0x00000003 System.Void ExampleGUI::HandleGooglePlayId(System.String)
extern void ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0 (void);
// 0x00000004 System.Void ExampleGUI::AttributionChangedCallback(com.adjust.sdk.AdjustAttribution)
extern void ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73 (void);
// 0x00000005 System.Void ExampleGUI::EventSuccessCallback(com.adjust.sdk.AdjustEventSuccess)
extern void ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D (void);
// 0x00000006 System.Void ExampleGUI::EventFailureCallback(com.adjust.sdk.AdjustEventFailure)
extern void ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE (void);
// 0x00000007 System.Void ExampleGUI::SessionSuccessCallback(com.adjust.sdk.AdjustSessionSuccess)
extern void ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C (void);
// 0x00000008 System.Void ExampleGUI::SessionFailureCallback(com.adjust.sdk.AdjustSessionFailure)
extern void ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059 (void);
// 0x00000009 System.Void ExampleGUI::DeferredDeeplinkCallback(System.String)
extern void ExampleGUI_DeferredDeeplinkCallback_m0490F45A5F0CAB4623C4DE98C9056D175511544B (void);
// 0x0000000A System.Void ExampleGUI::.ctor()
extern void ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D (void);
// 0x0000000B System.Void ExampleGUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m8004B56DF658186326E479AFFA0822C993DD72ED (void);
// 0x0000000C System.Void ExampleGUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6 (void);
// 0x0000000D System.Void ExampleGUI/<>c::<OnGUI>b__6_0(System.String)
extern void U3CU3Ec_U3COnGUIU3Eb__6_0_m44C8CA7B767C4F1F34EBEA0E08A25658315F0219 (void);
// 0x0000000E GoogleMobileAds.Api.RewardedAd AdsManager::GetRewardedAdInfo()
extern void AdsManager_GetRewardedAdInfo_m4930CED0217C9D5530AEDEC77D8CFCEF51F0FB33 (void);
// 0x0000000F System.Void AdsManager::add_OnAdRewardSuccess(AdsManager/AdsRewardSuccessDelegate)
extern void AdsManager_add_OnAdRewardSuccess_m4658B080D07C49D78A2EC7A21F099BDECB4AE95D (void);
// 0x00000010 System.Void AdsManager::remove_OnAdRewardSuccess(AdsManager/AdsRewardSuccessDelegate)
extern void AdsManager_remove_OnAdRewardSuccess_m7014CABED0E7AC7CF6B629B1B6CF1498FD01CCCD (void);
// 0x00000011 System.Void AdsManager::Awake()
extern void AdsManager_Awake_m41E841D8C091F00F7AB09B5403D9C3BD4BED7EBB (void);
// 0x00000012 System.Void AdsManager::Start()
extern void AdsManager_Start_m00CAFF84B03355B41A941006E80DB2F1D49E3EF3 (void);
// 0x00000013 System.Void AdsManager::SetInterstitialEvents()
extern void AdsManager_SetInterstitialEvents_m5BF06DEF0FFCF9591FC9FF64E463429855FAF7C9 (void);
// 0x00000014 System.Void AdsManager::SetRewardedEvents()
extern void AdsManager_SetRewardedEvents_m6D6B5755B2BEE37BB817051FF3A036F2117FD3B1 (void);
// 0x00000015 System.Void AdsManager::OnDisable()
extern void AdsManager_OnDisable_m6C06FADBF21F3490168ABAE34676F4854A48A47C (void);
// 0x00000016 System.Void AdsManager::Interstitial_OnAdPaid(GoogleMobileAds.Api.AdValue)
extern void AdsManager_Interstitial_OnAdPaid_mE1BB2946B860F37A6FDC26C037B7A9B7D5F994A2 (void);
// 0x00000017 System.Void AdsManager::Interstitial_OnAdImpressionRecorded()
extern void AdsManager_Interstitial_OnAdImpressionRecorded_m83F85AC7A2E1F3A2BB990FB81CADB9D0B74FC4B1 (void);
// 0x00000018 System.Void AdsManager::Interstitial_OnAdFullScreenContentOpened()
extern void AdsManager_Interstitial_OnAdFullScreenContentOpened_m4DAA50CD9F49E35ABAAA2FC21753A7FE470B6248 (void);
// 0x00000019 System.Void AdsManager::Interstitial_OnAdFullScreenContentFailed(GoogleMobileAds.Api.AdError)
extern void AdsManager_Interstitial_OnAdFullScreenContentFailed_mAD8110C843F051C1BFF4382BD8CACF4293BB2EB3 (void);
// 0x0000001A System.Void AdsManager::Interstitial_OnAdFullScreenContentClosed()
extern void AdsManager_Interstitial_OnAdFullScreenContentClosed_m20FFA8DCA618C20153B1DA7CA6120770A6B69EF9 (void);
// 0x0000001B System.Void AdsManager::Interstitial_OnAdClicked()
extern void AdsManager_Interstitial_OnAdClicked_m63AD53CF5E333C912918AE3B204F329E06C23B24 (void);
// 0x0000001C System.Void AdsManager::Rewarded_OnAdPaid(GoogleMobileAds.Api.AdValue)
extern void AdsManager_Rewarded_OnAdPaid_m69FFD13FBDA650184D36DF69B65946DA65E0870B (void);
// 0x0000001D System.Void AdsManager::Rewarded_OnAdImpressionRecorded()
extern void AdsManager_Rewarded_OnAdImpressionRecorded_m38F857DCC44071139F06C9F7564B42A23AD869C8 (void);
// 0x0000001E System.Void AdsManager::Rewarded_OnAdFullScreenContentOpened()
extern void AdsManager_Rewarded_OnAdFullScreenContentOpened_m7088C2C989C4A71DFD0C7C88D16A24AEAA309C31 (void);
// 0x0000001F System.Void AdsManager::Rewarded_OnAdFullScreenContentFailed(GoogleMobileAds.Api.AdError)
extern void AdsManager_Rewarded_OnAdFullScreenContentFailed_mC01D81197F5C50F54B41E1A67273E7FC907C0455 (void);
// 0x00000020 System.Void AdsManager::Rewarded_OnAdFullScreenContentClosed()
extern void AdsManager_Rewarded_OnAdFullScreenContentClosed_m6DEDB1D092E615C99FA69BB6759B097B3D86EADD (void);
// 0x00000021 System.Void AdsManager::Rewarded_OnAdClicked()
extern void AdsManager_Rewarded_OnAdClicked_m56129EFA6EE079FF344060A7316B8C0ACB81EB2C (void);
// 0x00000022 System.Void AdsManager::LoadBannerAds()
extern void AdsManager_LoadBannerAds_m76B46F4AEF2ECBD3E76711588D4445842CEAC9DA (void);
// 0x00000023 System.Void AdsManager::LoadInterstitialAds()
extern void AdsManager_LoadInterstitialAds_mA12DE7ED1810B0C535E6B02904F420C6C9F088EC (void);
// 0x00000024 System.Void AdsManager::LoadRewardedAds()
extern void AdsManager_LoadRewardedAds_m2546F5FC35D817F97A6BDC8C8C3400287E2EA9BE (void);
// 0x00000025 System.Void AdsManager::RequestRewardedAds(System.String)
extern void AdsManager_RequestRewardedAds_m2C6C742352D6BB34ED2F6E67F3212F72DF96D223 (void);
// 0x00000026 System.Void AdsManager::OnGetReward(GoogleMobileAds.Api.Reward)
extern void AdsManager_OnGetReward_mB1C6846FBB3ECC86581EF12D9677D0D72F3E3623 (void);
// 0x00000027 System.Void AdsManager::RequestInterstitial()
extern void AdsManager_RequestInterstitial_mFA0DD2D687719943460CDD053C44AE096661FC93 (void);
// 0x00000028 System.Void AdsManager::.ctor()
extern void AdsManager__ctor_m6A4EB92F72E9B7F28CC4E62F87F54F89CBAD6B3C (void);
// 0x00000029 System.Void AdsManager::.cctor()
extern void AdsManager__cctor_m9183945BB8BEA85401F1952C993E3FD6333D0A92 (void);
// 0x0000002A System.Void AdsManager::<Start>b__18_0(GoogleMobileAds.Api.InitializationStatus)
extern void AdsManager_U3CStartU3Eb__18_0_mD5CBA755AFE5045A563116519089A6E4C1D47B71 (void);
// 0x0000002B System.Void AdsManager::<LoadInterstitialAds>b__35_0(GoogleMobileAds.Api.InterstitialAd,GoogleMobileAds.Api.LoadAdError)
extern void AdsManager_U3CLoadInterstitialAdsU3Eb__35_0_m9FD502D1A32728CE332F1A8E4BD1811E5F0B2AF3 (void);
// 0x0000002C System.Void AdsManager::<LoadRewardedAds>b__36_0(GoogleMobileAds.Api.RewardedAd,GoogleMobileAds.Api.LoadAdError)
extern void AdsManager_U3CLoadRewardedAdsU3Eb__36_0_m2FD4A41592D38EB3CCF9E259DC66D0BC7677C650 (void);
// 0x0000002D System.Void AdsManager/AdsRewardSuccessDelegate::.ctor(System.Object,System.IntPtr)
extern void AdsRewardSuccessDelegate__ctor_m5D24EFB03154EC18222127814D6217678206B2DC (void);
// 0x0000002E System.Void AdsManager/AdsRewardSuccessDelegate::Invoke(System.String)
extern void AdsRewardSuccessDelegate_Invoke_m73D12F2CB60B95B30447C1D442483C7F7955A059 (void);
// 0x0000002F System.IAsyncResult AdsManager/AdsRewardSuccessDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void AdsRewardSuccessDelegate_BeginInvoke_m2D3F8251156FA7D4117514522E573A813006C17D (void);
// 0x00000030 System.Void AdsManager/AdsRewardSuccessDelegate::EndInvoke(System.IAsyncResult)
extern void AdsRewardSuccessDelegate_EndInvoke_mD42D874EE40FA61E68DB6C03DB86A0E966AC34B8 (void);
// 0x00000031 System.Void AuthenticationPage::Update()
extern void AuthenticationPage_Update_mF9E17C6FB363D3323CAD217432C3F9C3B4DA7F43 (void);
// 0x00000032 System.Void AuthenticationPage::SetLabel(System.String)
extern void AuthenticationPage_SetLabel_mD00348C4952CF200CDBC9B0785E4B7CBEC097953 (void);
// 0x00000033 System.Void AuthenticationPage::.ctor()
extern void AuthenticationPage__ctor_mB01D55AAA281991663012BEEA31C4D96FAE6F3A9 (void);
// 0x00000034 System.Void BonusCard::InitCard(RewardEnum,UnityEngine.Sprite,System.Int32)
extern void BonusCard_InitCard_m2DC0665E3456C3514D80406CDCAA62E736242CD0 (void);
// 0x00000035 System.Boolean BonusCard::isBonusClaimed()
extern void BonusCard_isBonusClaimed_m4C9BAFFA5BDC108ED12B39D9038B64E11CFC6348 (void);
// 0x00000036 System.Void BonusCard::ReadyToClaim()
extern void BonusCard_ReadyToClaim_m0D80D2226DEAE045C7B7683CC2D1FAD1F926264B (void);
// 0x00000037 System.Void BonusCard::Claim()
extern void BonusCard_Claim_mD08DB9690D726AD60903E36B68582FBB5FD89BE9 (void);
// 0x00000038 System.Int32 BonusCard::GetBonusCoin()
extern void BonusCard_GetBonusCoin_mFF2164E9743D42079253A5347129BAC02E98D48B (void);
// 0x00000039 System.Void BonusCard::.ctor()
extern void BonusCard__ctor_m96A5469B1886D33204EE34C0DA5741B4B092F4B8 (void);
// 0x0000003A System.Void CoinShop::Start()
extern void CoinShop_Start_mD6A479F750CFB256AEC73C3202070C47873B0510 (void);
// 0x0000003B System.Void CoinShop::SetCoinLabel()
extern void CoinShop_SetCoinLabel_mA4F5A28D23F1CBFDFDD7F5116D0CA615B43789D7 (void);
// 0x0000003C System.Void CoinShop::Buy()
extern void CoinShop_Buy_m7590780DCDBA63E33F4611E21B6F9F07F118FFAB (void);
// 0x0000003D System.Collections.IEnumerator CoinShop::Loading()
extern void CoinShop_Loading_m43720D02C7558970FB6C222256F3C6F2FF82EB3E (void);
// 0x0000003E System.Void CoinShop::OnSuccessPurchasing(System.Int32)
extern void CoinShop_OnSuccessPurchasing_m92357969D036D22DC8EF31C8DAB90437F96A9FE9 (void);
// 0x0000003F System.Void CoinShop::OnfailedPurchasing()
extern void CoinShop_OnfailedPurchasing_m6F03F22C92226DFA6FCE49306F6E1FC84BAB3050 (void);
// 0x00000040 System.Void CoinShop::.ctor()
extern void CoinShop__ctor_mFAB356CFFCB176E7F7A1E54A2032C2B6CC1BCCE5 (void);
// 0x00000041 System.Void CoinShop/<Loading>d__5::.ctor(System.Int32)
extern void U3CLoadingU3Ed__5__ctor_m9C90B91C82E928EF7A853E50760B569601E12B58 (void);
// 0x00000042 System.Void CoinShop/<Loading>d__5::System.IDisposable.Dispose()
extern void U3CLoadingU3Ed__5_System_IDisposable_Dispose_mE6C0D3D9053E2216B69107491C01A677250D45AF (void);
// 0x00000043 System.Boolean CoinShop/<Loading>d__5::MoveNext()
extern void U3CLoadingU3Ed__5_MoveNext_m3990C8AD3522A51B23E3C23911108510F60D49A3 (void);
// 0x00000044 System.Object CoinShop/<Loading>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadingU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDA4AB795ECCA2AF707571E07DE0B3F1A73055F12 (void);
// 0x00000045 System.Void CoinShop/<Loading>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadingU3Ed__5_System_Collections_IEnumerator_Reset_m1DF7BCA5CD420A31D590D3401433484F8E55B721 (void);
// 0x00000046 System.Object CoinShop/<Loading>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadingU3Ed__5_System_Collections_IEnumerator_get_Current_m6C801CE56B8729FAE8DC7D053DB23FB6F5102ADB (void);
// 0x00000047 System.Void DailyLoginPage::Start()
extern void DailyLoginPage_Start_mFE687C3075DBD5D36B40106D9A1A1FDEEEA9C724 (void);
// 0x00000048 System.Void DailyLoginPage::Init(System.Int32)
extern void DailyLoginPage_Init_mAF13C77A99CA228974C86C97D124C45A989702C2 (void);
// 0x00000049 System.Void DailyLoginPage::CheckClaimButton(System.Boolean)
extern void DailyLoginPage_CheckClaimButton_m5CEA81F9D629862292A1FC9BB53FDDBDE8250119 (void);
// 0x0000004A System.Void DailyLoginPage::.ctor()
extern void DailyLoginPage__ctor_m76FC80E8AD0F958EB74C75905A0C799DB53D5199 (void);
// 0x0000004B System.Void DailyLoginPage/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6804D13585A893E5B3C65E8C739957059B71FFDB (void);
// 0x0000004C System.Void DailyLoginPage/<>c__DisplayClass9_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__0_mC7C88E37AE8D8144D38096C7C34AB7AB40E9771B (void);
// 0x0000004D System.Void DailyLoginPage/<>c__DisplayClass9_0::<Start>b__1()
extern void U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__1_m59B3E43571A52CA52EC1B5DD185CA69A21C3F5B6 (void);
// 0x0000004E System.Void DailyLoginPage/<>c__DisplayClass9_0::<Start>b__2()
extern void U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__2_m945C3D0CD0718F3D6637E5CC9F94B8C8E10D8AEB (void);
// 0x0000004F System.Void RewardData::.ctor()
extern void RewardData__ctor_m56FA7E6414745F0980B560439A3FD1E8F76F5725 (void);
// 0x00000050 System.Void ForceApplicationPage::Start()
extern void ForceApplicationPage_Start_mDEE93BC8AFCDBC2A9BD0E9BBE4C7DC1294778E48 (void);
// 0x00000051 System.Collections.IEnumerator ForceApplicationPage::CheckVersion()
extern void ForceApplicationPage_CheckVersion_m0D0C26E0D14498CFA0CDB83F4C3A9B1D3D2BA1E1 (void);
// 0x00000052 System.Void ForceApplicationPage::.ctor()
extern void ForceApplicationPage__ctor_mE3788C569CDF901D1C109AADCA5CE2C55CE5F06A (void);
// 0x00000053 System.Void ForceApplicationPage::<Start>b__4_0()
extern void ForceApplicationPage_U3CStartU3Eb__4_0_mE7D1D61B8438C29D08881B30F181F454C96F4E27 (void);
// 0x00000054 System.Void ForceApplicationPage/<CheckVersion>d__5::.ctor(System.Int32)
extern void U3CCheckVersionU3Ed__5__ctor_mEF2AF7730ABDC5BA06BCED81A228B80929E98ACB (void);
// 0x00000055 System.Void ForceApplicationPage/<CheckVersion>d__5::System.IDisposable.Dispose()
extern void U3CCheckVersionU3Ed__5_System_IDisposable_Dispose_m69B4900DD1658211E6A938A2498DED794CF103F8 (void);
// 0x00000056 System.Boolean ForceApplicationPage/<CheckVersion>d__5::MoveNext()
extern void U3CCheckVersionU3Ed__5_MoveNext_m94A69DBD46F83AEB945C05441D4C2FB101B46C10 (void);
// 0x00000057 System.Object ForceApplicationPage/<CheckVersion>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckVersionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0386B7CDBC92D99AE0682B6D8FC452D476BAB10 (void);
// 0x00000058 System.Void ForceApplicationPage/<CheckVersion>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCheckVersionU3Ed__5_System_Collections_IEnumerator_Reset_m26429745C43250B887E1030FB31AB2085E1FD749 (void);
// 0x00000059 System.Object ForceApplicationPage/<CheckVersion>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCheckVersionU3Ed__5_System_Collections_IEnumerator_get_Current_mD78592F687BB2ECD39F70D10E527CB19942BA292 (void);
// 0x0000005A System.Int32 GameServices::GetJarakWaktu()
extern void GameServices_GetJarakWaktu_m8F16953B9F5148FA4B1CFA8096D18DB767406A32 (void);
// 0x0000005B System.Void GameServices::Awake()
extern void GameServices_Awake_m231631DEB029E8BA32ECF46F138327730724399C (void);
// 0x0000005C System.Collections.IEnumerator GameServices::CheckAppVersion()
extern void GameServices_CheckAppVersion_mA9038BA1C1B69C7F29FEC8D8C907281A291F94C6 (void);
// 0x0000005D System.Void GameServices::Instance_SignedIn()
extern void GameServices_Instance_SignedIn_m095C581AE22F22A20FCDFBED4A5FCE9D54C970BD (void);
// 0x0000005E System.Void GameServices::Instance_SignInFailed(Unity.Services.Core.RequestFailedException)
extern void GameServices_Instance_SignInFailed_mA0E8F7925C3D2791A9F49815FC0A9952DBB2E14E (void);
// 0x0000005F System.String GameServices::GetNewVersion()
extern void GameServices_GetNewVersion_m06BB42215BDAB16FCEA68858CDA1AFE4D62B8432 (void);
// 0x00000060 System.Void GameServices::Start()
extern void GameServices_Start_mCB5B371E22FA9C7F37D1AA6C8E7B662967BFA4AA (void);
// 0x00000061 System.Void GameServices::SetHighscoreButton()
extern void GameServices_SetHighscoreButton_mB6816C39B3F261F815C350C2629A68DD1618A0CC (void);
// 0x00000062 System.Void GameServices::OnDisable()
extern void GameServices_OnDisable_m01A32EB39C83E2ED3492FB20F4C5D47C3A106401 (void);
// 0x00000063 System.Void GameServices::AndroidNotificationCenter_OnNotificationReceived(Unity.Notifications.Android.AndroidNotificationIntentData)
extern void GameServices_AndroidNotificationCenter_OnNotificationReceived_mA7F538569F1B0189AAC48B52501C76663A71FEBB (void);
// 0x00000064 System.Void GameServices::SetNewLeaderboard(System.Int32)
extern void GameServices_SetNewLeaderboard_m6EE5310DF5817050CD9B91938292DDC45993E0A0 (void);
// 0x00000065 System.Void GameServices::ChangeName(System.String)
extern void GameServices_ChangeName_m31A855B0BD1CB281CAE5B7BE03F2859D1525CEAD (void);
// 0x00000066 System.Void GameServices::CheckTimeSave()
extern void GameServices_CheckTimeSave_m0A6F576ACB8154D336C3F0052AFC4E9AE6E6A682 (void);
// 0x00000067 System.Void GameServices::RequestNotification(System.String,System.String,System.Int32)
extern void GameServices_RequestNotification_mBBC0B2272F8EAA00C4F34F425372CCB86D0CEDD9 (void);
// 0x00000068 System.Collections.IEnumerator GameServices::RequestNotificationPermission(System.String,System.String,System.Int32)
extern void GameServices_RequestNotificationPermission_m011CED5BDF110C78256628064C964E99BFAF1F76 (void);
// 0x00000069 System.Void GameServices::CreateNotification(System.String,System.String,System.Int32)
extern void GameServices_CreateNotification_m7E4D5C9E12261FA472F48B13F0DF6580B5C2D712 (void);
// 0x0000006A System.Void GameServices::OnPlayGamesSignedIn(GooglePlayGames.BasicApi.SignInStatus)
extern void GameServices_OnPlayGamesSignedIn_m015EFA423FC417027273D6509B95CD1F2F9A8FFD (void);
// 0x0000006B System.Void GameServices::OnGetCallbacks(System.String)
extern void GameServices_OnGetCallbacks_m95043980ED50E6B328F663FC5D857D87AD7944E3 (void);
// 0x0000006C System.Threading.Tasks.Task GameServices::SignInWithGooglePlayGamesAsync(System.String)
extern void GameServices_SignInWithGooglePlayGamesAsync_mBE34B181A5D81CFB95735E9AE04F22046BE83256 (void);
// 0x0000006D System.Threading.Tasks.Task GameServices::SignInAnonymouslyAsync()
extern void GameServices_SignInAnonymouslyAsync_mE8BD0F882604907A6301D1FA7F54FEBDAD7FE287 (void);
// 0x0000006E System.Void GameServices::.ctor()
extern void GameServices__ctor_mEF5841AC407E60DBD0C708E03B7BEA0B7C3F07F3 (void);
// 0x0000006F System.Void GameServices::<Start>b__18_0()
extern void GameServices_U3CStartU3Eb__18_0_m6A3796982BFC67408936DCFE6F207DA0AC11BEB3 (void);
// 0x00000070 System.Void GameServices::<Start>b__18_1()
extern void GameServices_U3CStartU3Eb__18_1_m3160889901018D9193D3B6F2D0D97998916268DE (void);
// 0x00000071 System.Void GameServices::<SetNewLeaderboard>b__22_0(System.Boolean)
extern void GameServices_U3CSetNewLeaderboardU3Eb__22_0_mF55CE4E010995001F3989D395FC4ADA2C20B97E3 (void);
// 0x00000072 System.Void GameServices/<Awake>d__13::MoveNext()
extern void U3CAwakeU3Ed__13_MoveNext_mCF7E898FE5CEEDBFBA0B2181063C660EEB7F150C (void);
// 0x00000073 System.Void GameServices/<Awake>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAwakeU3Ed__13_SetStateMachine_m87671A5980EB6DD5A90613EF13F034A439D707F1 (void);
// 0x00000074 System.Void GameServices/<>c::.cctor()
extern void U3CU3Ec__cctor_mD3D1474B891E3824A99F67B8E9E15B11256F1F9E (void);
// 0x00000075 System.Void GameServices/<>c::.ctor()
extern void U3CU3Ec__ctor_m4F9B0E11F42B95E7F9C449D550561CE95A6CB928 (void);
// 0x00000076 System.Boolean GameServices/<>c::<CheckAppVersion>b__14_0(DataServer)
extern void U3CU3Ec_U3CCheckAppVersionU3Eb__14_0_m1EE8660BD5FD3777E0EEFC5D501DD61C7ABA5916 (void);
// 0x00000077 System.Void GameServices/<CheckAppVersion>d__14::.ctor(System.Int32)
extern void U3CCheckAppVersionU3Ed__14__ctor_m1EA04C2C47777509E49989F63DCA1CD88D0259AB (void);
// 0x00000078 System.Void GameServices/<CheckAppVersion>d__14::System.IDisposable.Dispose()
extern void U3CCheckAppVersionU3Ed__14_System_IDisposable_Dispose_m325A0BDA6EE5AA4A6D2541D785F0067BEB1C5F85 (void);
// 0x00000079 System.Boolean GameServices/<CheckAppVersion>d__14::MoveNext()
extern void U3CCheckAppVersionU3Ed__14_MoveNext_m04899718E75C7FCB5E2DE58877F14642ECEA8C52 (void);
// 0x0000007A System.Object GameServices/<CheckAppVersion>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAppVersionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB21710A43D6257D31873B8075B2646FC1447D2 (void);
// 0x0000007B System.Void GameServices/<CheckAppVersion>d__14::System.Collections.IEnumerator.Reset()
extern void U3CCheckAppVersionU3Ed__14_System_Collections_IEnumerator_Reset_m053061EFFB5925A8206D669B20A190CAEE69985F (void);
// 0x0000007C System.Object GameServices/<CheckAppVersion>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAppVersionU3Ed__14_System_Collections_IEnumerator_get_Current_mAE810789CB809B98459A3DFAA9EEBB073A3ADD70 (void);
// 0x0000007D System.Void GameServices/<ChangeName>d__23::MoveNext()
extern void U3CChangeNameU3Ed__23_MoveNext_m715380D2F220ECA84F1FD6C7768EA280FF87F06E (void);
// 0x0000007E System.Void GameServices/<ChangeName>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CChangeNameU3Ed__23_SetStateMachine_mEBC90E9B5632FF9FE2DC69AB44A82A0511006400 (void);
// 0x0000007F System.Void GameServices/<RequestNotificationPermission>d__26::.ctor(System.Int32)
extern void U3CRequestNotificationPermissionU3Ed__26__ctor_m6E9F7D0F834646C32C6EBB0155F7B11242E9AA70 (void);
// 0x00000080 System.Void GameServices/<RequestNotificationPermission>d__26::System.IDisposable.Dispose()
extern void U3CRequestNotificationPermissionU3Ed__26_System_IDisposable_Dispose_m461754E59EF20BB1A4A84D6080E71F64AC57FB15 (void);
// 0x00000081 System.Boolean GameServices/<RequestNotificationPermission>d__26::MoveNext()
extern void U3CRequestNotificationPermissionU3Ed__26_MoveNext_mAF5299AA530FE588CD39865AB1735CF942A9AB29 (void);
// 0x00000082 System.Object GameServices/<RequestNotificationPermission>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestNotificationPermissionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m527E69A8E393AA639DEB91380D6C763804D2BEA9 (void);
// 0x00000083 System.Void GameServices/<RequestNotificationPermission>d__26::System.Collections.IEnumerator.Reset()
extern void U3CRequestNotificationPermissionU3Ed__26_System_Collections_IEnumerator_Reset_mA53E92EAE38D26E1160682E7799905731A4FFB8A (void);
// 0x00000084 System.Object GameServices/<RequestNotificationPermission>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CRequestNotificationPermissionU3Ed__26_System_Collections_IEnumerator_get_Current_m743282682E50888566AFF733BDC850F6DBC6FC13 (void);
// 0x00000085 System.Void GameServices/<OnPlayGamesSignedIn>d__28::MoveNext()
extern void U3COnPlayGamesSignedInU3Ed__28_MoveNext_m58E4F170537D3683633F8C5A21D8754E132CB4F7 (void);
// 0x00000086 System.Void GameServices/<OnPlayGamesSignedIn>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnPlayGamesSignedInU3Ed__28_SetStateMachine_mE675A3114870098867823FD27648010FAC1EE263 (void);
// 0x00000087 System.Void GameServices/<OnGetCallbacks>d__29::MoveNext()
extern void U3COnGetCallbacksU3Ed__29_MoveNext_m348948E06A523F4B57F9A23D4FCEC79176C2B7CB (void);
// 0x00000088 System.Void GameServices/<OnGetCallbacks>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnGetCallbacksU3Ed__29_SetStateMachine_m846EB23EF52841941A1983569EFA2942BE2A3999 (void);
// 0x00000089 System.Void GameServices/<SignInWithGooglePlayGamesAsync>d__30::MoveNext()
extern void U3CSignInWithGooglePlayGamesAsyncU3Ed__30_MoveNext_m0A04B2BCA035997FD36186DBFE1BAC72993E9C68 (void);
// 0x0000008A System.Void GameServices/<SignInWithGooglePlayGamesAsync>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSignInWithGooglePlayGamesAsyncU3Ed__30_SetStateMachine_mF2A1FA66B49F9C79DF43A2E6970D8BDD74CB9F99 (void);
// 0x0000008B System.Void GameServices/<SignInAnonymouslyAsync>d__31::MoveNext()
extern void U3CSignInAnonymouslyAsyncU3Ed__31_MoveNext_mCA66806BE6EBDDF2ED3B120696DB58C656A15D3C (void);
// 0x0000008C System.Void GameServices/<SignInAnonymouslyAsync>d__31::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSignInAnonymouslyAsyncU3Ed__31_SetStateMachine_mF2072EE052257698D917DD162BF4DB7CADD8A9CB (void);
// 0x0000008D System.Void DataServer::.ctor()
extern void DataServer__ctor_m7045FF9E088140546AEDF9D77172739ED54E8B16 (void);
// 0x0000008E System.Void GamesonData::.ctor()
extern void GamesonData__ctor_m0967DE5A98F9C729AFF306FF28C373D0DCCD285F (void);
// 0x0000008F System.Void HomePage::OnEnable()
extern void HomePage_OnEnable_mDD26B1F57CCE8B74FEE064461C2832CAC1A30D73 (void);
// 0x00000090 System.Void HomePage::OnDisable()
extern void HomePage_OnDisable_mA70DC8E3819A32692FFF5C8E716DB8506F0D8F46 (void);
// 0x00000091 System.Void HomePage::Start()
extern void HomePage_Start_m2B172C37BA2990DA79B559EF8D61322CC9635E91 (void);
// 0x00000092 System.Void HomePage::Instance_OnAdRewardSuccess(System.String)
extern void HomePage_Instance_OnAdRewardSuccess_m6D0460BC5B620141BF0FDF50CFAD2FFE2E6D3FF1 (void);
// 0x00000093 System.Void HomePage::CheckHourly()
extern void HomePage_CheckHourly_mBF73BAA8C58766832A1F27B531362122D0945E69 (void);
// 0x00000094 System.Void HomePage::Update()
extern void HomePage_Update_m5E1D4387780D6ED369ADF41FC4556296A219AA25 (void);
// 0x00000095 System.Void HomePage::.ctor()
extern void HomePage__ctor_m95C69629EC5C5B408499B7AA3C69CFBD7CF70A1E (void);
// 0x00000096 System.Void HomePage::<Start>b__18_0()
extern void HomePage_U3CStartU3Eb__18_0_m43A230BD690EA07A027A52EA9F53139D54A31E0B (void);
// 0x00000097 System.Void HomePage::<Start>b__18_1()
extern void HomePage_U3CStartU3Eb__18_1_mFA5F6FECD9E571C17D97E2B0946EB7E51F878548 (void);
// 0x00000098 System.Void HomePage::<Start>b__18_2()
extern void HomePage_U3CStartU3Eb__18_2_m091E5F713EAE8C20B1200879F98DDF352F906188 (void);
// 0x00000099 System.Void HomePage::<Start>b__18_6()
extern void HomePage_U3CStartU3Eb__18_6_m659006878A679F42D1B7683E165DAD009D219B3F (void);
// 0x0000009A System.Void HomePage/<>c::.cctor()
extern void U3CU3Ec__cctor_mDC3B104D0105E60CAFE67313847A93623A6E2C8C (void);
// 0x0000009B System.Void HomePage/<>c::.ctor()
extern void U3CU3Ec__ctor_m3055D1B38AEB34340AA53D51536246EF9FDA398F (void);
// 0x0000009C System.Void HomePage/<>c::<Start>b__18_3()
extern void U3CU3Ec_U3CStartU3Eb__18_3_m5EB1FDBD269868EE1D66F7A7B6701C74B99D2839 (void);
// 0x0000009D System.Void HomePage/<>c::<Start>b__18_4()
extern void U3CU3Ec_U3CStartU3Eb__18_4_m1DE0C7D74DB536D0B4B1743AA64282A975FB25BA (void);
// 0x0000009E System.Void HomePage/<>c::<Start>b__18_5()
extern void U3CU3Ec_U3CStartU3Eb__18_5_m0E97550454914FE3B4F742D94E83709ACF9B79DD (void);
// 0x0000009F System.Void HomePage/<>c::<Start>b__18_7()
extern void U3CU3Ec_U3CStartU3Eb__18_7_m16C474CC910CC026F986A48CCB9831BA4665A4B8 (void);
// 0x000000A0 System.Void leaderboardCard::SetLeaderbordData(System.String,System.Int32,System.Int32)
extern void leaderboardCard_SetLeaderbordData_mBD492F0F8B17531080C9F7F2C5651188188AFC47 (void);
// 0x000000A1 System.Void leaderboardCard::.ctor()
extern void leaderboardCard__ctor_mC486DD947FB65B270361F147F0FD882B5DF8BE79 (void);
// 0x000000A2 System.Void LeaderboardPage::Start()
extern void LeaderboardPage_Start_m280B175DE3774A56F9D54233669E43D244975F3C (void);
// 0x000000A3 System.Void LeaderboardPage::OnEnable()
extern void LeaderboardPage_OnEnable_mCF02776FBCBDA89D8026417BFF1CAA6F7E11E387 (void);
// 0x000000A4 System.Void LeaderboardPage::GetLeaderboardBucketData()
extern void LeaderboardPage_GetLeaderboardBucketData_m76ABD728A190EFB413C61F2DBD1C975B80D331A6 (void);
// 0x000000A5 System.Void LeaderboardPage::Setleaderboard(System.String,System.Int32,System.Int32)
extern void LeaderboardPage_Setleaderboard_m1325F5230846E581D8D4BD7C3C826B1CC41B93E6 (void);
// 0x000000A6 System.Void LeaderboardPage::.ctor()
extern void LeaderboardPage__ctor_mD9FE83FEFC7FA5FCE42C645EC47AA6100AF0B179 (void);
// 0x000000A7 System.Void LeaderboardPage::<Start>b__8_1()
extern void LeaderboardPage_U3CStartU3Eb__8_1_m9365A95D2FE8BC9AB4ABDF89C040D7B501F748F6 (void);
// 0x000000A8 System.Void LeaderboardPage::<Start>b__8_2()
extern void LeaderboardPage_U3CStartU3Eb__8_2_m541E63BBC5800509D17576A4ABBAEE2F62C64F37 (void);
// 0x000000A9 System.Void LeaderboardPage/<>c::.cctor()
extern void U3CU3Ec__cctor_m5F913BEB1DBE713CF41B9D0B88AF578FF487E1B4 (void);
// 0x000000AA System.Void LeaderboardPage/<>c::.ctor()
extern void U3CU3Ec__ctor_m680A3AD472AF45C7CBB52A4F562E2E64514DE414 (void);
// 0x000000AB System.Void LeaderboardPage/<>c::<Start>b__8_0(System.String)
extern void U3CU3Ec_U3CStartU3Eb__8_0_m41E5C7E7495128D667E3C9021FF5D03259AE99F3 (void);
// 0x000000AC System.Void LockLevel::Start()
extern void LockLevel_Start_m28E89770A180FC69D20C149518CBAF74781F2A31 (void);
// 0x000000AD System.Void LockLevel::CheckLockLevel()
extern void LockLevel_CheckLockLevel_m533E7B09FCB92798D9CFFE4973C8D3F9C74A5D59 (void);
// 0x000000AE System.Void LockLevel::.ctor()
extern void LockLevel__ctor_m07E4121E67EE8586E2E6FF1F0B963CD970445047 (void);
// 0x000000AF System.Void LockLevel::<Start>b__4_0()
extern void LockLevel_U3CStartU3Eb__4_0_mE172E8E1A4F34F04D23B0295F7D45043FCCAF386 (void);
// 0x000000B0 System.Void LogManager::Awake()
extern void LogManager_Awake_m2BF00F6A757025035C6F9A5D07EB89E2DC623751 (void);
// 0x000000B1 System.Void LogManager::AddLog(System.String)
extern void LogManager_AddLog_mF66D82A38A5343E6969F9DC327059C9FB49D521F (void);
// 0x000000B2 System.Void LogManager::.ctor()
extern void LogManager__ctor_m37558B87A7D080BB5F3ABF8487FF1900BCD0136D (void);
// 0x000000B3 System.Void packageCard::OnEnable()
extern void packageCard_OnEnable_m78130AC8225BA066B902230E1E700DE40D9F1269 (void);
// 0x000000B4 System.Void packageCard::SetCard(System.Int32,System.Int32)
extern void packageCard_SetCard_mC6FDF80B360469AE7809E5C593A8293F209595A9 (void);
// 0x000000B5 System.Void packageCard::BuyPackage()
extern void packageCard_BuyPackage_mE578764F9F031B9B2E1632C5148398A70C3245BA (void);
// 0x000000B6 System.Void packageCard::.ctor()
extern void packageCard__ctor_m0EEAEADF7AC371247BA03098F562EE5E48D14184 (void);
// 0x000000B7 System.Void packageCard::<OnEnable>b__18_0()
extern void packageCard_U3COnEnableU3Eb__18_0_m7DF897B7F7FA994811ABE1DCCD52A719B31FA015 (void);
// 0x000000B8 System.Void packageCard::<OnEnable>b__18_1()
extern void packageCard_U3COnEnableU3Eb__18_1_m27622B3F0C50C9498158F829D45797F8CDC0AAAF (void);
// 0x000000B9 System.Void packageCard::<SetCard>b__19_0()
extern void packageCard_U3CSetCardU3Eb__19_0_mDD65B3B8238FC3712D35E26F98E6D5F5A6B6AE44 (void);
// 0x000000BA System.Void packageManager::Awake()
extern void packageManager_Awake_m2E2E54DDA0E59AADD45A54F292F2438E5F8D3458 (void);
// 0x000000BB packageManager/PackageLevel packageManager::GetPackageLevel(System.String)
extern void packageManager_GetPackageLevel_m54161E5C2FA19691A8B343AD0D1F9FB7FE7ECE73 (void);
// 0x000000BC System.Void packageManager::SaveData()
extern void packageManager_SaveData_m809351C0600AAAE3C89852844C7023868D35BECB (void);
// 0x000000BD System.Void packageManager::LoadData()
extern void packageManager_LoadData_mD45912A020D814A01295CA6B503828F19FC9862C (void);
// 0x000000BE System.Void packageManager::HapusData()
extern void packageManager_HapusData_mEFFC2FCAC63A1E6F0290B8C027410C123CE570E2 (void);
// 0x000000BF System.String packageManager::GetPackageName(System.Int32)
extern void packageManager_GetPackageName_m4ABE2F1F7D454FEA5AC8A33D651C1776E89A80D0 (void);
// 0x000000C0 System.Int32 packageManager::GetMaxPackageList()
extern void packageManager_GetMaxPackageList_mD92F69AA1DA750EA4197B192C6E083B27476F7BB (void);
// 0x000000C1 System.Void packageManager::AddNewPackage(System.String)
extern void packageManager_AddNewPackage_m34FA68F41D418A64DD55FF1CAA014CBDCCFA5126 (void);
// 0x000000C2 System.Void packageManager::.ctor()
extern void packageManager__ctor_m9D602D64F26EE1D636DDF2B52788FFBFA78BD669 (void);
// 0x000000C3 System.Void packageManager/PackageLevel::UpdateData()
extern void PackageLevel_UpdateData_mF90B10607F4580F01F9E08E5F354848699B7CDCB (void);
// 0x000000C4 System.Void packageManager/PackageLevel::.ctor()
extern void PackageLevel__ctor_m5528328C1BA781187E657A2562873D4C32C8343F (void);
// 0x000000C5 System.Void packageManager/SaveFile::.ctor(packageManager/PackageLevel)
extern void SaveFile__ctor_m3518A8BB29A19126C7299991BBD61759E6638EE0 (void);
// 0x000000C6 System.Void packageManager/SaveFile::.ctor(packageManager/PackageLevel[])
extern void SaveFile__ctor_m9E56B22855EDB49B5EE1AE4269D70669F5C24771 (void);
// 0x000000C7 System.Void packageManager/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m28754AB3BA483A061F107F994D996FC297A722DC (void);
// 0x000000C8 System.Boolean packageManager/<>c__DisplayClass8_0::<GetPackageLevel>b__0(packageManager/PackageLevel)
extern void U3CU3Ec__DisplayClass8_0_U3CGetPackageLevelU3Eb__0_m82D0ACAA612C699712BB64D2E25EAC1CFE992260 (void);
// 0x000000C9 System.Void ResultPrefab::OnEnable()
extern void ResultPrefab_OnEnable_m4C5EBCB8DF7DB574E81988EC942658FEC6EC5C3D (void);
// 0x000000CA System.Collections.IEnumerator ResultPrefab::SetLabel()
extern void ResultPrefab_SetLabel_m0AA213B4B6E1C48BB11482D3F8FA69353EBF226B (void);
// 0x000000CB System.Void ResultPrefab::.ctor()
extern void ResultPrefab__ctor_m8A48EC4B633196196926E4A78F93B1543DF0911E (void);
// 0x000000CC System.Void ResultPrefab/<SetLabel>d__3::.ctor(System.Int32)
extern void U3CSetLabelU3Ed__3__ctor_m34F77AE44F246B34F28A68A5C13589E6F27318F1 (void);
// 0x000000CD System.Void ResultPrefab/<SetLabel>d__3::System.IDisposable.Dispose()
extern void U3CSetLabelU3Ed__3_System_IDisposable_Dispose_m56BB7C00BD8D8D8D81F34C2C45DA99F1C501C4AF (void);
// 0x000000CE System.Boolean ResultPrefab/<SetLabel>d__3::MoveNext()
extern void U3CSetLabelU3Ed__3_MoveNext_m701C74074D5FA5DA71FF7CCC7634FA460846DB50 (void);
// 0x000000CF System.Object ResultPrefab/<SetLabel>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetLabelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7954407A6770F7870DE7C06E242F2688ECF2436B (void);
// 0x000000D0 System.Void ResultPrefab/<SetLabel>d__3::System.Collections.IEnumerator.Reset()
extern void U3CSetLabelU3Ed__3_System_Collections_IEnumerator_Reset_mFB4DBEE9C4DA0912165048EDDF688F47A5C8042F (void);
// 0x000000D1 System.Object ResultPrefab/<SetLabel>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CSetLabelU3Ed__3_System_Collections_IEnumerator_get_Current_mAAFC1A456194A6FC40D769E18AD958D44FFD5E7D (void);
// 0x000000D2 System.Void SettingPage::Start()
extern void SettingPage_Start_mBACD95B247C12F6E75C1D3B52386089DA8B5F2D9 (void);
// 0x000000D3 System.Void SettingPage::ResetGameData()
extern void SettingPage_ResetGameData_m5FE7B350E8C034CA76AC4CCE0EA3524844A3B855 (void);
// 0x000000D4 System.Void SettingPage::.ctor()
extern void SettingPage__ctor_m9721B199B8B7847FB9178D83194E58A615E3F7B6 (void);
// 0x000000D5 System.Void SettingPage::<Start>b__5_0()
extern void SettingPage_U3CStartU3Eb__5_0_mC7215AE82B2A64487F2DA47AFF0E209404689B94 (void);
// 0x000000D6 System.Void SettingPage::<Start>b__5_1()
extern void SettingPage_U3CStartU3Eb__5_1_mE061C6E84FDC653D062FFB92C223C79539722DFC (void);
// 0x000000D7 System.Void SpinWheelPage::Start()
extern void SpinWheelPage_Start_mE954A16E7CE061F92C87E0DB264B37A4BC44190A (void);
// 0x000000D8 System.Void SpinWheelPage::OnEnable()
extern void SpinWheelPage_OnEnable_m779FDD1996C9A8A0842E83CC7253F6E450DD911D (void);
// 0x000000D9 System.Void SpinWheelPage::OnDisable()
extern void SpinWheelPage_OnDisable_mE583894A7564408AD896CEF2CBC85A5E4FE49826 (void);
// 0x000000DA System.Void SpinWheelPage::OnRewardedAdLoadSuccess()
extern void SpinWheelPage_OnRewardedAdLoadSuccess_m0A447507082BB43E4142399AF7F567C5782D576A (void);
// 0x000000DB System.Void SpinWheelPage::OnEndSpin(EasyUI.PickerWheelUI.WheelPiece)
extern void SpinWheelPage_OnEndSpin_m175B7ADFFDD43227353CBB976F94D216E7B25DFA (void);
// 0x000000DC System.Void SpinWheelPage::ShowResult(System.String)
extern void SpinWheelPage_ShowResult_m87307DEF121BC016ED7E7969475AA706197DBDD5 (void);
// 0x000000DD System.Void SpinWheelPage::HandleSpin()
extern void SpinWheelPage_HandleSpin_m9FD2142228F2C318CFF0A7EE9372CCE9AD24E2EB (void);
// 0x000000DE System.Void SpinWheelPage::.ctor()
extern void SpinWheelPage__ctor_mBCF404349DF84F3C0EF08B3CD743D38457CC131B (void);
// 0x000000DF System.Void SpinWheelPage/<>c::.cctor()
extern void U3CU3Ec__cctor_m2D81B0E56587B3E4CB8041240E4762450CA5430C (void);
// 0x000000E0 System.Void SpinWheelPage/<>c::.ctor()
extern void U3CU3Ec__ctor_mA1DB08B42717BD1F1CC9FBD73BCBEF0F161C7B29 (void);
// 0x000000E1 System.Void SpinWheelPage/<>c::<Start>b__7_0()
extern void U3CU3Ec_U3CStartU3Eb__7_0_m4EB083235910A85C7D0C704EFC91547F210C2D7E (void);
// 0x000000E2 System.Void SplashscreenPage::Start()
extern void SplashscreenPage_Start_mDD117CEE39934E0ABC3008C79C9AEA1370A39375 (void);
// 0x000000E3 System.Void SplashscreenPage::Video_loopPointReached(UnityEngine.Video.VideoPlayer)
extern void SplashscreenPage_Video_loopPointReached_m8DC9F24DBC2FDB9B9AD9C87ED74C940E791C4ADB (void);
// 0x000000E4 System.Void SplashscreenPage::PindahScene(System.String)
extern void SplashscreenPage_PindahScene_mC2499C7062A0572B630DE765F9865CD7FEB9E415 (void);
// 0x000000E5 System.Void SplashscreenPage::.ctor()
extern void SplashscreenPage__ctor_m29E8B801D62C146D44F25320EEB52F7CDEC4EFC2 (void);
// 0x000000E6 System.Void SwipeMenu::Start()
extern void SwipeMenu_Start_mF49B3B2AF9955E8429EFABBC7F8C7A4C9D73ABE5 (void);
// 0x000000E7 System.Void SwipeMenu::Update()
extern void SwipeMenu_Update_mAE33874564EBE84862BA97BE8AEBDCD1D5007A9A (void);
// 0x000000E8 System.Void SwipeMenu::.ctor()
extern void SwipeMenu__ctor_mF71FD4B4351FB13F62DE12BC7C9C7F164D7B8DD2 (void);
// 0x000000E9 System.Void Tema::.ctor()
extern void Tema__ctor_m5C1B87E5B589340531E9EFBE77403FAEF61EF468 (void);
// 0x000000EA System.Void ThemeCard::Start()
extern void ThemeCard_Start_mDAC5E62ABAF8C10B155F6FB87FE0B1C63480CA2E (void);
// 0x000000EB System.Void ThemeCard::ApplyTheme()
extern void ThemeCard_ApplyTheme_m43F90DA46A831FC3657B5B0E760775547AB4C0D7 (void);
// 0x000000EC System.Void ThemeCard::.ctor()
extern void ThemeCard__ctor_m88E49A14A83AD3A2C3B25C897F7B4D049D282D18 (void);
// 0x000000ED System.Void ThemeCard::<Start>b__3_0()
extern void ThemeCard_U3CStartU3Eb__3_0_mED45853553FDCCC392A48CB355BECEA12A7BF464 (void);
// 0x000000EE System.Void ThemeManager::Awake()
extern void ThemeManager_Awake_m2AEE036E797A06A326764D45E47A50CE9CB8195E (void);
// 0x000000EF Tema ThemeManager::GetTheme()
extern void ThemeManager_GetTheme_m53E959D7B2187A9BE4B3817F64FA208A8E3E7962 (void);
// 0x000000F0 System.Void ThemeManager::SetTheme(System.String)
extern void ThemeManager_SetTheme_mFE31D6B059A1FE23E41D613F5F656A22ED31BFD7 (void);
// 0x000000F1 System.Void ThemeManager::.ctor()
extern void ThemeManager__ctor_m1B15A16D3C500F39D01E5B302126CC3BFC819FF7 (void);
// 0x000000F2 System.Boolean ThemeManager::<SetTheme>b__6_0(Tema)
extern void ThemeManager_U3CSetThemeU3Eb__6_0_mE4758C847ADA64CFD0A1E26D7E914A6F386C6EEA (void);
// 0x000000F3 System.Void ThemeHolder::.ctor()
extern void ThemeHolder__ctor_mC9B7A9F932C2D6027861A4EFF4E3CB3063468EDF (void);
// 0x000000F4 System.Void WidgetHabisHint::Start()
extern void WidgetHabisHint_Start_m6FB19BFF38636A1398570FBD5177F98CC10A8EDB (void);
// 0x000000F5 System.Void WidgetHabisHint::.ctor()
extern void WidgetHabisHint__ctor_m5128894F2B8DCD274E18E8E07D331D3FC3330BF9 (void);
// 0x000000F6 System.Void WidgetHabisHint::<Start>b__1_0()
extern void WidgetHabisHint_U3CStartU3Eb__1_0_m3BD6D272FD3C175FAE628EB2879666A6C870A32C (void);
// 0x000000F7 System.Void WidgetKurangCoin::Start()
extern void WidgetKurangCoin_Start_m27DB080A4CE73E6A7D8530E6EE8BEC56D211DBE6 (void);
// 0x000000F8 System.Void WidgetKurangCoin::.ctor()
extern void WidgetKurangCoin__ctor_m7EF6B49FF8C5E748D2AEAC4B130A629D39A489D7 (void);
// 0x000000F9 System.Void WidgetKurangCoin::<Start>b__2_0()
extern void WidgetKurangCoin_U3CStartU3Eb__2_0_mCA79C96E3D33E5174B33205212BC2BA29DE5ED32 (void);
// 0x000000FA System.Void WidgetKurangCoin::<Start>b__2_1()
extern void WidgetKurangCoin_U3CStartU3Eb__2_1_m307CB24C7C0A576670AE5C8BBF41D10BADF6EA2A (void);
// 0x000000FB System.Void IronSourceDemoScript::Start()
extern void IronSourceDemoScript_Start_m77BBBF6AE3F0DF2428C73C5D2D5F45FDF4EFB8BC (void);
// 0x000000FC System.Void IronSourceDemoScript::OnEnable()
extern void IronSourceDemoScript_OnEnable_m8946831B21535385ACF7C8E15325DF8608E47D34 (void);
// 0x000000FD System.Void IronSourceDemoScript::OnApplicationPause(System.Boolean)
extern void IronSourceDemoScript_OnApplicationPause_m2FF515EB862B1AE0BD7F86418D31B9F970DF59C6 (void);
// 0x000000FE System.Void IronSourceDemoScript::OnGUI()
extern void IronSourceDemoScript_OnGUI_mE2E5B983419B49DB74ED30F283AA6D5202F5DB4C (void);
// 0x000000FF System.Void IronSourceDemoScript::SdkInitializationCompletedEvent()
extern void IronSourceDemoScript_SdkInitializationCompletedEvent_m849CAE6B3FC42F029D1126497086EB1A68D1D663 (void);
// 0x00000100 System.Void IronSourceDemoScript::ReardedVideoOnAdOpenedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdOpenedEvent_mAA90C5DB459623CB82CF4C1757BA9982A0020F0D (void);
// 0x00000101 System.Void IronSourceDemoScript::ReardedVideoOnAdClosedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdClosedEvent_m5097FECA2AB4269F5162340D618B456EC7802D1A (void);
// 0x00000102 System.Void IronSourceDemoScript::ReardedVideoOnAdAvailable(IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdAvailable_mB8298C68A77BFDBE4E7F2642EDB817FB234ACCB5 (void);
// 0x00000103 System.Void IronSourceDemoScript::ReardedVideoOnAdUnavailable()
extern void IronSourceDemoScript_ReardedVideoOnAdUnavailable_mFDA7DB7C9514E6A2A98FBE8A694AC271756ABC1C (void);
// 0x00000104 System.Void IronSourceDemoScript::ReardedVideoOnAdShowFailedEvent(IronSourceError,IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdShowFailedEvent_mD8A88D647F5096B156A8A8E4D323D5B8A168BBBF (void);
// 0x00000105 System.Void IronSourceDemoScript::ReardedVideoOnAdRewardedEvent(IronSourcePlacement,IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdRewardedEvent_mF8F69DE0CF85726566FFF69075337A856AEC3347 (void);
// 0x00000106 System.Void IronSourceDemoScript::ReardedVideoOnAdClickedEvent(IronSourcePlacement,IronSourceAdInfo)
extern void IronSourceDemoScript_ReardedVideoOnAdClickedEvent_m4F99DDF14AEC2020478A08F6A3A1B7E911AACCDD (void);
// 0x00000107 System.Void IronSourceDemoScript::RewardedVideoAvailabilityChangedEvent(System.Boolean)
extern void IronSourceDemoScript_RewardedVideoAvailabilityChangedEvent_m8D6D030DD7DFC0AD4E22B96DAE1107018642955D (void);
// 0x00000108 System.Void IronSourceDemoScript::RewardedVideoAdOpenedEvent()
extern void IronSourceDemoScript_RewardedVideoAdOpenedEvent_mCA1EBEC4D751D2E0E99766AA4C468E09958C04E7 (void);
// 0x00000109 System.Void IronSourceDemoScript::RewardedVideoAdRewardedEvent(IronSourcePlacement)
extern void IronSourceDemoScript_RewardedVideoAdRewardedEvent_m1A57ED0C0318246EF3F82D592D2FF71A9479D8E1 (void);
// 0x0000010A System.Void IronSourceDemoScript::RewardedVideoAdClosedEvent()
extern void IronSourceDemoScript_RewardedVideoAdClosedEvent_mBD15C7A41C09B50A91A836C3E21FF5237D77E448 (void);
// 0x0000010B System.Void IronSourceDemoScript::RewardedVideoAdStartedEvent()
extern void IronSourceDemoScript_RewardedVideoAdStartedEvent_m15ADE4BB5E28B6996F707DE3A45AFDDCD46D8F70 (void);
// 0x0000010C System.Void IronSourceDemoScript::RewardedVideoAdEndedEvent()
extern void IronSourceDemoScript_RewardedVideoAdEndedEvent_mBBBCFBCB0BF6E31D925E8C55FEF143B74944E6CC (void);
// 0x0000010D System.Void IronSourceDemoScript::RewardedVideoAdShowFailedEvent(IronSourceError)
extern void IronSourceDemoScript_RewardedVideoAdShowFailedEvent_m12220F16740A72B2362B6C6A70232B22E6C9976D (void);
// 0x0000010E System.Void IronSourceDemoScript::RewardedVideoAdClickedEvent(IronSourcePlacement)
extern void IronSourceDemoScript_RewardedVideoAdClickedEvent_mD609F9E25702627A6A3BB3813DA8C2F654F421D4 (void);
// 0x0000010F System.Void IronSourceDemoScript::RewardedVideoAdLoadedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_RewardedVideoAdLoadedDemandOnlyEvent_m516EDB8DFEA9D89459C638DE070E9E80DE6E635B (void);
// 0x00000110 System.Void IronSourceDemoScript::RewardedVideoAdLoadFailedDemandOnlyEvent(System.String,IronSourceError)
extern void IronSourceDemoScript_RewardedVideoAdLoadFailedDemandOnlyEvent_mB25510FCA9F14003FA47BDFCF3F022ED0CA16EA0 (void);
// 0x00000111 System.Void IronSourceDemoScript::RewardedVideoAdOpenedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_RewardedVideoAdOpenedDemandOnlyEvent_mDD764123153CC8133ECBC85E569B3B2BD4295C87 (void);
// 0x00000112 System.Void IronSourceDemoScript::RewardedVideoAdRewardedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_RewardedVideoAdRewardedDemandOnlyEvent_m7E67AE25E69510F382C4F0233FAB27525CF426D9 (void);
// 0x00000113 System.Void IronSourceDemoScript::RewardedVideoAdClosedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_RewardedVideoAdClosedDemandOnlyEvent_m077D2168926533A87F1DC33D4A8280D597D3CC32 (void);
// 0x00000114 System.Void IronSourceDemoScript::RewardedVideoAdShowFailedDemandOnlyEvent(System.String,IronSourceError)
extern void IronSourceDemoScript_RewardedVideoAdShowFailedDemandOnlyEvent_mB1D85D07EFD5C99B7D91215FF91828AC7C00E463 (void);
// 0x00000115 System.Void IronSourceDemoScript::RewardedVideoAdClickedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_RewardedVideoAdClickedDemandOnlyEvent_mF46D7047786F04D06E33BD702CAEC29BB49E7F4A (void);
// 0x00000116 System.Void IronSourceDemoScript::InterstitialOnAdReadyEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdReadyEvent_m2D145827D504F5B16F4E60C3C1A9738033B43053 (void);
// 0x00000117 System.Void IronSourceDemoScript::InterstitialOnAdLoadFailed(IronSourceError)
extern void IronSourceDemoScript_InterstitialOnAdLoadFailed_m5B05AFE22E3073F324C16E226AA25F18CA206D0B (void);
// 0x00000118 System.Void IronSourceDemoScript::InterstitialOnAdOpenedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdOpenedEvent_m5EF442105B865E25655FD85B2CE82CEA176BB3ED (void);
// 0x00000119 System.Void IronSourceDemoScript::InterstitialOnAdClickedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdClickedEvent_m4CBCF8854B78C2EFF3A8FA9F2DC1465742442E1B (void);
// 0x0000011A System.Void IronSourceDemoScript::InterstitialOnAdShowSucceededEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdShowSucceededEvent_mEE8AFD2DC75F164289653B67D9F8B2A4BA90B43F (void);
// 0x0000011B System.Void IronSourceDemoScript::InterstitialOnAdShowFailedEvent(IronSourceError,IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdShowFailedEvent_m25479BB7773BB4C872700856551E14035CBEB388 (void);
// 0x0000011C System.Void IronSourceDemoScript::InterstitialOnAdClosedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_InterstitialOnAdClosedEvent_mA32A648AA1FF5FD94E481B8C6F4FE03526996937 (void);
// 0x0000011D System.Void IronSourceDemoScript::InterstitialAdReadyEvent()
extern void IronSourceDemoScript_InterstitialAdReadyEvent_m7540A41C2D8BC4BCBAF2F6D1150D3893B7E65796 (void);
// 0x0000011E System.Void IronSourceDemoScript::InterstitialAdLoadFailedEvent(IronSourceError)
extern void IronSourceDemoScript_InterstitialAdLoadFailedEvent_m49260AD6F3985A12BE62243D54382FB6F12F71F8 (void);
// 0x0000011F System.Void IronSourceDemoScript::InterstitialAdShowSucceededEvent()
extern void IronSourceDemoScript_InterstitialAdShowSucceededEvent_m8DC6A0EF9FF7D3BC35602DE70FF07D1368550779 (void);
// 0x00000120 System.Void IronSourceDemoScript::InterstitialAdShowFailedEvent(IronSourceError)
extern void IronSourceDemoScript_InterstitialAdShowFailedEvent_m044C2F8F84E3920F764B5C3B1BC05643C1F43303 (void);
// 0x00000121 System.Void IronSourceDemoScript::InterstitialAdClickedEvent()
extern void IronSourceDemoScript_InterstitialAdClickedEvent_m53B12D24C9AE85ACA81294CE11630EF167896B62 (void);
// 0x00000122 System.Void IronSourceDemoScript::InterstitialAdOpenedEvent()
extern void IronSourceDemoScript_InterstitialAdOpenedEvent_m7A11AF2D87DE4EF38304AC7AA59CC98D7AE1D05D (void);
// 0x00000123 System.Void IronSourceDemoScript::InterstitialAdClosedEvent()
extern void IronSourceDemoScript_InterstitialAdClosedEvent_m9F67DAB55D1CBB561095B97FACAE2D7426F9CCFF (void);
// 0x00000124 System.Void IronSourceDemoScript::InterstitialAdReadyDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_InterstitialAdReadyDemandOnlyEvent_m8D6E6D0B5A6A7BCDD87661C33EE0A0111402AB8E (void);
// 0x00000125 System.Void IronSourceDemoScript::InterstitialAdLoadFailedDemandOnlyEvent(System.String,IronSourceError)
extern void IronSourceDemoScript_InterstitialAdLoadFailedDemandOnlyEvent_m98CA7BF6A5D70BD8C6A85FEBB7B869F6F975AFFF (void);
// 0x00000126 System.Void IronSourceDemoScript::InterstitialAdShowFailedDemandOnlyEvent(System.String,IronSourceError)
extern void IronSourceDemoScript_InterstitialAdShowFailedDemandOnlyEvent_mC7EF101695BEC4C1215C4DB46CB9E8F818071B13 (void);
// 0x00000127 System.Void IronSourceDemoScript::InterstitialAdClickedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_InterstitialAdClickedDemandOnlyEvent_m5D260FF2ABA91E3F6550F334A3D02E7CE740A96E (void);
// 0x00000128 System.Void IronSourceDemoScript::InterstitialAdOpenedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_InterstitialAdOpenedDemandOnlyEvent_m56D5E479F14569874DF9EA09CA785E809C8D2681 (void);
// 0x00000129 System.Void IronSourceDemoScript::InterstitialAdClosedDemandOnlyEvent(System.String)
extern void IronSourceDemoScript_InterstitialAdClosedDemandOnlyEvent_mE3AB61CBEE02C43F9311D4E4E185A0329850D2AE (void);
// 0x0000012A System.Void IronSourceDemoScript::BannerOnAdLoadedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_BannerOnAdLoadedEvent_m80D002600F688EDFD2C98F404016ECE8C245D926 (void);
// 0x0000012B System.Void IronSourceDemoScript::BannerOnAdLoadFailedEvent(IronSourceError)
extern void IronSourceDemoScript_BannerOnAdLoadFailedEvent_mB1406D5A0CC8FCA60BDE7FCD56FE9206DD6F0240 (void);
// 0x0000012C System.Void IronSourceDemoScript::BannerOnAdClickedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_BannerOnAdClickedEvent_mC9F5264E62FC4748A37022A8EA81498A5E729EAE (void);
// 0x0000012D System.Void IronSourceDemoScript::BannerOnAdScreenPresentedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_BannerOnAdScreenPresentedEvent_mBF9C0A5EFE1BBD2C7717CFCFA69B4EF9B07BB5AB (void);
// 0x0000012E System.Void IronSourceDemoScript::BannerOnAdScreenDismissedEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_BannerOnAdScreenDismissedEvent_m8B60FE3ABF24B4CA33E7B4871F22813BADEEC437 (void);
// 0x0000012F System.Void IronSourceDemoScript::BannerOnAdLeftApplicationEvent(IronSourceAdInfo)
extern void IronSourceDemoScript_BannerOnAdLeftApplicationEvent_m4871D7055E2C609856B5B651DC8B9CFA6EC0F171 (void);
// 0x00000130 System.Void IronSourceDemoScript::BannerAdLoadedEvent()
extern void IronSourceDemoScript_BannerAdLoadedEvent_m72DF5D965B6B049B99738E946EC79F6884D20A31 (void);
// 0x00000131 System.Void IronSourceDemoScript::BannerAdLoadFailedEvent(IronSourceError)
extern void IronSourceDemoScript_BannerAdLoadFailedEvent_m583267AF7FF9B15DB1ED470688C92418E9651E3D (void);
// 0x00000132 System.Void IronSourceDemoScript::BannerAdClickedEvent()
extern void IronSourceDemoScript_BannerAdClickedEvent_mA16CEF59D8B2727C8F624427599FAE37F3ABAA7C (void);
// 0x00000133 System.Void IronSourceDemoScript::BannerAdScreenPresentedEvent()
extern void IronSourceDemoScript_BannerAdScreenPresentedEvent_mB196ADD5DBC96B406CE2B692B6A7AC337AEED1EA (void);
// 0x00000134 System.Void IronSourceDemoScript::BannerAdScreenDismissedEvent()
extern void IronSourceDemoScript_BannerAdScreenDismissedEvent_m0702C693A37BC01E7F308B1DD4F9E1D24E6C4D38 (void);
// 0x00000135 System.Void IronSourceDemoScript::BannerAdLeftApplicationEvent()
extern void IronSourceDemoScript_BannerAdLeftApplicationEvent_mCBE6E143B9DB8747A75FEC6CBD672CE5A117ACE2 (void);
// 0x00000136 System.Void IronSourceDemoScript::OfferwallOpenedEvent()
extern void IronSourceDemoScript_OfferwallOpenedEvent_m5F3F46BF3CA43F00EAD3F7BA321B668B239B837C (void);
// 0x00000137 System.Void IronSourceDemoScript::OfferwallClosedEvent()
extern void IronSourceDemoScript_OfferwallClosedEvent_m4346BD782B909613779E96541C9D7D14FA8D17A7 (void);
// 0x00000138 System.Void IronSourceDemoScript::OfferwallShowFailedEvent(IronSourceError)
extern void IronSourceDemoScript_OfferwallShowFailedEvent_m5B8BE354AF2EF7BB715110A748AB2F6D45560D66 (void);
// 0x00000139 System.Void IronSourceDemoScript::OfferwallAdCreditedEvent(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IronSourceDemoScript_OfferwallAdCreditedEvent_mE706AA91C365D2496B44BFA7A02663BEE4A39760 (void);
// 0x0000013A System.Void IronSourceDemoScript::GetOfferwallCreditsFailedEvent(IronSourceError)
extern void IronSourceDemoScript_GetOfferwallCreditsFailedEvent_mA4C78B923B8179668F1FB2DE7CB05C6AE0B478E6 (void);
// 0x0000013B System.Void IronSourceDemoScript::OfferwallAvailableEvent(System.Boolean)
extern void IronSourceDemoScript_OfferwallAvailableEvent_m5912223F9C4E4C9A6F07F2A82150E7C886286064 (void);
// 0x0000013C System.Void IronSourceDemoScript::ImpressionSuccessEvent(IronSourceImpressionData)
extern void IronSourceDemoScript_ImpressionSuccessEvent_m1A555AF2B938562B3D493A2CF13B3BAC76324811 (void);
// 0x0000013D System.Void IronSourceDemoScript::ImpressionDataReadyEvent(IronSourceImpressionData)
extern void IronSourceDemoScript_ImpressionDataReadyEvent_mF582B7C042903AE0932DB4296587E9487F108E07 (void);
// 0x0000013E System.Void IronSourceDemoScript::.ctor()
extern void IronSourceDemoScript__ctor_mDD3BB0AECC3B37BFFBA8DDB8EFD3D381893E20CB (void);
// 0x0000013F System.Void AndroidAgent::.ctor()
extern void AndroidAgent__ctor_m2F1A50D8D921A298DB2D6BA6CE5806F8603EFD4F (void);
// 0x00000140 UnityEngine.AndroidJavaObject AndroidAgent::getBridge()
extern void AndroidAgent_getBridge_m0A292F3DDA49DBF47DAE3F002B77003A3E1D7BEE (void);
// 0x00000141 System.Void AndroidAgent::initEventsDispatcher()
extern void AndroidAgent_initEventsDispatcher_mFF2C86FB1A82FF9F04D609820CDFA9CA59FFBC7A (void);
// 0x00000142 System.Void AndroidAgent::onApplicationPause(System.Boolean)
extern void AndroidAgent_onApplicationPause_m6EB98993705760C1F6999C692F25DB5D6B4A5CDB (void);
// 0x00000143 System.Void AndroidAgent::setMediationSegment(System.String)
extern void AndroidAgent_setMediationSegment_m3A0480804838E5E8ECC2BC5B8B5680A6105B9C70 (void);
// 0x00000144 System.String AndroidAgent::getAdvertiserId()
extern void AndroidAgent_getAdvertiserId_m3710C4C861EF0FF0F39063DA54BEFFEAF3CE030A (void);
// 0x00000145 System.Void AndroidAgent::validateIntegration()
extern void AndroidAgent_validateIntegration_mE08D2E6349AE514ABB189490FAE6DD951A8C327E (void);
// 0x00000146 System.Void AndroidAgent::shouldTrackNetworkState(System.Boolean)
extern void AndroidAgent_shouldTrackNetworkState_m8797D28C0404D921C99163A3343D89490FC9DE94 (void);
// 0x00000147 System.Boolean AndroidAgent::setDynamicUserId(System.String)
extern void AndroidAgent_setDynamicUserId_m2ECDDD36B92F7747B6528E51C37BB9E4D42D4EBC (void);
// 0x00000148 System.Void AndroidAgent::setAdaptersDebug(System.Boolean)
extern void AndroidAgent_setAdaptersDebug_mE7FD44DDEF492001F0F52576F1589F28F4B9DEF5 (void);
// 0x00000149 System.Void AndroidAgent::setMetaData(System.String,System.String)
extern void AndroidAgent_setMetaData_m065F83A2898C3CA9E804D074316ABACC362482E8 (void);
// 0x0000014A System.Void AndroidAgent::setMetaData(System.String,System.String[])
extern void AndroidAgent_setMetaData_m72A54E1A81C43E256EB7BDC40E15E4EFBDC218D1 (void);
// 0x0000014B System.Nullable`1<System.Int32> AndroidAgent::getConversionValue()
extern void AndroidAgent_getConversionValue_mEDF33AC51809A034B51713043D5572B4B34D9240 (void);
// 0x0000014C System.Void AndroidAgent::setManualLoadRewardedVideo(System.Boolean)
extern void AndroidAgent_setManualLoadRewardedVideo_m80960FB9EA98BF1DE19AC8F0FB3A68CBAD9895E5 (void);
// 0x0000014D System.Void AndroidAgent::setNetworkData(System.String,System.String)
extern void AndroidAgent_setNetworkData_m384413180ABF267A646072E19A80F46A2C92072E (void);
// 0x0000014E System.Void AndroidAgent::SetPauseGame(System.Boolean)
extern void AndroidAgent_SetPauseGame_m5C5B7470DCFBB7E55799773388D303B2F71D532C (void);
// 0x0000014F System.Void AndroidAgent::setUserId(System.String)
extern void AndroidAgent_setUserId_mEDEFB903A8927A6D7CD852EDBC69F8E975DF4DC0 (void);
// 0x00000150 System.Void AndroidAgent::init(System.String)
extern void AndroidAgent_init_m5518C696AD17BD78BFBE888EB2D6A753EEF32BA8 (void);
// 0x00000151 System.Void AndroidAgent::init(System.String,System.String[])
extern void AndroidAgent_init_m68BB3FA729DDAC86BC809D42D4B97F66C5A5B4A2 (void);
// 0x00000152 System.Void AndroidAgent::initISDemandOnly(System.String,System.String[])
extern void AndroidAgent_initISDemandOnly_m4741E6CAE778A2B01D5DD467AE8D9CF714B27BF2 (void);
// 0x00000153 System.Void AndroidAgent::loadRewardedVideo()
extern void AndroidAgent_loadRewardedVideo_m2AECD4A36B197F5725E25E2A1D01D43A2886D149 (void);
// 0x00000154 System.Void AndroidAgent::showRewardedVideo()
extern void AndroidAgent_showRewardedVideo_m6F0B3BC254FA12DA162E77145A15D660384BF631 (void);
// 0x00000155 System.Void AndroidAgent::showRewardedVideo(System.String)
extern void AndroidAgent_showRewardedVideo_mE300B80BA2F3EDA6652B3B24B86D4861508A1AA4 (void);
// 0x00000156 System.Boolean AndroidAgent::isRewardedVideoAvailable()
extern void AndroidAgent_isRewardedVideoAvailable_mFBD236A6085B36A46E3566F7B7BC0580DEEAB0BD (void);
// 0x00000157 System.Boolean AndroidAgent::isRewardedVideoPlacementCapped(System.String)
extern void AndroidAgent_isRewardedVideoPlacementCapped_m7A9480436E644353B9410CC6B9FC2F84EF426387 (void);
// 0x00000158 IronSourcePlacement AndroidAgent::getPlacementInfo(System.String)
extern void AndroidAgent_getPlacementInfo_m8F93E027CA731571B7D30EDC6E067281F6EF3065 (void);
// 0x00000159 System.Void AndroidAgent::setRewardedVideoServerParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AndroidAgent_setRewardedVideoServerParams_m7235399F36B0D265ADFA44D96038B48BFD9ED55C (void);
// 0x0000015A System.Void AndroidAgent::clearRewardedVideoServerParams()
extern void AndroidAgent_clearRewardedVideoServerParams_mEB34661A68B6BAAA955D1292E7B8DBF33127689E (void);
// 0x0000015B System.Void AndroidAgent::showISDemandOnlyRewardedVideo(System.String)
extern void AndroidAgent_showISDemandOnlyRewardedVideo_m8877C5FCCB0FE1A314605BAF49C8F462EA8C53DB (void);
// 0x0000015C System.Void AndroidAgent::loadISDemandOnlyRewardedVideo(System.String)
extern void AndroidAgent_loadISDemandOnlyRewardedVideo_mCBC4DB9E07A597F4E2889FCE1CF6C161EDE4B51F (void);
// 0x0000015D System.Boolean AndroidAgent::isISDemandOnlyRewardedVideoAvailable(System.String)
extern void AndroidAgent_isISDemandOnlyRewardedVideoAvailable_m9C8F0CFB56E5C155C6DFE4ED161C79E6472A9153 (void);
// 0x0000015E System.Void AndroidAgent::loadInterstitial()
extern void AndroidAgent_loadInterstitial_m21905E46FD67D875EFFE454A67D29E956441FD8B (void);
// 0x0000015F System.Void AndroidAgent::showInterstitial()
extern void AndroidAgent_showInterstitial_m4A7D3D84077A51BE807150D94F86CC4FE104B092 (void);
// 0x00000160 System.Void AndroidAgent::showInterstitial(System.String)
extern void AndroidAgent_showInterstitial_m6FB72A001E84E21A0E215AC2B757CC88D85E4B83 (void);
// 0x00000161 System.Boolean AndroidAgent::isInterstitialReady()
extern void AndroidAgent_isInterstitialReady_m51C44AB312CF8632C45A69C605A8BB79E56B2EE5 (void);
// 0x00000162 System.Boolean AndroidAgent::isInterstitialPlacementCapped(System.String)
extern void AndroidAgent_isInterstitialPlacementCapped_m12972293AB0894E88ADC862DD212617879EF11B1 (void);
// 0x00000163 System.Void AndroidAgent::loadISDemandOnlyInterstitial(System.String)
extern void AndroidAgent_loadISDemandOnlyInterstitial_m01237D1EDFB1FF359820AAB67936F83514C99EAD (void);
// 0x00000164 System.Void AndroidAgent::showISDemandOnlyInterstitial(System.String)
extern void AndroidAgent_showISDemandOnlyInterstitial_m22FDF895CEE08619BE6CD4F08B0B01677B698EC6 (void);
// 0x00000165 System.Boolean AndroidAgent::isISDemandOnlyInterstitialReady(System.String)
extern void AndroidAgent_isISDemandOnlyInterstitialReady_mAE5E73422969E3165B9744D1183A8891BB28464C (void);
// 0x00000166 System.Void AndroidAgent::showOfferwall()
extern void AndroidAgent_showOfferwall_mD5A0802243C5E038945E3E9B146E2B8B9DE077E8 (void);
// 0x00000167 System.Void AndroidAgent::showOfferwall(System.String)
extern void AndroidAgent_showOfferwall_m43C6328874CE364DA3EEDBAC887CBE285C9DA355 (void);
// 0x00000168 System.Void AndroidAgent::getOfferwallCredits()
extern void AndroidAgent_getOfferwallCredits_m4733426D27DA45621DE45CC0B55793CBA88F4A8F (void);
// 0x00000169 System.Boolean AndroidAgent::isOfferwallAvailable()
extern void AndroidAgent_isOfferwallAvailable_m53AE0C27A803FC14EE460DCD77C660F052A045B1 (void);
// 0x0000016A System.Void AndroidAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition)
extern void AndroidAgent_loadBanner_mF2B2F9AA79ED6B1655B15FB856ABC74528594E7F (void);
// 0x0000016B System.Void AndroidAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition,System.String)
extern void AndroidAgent_loadBanner_mC3E441ACADA0DCA0E5736F0D16D5B3C0F66A3818 (void);
// 0x0000016C System.Void AndroidAgent::destroyBanner()
extern void AndroidAgent_destroyBanner_mCCBFE09EBC2124EE181F809186BD428D04118CBD (void);
// 0x0000016D System.Void AndroidAgent::displayBanner()
extern void AndroidAgent_displayBanner_m7B7FB2A3D813AE3B0665C25DD833A806136FF79B (void);
// 0x0000016E System.Void AndroidAgent::hideBanner()
extern void AndroidAgent_hideBanner_m39208475B2396C5BD2B49E0AF73B8BC5A1FA3CA5 (void);
// 0x0000016F System.Boolean AndroidAgent::isBannerPlacementCapped(System.String)
extern void AndroidAgent_isBannerPlacementCapped_mEAD2A063BF068156B01DA9E5663F1E7BD79D56F2 (void);
// 0x00000170 System.Void AndroidAgent::setSegment(IronSourceSegment)
extern void AndroidAgent_setSegment_m3776055F5BC0F962D828BEB75534EDCF0BBE67AC (void);
// 0x00000171 System.Void AndroidAgent::setConsent(System.Boolean)
extern void AndroidAgent_setConsent_mD5D3EEE4AAA4CC6251760222C84CCB5CEDBC5D63 (void);
// 0x00000172 System.Void AndroidAgent::loadConsentViewWithType(System.String)
extern void AndroidAgent_loadConsentViewWithType_m226A2F1C1FA3F8B23AA13E03205D840E351285F8 (void);
// 0x00000173 System.Void AndroidAgent::showConsentViewWithType(System.String)
extern void AndroidAgent_showConsentViewWithType_mD106E1957A62FF6A2744D877E7E8F8C31F3ABC0F (void);
// 0x00000174 System.Void AndroidAgent::setAdRevenueData(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AndroidAgent_setAdRevenueData_m879D0D032F29178EE457A000BD506FD4C0A2B98E (void);
// 0x00000175 System.Void AndroidAgent::.cctor()
extern void AndroidAgent__cctor_m7177F073688A780EFA2F13786FF39B5CBA697DE6 (void);
// 0x00000176 System.Void IronSource::.ctor()
extern void IronSource__ctor_mCD8F82382069D7C032538FE2FE4291227D7C32B3 (void);
// 0x00000177 IronSource IronSource::get_Agent()
extern void IronSource_get_Agent_m263B42666F99FAD3CCC93BBCF79EE3E85D485566 (void);
// 0x00000178 System.String IronSource::pluginVersion()
extern void IronSource_pluginVersion_mE6F3D9B41DECA6F53E668929809A46FA096CF732 (void);
// 0x00000179 System.String IronSource::unityVersion()
extern void IronSource_unityVersion_m5312B4562DD63B0B17536089D1F92D362FD82AD8 (void);
// 0x0000017A System.Void IronSource::setUnsupportedPlatform()
extern void IronSource_setUnsupportedPlatform_m3C90507AB0F3118C57D1DF37C893598D339C4F92 (void);
// 0x0000017B System.Void IronSource::onApplicationPause(System.Boolean)
extern void IronSource_onApplicationPause_m7F0FED1D5D5C76B1446294878864374AF4AC7315 (void);
// 0x0000017C System.Void IronSource::setMediationSegment(System.String)
extern void IronSource_setMediationSegment_m511F2EF8432A9F65BF274821099D452152A72847 (void);
// 0x0000017D System.String IronSource::getAdvertiserId()
extern void IronSource_getAdvertiserId_mC04AD8567F617BF37858BC884BDFD3BD2E93F44F (void);
// 0x0000017E System.Void IronSource::validateIntegration()
extern void IronSource_validateIntegration_mAA95A52D8F37C0D5A2DC530F73865E40FF07DB67 (void);
// 0x0000017F System.Void IronSource::shouldTrackNetworkState(System.Boolean)
extern void IronSource_shouldTrackNetworkState_mB5FF34F60168FF821571775E654BC5FDD34DBF13 (void);
// 0x00000180 System.Boolean IronSource::setDynamicUserId(System.String)
extern void IronSource_setDynamicUserId_mEDDDA71582DB31D68C30D420589A549E6E5942D2 (void);
// 0x00000181 System.Void IronSource::setAdaptersDebug(System.Boolean)
extern void IronSource_setAdaptersDebug_mCAEACE5C50C8D61CF867CDD2783E9F94BB881D0D (void);
// 0x00000182 System.Void IronSource::setMetaData(System.String,System.String)
extern void IronSource_setMetaData_mE670A504A91308D04F9EEA81EAE8473CD99DB804 (void);
// 0x00000183 System.Void IronSource::setMetaData(System.String,System.String[])
extern void IronSource_setMetaData_mFD139A925E30253CAB1CC237CD057BFE6E275DEE (void);
// 0x00000184 System.Nullable`1<System.Int32> IronSource::getConversionValue()
extern void IronSource_getConversionValue_m35E84D2BCCE4F62DA953898B53655588D6A1B986 (void);
// 0x00000185 System.Void IronSource::setManualLoadRewardedVideo(System.Boolean)
extern void IronSource_setManualLoadRewardedVideo_m5DED3093B53CBBBDF92FEF516BEE793C8DC3BE87 (void);
// 0x00000186 System.Void IronSource::setNetworkData(System.String,System.String)
extern void IronSource_setNetworkData_mB3C6BEFDB0F29CEF39064FCB79797C3B0BAD66A3 (void);
// 0x00000187 System.Void IronSource::SetPauseGame(System.Boolean)
extern void IronSource_SetPauseGame_m3F590CC02DB95FB5E5D17C82AF2EBDE7C27CCCC1 (void);
// 0x00000188 System.Void IronSource::setUserId(System.String)
extern void IronSource_setUserId_m4475193FBF49B08AD0D5618D8005BB04E931DE5C (void);
// 0x00000189 System.Void IronSource::init(System.String)
extern void IronSource_init_mC35175772BE07A91B6CD7FD09E744FC96BA8520E (void);
// 0x0000018A System.Void IronSource::init(System.String,System.String[])
extern void IronSource_init_mC764BC165DE9127E939FF7DA044367690B4DD3DA (void);
// 0x0000018B System.Void IronSource::initISDemandOnly(System.String,System.String[])
extern void IronSource_initISDemandOnly_mD621820D8DE879B90CDEA47D6086E09C4BB3F907 (void);
// 0x0000018C System.Void IronSource::loadManualRewardedVideo()
extern void IronSource_loadManualRewardedVideo_m30443CD7B65D40B1D506C615EF1A724B010CED3F (void);
// 0x0000018D System.Void IronSource::loadRewardedVideo()
extern void IronSource_loadRewardedVideo_m6420CED59C78ECF65D94743DDA66766DC414B798 (void);
// 0x0000018E System.Void IronSource::showRewardedVideo()
extern void IronSource_showRewardedVideo_m5BF8747147A5AB0C167BBBCED99B300DB0F257AD (void);
// 0x0000018F System.Void IronSource::showRewardedVideo(System.String)
extern void IronSource_showRewardedVideo_m9213D17AB1D6653ECB61DD7418030C7776896302 (void);
// 0x00000190 IronSourcePlacement IronSource::getPlacementInfo(System.String)
extern void IronSource_getPlacementInfo_mCF44F07E63A48BF07FE8CB4F3861AB726773E840 (void);
// 0x00000191 System.Boolean IronSource::isRewardedVideoAvailable()
extern void IronSource_isRewardedVideoAvailable_m3E4AF1257B0B1F70940E99DB8A8FBC8E0B5A41BB (void);
// 0x00000192 System.Boolean IronSource::isRewardedVideoPlacementCapped(System.String)
extern void IronSource_isRewardedVideoPlacementCapped_mE0B04B25FCAB7B1729128AD4C986D2F770D1AF7E (void);
// 0x00000193 System.Void IronSource::setRewardedVideoServerParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void IronSource_setRewardedVideoServerParams_mA9C2AE88E92ABE4E8062AE06491A9204D1BE7688 (void);
// 0x00000194 System.Void IronSource::clearRewardedVideoServerParams()
extern void IronSource_clearRewardedVideoServerParams_m2C815B918801933E3191564227B81B008711D8CC (void);
// 0x00000195 System.Void IronSource::showISDemandOnlyRewardedVideo(System.String)
extern void IronSource_showISDemandOnlyRewardedVideo_mC5A0E1909AD1E45E4754EBE2AF7B34BB95039F2D (void);
// 0x00000196 System.Void IronSource::loadISDemandOnlyRewardedVideo(System.String)
extern void IronSource_loadISDemandOnlyRewardedVideo_m522FF1BB75B2F1C64DED1F27FA9C0DF2875D0427 (void);
// 0x00000197 System.Boolean IronSource::isISDemandOnlyRewardedVideoAvailable(System.String)
extern void IronSource_isISDemandOnlyRewardedVideoAvailable_m71030208B1C3E6BA2CA95688F612EC1C61A5616E (void);
// 0x00000198 System.Void IronSource::loadInterstitial()
extern void IronSource_loadInterstitial_m39088A580601883B6379FF62DA10597DC2C28694 (void);
// 0x00000199 System.Void IronSource::showInterstitial()
extern void IronSource_showInterstitial_m3ADEF617BDE29F84C81F21FF802B040AD6C94C4C (void);
// 0x0000019A System.Void IronSource::showInterstitial(System.String)
extern void IronSource_showInterstitial_mA4E2434AE0A6D8A203E918A28E74ED12EFF13EE0 (void);
// 0x0000019B System.Boolean IronSource::isInterstitialReady()
extern void IronSource_isInterstitialReady_m58079B2E013BC54CE46B257F59E7073722DC5765 (void);
// 0x0000019C System.Boolean IronSource::isInterstitialPlacementCapped(System.String)
extern void IronSource_isInterstitialPlacementCapped_m2D45FBD7ED41EC08B9F447937144F919DF1A2D2D (void);
// 0x0000019D System.Void IronSource::loadISDemandOnlyInterstitial(System.String)
extern void IronSource_loadISDemandOnlyInterstitial_m77F67456C869D60DF73DD3E67D6D22572E6C81F8 (void);
// 0x0000019E System.Void IronSource::showISDemandOnlyInterstitial(System.String)
extern void IronSource_showISDemandOnlyInterstitial_mAF78884276FD07D7C889983A7D8CC9106EC2DCB3 (void);
// 0x0000019F System.Boolean IronSource::isISDemandOnlyInterstitialReady(System.String)
extern void IronSource_isISDemandOnlyInterstitialReady_mE2AC65C5A0AC2F4E84B0E533C0EE0514ECF0B369 (void);
// 0x000001A0 System.Void IronSource::showOfferwall()
extern void IronSource_showOfferwall_mCC6506DAC941DDC6BDB129DE8CDDC911A86FDA4C (void);
// 0x000001A1 System.Void IronSource::showOfferwall(System.String)
extern void IronSource_showOfferwall_mECEA5E201AFB0ABF2421C762328BBBB51D5B5A5E (void);
// 0x000001A2 System.Void IronSource::getOfferwallCredits()
extern void IronSource_getOfferwallCredits_m8E03240FF37A04A5F656011201C1119CD82CDE75 (void);
// 0x000001A3 System.Boolean IronSource::isOfferwallAvailable()
extern void IronSource_isOfferwallAvailable_mD2CE4B92C8E643570FB0B2856F61437BBE9D3A61 (void);
// 0x000001A4 System.Void IronSource::loadBanner(IronSourceBannerSize,IronSourceBannerPosition)
extern void IronSource_loadBanner_m03C65D66F8966F461CA14957430B10C74DCB3DA6 (void);
// 0x000001A5 System.Void IronSource::loadBanner(IronSourceBannerSize,IronSourceBannerPosition,System.String)
extern void IronSource_loadBanner_m89535BCB06C68314DCB235D711D83B5E31E204D5 (void);
// 0x000001A6 System.Void IronSource::destroyBanner()
extern void IronSource_destroyBanner_mB66FBD0FD978EBF8FF29CB9436152BB6AB710C05 (void);
// 0x000001A7 System.Void IronSource::displayBanner()
extern void IronSource_displayBanner_mA7F41E7675B6D3723B8E7952027DF1D83F78DFDC (void);
// 0x000001A8 System.Void IronSource::hideBanner()
extern void IronSource_hideBanner_m1E057C27EECC6DA5C438A95EAA251D86F1851163 (void);
// 0x000001A9 System.Boolean IronSource::isBannerPlacementCapped(System.String)
extern void IronSource_isBannerPlacementCapped_mA556F2F7474259098D57D3567C6752F84DA16D23 (void);
// 0x000001AA System.Void IronSource::setSegment(IronSourceSegment)
extern void IronSource_setSegment_m42254418A0802B073A4E46AA6572E8FBD653D30E (void);
// 0x000001AB System.Void IronSource::setConsent(System.Boolean)
extern void IronSource_setConsent_m7BF5F76ACAC26533651B45DF82D9EB1A70EA38F6 (void);
// 0x000001AC System.Void IronSource::loadConsentViewWithType(System.String)
extern void IronSource_loadConsentViewWithType_m4576A4E53A31AFCA9542A3BF03416497F81197E4 (void);
// 0x000001AD System.Void IronSource::showConsentViewWithType(System.String)
extern void IronSource_showConsentViewWithType_mB18B211CAA1D43315E1DE9DA1D06F44202295538 (void);
// 0x000001AE System.Void IronSource::setAdRevenueData(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void IronSource_setAdRevenueData_mD94656C154410DB9040D4BDEA1C0BB9EF7513AC0 (void);
// 0x000001AF System.Void IronSource::.cctor()
extern void IronSource__cctor_m3E55470F854938EE38046A445A225A3E17280C10 (void);
// 0x000001B0 System.Void IronSourceAdInfo::.ctor(System.String)
extern void IronSourceAdInfo__ctor_mF7D8AF83461D224E8C08A74C66E71FB07648BC95 (void);
// 0x000001B1 System.String IronSourceAdInfo::ToString()
extern void IronSourceAdInfo_ToString_m623348DE306F85979183DCFAE50A14EF0309D592 (void);
// 0x000001B2 System.Void IronSourceBannerAndroid::add_OnBannerAdLoaded(System.Action)
extern void IronSourceBannerAndroid_add_OnBannerAdLoaded_mDF5C33697B5715B87F6A3464B1D986B116C3B257 (void);
// 0x000001B3 System.Void IronSourceBannerAndroid::remove_OnBannerAdLoaded(System.Action)
extern void IronSourceBannerAndroid_remove_OnBannerAdLoaded_m2BF2743E161EC5E4A8836DDD64EB12534CA5306C (void);
// 0x000001B4 System.Void IronSourceBannerAndroid::add_OnBannerAdLeftApplication(System.Action)
extern void IronSourceBannerAndroid_add_OnBannerAdLeftApplication_mF02C0A9E80ECE42311720E76211CFC5B1B2DBDE9 (void);
// 0x000001B5 System.Void IronSourceBannerAndroid::remove_OnBannerAdLeftApplication(System.Action)
extern void IronSourceBannerAndroid_remove_OnBannerAdLeftApplication_m0C86C8F7C04ACD8860B861CF9031DEE8034C1D05 (void);
// 0x000001B6 System.Void IronSourceBannerAndroid::add_OnBannerAdScreenDismissed(System.Action)
extern void IronSourceBannerAndroid_add_OnBannerAdScreenDismissed_mF894FA3B2EB7A76878B135CAE7E09E5516E5B5C1 (void);
// 0x000001B7 System.Void IronSourceBannerAndroid::remove_OnBannerAdScreenDismissed(System.Action)
extern void IronSourceBannerAndroid_remove_OnBannerAdScreenDismissed_mFADB9036AFEEB3A67B08CA52206032E2E5AC5419 (void);
// 0x000001B8 System.Void IronSourceBannerAndroid::add_OnBannerAdScreenPresented(System.Action)
extern void IronSourceBannerAndroid_add_OnBannerAdScreenPresented_m20849CEF8A9A61E3C79D003E1728CF97072DF7BA (void);
// 0x000001B9 System.Void IronSourceBannerAndroid::remove_OnBannerAdScreenPresented(System.Action)
extern void IronSourceBannerAndroid_remove_OnBannerAdScreenPresented_mAA130FB7E0C5CE53A74617305CD1EC090C5015FF (void);
// 0x000001BA System.Void IronSourceBannerAndroid::add_OnBannerAdClicked(System.Action)
extern void IronSourceBannerAndroid_add_OnBannerAdClicked_mC32FE194C4AE994DEBC4CEA34960C4D550F85BDF (void);
// 0x000001BB System.Void IronSourceBannerAndroid::remove_OnBannerAdClicked(System.Action)
extern void IronSourceBannerAndroid_remove_OnBannerAdClicked_m65000F5AD6D4A4B4C0FDEF8635BF1164D51F8A63 (void);
// 0x000001BC System.Void IronSourceBannerAndroid::add_OnBannerAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceBannerAndroid_add_OnBannerAdLoadFailed_m0FBEC248687E62048447C1AABEB96112A1B6261F (void);
// 0x000001BD System.Void IronSourceBannerAndroid::remove_OnBannerAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceBannerAndroid_remove_OnBannerAdLoadFailed_m03721B3E054E9366CDA223D1140C137C0DAEBEC9 (void);
// 0x000001BE System.Void IronSourceBannerAndroid::.ctor()
extern void IronSourceBannerAndroid__ctor_m046B28BC9EC7E766448B0E2ED7B527903A17F060 (void);
// 0x000001BF System.Void IronSourceBannerAndroid::onBannerAdLoaded()
extern void IronSourceBannerAndroid_onBannerAdLoaded_m584C812249D7D01EA7AC159D93B4540313890823 (void);
// 0x000001C0 System.Void IronSourceBannerAndroid::onBannerAdLoadFailed(System.String)
extern void IronSourceBannerAndroid_onBannerAdLoadFailed_m86E49994A320BC2BBBC63DDA296C8CF1F4150819 (void);
// 0x000001C1 System.Void IronSourceBannerAndroid::onBannerAdClicked()
extern void IronSourceBannerAndroid_onBannerAdClicked_m98C279D56D3641DE033911E905723355C09E9756 (void);
// 0x000001C2 System.Void IronSourceBannerAndroid::onBannerAdScreenPresented()
extern void IronSourceBannerAndroid_onBannerAdScreenPresented_mF5C8622256201B97258A91B2D01531471A9F7A86 (void);
// 0x000001C3 System.Void IronSourceBannerAndroid::onBannerAdScreenDismissed()
extern void IronSourceBannerAndroid_onBannerAdScreenDismissed_m450C5AA743D1F1D6B8F7B2EC10C0D1A42E275CF1 (void);
// 0x000001C4 System.Void IronSourceBannerAndroid::onBannerAdLeftApplication()
extern void IronSourceBannerAndroid_onBannerAdLeftApplication_mD700DF6065DE2E2DE5193FBDC9750FA3CC4BECA7 (void);
// 0x000001C5 System.Void IronSourceBannerAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m5211CDE90E7FB460727CD8060454F38E1C99A0EF (void);
// 0x000001C6 System.Void IronSourceBannerAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mBFD7A70F3704CB9D33A24218E35B3ED9AA5DA7E4 (void);
// 0x000001C7 System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_0()
extern void U3CU3Ec_U3C_ctorU3Eb__18_0_mA9D1628CE95A035959D388DC09CF4D4C9D2BAAA1 (void);
// 0x000001C8 System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_1()
extern void U3CU3Ec_U3C_ctorU3Eb__18_1_mEDAD022E38444B18A1F8A31C5750A5587FCE7824 (void);
// 0x000001C9 System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_2()
extern void U3CU3Ec_U3C_ctorU3Eb__18_2_m5F1FC9EE84572B40D3BD0F25969079F3779F9975 (void);
// 0x000001CA System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_3()
extern void U3CU3Ec_U3C_ctorU3Eb__18_3_m6F87CC805517B6142802989A198A77E5755CFF2F (void);
// 0x000001CB System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_4()
extern void U3CU3Ec_U3C_ctorU3Eb__18_4_m1E7C89203B4B2400FA115B32CDAF8C31FB47BE33 (void);
// 0x000001CC System.Void IronSourceBannerAndroid/<>c::<.ctor>b__18_5(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__18_5_m148561F53AE393AC9CE3DE54878905AA7BD1C21C (void);
// 0x000001CD System.Void IronSourceBannerEvents::add_onAdLoadedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_add_onAdLoadedEvent_mAB7C723AEBDD230032F2C0E7A3181E766F9B6782 (void);
// 0x000001CE System.Void IronSourceBannerEvents::remove_onAdLoadedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_remove_onAdLoadedEvent_mAA8ECCC8241A0D08A6D9B0046B670C25D11DFB94 (void);
// 0x000001CF System.Void IronSourceBannerEvents::add_onAdLeftApplicationEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_add_onAdLeftApplicationEvent_m364538208BEEF7C3ED2F45326789074552D1801A (void);
// 0x000001D0 System.Void IronSourceBannerEvents::remove_onAdLeftApplicationEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_remove_onAdLeftApplicationEvent_m389EA33DA3F4CF2C3E5B484FF99D6C1D50C9865E (void);
// 0x000001D1 System.Void IronSourceBannerEvents::add_onAdScreenDismissedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_add_onAdScreenDismissedEvent_m0E7948623E369E5E3CD05DC4D063B271A9DD3877 (void);
// 0x000001D2 System.Void IronSourceBannerEvents::remove_onAdScreenDismissedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_remove_onAdScreenDismissedEvent_m12DA29BEFB70CEA0A85965C7353EBEB464D71797 (void);
// 0x000001D3 System.Void IronSourceBannerEvents::add_onAdScreenPresentedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_add_onAdScreenPresentedEvent_m9219387E02653C21FE739D020CBBA2B9E81C4FC8 (void);
// 0x000001D4 System.Void IronSourceBannerEvents::remove_onAdScreenPresentedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_remove_onAdScreenPresentedEvent_mE38F36C5B00793A655E6E7EABE8A4F2D5CCC8405 (void);
// 0x000001D5 System.Void IronSourceBannerEvents::add_onAdClickedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_add_onAdClickedEvent_mDE542295FDD50DAE96B0E7AB0C2FE717431D2519 (void);
// 0x000001D6 System.Void IronSourceBannerEvents::remove_onAdClickedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerEvents_remove_onAdClickedEvent_mE3505586962B6AA519EB0F305CC091B47F962E89 (void);
// 0x000001D7 System.Void IronSourceBannerEvents::add_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceBannerEvents_add_onAdLoadFailedEvent_m20245573E61E5D57AAAA1CB50EC7B1990F6B698F (void);
// 0x000001D8 System.Void IronSourceBannerEvents::remove_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceBannerEvents_remove_onAdLoadFailedEvent_m793EB4010C773B21B70D983A6A13F3A3B3A580CA (void);
// 0x000001D9 System.Void IronSourceBannerEvents::Awake()
extern void IronSourceBannerEvents_Awake_mDF073F75C563D3944C06ECF35A9E240CE5DA18A8 (void);
// 0x000001DA System.Void IronSourceBannerEvents::registerBannerEvents()
extern void IronSourceBannerEvents_registerBannerEvents_m299B4CE3E629F5DDB071711AE79E1A5CFEED0835 (void);
// 0x000001DB IronSourceError IronSourceBannerEvents::getErrorFromErrorObject(System.Object)
extern void IronSourceBannerEvents_getErrorFromErrorObject_m556ED5C825602D48726CA81B573AFAFCCBEB970D (void);
// 0x000001DC IronSourcePlacement IronSourceBannerEvents::getPlacementFromObject(System.Object)
extern void IronSourceBannerEvents_getPlacementFromObject_m55A925AFA6A74551CD777EA8552ED936C72C74E8 (void);
// 0x000001DD System.Void IronSourceBannerEvents::.ctor()
extern void IronSourceBannerEvents__ctor_mC266A7D537841B189D398F4FDB66827C58E4D9F7 (void);
// 0x000001DE System.Void IronSourceBannerEvents/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m2517437E4858DE1CF59F79E3CCA0CB7B5C039924 (void);
// 0x000001DF System.Void IronSourceBannerEvents/<>c__DisplayClass20_0::<registerBannerEvents>b__6()
extern void U3CU3Ec__DisplayClass20_0_U3CregisterBannerEventsU3Eb__6_mAA654A109960A234B9B35ECF0CE7478E043B3101 (void);
// 0x000001E0 System.Void IronSourceBannerEvents/<>c__DisplayClass20_1::.ctor()
extern void U3CU3Ec__DisplayClass20_1__ctor_m1A47472451937CF8013DA93B8A3F84BAF997E89D (void);
// 0x000001E1 System.Void IronSourceBannerEvents/<>c__DisplayClass20_1::<registerBannerEvents>b__7()
extern void U3CU3Ec__DisplayClass20_1_U3CregisterBannerEventsU3Eb__7_mB218B5DBF1F7A693C7659E494D1D17BF40DA367F (void);
// 0x000001E2 System.Void IronSourceBannerEvents/<>c__DisplayClass20_2::.ctor()
extern void U3CU3Ec__DisplayClass20_2__ctor_mE7762ED8A14B6A7359798AF0FBCED6F957B9C9D0 (void);
// 0x000001E3 System.Void IronSourceBannerEvents/<>c__DisplayClass20_2::<registerBannerEvents>b__8()
extern void U3CU3Ec__DisplayClass20_2_U3CregisterBannerEventsU3Eb__8_m5C27A65340A0D9310B62B7591581BD4732848B4E (void);
// 0x000001E4 System.Void IronSourceBannerEvents/<>c__DisplayClass20_3::.ctor()
extern void U3CU3Ec__DisplayClass20_3__ctor_m5A7B50AB8AA48F2FA9FF8604E14A5A76CDCDB5BF (void);
// 0x000001E5 System.Void IronSourceBannerEvents/<>c__DisplayClass20_3::<registerBannerEvents>b__9()
extern void U3CU3Ec__DisplayClass20_3_U3CregisterBannerEventsU3Eb__9_m404E2A857A16C98E490DC0D4F00EF9A98F3C5BE6 (void);
// 0x000001E6 System.Void IronSourceBannerEvents/<>c__DisplayClass20_4::.ctor()
extern void U3CU3Ec__DisplayClass20_4__ctor_mC82F5474D6C5D6391AFCBC3106E80787CE99F70C (void);
// 0x000001E7 System.Void IronSourceBannerEvents/<>c__DisplayClass20_4::<registerBannerEvents>b__10()
extern void U3CU3Ec__DisplayClass20_4_U3CregisterBannerEventsU3Eb__10_mC892FE1F96F7DA45D97D7780D039A62A2ED2E77F (void);
// 0x000001E8 System.Void IronSourceBannerEvents/<>c__DisplayClass20_5::.ctor()
extern void U3CU3Ec__DisplayClass20_5__ctor_mA9193729A3045A09C106B620E9DE17E4B50EF27B (void);
// 0x000001E9 System.Void IronSourceBannerEvents/<>c__DisplayClass20_5::<registerBannerEvents>b__11()
extern void U3CU3Ec__DisplayClass20_5_U3CregisterBannerEventsU3Eb__11_m06E54B7136D2B36A19943B554336E264D2F55F91 (void);
// 0x000001EA System.Void IronSourceBannerEvents/<>c::.cctor()
extern void U3CU3Ec__cctor_mD339A4CAFC5F796EB1B714B5E2767C99CDC7EEDA (void);
// 0x000001EB System.Void IronSourceBannerEvents/<>c::.ctor()
extern void U3CU3Ec__ctor_mE836590DDEE358CB1B3F2FD4F8E920101D1343CA (void);
// 0x000001EC System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_0(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_0_mD4594D6839FD8D45637413FDD7B1C6F10BD6B9D7 (void);
// 0x000001ED System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_1(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_1_m8D1ACE2A3539A2711348B076E1EC0DAF20CE2739 (void);
// 0x000001EE System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_2(IronSourceError)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_2_m4EDC1C5641309446B7E72F07997DF5C2D2AEB2A4 (void);
// 0x000001EF System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_3(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_3_m7F4C9335A1205B5F2AFA8BD87C67DAF8D627AB7E (void);
// 0x000001F0 System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_4(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_4_mF7407AB7F3115DD76DC841261202EB39EE35E104 (void);
// 0x000001F1 System.Void IronSourceBannerEvents/<>c::<registerBannerEvents>b__20_5(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__20_5_m777C96B012685E3A88AF31D2AE54A0967A927012 (void);
// 0x000001F2 System.Void IronSourceBannerLevelPlayAndroid::add_OnAdLoaded(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdLoaded_m8D0C1C08B64E35487A3C7E5E09ABCCA0F7ECFCE7 (void);
// 0x000001F3 System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdLoaded(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdLoaded_m0F4E7C083A5FEAAC377501D23C913FA016F8E37D (void);
// 0x000001F4 System.Void IronSourceBannerLevelPlayAndroid::add_OnAdLeftApplication(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdLeftApplication_mAE8E0216AB45D618B87E7ED5B6D9694A2861AFAD (void);
// 0x000001F5 System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdLeftApplication(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdLeftApplication_m4054FCCDDAC1DBDBEC4647146DCB389C7402AF58 (void);
// 0x000001F6 System.Void IronSourceBannerLevelPlayAndroid::add_OnAdScreenDismissed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdScreenDismissed_m7A57A3315A9A6A4B5B31D6CFB208BC2EAEB1F427 (void);
// 0x000001F7 System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdScreenDismissed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdScreenDismissed_m18B2FA386F8E29EA5CB8F250BD68F0211489F1C1 (void);
// 0x000001F8 System.Void IronSourceBannerLevelPlayAndroid::add_OnAdScreenPresented(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdScreenPresented_m9C5EBA20F672A33187FD8F74A27A36A83CFE793B (void);
// 0x000001F9 System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdScreenPresented(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdScreenPresented_m76A36E7298826F585B748CF537891C819D14302B (void);
// 0x000001FA System.Void IronSourceBannerLevelPlayAndroid::add_OnAdClicked(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdClicked_mABF09F6553B809AFE05B6F3815EF2CDCE4AC3057 (void);
// 0x000001FB System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdClicked(System.Action`1<IronSourceAdInfo>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdClicked_mA7C4C7EB81F9B16555A0CABA593C6A58808979F6 (void);
// 0x000001FC System.Void IronSourceBannerLevelPlayAndroid::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceBannerLevelPlayAndroid_add_OnAdLoadFailed_m9E64BDDD38DBCBF9037352A72BF886B389031DB0 (void);
// 0x000001FD System.Void IronSourceBannerLevelPlayAndroid::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceBannerLevelPlayAndroid_remove_OnAdLoadFailed_mFD0D76AC648F48F27C7E2C7FFF901C1D612A4AD0 (void);
// 0x000001FE System.Void IronSourceBannerLevelPlayAndroid::.ctor()
extern void IronSourceBannerLevelPlayAndroid__ctor_m9065B57B265EC9D60F65C21E4C4EBA59EB144269 (void);
// 0x000001FF System.Void IronSourceBannerLevelPlayAndroid::onAdLoaded(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdLoaded_mEEB52915F8014FC0E18697A82672CC3635DC4464 (void);
// 0x00000200 System.Void IronSourceBannerLevelPlayAndroid::onAdLoadFailed(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdLoadFailed_m202B6BA11EAA5BE6B79D878543E2AE421E523A12 (void);
// 0x00000201 System.Void IronSourceBannerLevelPlayAndroid::onAdClicked(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdClicked_mD48EB773C7AD76360B926A7F3D4F3E92F15CB045 (void);
// 0x00000202 System.Void IronSourceBannerLevelPlayAndroid::onAdScreenPresented(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdScreenPresented_m00934215E8F282C17D37187A4CF0600247D9C073 (void);
// 0x00000203 System.Void IronSourceBannerLevelPlayAndroid::onAdScreenDismissed(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdScreenDismissed_m958010DC3285FE02595B7B7964064C68032061D9 (void);
// 0x00000204 System.Void IronSourceBannerLevelPlayAndroid::onAdLeftApplication(System.String)
extern void IronSourceBannerLevelPlayAndroid_onAdLeftApplication_m95AA7B0145781994AB4AB96304FE266EA495B744 (void);
// 0x00000205 System.Void IronSourceBannerLevelPlayAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m1A0E84DF521F89B62BCCCC2C1C071C9E18D1BFA2 (void);
// 0x00000206 System.Void IronSourceBannerLevelPlayAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mACD1B0DA8066DDCA825BDAC2691A81B0BF0C3F56 (void);
// 0x00000207 System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_0(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__18_0_m683D6190D8D7387F12659738C19177593B3F046E (void);
// 0x00000208 System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_1(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__18_1_m43B9EFF4E586D5F50AC5D387E9DDE6261FD6C335 (void);
// 0x00000209 System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_2(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__18_2_mC4E4EDB598DFFAF8CB90B7349AAC79DB84D934C3 (void);
// 0x0000020A System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_3(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__18_3_m3015019EE3B07B250BE1FBC71FEE7549B8DAB96A (void);
// 0x0000020B System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_4(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__18_4_mBF9AEC6824556206CF36FBD2F9166F181A00E7EC (void);
// 0x0000020C System.Void IronSourceBannerLevelPlayAndroid/<>c::<.ctor>b__18_5(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__18_5_mDE3FABD48D1210EEB765E21BEDFFE18AEE1610A4 (void);
// 0x0000020D IronSourceConfig IronSourceConfig::get_Instance()
extern void IronSourceConfig_get_Instance_m0378F040E15F2A50A09D6FB92BC8EF574567AD66 (void);
// 0x0000020E System.Void IronSourceConfig::.ctor()
extern void IronSourceConfig__ctor_mE3DE536BB1303D6358FCB9E67903E797C52A7999 (void);
// 0x0000020F System.Void IronSourceConfig::setLanguage(System.String)
extern void IronSourceConfig_setLanguage_m1B96A863FC8FEB5E966153C42A009F7EAE954C6B (void);
// 0x00000210 System.Void IronSourceConfig::setClientSideCallbacks(System.Boolean)
extern void IronSourceConfig_setClientSideCallbacks_m403FACCCA99A9B395CF62210EF543BFE43D42D54 (void);
// 0x00000211 System.Void IronSourceConfig::setRewardedVideoCustomParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void IronSourceConfig_setRewardedVideoCustomParams_m8F9337882315A174B87CDC49559808AAEBD3E914 (void);
// 0x00000212 System.Void IronSourceConfig::setOfferwallCustomParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void IronSourceConfig_setOfferwallCustomParams_m6A7A28411A5014E4B951DE2249173162FB7DB5F4 (void);
// 0x00000213 System.Void IronSourceConfig::.cctor()
extern void IronSourceConfig__cctor_m1A5AC5F7A51F07730D169FACF3E7F742795E1B1E (void);
// 0x00000214 System.Int32 IronSourceError::getErrorCode()
extern void IronSourceError_getErrorCode_m52FDA5DC9A5907273FA8295C0126098D72D53583 (void);
// 0x00000215 System.String IronSourceError::getDescription()
extern void IronSourceError_getDescription_m5B0FEA9AB9AA406E2EC2582B81FFB8F1AD99F5CB (void);
// 0x00000216 System.Int32 IronSourceError::getCode()
extern void IronSourceError_getCode_mECF7AA882A931B515CBB012FB97D5D8F9B3D7194 (void);
// 0x00000217 System.Void IronSourceError::.ctor(System.Int32,System.String)
extern void IronSourceError__ctor_mA09EE012D497AB24FD88631DEBD67A9FBB801E83 (void);
// 0x00000218 System.String IronSourceError::ToString()
extern void IronSourceError_ToString_m4C41B343FE87831C5DEB35AD4A33A66E9CF374E1 (void);
// 0x00000219 System.Void IronSourceEvents::add_onSdkInitializationCompletedEvent(System.Action)
extern void IronSourceEvents_add_onSdkInitializationCompletedEvent_m471FCAE6872A1E5A9AC33A8AC594D81C8615E4AC (void);
// 0x0000021A System.Void IronSourceEvents::remove_onSdkInitializationCompletedEvent(System.Action)
extern void IronSourceEvents_remove_onSdkInitializationCompletedEvent_m91D5D675910C3CBC3B0F0DC97E1A2B18839EF860 (void);
// 0x0000021B System.Void IronSourceEvents::add_onRewardedVideoAdShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onRewardedVideoAdShowFailedEvent_m29722602CE97A6FDF8523AC431E189C2048F16E4 (void);
// 0x0000021C System.Void IronSourceEvents::remove_onRewardedVideoAdShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onRewardedVideoAdShowFailedEvent_m73E0FE05A1CEBBFA062FBE224F27D5374446A731 (void);
// 0x0000021D System.Void IronSourceEvents::add_onRewardedVideoAdOpenedEvent(System.Action)
extern void IronSourceEvents_add_onRewardedVideoAdOpenedEvent_mB55F555C6F8D9EAE02B2E5D4CDE14128CBC77B73 (void);
// 0x0000021E System.Void IronSourceEvents::remove_onRewardedVideoAdOpenedEvent(System.Action)
extern void IronSourceEvents_remove_onRewardedVideoAdOpenedEvent_m8D54F4E940A98474E897E02805BEC2B7D6A5F772 (void);
// 0x0000021F System.Void IronSourceEvents::add_onRewardedVideoAdClosedEvent(System.Action)
extern void IronSourceEvents_add_onRewardedVideoAdClosedEvent_m959D5DD7F99D48A65FDC3441C934EB3D4E7300E4 (void);
// 0x00000220 System.Void IronSourceEvents::remove_onRewardedVideoAdClosedEvent(System.Action)
extern void IronSourceEvents_remove_onRewardedVideoAdClosedEvent_mEB76930F842DB23E0B7EE650D19F3C183E81C36A (void);
// 0x00000221 System.Void IronSourceEvents::add_onRewardedVideoAdStartedEvent(System.Action)
extern void IronSourceEvents_add_onRewardedVideoAdStartedEvent_m6620D9C25FFCDF3DE9A90C29E882DD2C280A2763 (void);
// 0x00000222 System.Void IronSourceEvents::remove_onRewardedVideoAdStartedEvent(System.Action)
extern void IronSourceEvents_remove_onRewardedVideoAdStartedEvent_m0B5E8A2A39DAD8DA0962D509A040948662C62004 (void);
// 0x00000223 System.Void IronSourceEvents::add_onRewardedVideoAdEndedEvent(System.Action)
extern void IronSourceEvents_add_onRewardedVideoAdEndedEvent_m3250118B6571BB74D01A028F606D1206167BADA1 (void);
// 0x00000224 System.Void IronSourceEvents::remove_onRewardedVideoAdEndedEvent(System.Action)
extern void IronSourceEvents_remove_onRewardedVideoAdEndedEvent_mCB3FD0F8A3A964DF972F87B124C0CFBB413C17F8 (void);
// 0x00000225 System.Void IronSourceEvents::add_onRewardedVideoAdRewardedEvent(System.Action`1<IronSourcePlacement>)
extern void IronSourceEvents_add_onRewardedVideoAdRewardedEvent_mA71DC1C296BDAFB7EF00BC2937C0D8838E93AFAD (void);
// 0x00000226 System.Void IronSourceEvents::remove_onRewardedVideoAdRewardedEvent(System.Action`1<IronSourcePlacement>)
extern void IronSourceEvents_remove_onRewardedVideoAdRewardedEvent_m357D37267736CC1C2D77718D7685A32208B46329 (void);
// 0x00000227 System.Void IronSourceEvents::add_onRewardedVideoAdClickedEvent(System.Action`1<IronSourcePlacement>)
extern void IronSourceEvents_add_onRewardedVideoAdClickedEvent_m8B8BEA58A629E567AFF177241995FDF24F830A59 (void);
// 0x00000228 System.Void IronSourceEvents::remove_onRewardedVideoAdClickedEvent(System.Action`1<IronSourcePlacement>)
extern void IronSourceEvents_remove_onRewardedVideoAdClickedEvent_m3C0B966592CD46F8576B099166726F6137613A0D (void);
// 0x00000229 System.Void IronSourceEvents::add_onRewardedVideoAvailabilityChangedEvent(System.Action`1<System.Boolean>)
extern void IronSourceEvents_add_onRewardedVideoAvailabilityChangedEvent_m5A19B1A1EBFDC945BA01B41D2CE0BA9BE5734B72 (void);
// 0x0000022A System.Void IronSourceEvents::remove_onRewardedVideoAvailabilityChangedEvent(System.Action`1<System.Boolean>)
extern void IronSourceEvents_remove_onRewardedVideoAvailabilityChangedEvent_mD4A3B6C153DA2992A8C6985132F7810B6A4C1718 (void);
// 0x0000022B System.Void IronSourceEvents::add_onRewardedVideoAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onRewardedVideoAdLoadFailedEvent_m3A6F6307B15CA482926E295A75C566E141622B4B (void);
// 0x0000022C System.Void IronSourceEvents::remove_onRewardedVideoAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onRewardedVideoAdLoadFailedEvent_mB9C1F50B6C29D9791BBC293CCCA891F953A94AD6 (void);
// 0x0000022D System.Void IronSourceEvents::add_onRewardedVideoAdReadyEvent(System.Action)
extern void IronSourceEvents_add_onRewardedVideoAdReadyEvent_mD6E4535916F412CA974342BC5E1BBB5B6C17155B (void);
// 0x0000022E System.Void IronSourceEvents::remove_onRewardedVideoAdReadyEvent(System.Action)
extern void IronSourceEvents_remove_onRewardedVideoAdReadyEvent_m72ABF85DAEDAD5C0F49E55951C2664342AE74BE2 (void);
// 0x0000022F System.Void IronSourceEvents::add_onRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onRewardedVideoAdOpenedDemandOnlyEvent_mEC82D0E4958FBB13D533D4C0679E29B2D6549F10 (void);
// 0x00000230 System.Void IronSourceEvents::remove_onRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onRewardedVideoAdOpenedDemandOnlyEvent_mA32E0DB6FEEA1485DDFE6A1B718E289C8030D804 (void);
// 0x00000231 System.Void IronSourceEvents::add_onRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onRewardedVideoAdClosedDemandOnlyEvent_m3C67F506D142671A261C04A10AC3F9AB0AEA081A (void);
// 0x00000232 System.Void IronSourceEvents::remove_onRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onRewardedVideoAdClosedDemandOnlyEvent_mB08D7D65220F81075C72419BD473855FF2A4C5ED (void);
// 0x00000233 System.Void IronSourceEvents::add_onRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onRewardedVideoAdLoadedDemandOnlyEvent_m0D48658F2FD93FC2E3131CE20136099A34CCCE6D (void);
// 0x00000234 System.Void IronSourceEvents::remove_onRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onRewardedVideoAdLoadedDemandOnlyEvent_m8E0833F6DEB0FC8DAC856408437CA7350144498C (void);
// 0x00000235 System.Void IronSourceEvents::add_onRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onRewardedVideoAdRewardedDemandOnlyEvent_mFCEE5A8D8368EEBCBF7191665AE7ECD67418A980 (void);
// 0x00000236 System.Void IronSourceEvents::remove_onRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onRewardedVideoAdRewardedDemandOnlyEvent_mE5EC2B6BE2A60A199144DA0F244599107592DB99 (void);
// 0x00000237 System.Void IronSourceEvents::add_onRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onRewardedVideoAdShowFailedDemandOnlyEvent_m4DF230EC01A9F727A11251AC6B2F8539ADE29ED1 (void);
// 0x00000238 System.Void IronSourceEvents::remove_onRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onRewardedVideoAdShowFailedDemandOnlyEvent_m4498C29227026D44338E6E4E37083BF5481421D9 (void);
// 0x00000239 System.Void IronSourceEvents::add_onRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onRewardedVideoAdClickedDemandOnlyEvent_mA9C688E33BB7D9B92295EBF945BF8768ED90C4F3 (void);
// 0x0000023A System.Void IronSourceEvents::remove_onRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onRewardedVideoAdClickedDemandOnlyEvent_m6104623178966A79B4039CDEE64F9615C12355DC (void);
// 0x0000023B System.Void IronSourceEvents::add_onRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onRewardedVideoAdLoadFailedDemandOnlyEvent_m4B826C2CCD77AA6F70B04D9964102BB3A1EFE5C6 (void);
// 0x0000023C System.Void IronSourceEvents::remove_onRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onRewardedVideoAdLoadFailedDemandOnlyEvent_mC253AFA347960767255B15EF643B0FEA194786BB (void);
// 0x0000023D System.Void IronSourceEvents::add_onInterstitialAdReadyEvent(System.Action)
extern void IronSourceEvents_add_onInterstitialAdReadyEvent_m34F94121A366099D40030542EABDDAF33BC7FE7C (void);
// 0x0000023E System.Void IronSourceEvents::remove_onInterstitialAdReadyEvent(System.Action)
extern void IronSourceEvents_remove_onInterstitialAdReadyEvent_m1870C568C60D032D143D8DA1F1222D4207FF33FA (void);
// 0x0000023F System.Void IronSourceEvents::add_onInterstitialAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onInterstitialAdLoadFailedEvent_m24CF2CD7A6382B1EE2F14998784AC3F6632479AE (void);
// 0x00000240 System.Void IronSourceEvents::remove_onInterstitialAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onInterstitialAdLoadFailedEvent_m3C04C71B599FC0A451242EFBBA9ABD9CD14D7493 (void);
// 0x00000241 System.Void IronSourceEvents::add_onInterstitialAdOpenedEvent(System.Action)
extern void IronSourceEvents_add_onInterstitialAdOpenedEvent_m00C0BC496463FE263851C412124530A40EA09A36 (void);
// 0x00000242 System.Void IronSourceEvents::remove_onInterstitialAdOpenedEvent(System.Action)
extern void IronSourceEvents_remove_onInterstitialAdOpenedEvent_m91690127DBA1BF88842E0A3CF474FEB9E492BC88 (void);
// 0x00000243 System.Void IronSourceEvents::add_onInterstitialAdClosedEvent(System.Action)
extern void IronSourceEvents_add_onInterstitialAdClosedEvent_mD6847B3D2C68F8079CC790C13FAD39052E15273F (void);
// 0x00000244 System.Void IronSourceEvents::remove_onInterstitialAdClosedEvent(System.Action)
extern void IronSourceEvents_remove_onInterstitialAdClosedEvent_mE9B906EB9F8F06E59ADD4DABDD5EBBF11AC5CF57 (void);
// 0x00000245 System.Void IronSourceEvents::add_onInterstitialAdShowSucceededEvent(System.Action)
extern void IronSourceEvents_add_onInterstitialAdShowSucceededEvent_m097D856DB436ACEA9CC31EE08B90B31464D673A3 (void);
// 0x00000246 System.Void IronSourceEvents::remove_onInterstitialAdShowSucceededEvent(System.Action)
extern void IronSourceEvents_remove_onInterstitialAdShowSucceededEvent_m68F162EEAF81CF1BF0636B01943E13639B958291 (void);
// 0x00000247 System.Void IronSourceEvents::add_onInterstitialAdShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onInterstitialAdShowFailedEvent_mF176DC5A365A7C77CC9707D2146E96FAB21D16E0 (void);
// 0x00000248 System.Void IronSourceEvents::remove_onInterstitialAdShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onInterstitialAdShowFailedEvent_m5664BE553972DAEC65AC31F1EFA98276ECF5FE2E (void);
// 0x00000249 System.Void IronSourceEvents::add_onInterstitialAdClickedEvent(System.Action)
extern void IronSourceEvents_add_onInterstitialAdClickedEvent_m78608D4D8D8A967090F89B172BA5D080ACB6D714 (void);
// 0x0000024A System.Void IronSourceEvents::remove_onInterstitialAdClickedEvent(System.Action)
extern void IronSourceEvents_remove_onInterstitialAdClickedEvent_m3D4F92373DD7D9950D8CEE254E159DB723630078 (void);
// 0x0000024B System.Void IronSourceEvents::add_onInterstitialAdReadyDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onInterstitialAdReadyDemandOnlyEvent_m2D877B96503ABECE2D8D0B2A92A24E5A65EBFD22 (void);
// 0x0000024C System.Void IronSourceEvents::remove_onInterstitialAdReadyDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onInterstitialAdReadyDemandOnlyEvent_m4EB1DC2ADBAD6A56F7AD2D1CEC531F2DC2CCDFA9 (void);
// 0x0000024D System.Void IronSourceEvents::add_onInterstitialAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onInterstitialAdOpenedDemandOnlyEvent_mEE95BFD58A4DA7278AB65094ED4CAC3D2A32C27F (void);
// 0x0000024E System.Void IronSourceEvents::remove_onInterstitialAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onInterstitialAdOpenedDemandOnlyEvent_mA2527D6E6FE0265C3A99CDF5048F15CE7FD0A6AB (void);
// 0x0000024F System.Void IronSourceEvents::add_onInterstitialAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onInterstitialAdClosedDemandOnlyEvent_mEE673AAE31EEE663A13E21DD557D095C4660DFF6 (void);
// 0x00000250 System.Void IronSourceEvents::remove_onInterstitialAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onInterstitialAdClosedDemandOnlyEvent_m6AA19068C126F8B6D6A4AB39EADAC377BDAB3B88 (void);
// 0x00000251 System.Void IronSourceEvents::add_onInterstitialAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onInterstitialAdLoadFailedDemandOnlyEvent_m9F00538F8DF6D450EC86F26B0F735D908CB8B04D (void);
// 0x00000252 System.Void IronSourceEvents::remove_onInterstitialAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onInterstitialAdLoadFailedDemandOnlyEvent_mE7C58951819292C37BA18E816E5FD2CD7174624A (void);
// 0x00000253 System.Void IronSourceEvents::add_onInterstitialAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onInterstitialAdClickedDemandOnlyEvent_m215930C2F54D9368B1F0C49DA0D9E4CC75D3E916 (void);
// 0x00000254 System.Void IronSourceEvents::remove_onInterstitialAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onInterstitialAdClickedDemandOnlyEvent_m1EB5EA8245922CFCDB42A6B5D0F1BAD3DD55C5B7 (void);
// 0x00000255 System.Void IronSourceEvents::add_onInterstitialAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onInterstitialAdShowFailedDemandOnlyEvent_m802B30D3869C47BC6024EAC4D132A6ED3ADD631E (void);
// 0x00000256 System.Void IronSourceEvents::remove_onInterstitialAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onInterstitialAdShowFailedDemandOnlyEvent_m5BB86A5CF3E509D2FB92CCF2330AB95D2D6A8665 (void);
// 0x00000257 System.Void IronSourceEvents::add_onOfferwallAvailableEvent(System.Action`1<System.Boolean>)
extern void IronSourceEvents_add_onOfferwallAvailableEvent_mABE61BE1908998144A168AD58666025107D40D74 (void);
// 0x00000258 System.Void IronSourceEvents::remove_onOfferwallAvailableEvent(System.Action`1<System.Boolean>)
extern void IronSourceEvents_remove_onOfferwallAvailableEvent_m1AB4884508986007B674ACE8DF82BD251A90BB99 (void);
// 0x00000259 System.Void IronSourceEvents::add_onOfferwallOpenedEvent(System.Action)
extern void IronSourceEvents_add_onOfferwallOpenedEvent_m26EB82756530ECF5CCD44B6715ED88FAAAC153D0 (void);
// 0x0000025A System.Void IronSourceEvents::remove_onOfferwallOpenedEvent(System.Action)
extern void IronSourceEvents_remove_onOfferwallOpenedEvent_mBF7745ACC3B197ECC0C28423B4C83421A78BA6D2 (void);
// 0x0000025B System.Void IronSourceEvents::add_onOfferwallAdCreditedEvent(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void IronSourceEvents_add_onOfferwallAdCreditedEvent_m625FE6E328C52D10FC5C9E8F759363510D5E8273 (void);
// 0x0000025C System.Void IronSourceEvents::remove_onOfferwallAdCreditedEvent(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void IronSourceEvents_remove_onOfferwallAdCreditedEvent_m0657B0EC312EFC2AC432DC6C56CD7FAA807F9147 (void);
// 0x0000025D System.Void IronSourceEvents::add_onGetOfferwallCreditsFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onGetOfferwallCreditsFailedEvent_m677F48F28418CD6E1A6014C6C1B8076E0934C545 (void);
// 0x0000025E System.Void IronSourceEvents::remove_onGetOfferwallCreditsFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onGetOfferwallCreditsFailedEvent_mAAA2A15BCA447C3928E8A8B228123D389DEE5528 (void);
// 0x0000025F System.Void IronSourceEvents::add_onOfferwallClosedEvent(System.Action)
extern void IronSourceEvents_add_onOfferwallClosedEvent_m031BB82A7474309857A686074FE8B715B194C7A3 (void);
// 0x00000260 System.Void IronSourceEvents::remove_onOfferwallClosedEvent(System.Action)
extern void IronSourceEvents_remove_onOfferwallClosedEvent_m66799C27234453F140B9CC2846894EEF2D95DD43 (void);
// 0x00000261 System.Void IronSourceEvents::add_onOfferwallShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onOfferwallShowFailedEvent_m2B47334DF4BB7C140970479B8D19CCC0D084BD60 (void);
// 0x00000262 System.Void IronSourceEvents::remove_onOfferwallShowFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onOfferwallShowFailedEvent_m8419C9F07B707125B5816BFEA8FBAEE721E07EEC (void);
// 0x00000263 System.Void IronSourceEvents::add_onBannerAdLoadedEvent(System.Action)
extern void IronSourceEvents_add_onBannerAdLoadedEvent_m5AC72619E274F9F242C060EFE25475FB5A8C2730 (void);
// 0x00000264 System.Void IronSourceEvents::remove_onBannerAdLoadedEvent(System.Action)
extern void IronSourceEvents_remove_onBannerAdLoadedEvent_m18EFCD0F6B0D380EE81D5A4CB2B95E6E6B79626C (void);
// 0x00000265 System.Void IronSourceEvents::add_onBannerAdLeftApplicationEvent(System.Action)
extern void IronSourceEvents_add_onBannerAdLeftApplicationEvent_m710AA72BED846467CE545277882E0DC15474BD42 (void);
// 0x00000266 System.Void IronSourceEvents::remove_onBannerAdLeftApplicationEvent(System.Action)
extern void IronSourceEvents_remove_onBannerAdLeftApplicationEvent_mBB047FC8EA03E3C1E56149EA35E1A2AD221ABD35 (void);
// 0x00000267 System.Void IronSourceEvents::add_onBannerAdScreenDismissedEvent(System.Action)
extern void IronSourceEvents_add_onBannerAdScreenDismissedEvent_m38B8276614DD95132A504FF6C2C3621E8CCDD027 (void);
// 0x00000268 System.Void IronSourceEvents::remove_onBannerAdScreenDismissedEvent(System.Action)
extern void IronSourceEvents_remove_onBannerAdScreenDismissedEvent_m3FFBB4F494DF1439DADA50638021FB119D922337 (void);
// 0x00000269 System.Void IronSourceEvents::add_onBannerAdScreenPresentedEvent(System.Action)
extern void IronSourceEvents_add_onBannerAdScreenPresentedEvent_m99C9D07939297D3B847AD13A5A05AC5798310FF0 (void);
// 0x0000026A System.Void IronSourceEvents::remove_onBannerAdScreenPresentedEvent(System.Action)
extern void IronSourceEvents_remove_onBannerAdScreenPresentedEvent_m98B8F5DADD79B17DE9819CCAF2E49795122652BF (void);
// 0x0000026B System.Void IronSourceEvents::add_onBannerAdClickedEvent(System.Action)
extern void IronSourceEvents_add_onBannerAdClickedEvent_mA997638A3550C426CF2BFEEE5890F773C057F6C1 (void);
// 0x0000026C System.Void IronSourceEvents::remove_onBannerAdClickedEvent(System.Action)
extern void IronSourceEvents_remove_onBannerAdClickedEvent_mFC885813154EFF3F0677E1D81D100D342CA5C56C (void);
// 0x0000026D System.Void IronSourceEvents::add_onBannerAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_add_onBannerAdLoadFailedEvent_m9AA6305404DB70F0C187B59FD2D6DCA83D0D144F (void);
// 0x0000026E System.Void IronSourceEvents::remove_onBannerAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceEvents_remove_onBannerAdLoadFailedEvent_mAD3C8E30972196A50CCA5521D18133DF32FE3C29 (void);
// 0x0000026F System.Void IronSourceEvents::add_onSegmentReceivedEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onSegmentReceivedEvent_m6998D836B150F5A65BCFA8EEA54EAE8D7CC3E2B2 (void);
// 0x00000270 System.Void IronSourceEvents::remove_onSegmentReceivedEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onSegmentReceivedEvent_m6753C63ED4D5AFFD58840D2D343430D7BAEC3EFE (void);
// 0x00000271 System.Void IronSourceEvents::add_onImpressionSuccessEvent(System.Action`1<IronSourceImpressionData>)
extern void IronSourceEvents_add_onImpressionSuccessEvent_m047522F303674EF194FEB9F8A03CC9457EDDE53F (void);
// 0x00000272 System.Void IronSourceEvents::remove_onImpressionSuccessEvent(System.Action`1<IronSourceImpressionData>)
extern void IronSourceEvents_remove_onImpressionSuccessEvent_m0461F3E2D5A62A4BCD4538B35B567A16B590CF3D (void);
// 0x00000273 System.Void IronSourceEvents::add_onImpressionDataReadyEvent(System.Action`1<IronSourceImpressionData>)
extern void IronSourceEvents_add_onImpressionDataReadyEvent_mCDB20226BEEF3502327CC2E5DFF014175E0661BB (void);
// 0x00000274 System.Void IronSourceEvents::remove_onImpressionDataReadyEvent(System.Action`1<IronSourceImpressionData>)
extern void IronSourceEvents_remove_onImpressionDataReadyEvent_m8EA6BD62BEC2C682BD314EF1B630D3C87EDBFADB (void);
// 0x00000275 System.Void IronSourceEvents::Awake()
extern void IronSourceEvents_Awake_m0CB3C1DF700B1BD58C8813A0653F43C8471B39E0 (void);
// 0x00000276 System.Void IronSourceEvents::registerInitializationEvents()
extern void IronSourceEvents_registerInitializationEvents_m0440A12C16761B186092C978E3C77D92AAFCCAA8 (void);
// 0x00000277 System.Void IronSourceEvents::registerBannerEvents()
extern void IronSourceEvents_registerBannerEvents_m797C8E569208993EE2D451731E7BDACB20CA9733 (void);
// 0x00000278 System.Void IronSourceEvents::registerInterstitialEvents()
extern void IronSourceEvents_registerInterstitialEvents_m5E613746944E0EFEEB7FD6D5ACA60D6A99960B9E (void);
// 0x00000279 System.Void IronSourceEvents::registerInterstitialDemandOnlyEvents()
extern void IronSourceEvents_registerInterstitialDemandOnlyEvents_m08D5A94CBFB61A5444C52CD4E93C3CCCF8A92C3F (void);
// 0x0000027A System.Void IronSourceEvents::registerOfferwallEvents()
extern void IronSourceEvents_registerOfferwallEvents_mF8243F660BE299D772D70F8A01E6F836C9FCBC95 (void);
// 0x0000027B System.Void IronSourceEvents::registerSegmentEvents()
extern void IronSourceEvents_registerSegmentEvents_m3D00BB865B5D40C1E00376B56291B91836E5496B (void);
// 0x0000027C System.Void IronSourceEvents::registerImpressionDataEvents()
extern void IronSourceEvents_registerImpressionDataEvents_m8B7756CC432FE7DD754786D20DCCC594A0D5DDFF (void);
// 0x0000027D System.Void IronSourceEvents::registerRewardedVideoDemandOnlyEvents()
extern void IronSourceEvents_registerRewardedVideoDemandOnlyEvents_mF2B83116D195B43F9D8A127B0213F1E0D6C6D177 (void);
// 0x0000027E System.Void IronSourceEvents::registerRewardedVideoEvents()
extern void IronSourceEvents_registerRewardedVideoEvents_mD992ECAFB586F4198F11E8249AF6357EA60CB812 (void);
// 0x0000027F System.Void IronSourceEvents::registerRewardedVideoManualEvents()
extern void IronSourceEvents_registerRewardedVideoManualEvents_m37914E35D820D14202C42232D19BB9E4161D3A38 (void);
// 0x00000280 System.Void IronSourceEvents::add__onConsentViewDidFailToLoadWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add__onConsentViewDidFailToLoadWithErrorEvent_m7E83CDADEFD2BFED62D3627CB462E48E7A53F130 (void);
// 0x00000281 System.Void IronSourceEvents::remove__onConsentViewDidFailToLoadWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove__onConsentViewDidFailToLoadWithErrorEvent_mB7CCDA70650FDAB369DF961F3338A9DC1EFE8854 (void);
// 0x00000282 System.Void IronSourceEvents::add_onConsentViewDidFailToLoadWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onConsentViewDidFailToLoadWithErrorEvent_m7AF225A27FA8F204DAFB841FBA5F1F5370F2ABF6 (void);
// 0x00000283 System.Void IronSourceEvents::remove_onConsentViewDidFailToLoadWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onConsentViewDidFailToLoadWithErrorEvent_m7D9F5092EDE934B8EF3605B75C0376DDBB4E03D6 (void);
// 0x00000284 System.Void IronSourceEvents::onConsentViewDidFailToLoadWithError(System.String)
extern void IronSourceEvents_onConsentViewDidFailToLoadWithError_mD7994A50F8CAB78E71DE22F3FCFDD14D9AC182DE (void);
// 0x00000285 System.Void IronSourceEvents::add__onConsentViewDidFailToShowWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add__onConsentViewDidFailToShowWithErrorEvent_mBADA58A3A8DD457A76F7EF3CDA062C115A7431F6 (void);
// 0x00000286 System.Void IronSourceEvents::remove__onConsentViewDidFailToShowWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove__onConsentViewDidFailToShowWithErrorEvent_m955580FA39FB6334B09958CA3987E6AC818F1E44 (void);
// 0x00000287 System.Void IronSourceEvents::add_onConsentViewDidFailToShowWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_add_onConsentViewDidFailToShowWithErrorEvent_m714366B0D0F10BC22383A7AAE182B2D7C0FD90BC (void);
// 0x00000288 System.Void IronSourceEvents::remove_onConsentViewDidFailToShowWithErrorEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceEvents_remove_onConsentViewDidFailToShowWithErrorEvent_m69C51056180A357D572FB98D1476F69AFB783A7A (void);
// 0x00000289 System.Void IronSourceEvents::onConsentViewDidFailToShowWithError(System.String)
extern void IronSourceEvents_onConsentViewDidFailToShowWithError_mB67F917F6ECCC1AB1FF1B82343E36E81986E73DD (void);
// 0x0000028A System.Void IronSourceEvents::add__onConsentViewDidAcceptEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add__onConsentViewDidAcceptEvent_mFC0762C460226A61102E0ADE2DB6673A67CFB4A1 (void);
// 0x0000028B System.Void IronSourceEvents::remove__onConsentViewDidAcceptEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove__onConsentViewDidAcceptEvent_mFADA8AF86FCFA9FFDCC28733F1FB74B37A301C09 (void);
// 0x0000028C System.Void IronSourceEvents::add_onConsentViewDidAcceptEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onConsentViewDidAcceptEvent_mD858F0C13086EF25D39A1FC3ECCF198E1DABB37C (void);
// 0x0000028D System.Void IronSourceEvents::remove_onConsentViewDidAcceptEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onConsentViewDidAcceptEvent_mD755AEC6E7B01AD08DCA6D7553A7720993EABE2D (void);
// 0x0000028E System.Void IronSourceEvents::onConsentViewDidAccept(System.String)
extern void IronSourceEvents_onConsentViewDidAccept_m456D33695A84A713912C46E0C41262E5E21CC022 (void);
// 0x0000028F System.Void IronSourceEvents::add__onConsentViewDidDismissEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add__onConsentViewDidDismissEvent_m3F7A4DA3B6FA218C21EF115E99E26AF12222D039 (void);
// 0x00000290 System.Void IronSourceEvents::remove__onConsentViewDidDismissEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove__onConsentViewDidDismissEvent_m96ABDD24E844C9F0A5C8E609A774EDA41F761AB9 (void);
// 0x00000291 System.Void IronSourceEvents::add_onConsentViewDidDismissEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onConsentViewDidDismissEvent_m6ADA373C67B8C73693388D8CEB8AB3F4F0EA535C (void);
// 0x00000292 System.Void IronSourceEvents::remove_onConsentViewDidDismissEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onConsentViewDidDismissEvent_mD76ACB56866CEAF43733F2372FDC947E4D99E36B (void);
// 0x00000293 System.Void IronSourceEvents::onConsentViewDidDismiss(System.String)
extern void IronSourceEvents_onConsentViewDidDismiss_m41190FB7B040AC2E390775276AA102A8E2077682 (void);
// 0x00000294 System.Void IronSourceEvents::add__onConsentViewDidLoadSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add__onConsentViewDidLoadSuccessEvent_m091FA7804E1529C0FC10E923ECA9E9E59F8CC923 (void);
// 0x00000295 System.Void IronSourceEvents::remove__onConsentViewDidLoadSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove__onConsentViewDidLoadSuccessEvent_m9724FEB1CB26470BB9A85CF61D40CE4789B293F2 (void);
// 0x00000296 System.Void IronSourceEvents::add_onConsentViewDidLoadSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onConsentViewDidLoadSuccessEvent_mF450ADA3183ABEB3193B0CAC819E72A84B990233 (void);
// 0x00000297 System.Void IronSourceEvents::remove_onConsentViewDidLoadSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onConsentViewDidLoadSuccessEvent_m969EB641479A20FD91FC86E68C8109FFCAF12E83 (void);
// 0x00000298 System.Void IronSourceEvents::onConsentViewDidLoadSuccess(System.String)
extern void IronSourceEvents_onConsentViewDidLoadSuccess_mF6046C66B99CCD582F189FB382B1574AF663B0F9 (void);
// 0x00000299 System.Void IronSourceEvents::add__onConsentViewDidShowSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add__onConsentViewDidShowSuccessEvent_mE7A70EA11EF021350802F9E18B39C2CFC0C16068 (void);
// 0x0000029A System.Void IronSourceEvents::remove__onConsentViewDidShowSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove__onConsentViewDidShowSuccessEvent_m689A5DD54D76363486ACB300B69629EEE40B2E76 (void);
// 0x0000029B System.Void IronSourceEvents::add_onConsentViewDidShowSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_add_onConsentViewDidShowSuccessEvent_mE2CB53F9E07E8FB957E2822BEBC769FAABFC9D78 (void);
// 0x0000029C System.Void IronSourceEvents::remove_onConsentViewDidShowSuccessEvent(System.Action`1<System.String>)
extern void IronSourceEvents_remove_onConsentViewDidShowSuccessEvent_mA20C70BB78000506E991D35F86A1713A5B822711 (void);
// 0x0000029D System.Void IronSourceEvents::onConsentViewDidShowSuccess(System.String)
extern void IronSourceEvents_onConsentViewDidShowSuccess_mDD064CF212AC73248A0C9B37C3C08606181F77A0 (void);
// 0x0000029E IronSourceError IronSourceEvents::getErrorFromErrorObject(System.Object)
extern void IronSourceEvents_getErrorFromErrorObject_mFD793978EFB2037D980106D7A18F385A86880B96 (void);
// 0x0000029F IronSourcePlacement IronSourceEvents::getPlacementFromObject(System.Object)
extern void IronSourceEvents_getPlacementFromObject_m95E8B3F58AB072286914C92B282350B12F797E72 (void);
// 0x000002A0 System.Void IronSourceEvents::InvokeEvent(System.Action`1<IronSourceImpressionData>,System.String)
extern void IronSourceEvents_InvokeEvent_mC492E94F6DFF61B20560DE716E1116E5CBB99239 (void);
// 0x000002A1 System.Void IronSourceEvents::.ctor()
extern void IronSourceEvents__ctor_mA551D770B0AADFBA26A1C7F3AFA0B9C304654F5F (void);
// 0x000002A2 System.Void IronSourceEvents/<>c::.cctor()
extern void U3CU3Ec__cctor_m09FE091D42C3C439D661E1E724D0AA5E7EF03CB2 (void);
// 0x000002A3 System.Void IronSourceEvents/<>c::.ctor()
extern void U3CU3Ec__ctor_m4E5FB9428004D80CB5FACA7C5193AB7575109A62 (void);
// 0x000002A4 System.Void IronSourceEvents/<>c::<registerInitializationEvents>b__151_0()
extern void U3CU3Ec_U3CregisterInitializationEventsU3Eb__151_0_m267F582A13C7915F968FE82BB0FDE7F93CBB2992 (void);
// 0x000002A5 System.Void IronSourceEvents/<>c::<registerInitializationEvents>b__151_1()
extern void U3CU3Ec_U3CregisterInitializationEventsU3Eb__151_1_m003CFDE9E63B60438D38CFB3C10D1A854440B555 (void);
// 0x000002A6 System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_0()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_0_mB9495556BBCFE038D77BCD61A35FEE0C0EBB57C4 (void);
// 0x000002A7 System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_6()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_6_m097161BCC8F9E9A58E37DD3FCAF572F144FDC307 (void);
// 0x000002A8 System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_1()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_1_m26F63B1DFEF3F0354953E83F75BD38D0650CCE96 (void);
// 0x000002A9 System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_7()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_7_mA6CE38103BB13E711C910BE79C0DB6E86DC4617F (void);
// 0x000002AA System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_2(IronSourceError)
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_2_m7F691D164F04E9C61BA7B166C5C8FEC5440FDC56 (void);
// 0x000002AB System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_3()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_3_mDD9F856FED30B26C3AC3C4BB0C4089DF185C79D3 (void);
// 0x000002AC System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_9()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_9_m4847192AE0E7AB3F33E43FD4712876F109ADA5CA (void);
// 0x000002AD System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_4()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_4_m4FEDA56B8A8544543A8D272A82A82E81AAD753B9 (void);
// 0x000002AE System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_10()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_10_m8137E8D7C89BF1038FCB479A269A140A2B43C505 (void);
// 0x000002AF System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_5()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_5_m6C943703F9A2956B68E997C51C60AD3F241B9CAB (void);
// 0x000002B0 System.Void IronSourceEvents/<>c::<registerBannerEvents>b__152_11()
extern void U3CU3Ec_U3CregisterBannerEventsU3Eb__152_11_mF276D5137121A098BC1CE0658C95F26213757A33 (void);
// 0x000002B1 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_0()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_0_m4A7558CFFEDFDA653EE01CB0CFAA5C0A0277BCDC (void);
// 0x000002B2 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_7()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_7_mE5691673171522EA00B0AB3A0AD9DFE9163CC55D (void);
// 0x000002B3 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_1()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_1_m3582828D0EE6D54E1DE4128B119FCE86CA78051D (void);
// 0x000002B4 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_8()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_8_m39C211CCF95564D07FF032D3E45FDE658CA0013F (void);
// 0x000002B5 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_2()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_2_m64A6AF2DB383B2B1573FB1A6D435D9AD6C7BD0DC (void);
// 0x000002B6 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_9()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_9_m0B734F5A33D69E91DFB6949344A2DD99C6AD3713 (void);
// 0x000002B7 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_3()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_3_m5366E23B48A8B057DBC6BB605272827E38B27BB1 (void);
// 0x000002B8 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_10()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_10_m2D87627E100EDF679EED31F0F7658C96B067E91E (void);
// 0x000002B9 System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_4(IronSourceError)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_4_mFE9E6CF4C0CCC9C0FDDD3459A977EFEAA6B41984 (void);
// 0x000002BA System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_5(IronSourceError)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_5_m493F560BEE792C3A5E06E559A74AFEDD7FBCEF43 (void);
// 0x000002BB System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_6()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_6_mC640ED8B3DB5FB3DAA56EAB63E774FA0FF342ED7 (void);
// 0x000002BC System.Void IronSourceEvents/<>c::<registerInterstitialEvents>b__153_13()
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_13_mD8149772CCE65E73140A99285C125B1613AC42B8 (void);
// 0x000002BD System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_0(System.String)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_0_mE4E4688C33184433DCE90917B3D243F22A383EFB (void);
// 0x000002BE System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_1(System.String)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_1_m045296B7F88EE69D8A439BF4F7F587774AD4A2F1 (void);
// 0x000002BF System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_2(System.String)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_2_m472AC489ACAD14C883925657A0F424E9D7780F89 (void);
// 0x000002C0 System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_3(System.String)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_3_m8076D52F6653BD7765485135A4A815C9754671E9 (void);
// 0x000002C1 System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_4(System.String,IronSourceError)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_4_m33635C0D043B9211B1FFF4582EDDB536C3DA1651 (void);
// 0x000002C2 System.Void IronSourceEvents/<>c::<registerInterstitialDemandOnlyEvents>b__154_5(System.String,IronSourceError)
extern void U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_5_mBBA8A6FD3AB82417B649F645FCEACB447192ABE7 (void);
// 0x000002C3 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_0()
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_0_m7B217917CEFC90024E18C4034FBF3AD4151F8D0B (void);
// 0x000002C4 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_6()
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_6_m1766D299108457E9FAB4D48E43AE7F232743D1B1 (void);
// 0x000002C5 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_1(IronSourceError)
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_1_m9D710421B7D0C0CED535D373A7E9EFD913C86007 (void);
// 0x000002C6 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_2()
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_2_m5DED9F01D4AB1FE0B0A8208B65202C4E952C30E3 (void);
// 0x000002C7 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_8()
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_8_m8B984ABE766FC1E1BB67D49A258F376DD5C8EB5E (void);
// 0x000002C8 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_3(System.Boolean)
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_3_m39E3B4DB6FB9FD1E8573E39BCBB7E46D4730C2E2 (void);
// 0x000002C9 System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_4(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_4_m7380989133E4F025EF316EB0C12ED8452ADD157E (void);
// 0x000002CA System.Void IronSourceEvents/<>c::<registerOfferwallEvents>b__155_5(IronSourceError)
extern void U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_5_mB57C982CC5AB6613E2FD8AEDC49824E6DFCAD7F7 (void);
// 0x000002CB System.Void IronSourceEvents/<>c::<registerSegmentEvents>b__156_0(System.String)
extern void U3CU3Ec_U3CregisterSegmentEventsU3Eb__156_0_m2CA1A54497EFA1A07D7BB1C9E953335147D3E4F7 (void);
// 0x000002CC System.Void IronSourceEvents/<>c::<registerImpressionDataEvents>b__157_0(IronSourceImpressionData)
extern void U3CU3Ec_U3CregisterImpressionDataEventsU3Eb__157_0_m2C40397002ACAF526EDAF4594438671D7C467EBC (void);
// 0x000002CD System.Void IronSourceEvents/<>c::<registerImpressionDataEvents>b__157_1(IronSourceImpressionData)
extern void U3CU3Ec_U3CregisterImpressionDataEventsU3Eb__157_1_mDDB8B55669C3FA2E577219435EF2ADD0097E434F (void);
// 0x000002CE System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_0(System.String)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_0_m666939FCE07FD70F37016499DD06C20C033B592B (void);
// 0x000002CF System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_1(System.String)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_1_m5F50069909A896E515A75AE0357D4A12C636184D (void);
// 0x000002D0 System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_2(System.String)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_2_mC284C8D0B62E6DD0F3F46CAA9982D59D5B52FA90 (void);
// 0x000002D1 System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_3(System.String)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_3_m81F286DCC5D7AE2420123F4126E5B23571674FCD (void);
// 0x000002D2 System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_4(System.String)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_4_m7A2A14D5CE45B23B4716251730D1E94E4A1303CE (void);
// 0x000002D3 System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_5(System.String,IronSourceError)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_5_m465E6BFB04BB10FA1FF6D3C3BF3B314D91FE3F1B (void);
// 0x000002D4 System.Void IronSourceEvents/<>c::<registerRewardedVideoDemandOnlyEvents>b__158_6(System.String,IronSourceError)
extern void U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_6_m6E54808B4C194B0CFE80A5F063DE2E775FC04CEF (void);
// 0x000002D5 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_0(IronSourcePlacement)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_0_m4741C869EF9E6E8DFA9C2F61E58D4D295D2B4F26 (void);
// 0x000002D6 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_1(IronSourceError)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_1_mEDD845C21C41D43EDE9D06AF96126ADCB79F46FA (void);
// 0x000002D7 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_2()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_2_m83FDA9435D3ED62A0899394207736907181953B3 (void);
// 0x000002D8 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_10()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_10_m7A397EE7CFBBB0B2AC782508CFDB1B16AC746A64 (void);
// 0x000002D9 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_3()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_3_mA2CF9C376F308E8E75634F77FF3BB8E487AC51F2 (void);
// 0x000002DA System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_11()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_11_mB3DAA5B0BE0750D86234960D034E57103D4040F0 (void);
// 0x000002DB System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_4()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_4_mE17FDF09605C42C78FB55B592EA472EAB8D946DC (void);
// 0x000002DC System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_12()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_12_m6E6F4C9ABE718600B51B05C5230BD2EAA25F11CB (void);
// 0x000002DD System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_5()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_5_m8CB264ADF9AF9330E15D120269E9B956495327A1 (void);
// 0x000002DE System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_13()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_13_mCC6E6DF4149B482DD2328C7244F04D76C9A6CE69 (void);
// 0x000002DF System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_6(IronSourcePlacement)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_6_m855A0D5E61791C28B851F47DB5B84C10BC251B45 (void);
// 0x000002E0 System.Void IronSourceEvents/<>c::<registerRewardedVideoEvents>b__159_7(System.Boolean)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_7_m6B288B00DF7E57824879A73CAB60C2A88B59BFCC (void);
// 0x000002E1 System.Void IronSourceEvents/<>c::<registerRewardedVideoManualEvents>b__160_0()
extern void U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_0_m10BF938092BD468EE729CA25AA0C02DE670E73A5 (void);
// 0x000002E2 System.Void IronSourceEvents/<>c::<registerRewardedVideoManualEvents>b__160_2()
extern void U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_2_mEA999A8DBCBC66E8965964A3D3D9BB551E7E8266 (void);
// 0x000002E3 System.Void IronSourceEvents/<>c::<registerRewardedVideoManualEvents>b__160_1(IronSourceError)
extern void U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_1_m9A20CEC5FE8D7284AD3BBD09C62E9D7C1363C2BE (void);
// 0x000002E4 System.Void IronSourceEvents/<>c__DisplayClass152_0::.ctor()
extern void U3CU3Ec__DisplayClass152_0__ctor_m6A355C1D8CD82BB75B41F08C3683EC12076D61AF (void);
// 0x000002E5 System.Void IronSourceEvents/<>c__DisplayClass152_0::<registerBannerEvents>b__8()
extern void U3CU3Ec__DisplayClass152_0_U3CregisterBannerEventsU3Eb__8_m5BFF953BFEBB79A771614CBDA1F752B3EE828E84 (void);
// 0x000002E6 System.Void IronSourceEvents/<>c__DisplayClass153_0::.ctor()
extern void U3CU3Ec__DisplayClass153_0__ctor_m178C1936B695056979383263963ABB868749E29E (void);
// 0x000002E7 System.Void IronSourceEvents/<>c__DisplayClass153_0::<registerInterstitialEvents>b__11()
extern void U3CU3Ec__DisplayClass153_0_U3CregisterInterstitialEventsU3Eb__11_m96D91C841D4E1DCF5A5A7F6207D22B0AD9B7DA5E (void);
// 0x000002E8 System.Void IronSourceEvents/<>c__DisplayClass153_1::.ctor()
extern void U3CU3Ec__DisplayClass153_1__ctor_mA3D2A8C0B1AF540E4A2530A068C0E0164DD1361C (void);
// 0x000002E9 System.Void IronSourceEvents/<>c__DisplayClass153_1::<registerInterstitialEvents>b__12()
extern void U3CU3Ec__DisplayClass153_1_U3CregisterInterstitialEventsU3Eb__12_mB67F9D98B59A93B859E4E6CAEC135DC5890872BA (void);
// 0x000002EA System.Void IronSourceEvents/<>c__DisplayClass154_0::.ctor()
extern void U3CU3Ec__DisplayClass154_0__ctor_mBFDAC64F658FC6FF2C5E887169C3442B52E2A9B9 (void);
// 0x000002EB System.Void IronSourceEvents/<>c__DisplayClass154_0::<registerInterstitialDemandOnlyEvents>b__6()
extern void U3CU3Ec__DisplayClass154_0_U3CregisterInterstitialDemandOnlyEventsU3Eb__6_m5C4BBCABC6BE1A014B3CAA98267CD46BF762B16F (void);
// 0x000002EC System.Void IronSourceEvents/<>c__DisplayClass154_1::.ctor()
extern void U3CU3Ec__DisplayClass154_1__ctor_m58C641D058DCFA292409B0DF75BB1E3B52A9A236 (void);
// 0x000002ED System.Void IronSourceEvents/<>c__DisplayClass154_1::<registerInterstitialDemandOnlyEvents>b__7()
extern void U3CU3Ec__DisplayClass154_1_U3CregisterInterstitialDemandOnlyEventsU3Eb__7_mEB99D3D1945B54D38A5711ACC668818EDDEDD611 (void);
// 0x000002EE System.Void IronSourceEvents/<>c__DisplayClass154_2::.ctor()
extern void U3CU3Ec__DisplayClass154_2__ctor_m4C41BB1B094EE8B29CBDB22B6F17F232B1E62297 (void);
// 0x000002EF System.Void IronSourceEvents/<>c__DisplayClass154_2::<registerInterstitialDemandOnlyEvents>b__8()
extern void U3CU3Ec__DisplayClass154_2_U3CregisterInterstitialDemandOnlyEventsU3Eb__8_m5E3D6A02ABB26419FF103E1EDD094DF656D35890 (void);
// 0x000002F0 System.Void IronSourceEvents/<>c__DisplayClass154_3::.ctor()
extern void U3CU3Ec__DisplayClass154_3__ctor_mC1CDF832459744D578CF619D34A6E3578C75BA28 (void);
// 0x000002F1 System.Void IronSourceEvents/<>c__DisplayClass154_3::<registerInterstitialDemandOnlyEvents>b__9()
extern void U3CU3Ec__DisplayClass154_3_U3CregisterInterstitialDemandOnlyEventsU3Eb__9_mC77635684E0278E1E7D7B0D8AB5A91EF296A5C02 (void);
// 0x000002F2 System.Void IronSourceEvents/<>c__DisplayClass154_4::.ctor()
extern void U3CU3Ec__DisplayClass154_4__ctor_mFF01C71E7F449A3DD9F0B4E60FB2B3B9B555956A (void);
// 0x000002F3 System.Void IronSourceEvents/<>c__DisplayClass154_4::<registerInterstitialDemandOnlyEvents>b__10()
extern void U3CU3Ec__DisplayClass154_4_U3CregisterInterstitialDemandOnlyEventsU3Eb__10_m4C0325E6B2F6E58FABC8BF6D5A95B37CA18BB523 (void);
// 0x000002F4 System.Void IronSourceEvents/<>c__DisplayClass154_5::.ctor()
extern void U3CU3Ec__DisplayClass154_5__ctor_mD4A1CF8BF2DAEE01EF648D8097776C47280458A6 (void);
// 0x000002F5 System.Void IronSourceEvents/<>c__DisplayClass154_5::<registerInterstitialDemandOnlyEvents>b__11()
extern void U3CU3Ec__DisplayClass154_5_U3CregisterInterstitialDemandOnlyEventsU3Eb__11_mEE31B6A2B4039573A33E76D6BB69FB256DB041FA (void);
// 0x000002F6 System.Void IronSourceEvents/<>c__DisplayClass155_0::.ctor()
extern void U3CU3Ec__DisplayClass155_0__ctor_mE6AEB4A8BB3C5F8C876C7D236136EE9C04EB506A (void);
// 0x000002F7 System.Void IronSourceEvents/<>c__DisplayClass155_0::<registerOfferwallEvents>b__7()
extern void U3CU3Ec__DisplayClass155_0_U3CregisterOfferwallEventsU3Eb__7_m6188260F4318F082D4C0673F223E985677845F76 (void);
// 0x000002F8 System.Void IronSourceEvents/<>c__DisplayClass155_1::.ctor()
extern void U3CU3Ec__DisplayClass155_1__ctor_mBBC3D9C2C5297CEF23EB9ACB2FD6BC9AED4B5743 (void);
// 0x000002F9 System.Void IronSourceEvents/<>c__DisplayClass155_1::<registerOfferwallEvents>b__9()
extern void U3CU3Ec__DisplayClass155_1_U3CregisterOfferwallEventsU3Eb__9_m64FBBEC8BBFB05A2225D0EF8CE1E0C90B37314EC (void);
// 0x000002FA System.Void IronSourceEvents/<>c__DisplayClass155_2::.ctor()
extern void U3CU3Ec__DisplayClass155_2__ctor_m1141FD53B4B3CEC6A55FBE50D640E5D1A1838778 (void);
// 0x000002FB System.Void IronSourceEvents/<>c__DisplayClass155_2::<registerOfferwallEvents>b__10()
extern void U3CU3Ec__DisplayClass155_2_U3CregisterOfferwallEventsU3Eb__10_m5B3FDA7B6C7C4CF1A21218A6811B61C287B776CC (void);
// 0x000002FC System.Void IronSourceEvents/<>c__DisplayClass155_3::.ctor()
extern void U3CU3Ec__DisplayClass155_3__ctor_m01E13F6D2BCFFEEFCD0D2595D55C2F3E8C2DB3D1 (void);
// 0x000002FD System.Void IronSourceEvents/<>c__DisplayClass155_3::<registerOfferwallEvents>b__11()
extern void U3CU3Ec__DisplayClass155_3_U3CregisterOfferwallEventsU3Eb__11_m0863C0242E65468B620A344BDB16EE3420BCE1FB (void);
// 0x000002FE System.Void IronSourceEvents/<>c__DisplayClass156_0::.ctor()
extern void U3CU3Ec__DisplayClass156_0__ctor_mDA5E79798A3D4CCCBEABBF9F68D7EE82B90D4259 (void);
// 0x000002FF System.Void IronSourceEvents/<>c__DisplayClass156_0::<registerSegmentEvents>b__1()
extern void U3CU3Ec__DisplayClass156_0_U3CregisterSegmentEventsU3Eb__1_m7D371671B267D0E36F44A570ECADB45B6BE13BB5 (void);
// 0x00000300 System.Void IronSourceEvents/<>c__DisplayClass157_0::.ctor()
extern void U3CU3Ec__DisplayClass157_0__ctor_m041E77209555597242A828A9EC8C908F739638FD (void);
// 0x00000301 System.Void IronSourceEvents/<>c__DisplayClass157_0::<registerImpressionDataEvents>b__2()
extern void U3CU3Ec__DisplayClass157_0_U3CregisterImpressionDataEventsU3Eb__2_m7484AED40082B8A3DF3DEAECDAD59C39C0C59512 (void);
// 0x00000302 System.Void IronSourceEvents/<>c__DisplayClass158_0::.ctor()
extern void U3CU3Ec__DisplayClass158_0__ctor_m26C1AC241009DAEC5E3C75CC8C6A3C8C9754DC9A (void);
// 0x00000303 System.Void IronSourceEvents/<>c__DisplayClass158_0::<registerRewardedVideoDemandOnlyEvents>b__7()
extern void U3CU3Ec__DisplayClass158_0_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__7_m6B01FA57A255D963CD093D9F90C3E5815C9C4537 (void);
// 0x00000304 System.Void IronSourceEvents/<>c__DisplayClass158_1::.ctor()
extern void U3CU3Ec__DisplayClass158_1__ctor_m131F3F614940EF9F42495715B55D83A73606BDDD (void);
// 0x00000305 System.Void IronSourceEvents/<>c__DisplayClass158_1::<registerRewardedVideoDemandOnlyEvents>b__8()
extern void U3CU3Ec__DisplayClass158_1_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__8_m50F8967A8986E45C89B21BEFF5F1376789C086E2 (void);
// 0x00000306 System.Void IronSourceEvents/<>c__DisplayClass158_2::.ctor()
extern void U3CU3Ec__DisplayClass158_2__ctor_m75EFAF8524D81979F34F640E7348D66808278B8B (void);
// 0x00000307 System.Void IronSourceEvents/<>c__DisplayClass158_2::<registerRewardedVideoDemandOnlyEvents>b__9()
extern void U3CU3Ec__DisplayClass158_2_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__9_m910FF67C933109973CFACE20DAE6DE17996B8CED (void);
// 0x00000308 System.Void IronSourceEvents/<>c__DisplayClass158_3::.ctor()
extern void U3CU3Ec__DisplayClass158_3__ctor_mB646E057F4FBDE93E26350FB3DE97AA8DFDD74FE (void);
// 0x00000309 System.Void IronSourceEvents/<>c__DisplayClass158_3::<registerRewardedVideoDemandOnlyEvents>b__10()
extern void U3CU3Ec__DisplayClass158_3_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__10_mA40E042247CD77C85233709DF43B2D45F58D417B (void);
// 0x0000030A System.Void IronSourceEvents/<>c__DisplayClass158_4::.ctor()
extern void U3CU3Ec__DisplayClass158_4__ctor_m0BDCA311035FB051EF91E579A50F3B1AB4F789CA (void);
// 0x0000030B System.Void IronSourceEvents/<>c__DisplayClass158_4::<registerRewardedVideoDemandOnlyEvents>b__11()
extern void U3CU3Ec__DisplayClass158_4_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__11_m29263CCD5FAA266416E898F9E6EC295839994C8E (void);
// 0x0000030C System.Void IronSourceEvents/<>c__DisplayClass158_5::.ctor()
extern void U3CU3Ec__DisplayClass158_5__ctor_mEA6E43556D224EBA8EB390CE614E6BFBB9BE7606 (void);
// 0x0000030D System.Void IronSourceEvents/<>c__DisplayClass158_5::<registerRewardedVideoDemandOnlyEvents>b__12()
extern void U3CU3Ec__DisplayClass158_5_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__12_m8972E3D27914CFCBAD53F9846D8A3A4EE906254E (void);
// 0x0000030E System.Void IronSourceEvents/<>c__DisplayClass158_6::.ctor()
extern void U3CU3Ec__DisplayClass158_6__ctor_mC31FEA67D2A752E5A9D10A0E5271AD5207859AFC (void);
// 0x0000030F System.Void IronSourceEvents/<>c__DisplayClass158_6::<registerRewardedVideoDemandOnlyEvents>b__13()
extern void U3CU3Ec__DisplayClass158_6_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__13_mBD03F556F8C83418AC8A4E525511BB6E04FAD044 (void);
// 0x00000310 System.Void IronSourceEvents/<>c__DisplayClass159_0::.ctor()
extern void U3CU3Ec__DisplayClass159_0__ctor_mC1136824EE160773F4F3AB9A79842D8C2412DDEB (void);
// 0x00000311 System.Void IronSourceEvents/<>c__DisplayClass159_0::<registerRewardedVideoEvents>b__8()
extern void U3CU3Ec__DisplayClass159_0_U3CregisterRewardedVideoEventsU3Eb__8_m329EC8C67BDF8A81586D42B3FB9A40FD618A8135 (void);
// 0x00000312 System.Void IronSourceEvents/<>c__DisplayClass159_1::.ctor()
extern void U3CU3Ec__DisplayClass159_1__ctor_m1DDC6A4752829C86CFEB377ACB3FB537D9BD7924 (void);
// 0x00000313 System.Void IronSourceEvents/<>c__DisplayClass159_1::<registerRewardedVideoEvents>b__9()
extern void U3CU3Ec__DisplayClass159_1_U3CregisterRewardedVideoEventsU3Eb__9_m5758472D6C8CD34852FE7BE3DB62BC8740C4B967 (void);
// 0x00000314 System.Void IronSourceEvents/<>c__DisplayClass159_2::.ctor()
extern void U3CU3Ec__DisplayClass159_2__ctor_mD6653B25320E22BC2D0C531E4EFE9D1D377F05D1 (void);
// 0x00000315 System.Void IronSourceEvents/<>c__DisplayClass159_2::<registerRewardedVideoEvents>b__14()
extern void U3CU3Ec__DisplayClass159_2_U3CregisterRewardedVideoEventsU3Eb__14_m4C37FE706E0D48F921F5639C55E2624F9E6B2854 (void);
// 0x00000316 System.Void IronSourceEvents/<>c__DisplayClass159_3::.ctor()
extern void U3CU3Ec__DisplayClass159_3__ctor_mD0D0275433E36531D74B8564419F8F90CAFBDDA3 (void);
// 0x00000317 System.Void IronSourceEvents/<>c__DisplayClass159_3::<registerRewardedVideoEvents>b__15()
extern void U3CU3Ec__DisplayClass159_3_U3CregisterRewardedVideoEventsU3Eb__15_m8DB25B22DD06DA3114BBF9A0F1525C8AEA536362 (void);
// 0x00000318 System.Void IronSourceEvents/<>c__DisplayClass160_0::.ctor()
extern void U3CU3Ec__DisplayClass160_0__ctor_m65FE38ECE69357A32C429247E8B003932464FCC4 (void);
// 0x00000319 System.Void IronSourceEvents/<>c__DisplayClass160_0::<registerRewardedVideoManualEvents>b__3()
extern void U3CU3Ec__DisplayClass160_0_U3CregisterRewardedVideoManualEventsU3Eb__3_m82218F491C9EB9001E9056C4FCAAD0E695BE2286 (void);
// 0x0000031A System.Void IronSourceEventsDispatcher::executeAction(System.Action)
extern void IronSourceEventsDispatcher_executeAction_m36717C759E39796E1B872179DC1F07729235ED9D (void);
// 0x0000031B System.Void IronSourceEventsDispatcher::Update()
extern void IronSourceEventsDispatcher_Update_m91047BCF6ADDB0CE7126D4B573F899938071F894 (void);
// 0x0000031C System.Void IronSourceEventsDispatcher::removeFromParent()
extern void IronSourceEventsDispatcher_removeFromParent_m0CD4DCB5A160BCCE0EC99B5AC0102FB3C974F927 (void);
// 0x0000031D System.Void IronSourceEventsDispatcher::initialize()
extern void IronSourceEventsDispatcher_initialize_m653E787D78AB21D66620C755C2256674C5C9D934 (void);
// 0x0000031E System.Boolean IronSourceEventsDispatcher::isCreated()
extern void IronSourceEventsDispatcher_isCreated_m1B163868049A565CB45A7F8D702DD9B83D9FB770 (void);
// 0x0000031F System.Void IronSourceEventsDispatcher::Awake()
extern void IronSourceEventsDispatcher_Awake_mE82E33B01D9824575A5BCC0B5B1C42F1BC97E4D2 (void);
// 0x00000320 System.Void IronSourceEventsDispatcher::OnDisable()
extern void IronSourceEventsDispatcher_OnDisable_mEFC008C61043801393F1A4650DB274E2FF359A8D (void);
// 0x00000321 System.Void IronSourceEventsDispatcher::.ctor()
extern void IronSourceEventsDispatcher__ctor_m382478BE3147BCCB476866C7930B8DF8945592DA (void);
// 0x00000322 System.Void IronSourceEventsDispatcher::.cctor()
extern void IronSourceEventsDispatcher__cctor_mB2D7F32F1E19F6EF0C342F6544323756D1D93C3A (void);
// 0x00000323 System.Void IronSourceIAgent::onApplicationPause(System.Boolean)
// 0x00000324 System.Void IronSourceIAgent::setMediationSegment(System.String)
// 0x00000325 System.String IronSourceIAgent::getAdvertiserId()
// 0x00000326 System.Void IronSourceIAgent::validateIntegration()
// 0x00000327 System.Void IronSourceIAgent::shouldTrackNetworkState(System.Boolean)
// 0x00000328 System.Boolean IronSourceIAgent::setDynamicUserId(System.String)
// 0x00000329 System.Void IronSourceIAgent::setAdaptersDebug(System.Boolean)
// 0x0000032A System.Void IronSourceIAgent::setMetaData(System.String,System.String)
// 0x0000032B System.Void IronSourceIAgent::setMetaData(System.String,System.String[])
// 0x0000032C System.Nullable`1<System.Int32> IronSourceIAgent::getConversionValue()
// 0x0000032D System.Void IronSourceIAgent::setManualLoadRewardedVideo(System.Boolean)
// 0x0000032E System.Void IronSourceIAgent::setNetworkData(System.String,System.String)
// 0x0000032F System.Void IronSourceIAgent::SetPauseGame(System.Boolean)
// 0x00000330 System.Void IronSourceIAgent::setUserId(System.String)
// 0x00000331 System.Void IronSourceIAgent::init(System.String)
// 0x00000332 System.Void IronSourceIAgent::init(System.String,System.String[])
// 0x00000333 System.Void IronSourceIAgent::initISDemandOnly(System.String,System.String[])
// 0x00000334 System.Void IronSourceIAgent::loadRewardedVideo()
// 0x00000335 System.Void IronSourceIAgent::showRewardedVideo()
// 0x00000336 System.Void IronSourceIAgent::showRewardedVideo(System.String)
// 0x00000337 System.Boolean IronSourceIAgent::isRewardedVideoAvailable()
// 0x00000338 System.Boolean IronSourceIAgent::isRewardedVideoPlacementCapped(System.String)
// 0x00000339 IronSourcePlacement IronSourceIAgent::getPlacementInfo(System.String)
// 0x0000033A System.Void IronSourceIAgent::setRewardedVideoServerParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x0000033B System.Void IronSourceIAgent::clearRewardedVideoServerParams()
// 0x0000033C System.Void IronSourceIAgent::showISDemandOnlyRewardedVideo(System.String)
// 0x0000033D System.Void IronSourceIAgent::loadISDemandOnlyRewardedVideo(System.String)
// 0x0000033E System.Boolean IronSourceIAgent::isISDemandOnlyRewardedVideoAvailable(System.String)
// 0x0000033F System.Void IronSourceIAgent::loadInterstitial()
// 0x00000340 System.Void IronSourceIAgent::showInterstitial()
// 0x00000341 System.Void IronSourceIAgent::showInterstitial(System.String)
// 0x00000342 System.Boolean IronSourceIAgent::isInterstitialReady()
// 0x00000343 System.Boolean IronSourceIAgent::isInterstitialPlacementCapped(System.String)
// 0x00000344 System.Void IronSourceIAgent::loadISDemandOnlyInterstitial(System.String)
// 0x00000345 System.Void IronSourceIAgent::showISDemandOnlyInterstitial(System.String)
// 0x00000346 System.Boolean IronSourceIAgent::isISDemandOnlyInterstitialReady(System.String)
// 0x00000347 System.Void IronSourceIAgent::showOfferwall()
// 0x00000348 System.Void IronSourceIAgent::showOfferwall(System.String)
// 0x00000349 System.Boolean IronSourceIAgent::isOfferwallAvailable()
// 0x0000034A System.Void IronSourceIAgent::getOfferwallCredits()
// 0x0000034B System.Void IronSourceIAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition)
// 0x0000034C System.Void IronSourceIAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition,System.String)
// 0x0000034D System.Void IronSourceIAgent::destroyBanner()
// 0x0000034E System.Void IronSourceIAgent::displayBanner()
// 0x0000034F System.Void IronSourceIAgent::hideBanner()
// 0x00000350 System.Boolean IronSourceIAgent::isBannerPlacementCapped(System.String)
// 0x00000351 System.Void IronSourceIAgent::setSegment(IronSourceSegment)
// 0x00000352 System.Void IronSourceIAgent::setConsent(System.Boolean)
// 0x00000353 System.Void IronSourceIAgent::loadConsentViewWithType(System.String)
// 0x00000354 System.Void IronSourceIAgent::showConsentViewWithType(System.String)
// 0x00000355 System.Void IronSourceIAgent::setAdRevenueData(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x00000356 System.String dataSource::get_MOPUB()
extern void dataSource_get_MOPUB_m47E06E361A32AE39794C7A89264FD31C5DDA4AB2 (void);
// 0x00000357 System.String IronSourceAdUnits::get_REWARDED_VIDEO()
extern void IronSourceAdUnits_get_REWARDED_VIDEO_mA0E5A6A8356BC1F538996BDE9FC07C1B51D86057 (void);
// 0x00000358 System.String IronSourceAdUnits::get_INTERSTITIAL()
extern void IronSourceAdUnits_get_INTERSTITIAL_m1515DA902042C6D1B5E243D6385F740672D7EEFA (void);
// 0x00000359 System.String IronSourceAdUnits::get_OFFERWALL()
extern void IronSourceAdUnits_get_OFFERWALL_m8687E1E2BA632B49C3B555680D1D3CC6EB02165F (void);
// 0x0000035A System.String IronSourceAdUnits::get_BANNER()
extern void IronSourceAdUnits_get_BANNER_mCB493DAD554E65E501179B44EE1BD9652ECCBDE1 (void);
// 0x0000035B System.Void IronSourceBannerSize::.ctor()
extern void IronSourceBannerSize__ctor_mFB14AA94EE7FB60C982443D564B3907AB0077F06 (void);
// 0x0000035C System.Void IronSourceBannerSize::.ctor(System.Int32,System.Int32)
extern void IronSourceBannerSize__ctor_m8CA67299323746F72DBEB070AEE991E0BD99F794 (void);
// 0x0000035D System.Void IronSourceBannerSize::.ctor(System.String)
extern void IronSourceBannerSize__ctor_m026976835E1345597A5196F6A46136E494EA3A70 (void);
// 0x0000035E System.Void IronSourceBannerSize::SetAdaptive(System.Boolean)
extern void IronSourceBannerSize_SetAdaptive_m899E84CD99F0434C7296AE50E239A116C784858C (void);
// 0x0000035F System.Boolean IronSourceBannerSize::IsAdaptiveEnabled()
extern void IronSourceBannerSize_IsAdaptiveEnabled_m003E2079AF51947BB79259443AEF81063D521E96 (void);
// 0x00000360 System.String IronSourceBannerSize::get_Description()
extern void IronSourceBannerSize_get_Description_mF8C8EA3AA5791A341E4AE11699DFC702DC110FD2 (void);
// 0x00000361 System.Int32 IronSourceBannerSize::get_Width()
extern void IronSourceBannerSize_get_Width_mCE75041D93CB0F822A6DF0CCFFD044C1640CF70A (void);
// 0x00000362 System.Int32 IronSourceBannerSize::get_Height()
extern void IronSourceBannerSize_get_Height_m3342A6235EB2B68B0F6DE323D2FCEF8D59ACCBEF (void);
// 0x00000363 System.Void IronSourceBannerSize::.cctor()
extern void IronSourceBannerSize__cctor_m8A4C2198BD3EC84B4F88014F9D2B3ADFA90AA5CD (void);
// 0x00000364 System.Void IronSourceImpressionData::.ctor(System.String)
extern void IronSourceImpressionData__ctor_m8A9655672C03A50586C8C082AE9F4AE7112684FA (void);
// 0x00000365 System.String IronSourceImpressionData::ToString()
extern void IronSourceImpressionData_ToString_m31EFDF4D06DDC7C45ACB24A41D5DD3D21C324750 (void);
// 0x00000366 System.Void IronSourceImpressionDataAndroid::add_OnImpressionSuccess(System.Action`1<IronSourceImpressionData>)
extern void IronSourceImpressionDataAndroid_add_OnImpressionSuccess_m3399492A2C1F73621FD554ABFD129839957C1492 (void);
// 0x00000367 System.Void IronSourceImpressionDataAndroid::remove_OnImpressionSuccess(System.Action`1<IronSourceImpressionData>)
extern void IronSourceImpressionDataAndroid_remove_OnImpressionSuccess_mF351CD3EC83142D2A913716B5EE2A22971F2CF33 (void);
// 0x00000368 System.Void IronSourceImpressionDataAndroid::add_OnImpressionDataReady(System.Action`1<IronSourceImpressionData>)
extern void IronSourceImpressionDataAndroid_add_OnImpressionDataReady_mE3F8F15265245FD703E708CD364D0D99F7A16414 (void);
// 0x00000369 System.Void IronSourceImpressionDataAndroid::remove_OnImpressionDataReady(System.Action`1<IronSourceImpressionData>)
extern void IronSourceImpressionDataAndroid_remove_OnImpressionDataReady_m8571DE3278B1C1719BA66F8E2045B5D3AB76A1FC (void);
// 0x0000036A System.Void IronSourceImpressionDataAndroid::.ctor()
extern void IronSourceImpressionDataAndroid__ctor_m98E1B0B8B4A116995C0909BC8004A8CA70406693 (void);
// 0x0000036B System.Void IronSourceImpressionDataAndroid::onImpressionSuccess(System.String)
extern void IronSourceImpressionDataAndroid_onImpressionSuccess_m45BF76217DDA3C564FDE8FC7C3ADFA7C928EC017 (void);
// 0x0000036C System.Void IronSourceImpressionDataAndroid::onImpressionDataReady(System.String)
extern void IronSourceImpressionDataAndroid_onImpressionDataReady_mA96415A0C5810D870EE93A38B690A0392030AEDB (void);
// 0x0000036D System.Void IronSourceImpressionDataAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m493CCF82AF29E03D97A6878263A993D2C8F64C10 (void);
// 0x0000036E System.Void IronSourceImpressionDataAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_m91C1A6267A5D37E31C27DAECC44619B84508F37E (void);
// 0x0000036F System.Void IronSourceImpressionDataAndroid/<>c::<.ctor>b__6_0(IronSourceImpressionData)
extern void U3CU3Ec_U3C_ctorU3Eb__6_0_mE0B10BB184C402ED6582235B342C754DBE6081D1 (void);
// 0x00000370 System.Void IronSourceImpressionDataAndroid/<>c::<.ctor>b__6_1(IronSourceImpressionData)
extern void U3CU3Ec_U3C_ctorU3Eb__6_1_mC38AD512107A207631DCA1B45DF8B3BD090F35BF (void);
// 0x00000371 System.Void IronSourceInitializationAndroid::add_OnSdkInitializationCompletedEvent(System.Action)
extern void IronSourceInitializationAndroid_add_OnSdkInitializationCompletedEvent_m6B7AED378A4719382B64892590CF9425D3CBB662 (void);
// 0x00000372 System.Void IronSourceInitializationAndroid::remove_OnSdkInitializationCompletedEvent(System.Action)
extern void IronSourceInitializationAndroid_remove_OnSdkInitializationCompletedEvent_mBBEEEE8898BFA0B74F5354B985AE527C57EC6E48 (void);
// 0x00000373 System.Void IronSourceInitializationAndroid::.ctor()
extern void IronSourceInitializationAndroid__ctor_m7E5E2D060E9245221B232F119F7BD4D4655AC036 (void);
// 0x00000374 System.Void IronSourceInitializationAndroid::onSdkInitializationCompleted()
extern void IronSourceInitializationAndroid_onSdkInitializationCompleted_mD833C6AF2C5F6107F6BD5B2BC966F8206A2D59FE (void);
// 0x00000375 System.Void IronSourceInitializationAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_mB464ADDA936D38152F7AAB1ACD9BC07782701AEF (void);
// 0x00000376 System.Void IronSourceInitializationAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mA1E596EC5B0D6DDF7D1D68D79E3130B41D231634 (void);
// 0x00000377 System.Void IronSourceInitializationAndroid/<>c::<.ctor>b__3_0()
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_m20C2D3C6498B4F204EA3DE5C13C82A07C5D33C1D (void);
// 0x00000378 System.Void IronSourceInitilizer::Initilize()
extern void IronSourceInitilizer_Initilize_m8EAF5195C6D5479A1DA01FD06B63492CFC349A5B (void);
// 0x00000379 System.Void IronSourceInitilizer::.ctor()
extern void IronSourceInitilizer__ctor_mA332DD98DB4565F71AB6CC7BB7026AF382BDF8D7 (void);
// 0x0000037A System.Void IronSourceInterstitialAndroid::.ctor()
extern void IronSourceInterstitialAndroid__ctor_m4A8A6104D3D43915FF89DFF53792FA9217CE10A2 (void);
// 0x0000037B System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdShowFailed_mA1EE744F54B74834E229C7D5EE78FFE263280A90 (void);
// 0x0000037C System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdShowFailed_mBBCB251CA2CDEC72CF85274781F4531E948B2AC9 (void);
// 0x0000037D System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdLoadFailed_mFD989BA0F515E2210EF58F5945939B340AE86571 (void);
// 0x0000037E System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdLoadFailed_mDF2DCDC425F759C9045A116399D80DCA01334C4E (void);
// 0x0000037F System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdReady(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdReady_m899724818295CBD5A61BDC38BC98ECF14A84E76E (void);
// 0x00000380 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdReady(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdReady_mA3971C5364E1546F36DDDE73B30537A3E9AD9C8B (void);
// 0x00000381 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdOpened(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdOpened_m09F1E193364681953777B8A29B1D9C640EB0C3CC (void);
// 0x00000382 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdOpened(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdOpened_m914ED38FA0311900B2C40DE6E300AFC57D58E408 (void);
// 0x00000383 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdClosed(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdClosed_m31BDD7D11918E5CBC22C6EECBC6BFE7F27B2BAA5 (void);
// 0x00000384 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdClosed(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdClosed_mF1878DD36CCD2539844805379133738A2918C93C (void);
// 0x00000385 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdShowSucceeded(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdShowSucceeded_mAFD13301C4C87792F073825B290D5FB19A242C3D (void);
// 0x00000386 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdShowSucceeded(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdShowSucceeded_m6BB20F1FB7BF4BFB5C94BB97AB6F7E55E6D60F50 (void);
// 0x00000387 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdClicked(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdClicked_mD1B0380BFFD9720765402615820283D2033A4404 (void);
// 0x00000388 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdClicked(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdClicked_m0A931020B0897D361AA70EDD5107D870684F5B31 (void);
// 0x00000389 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdRewarded(System.Action)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdRewarded_m1354BD1EEF257E119EE3793C092BACCB644C30A2 (void);
// 0x0000038A System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdRewarded(System.Action)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdRewarded_m55BC59327C46B26A7283B97D35C65B535943F50C (void);
// 0x0000038B System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdReadyDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdReadyDemandOnly_mD620FC42994EC546E9D3F16811BBFE883BF4726E (void);
// 0x0000038C System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdReadyDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdReadyDemandOnly_m9DBC04A573A1189B634D8F68744AC799F6A9AABE (void);
// 0x0000038D System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdOpenedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdOpenedDemandOnly_mE296BE8FCDD52C7715FF10B4EE3A4E5149B084FE (void);
// 0x0000038E System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdOpenedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdOpenedDemandOnly_mDEBAFB541845193AA7C80958B7E6CD456421E859 (void);
// 0x0000038F System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdClosedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdClosedDemandOnly_m05A9B1328274BB4707B595CD2CBECCB2835FB7CC (void);
// 0x00000390 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdClosedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdClosedDemandOnly_m34630D2B0D73FC7C966226A19277EC4453D7488F (void);
// 0x00000391 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdLoadFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdLoadFailedDemandOnly_mB3AA4E9F6D1259AAA4C7FDCF0A8E5FA9C730897D (void);
// 0x00000392 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdLoadFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdLoadFailedDemandOnly_m7FA513D9D3DF827BB085790C7E329C43DB326BA3 (void);
// 0x00000393 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdClickedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdClickedDemandOnly_mC38D598385FF6B83192A28AD6C3804DCC807E256 (void);
// 0x00000394 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdClickedDemandOnly(System.Action`1<System.String>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdClickedDemandOnly_mBCB300371D3CD601FCA7F98BA1F4667362B57A38 (void);
// 0x00000395 System.Void IronSourceInterstitialAndroid::add_OnInterstitialAdShowFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
extern void IronSourceInterstitialAndroid_add_OnInterstitialAdShowFailedDemandOnly_m44F8EF5A1721E4522BC04D5A8A4898BD98EA0E8D (void);
// 0x00000396 System.Void IronSourceInterstitialAndroid::remove_OnInterstitialAdShowFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
extern void IronSourceInterstitialAndroid_remove_OnInterstitialAdShowFailedDemandOnly_mD035C9B5C734113127E53CE4C91A4005D48B3B90 (void);
// 0x00000397 System.Void IronSourceInterstitialAndroid::onInterstitialAdShowFailed(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdShowFailed_m4FEBBE144213CCCF22211767197B202B45EBCB9D (void);
// 0x00000398 System.Void IronSourceInterstitialAndroid::onInterstitialAdReady()
extern void IronSourceInterstitialAndroid_onInterstitialAdReady_mCA02398737E86A45B3DEECE90A240269C76F3AB3 (void);
// 0x00000399 System.Void IronSourceInterstitialAndroid::onInterstitialAdOpened()
extern void IronSourceInterstitialAndroid_onInterstitialAdOpened_m19CF785A56B24A721C9AD623AEAF8B00892F6E52 (void);
// 0x0000039A System.Void IronSourceInterstitialAndroid::onInterstitialAdClosed()
extern void IronSourceInterstitialAndroid_onInterstitialAdClosed_m023BC6888D68F446FF4681721E5F8992AEFCCC96 (void);
// 0x0000039B System.Void IronSourceInterstitialAndroid::onInterstitialAdShowSucceeded()
extern void IronSourceInterstitialAndroid_onInterstitialAdShowSucceeded_m7521B6EF0B4A49A7D1DDD2F1C9871D942BCDC5DB (void);
// 0x0000039C System.Void IronSourceInterstitialAndroid::onInterstitialAdClicked()
extern void IronSourceInterstitialAndroid_onInterstitialAdClicked_m6FB4B00694B26981EC5FBF2E527FA1395E6D0A98 (void);
// 0x0000039D System.Void IronSourceInterstitialAndroid::onInterstitialAdLoadFailed(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdLoadFailed_mDD1BFEAC16F33540918599F85ED02FDE75F42988 (void);
// 0x0000039E System.Void IronSourceInterstitialAndroid::onInterstitialAdRewarded()
extern void IronSourceInterstitialAndroid_onInterstitialAdRewarded_m369797B2CAE1DE627F4D8AA6E71C5A9C460436A6 (void);
// 0x0000039F System.Void IronSourceInterstitialAndroid::onInterstitialAdClickedDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdClickedDemandOnly_m0481955C954EF1BA9490B59CB0D100D80D7CA246 (void);
// 0x000003A0 System.Void IronSourceInterstitialAndroid::onInterstitialAdClosedDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdClosedDemandOnly_m5B76588151E01C87F8DCB366C6C862809C84153E (void);
// 0x000003A1 System.Void IronSourceInterstitialAndroid::onInterstitialAdOpenedDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdOpenedDemandOnly_m31A2AF5B3705CEE1DB7CBB0925B52829E576C995 (void);
// 0x000003A2 System.Void IronSourceInterstitialAndroid::onInterstitialAdReadyDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdReadyDemandOnly_m53A3CC1C14475BA1FB5CE9926A75AE75488E3255 (void);
// 0x000003A3 System.Void IronSourceInterstitialAndroid::onInterstitialAdLoadFailedDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdLoadFailedDemandOnly_mA40C8F70C6E54CA7E23FA1E8F38D2B05947E8FF1 (void);
// 0x000003A4 System.Void IronSourceInterstitialAndroid::onInterstitialAdShowFailedDemandOnly(System.String)
extern void IronSourceInterstitialAndroid_onInterstitialAdShowFailedDemandOnly_mD57969AFA8D8E169E0014DCC59E513022B59B057 (void);
// 0x000003A5 System.Void IronSourceInterstitialAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_mE77A583537674ABC48BA7DD4E3719F71816F0D47 (void);
// 0x000003A6 System.Void IronSourceInterstitialAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_m562D7BF420B3ADEDDCEADCA73782A047D7AE6D82 (void);
// 0x000003A7 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_0(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_m0DB812B92882D29B0F1A6D859C5F4DB953A707EB (void);
// 0x000003A8 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_1(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_m77B9D57CBCFE6298E392DF70A858A9A0532455EE (void);
// 0x000003A9 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_2()
extern void U3CU3Ec_U3C_ctorU3Eb__0_2_m2EC460FB7659DF110A84D717DAABD7F632D5BA65 (void);
// 0x000003AA System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_3()
extern void U3CU3Ec_U3C_ctorU3Eb__0_3_m263A1286C46BB79E20FCD599530DFA62C388E6FF (void);
// 0x000003AB System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_4()
extern void U3CU3Ec_U3C_ctorU3Eb__0_4_mA45436BA889438FD6495457EB3A03DEEDE8D63BA (void);
// 0x000003AC System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_5()
extern void U3CU3Ec_U3C_ctorU3Eb__0_5_m6AE31EFCE6A6672E2B1850D64688FDBA746C5E55 (void);
// 0x000003AD System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_6()
extern void U3CU3Ec_U3C_ctorU3Eb__0_6_mC1EEC8BAECB2C7255A5920EF9E546C6CFCA91E0C (void);
// 0x000003AE System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_7()
extern void U3CU3Ec_U3C_ctorU3Eb__0_7_m07C4EA13548ADA97224CB1057607BEDAC5DCCCBD (void);
// 0x000003AF System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_8(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_8_m1655F6F1703AF3FBA92C83D54EE8D0DA500CD90E (void);
// 0x000003B0 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_9(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_9_m4485296FF3508B90B43BB66F64EA24600CA36686 (void);
// 0x000003B1 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_10(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_10_mB9DBE8F5A3E9DD1BC1B0FCB0778102A996353B8C (void);
// 0x000003B2 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_11(System.String,IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_11_m99065CDEA2707CDB445B5F84FA10EA139E60F0A2 (void);
// 0x000003B3 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_12(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_12_m3830E0FCD4CFEB68C2EA06F0CCF0A10994DF7E08 (void);
// 0x000003B4 System.Void IronSourceInterstitialAndroid/<>c::<.ctor>b__0_13(System.String,IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_13_m21A7BD3BC0FAB79B29F925D6C2FEF6866218E5CF (void);
// 0x000003B5 System.Void IronSourceInterstitialEvents::add_onAdReadyEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdReadyEvent_mAC86194F45F7E7AB77073640E4ECEFC9236E93E9 (void);
// 0x000003B6 System.Void IronSourceInterstitialEvents::remove_onAdReadyEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdReadyEvent_mF838F0C283E45E752259F77D6962694CCD3D80CC (void);
// 0x000003B7 System.Void IronSourceInterstitialEvents::add_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialEvents_add_onAdLoadFailedEvent_m8A64B3148241AB6CE98D50C21B7E6C91DD8D7FBC (void);
// 0x000003B8 System.Void IronSourceInterstitialEvents::remove_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialEvents_remove_onAdLoadFailedEvent_m89229DFEC32D77A5C697B48435D9268D4275C98C (void);
// 0x000003B9 System.Void IronSourceInterstitialEvents::add_onAdOpenedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdOpenedEvent_m29EBD9E72926592F221ED242B476AF1B8A55597D (void);
// 0x000003BA System.Void IronSourceInterstitialEvents::remove_onAdOpenedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdOpenedEvent_m0C5E81F12E50E7645ADC1B68372305E1F7D7C6F3 (void);
// 0x000003BB System.Void IronSourceInterstitialEvents::add_onAdClosedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdClosedEvent_mF3A5888FC5A45C7FA1CDB42F0F4DD7F6530D31FD (void);
// 0x000003BC System.Void IronSourceInterstitialEvents::remove_onAdClosedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdClosedEvent_m4808088E893C9B690B0B454704A97B6334CE80FB (void);
// 0x000003BD System.Void IronSourceInterstitialEvents::add_onAdShowSucceededEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdShowSucceededEvent_mB1A08240F65DBB4B50267C968D5CA5CC3E0B3490 (void);
// 0x000003BE System.Void IronSourceInterstitialEvents::remove_onAdShowSucceededEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdShowSucceededEvent_mE053C5C2EC888A96992E68538DE2ECC7EB1E9048 (void);
// 0x000003BF System.Void IronSourceInterstitialEvents::add_onAdShowFailedEvent(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdShowFailedEvent_mCB00D02E0BB50F0CDF7C3CEADD09F200593F9C04 (void);
// 0x000003C0 System.Void IronSourceInterstitialEvents::remove_onAdShowFailedEvent(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdShowFailedEvent_mD40AFC63586F8D09CC4E3827AA8856A19B5899DD (void);
// 0x000003C1 System.Void IronSourceInterstitialEvents::add_onAdClickedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_add_onAdClickedEvent_mD40F90BDA270CEBCD8D843AFBEBA45DB1CAA22F6 (void);
// 0x000003C2 System.Void IronSourceInterstitialEvents::remove_onAdClickedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialEvents_remove_onAdClickedEvent_m3FC1E797ABDFA8E1A86BF73F6CF680164152B5C8 (void);
// 0x000003C3 System.Void IronSourceInterstitialEvents::Awake()
extern void IronSourceInterstitialEvents_Awake_m644207F3532D065D87013515DDAED03839CECF40 (void);
// 0x000003C4 System.Void IronSourceInterstitialEvents::registerInterstitialEvents()
extern void IronSourceInterstitialEvents_registerInterstitialEvents_mD3CB3019CA61E1AE823913A1516860C24089EF8F (void);
// 0x000003C5 IronSourceError IronSourceInterstitialEvents::getErrorFromErrorObject(System.Object)
extern void IronSourceInterstitialEvents_getErrorFromErrorObject_mC6D1790E1EBAF37257227AD55E926BB9B3CD4511 (void);
// 0x000003C6 IronSourcePlacement IronSourceInterstitialEvents::getPlacementFromObject(System.Object)
extern void IronSourceInterstitialEvents_getPlacementFromObject_mDE0ECD343EA317A05CC7562FFB951A7CBB5400D9 (void);
// 0x000003C7 System.Void IronSourceInterstitialEvents::.ctor()
extern void IronSourceInterstitialEvents__ctor_m634331C3B79329F8EA8BE17C65A4655D6714C3A5 (void);
// 0x000003C8 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m1C094D3FA404FDA6FDDA4BDD71BE423C943381EB (void);
// 0x000003C9 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_0::<registerInterstitialEvents>b__7()
extern void U3CU3Ec__DisplayClass23_0_U3CregisterInterstitialEventsU3Eb__7_mD80D20F287879F11C21E62CEF90ECAAF19E220F0 (void);
// 0x000003CA System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_1::.ctor()
extern void U3CU3Ec__DisplayClass23_1__ctor_m4DA0B4DF18C51B3603DB2038209EBE09EED748DE (void);
// 0x000003CB System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_1::<registerInterstitialEvents>b__8()
extern void U3CU3Ec__DisplayClass23_1_U3CregisterInterstitialEventsU3Eb__8_m243C8062418FB8C074E524C9FA35808CF59C4D25 (void);
// 0x000003CC System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_2::.ctor()
extern void U3CU3Ec__DisplayClass23_2__ctor_mE3454DA88A3CAF22DBC823D105AB00B8DC14B2B0 (void);
// 0x000003CD System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_2::<registerInterstitialEvents>b__9()
extern void U3CU3Ec__DisplayClass23_2_U3CregisterInterstitialEventsU3Eb__9_m37F77F5433F6A17B2A9D325FF363D408671A8918 (void);
// 0x000003CE System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_3::.ctor()
extern void U3CU3Ec__DisplayClass23_3__ctor_m90009FD1DCB3587D3B764148FCB1827378A053D6 (void);
// 0x000003CF System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_3::<registerInterstitialEvents>b__10()
extern void U3CU3Ec__DisplayClass23_3_U3CregisterInterstitialEventsU3Eb__10_mD3C53BD3DB291581BC0269551EEF15968E8FA54B (void);
// 0x000003D0 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_4::.ctor()
extern void U3CU3Ec__DisplayClass23_4__ctor_m30F241858F676299B827419E16680BFE893C609E (void);
// 0x000003D1 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_4::<registerInterstitialEvents>b__11()
extern void U3CU3Ec__DisplayClass23_4_U3CregisterInterstitialEventsU3Eb__11_mAA7CF3684A3BADA16C7FC974C1ECEF19048D3FAC (void);
// 0x000003D2 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_5::.ctor()
extern void U3CU3Ec__DisplayClass23_5__ctor_mDC4333569D60555DC64584F4ED1BEF96692AD059 (void);
// 0x000003D3 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_5::<registerInterstitialEvents>b__12()
extern void U3CU3Ec__DisplayClass23_5_U3CregisterInterstitialEventsU3Eb__12_m936336AAC063D9768BA844593CBC9D0684B0CF00 (void);
// 0x000003D4 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_6::.ctor()
extern void U3CU3Ec__DisplayClass23_6__ctor_m54112CAB4E3D1D6DB2DAF54CB7CE2370B03629E3 (void);
// 0x000003D5 System.Void IronSourceInterstitialEvents/<>c__DisplayClass23_6::<registerInterstitialEvents>b__13()
extern void U3CU3Ec__DisplayClass23_6_U3CregisterInterstitialEventsU3Eb__13_m191F35EE6421B347FA437A2D980DA7E3EC82748C (void);
// 0x000003D6 System.Void IronSourceInterstitialEvents/<>c::.cctor()
extern void U3CU3Ec__cctor_m4046FA9D5FDB2C18E21C3D2BCCC031FF50D2D39A (void);
// 0x000003D7 System.Void IronSourceInterstitialEvents/<>c::.ctor()
extern void U3CU3Ec__ctor_m5876A49F87AECDD5F8EED6FACA7BBAC6C5FFC90F (void);
// 0x000003D8 System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_0(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_0_mC3D86AA78AE4D55B10AA3A61F707594B5863A727 (void);
// 0x000003D9 System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_1(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_1_m0FFE23191644F6B8CE33B0882FCD0EBCD21BC2F5 (void);
// 0x000003DA System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_2(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_2_m6EA8C05848C332C070E681AD129D2ECFCB9F9BEF (void);
// 0x000003DB System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_3(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_3_m4794066A6DA6ED97190E543B0138178BF41C8546 (void);
// 0x000003DC System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_4(IronSourceError)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_4_mE8D3266BEF8BD9C3BDDA4BC9CC09E04274C17081 (void);
// 0x000003DD System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_5(IronSourceError,IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_5_m3C4876BB26B25ADADCEFCDC904660CFE5FA857EE (void);
// 0x000003DE System.Void IronSourceInterstitialEvents/<>c::<registerInterstitialEvents>b__23_6(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_6_m96B79E7ECAF568091B2E7C46EB9566866C11689B (void);
// 0x000003DF System.Void IronSourceInterstitialLevelPlayAndroid::.ctor()
extern void IronSourceInterstitialLevelPlayAndroid__ctor_m4D66678392C50640BED08C8BFFF35B3F23D105CD (void);
// 0x000003E0 System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdShowFailed_mBBB3198C565F27B74F1A62EC95C0EE4738EE808D (void);
// 0x000003E1 System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdShowFailed_m0A33710EF3DF87B2079258DDB6D119BEDF51AEAD (void);
// 0x000003E2 System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdLoadFailed_m8A7FE8F651E2491B2B6581F34174B1CDD52015B5 (void);
// 0x000003E3 System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdLoadFailed_m7F843E96A99A0FC8178AC4C867C0CBC9967874BF (void);
// 0x000003E4 System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdReady(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdReady_mACC99F83BC597A3B12E3DFC27262380819B9DE77 (void);
// 0x000003E5 System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdReady(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdReady_mC6990FF33F1862E78C888202F4644E6E2795F1E7 (void);
// 0x000003E6 System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdOpened(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdOpened_m8BF1404E9A683AA999AC8B921F8EFAA3B317988B (void);
// 0x000003E7 System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdOpened(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdOpened_mC0747F7C2FE6CAE38FAD822E1565A6F31813474F (void);
// 0x000003E8 System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdClosed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdClosed_m7F7CE9DAF62DB15F1A40D934D66040368B92BBC3 (void);
// 0x000003E9 System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdClosed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdClosed_m1150737A24A7D0CD91747EB46E6FF375666232CF (void);
// 0x000003EA System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdShowSucceeded(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdShowSucceeded_mDB7A4AAB128DE1C8178AF9F5EAEDB828640257C2 (void);
// 0x000003EB System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdShowSucceeded(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdShowSucceeded_m228612E1AC3E37A894DF232E7D012FC96C677F85 (void);
// 0x000003EC System.Void IronSourceInterstitialLevelPlayAndroid::add_OnAdClicked(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_add_OnAdClicked_mA25B563E7FEC2EEDC6052C8B48911A3CB973C0A9 (void);
// 0x000003ED System.Void IronSourceInterstitialLevelPlayAndroid::remove_OnAdClicked(System.Action`1<IronSourceAdInfo>)
extern void IronSourceInterstitialLevelPlayAndroid_remove_OnAdClicked_m9446764F768D848221B8CECBD1DFA41C743CEB98 (void);
// 0x000003EE System.Void IronSourceInterstitialLevelPlayAndroid::onAdShowFailed(System.String,System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdShowFailed_m5A0082D189A9254B5E26496D1E3B46C99C1ECEC9 (void);
// 0x000003EF System.Void IronSourceInterstitialLevelPlayAndroid::onAdReady(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdReady_m8BAB3976328D11FE80EA7FE4625BAA0274222746 (void);
// 0x000003F0 System.Void IronSourceInterstitialLevelPlayAndroid::onAdOpened(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdOpened_mBCC32BDAE741AE7CCD2804E4A58E31EBF638B567 (void);
// 0x000003F1 System.Void IronSourceInterstitialLevelPlayAndroid::onAdClosed(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdClosed_mF2DCF3348B5EE6A8D32A186465D080A34D3F7BEC (void);
// 0x000003F2 System.Void IronSourceInterstitialLevelPlayAndroid::onAdShowSucceeded(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdShowSucceeded_m7812ED2F0A7C0B023E03CD57BD51B2E121FD1DB3 (void);
// 0x000003F3 System.Void IronSourceInterstitialLevelPlayAndroid::onAdClicked(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdClicked_m0A78B8FF2AC090802CDB6421CD8B48A50FEC1EE3 (void);
// 0x000003F4 System.Void IronSourceInterstitialLevelPlayAndroid::onAdLoadFailed(System.String)
extern void IronSourceInterstitialLevelPlayAndroid_onAdLoadFailed_mE143A820508F7C0E3DD0D504558752EBD6C4C09B (void);
// 0x000003F5 System.Void IronSourceInterstitialLevelPlayAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_mBE48F3A7C83B413BBBBE59E41CC8CF9B67DB394D (void);
// 0x000003F6 System.Void IronSourceInterstitialLevelPlayAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mBD63DC0F65F506CD1E7F4D41A6965FDB1D089B3D (void);
// 0x000003F7 System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_0(IronSourceError,IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_m8C830AEA0A269E8A43E03F9488BF0BA0389D691A (void);
// 0x000003F8 System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_1(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_mBEC849AA094EBF0948A1FF1A993E4C6508C1176A (void);
// 0x000003F9 System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_2(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_2_m98B1AF9A48ACB9E505B83341A7E8A6B00A03BBB1 (void);
// 0x000003FA System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_3(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_3_mAAE9356002E1DA43080A75929AC8B18CBDA19DFA (void);
// 0x000003FB System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_4(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_4_m7A402326D6784F6997381821D720B3B13BFE6250 (void);
// 0x000003FC System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_5(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_5_m4D9C171DF4E7E19675CF282ADEE1E3DC7C29FCDC (void);
// 0x000003FD System.Void IronSourceInterstitialLevelPlayAndroid/<>c::<.ctor>b__0_6(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_6_m64684466919D66890E4D04A2F107D03934E78E93 (void);
// 0x000003FE System.Void IronSourceMediationSettings::.ctor()
extern void IronSourceMediationSettings__ctor_mF5C76171B0F547C34638A7DF59ACFC1F2029D09F (void);
// 0x000003FF System.Void IronSourceMediationSettings::.cctor()
extern void IronSourceMediationSettings__cctor_m302FAD38ED27A70D3CED684383F050D1F5D0F404 (void);
// 0x00000400 System.Void IronSourceOfferwallAndroid::.ctor()
extern void IronSourceOfferwallAndroid__ctor_m72002BC554C2C5C28766EE83742D888DBF2771DC (void);
// 0x00000401 System.Void IronSourceOfferwallAndroid::add_OnOfferwallShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceOfferwallAndroid_add_OnOfferwallShowFailed_m03FE762044E07CE83592A205CB6897424E9DA2D0 (void);
// 0x00000402 System.Void IronSourceOfferwallAndroid::remove_OnOfferwallShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceOfferwallAndroid_remove_OnOfferwallShowFailed_m26C85AAA7B279E66BDFC6D70124F0C01D800E41A (void);
// 0x00000403 System.Void IronSourceOfferwallAndroid::add_OnOfferwallOpened(System.Action)
extern void IronSourceOfferwallAndroid_add_OnOfferwallOpened_m9E0870D63167DD18C1CD64F7CFC6936F646CEEC4 (void);
// 0x00000404 System.Void IronSourceOfferwallAndroid::remove_OnOfferwallOpened(System.Action)
extern void IronSourceOfferwallAndroid_remove_OnOfferwallOpened_m5B6B2CCACC2A4CD8CD43F1498C7EEB1C7F892563 (void);
// 0x00000405 System.Void IronSourceOfferwallAndroid::add_OnOfferwallClosed(System.Action)
extern void IronSourceOfferwallAndroid_add_OnOfferwallClosed_m5F4E1278ACAA1F6DFE97A05840AA9EB8B191AD9A (void);
// 0x00000406 System.Void IronSourceOfferwallAndroid::remove_OnOfferwallClosed(System.Action)
extern void IronSourceOfferwallAndroid_remove_OnOfferwallClosed_m07BF664AE49BD7D7D89BA7F1AB259A59549BDCD3 (void);
// 0x00000407 System.Void IronSourceOfferwallAndroid::add_OnGetOfferwallCreditsFailed(System.Action`1<IronSourceError>)
extern void IronSourceOfferwallAndroid_add_OnGetOfferwallCreditsFailed_m757A4494C6F763B05B18FF58A9D36231C18B62D2 (void);
// 0x00000408 System.Void IronSourceOfferwallAndroid::remove_OnGetOfferwallCreditsFailed(System.Action`1<IronSourceError>)
extern void IronSourceOfferwallAndroid_remove_OnGetOfferwallCreditsFailed_m054230D0DF27A4D5976C138AF5698D62B3441A07 (void);
// 0x00000409 System.Void IronSourceOfferwallAndroid::add_OnOfferwallAdCredited(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void IronSourceOfferwallAndroid_add_OnOfferwallAdCredited_m01FE6E0DE70271C8567238C30675981CCE338DB5 (void);
// 0x0000040A System.Void IronSourceOfferwallAndroid::remove_OnOfferwallAdCredited(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void IronSourceOfferwallAndroid_remove_OnOfferwallAdCredited_m9FD3EE48155C30AF9FBE37D557712FCC63A333E1 (void);
// 0x0000040B System.Void IronSourceOfferwallAndroid::add_OnOfferwallAvailable(System.Action`1<System.Boolean>)
extern void IronSourceOfferwallAndroid_add_OnOfferwallAvailable_m38BFF08E770E756393CBEE765DF45CC441B0CEFB (void);
// 0x0000040C System.Void IronSourceOfferwallAndroid::remove_OnOfferwallAvailable(System.Action`1<System.Boolean>)
extern void IronSourceOfferwallAndroid_remove_OnOfferwallAvailable_mDAC0CA382F4102EFC9B2E6C21E74F81271C416A1 (void);
// 0x0000040D System.Void IronSourceOfferwallAndroid::onOfferwallOpened()
extern void IronSourceOfferwallAndroid_onOfferwallOpened_m2757B5FB4D9B7B8CB4C80DE47CFBE30232BA395B (void);
// 0x0000040E System.Void IronSourceOfferwallAndroid::onOfferwallShowFailed(System.String)
extern void IronSourceOfferwallAndroid_onOfferwallShowFailed_mAFA3D6A84D98E1EFACBE1DE7357D268FDBB13B11 (void);
// 0x0000040F System.Void IronSourceOfferwallAndroid::onOfferwallClosed()
extern void IronSourceOfferwallAndroid_onOfferwallClosed_m8EF0D9A8DBE7DAAA90CB63B375581C9EBF07B70F (void);
// 0x00000410 System.Void IronSourceOfferwallAndroid::onGetOfferwallCreditsFailed(System.String)
extern void IronSourceOfferwallAndroid_onGetOfferwallCreditsFailed_m303296D3330D432E9FAA5C36DC3A06ED7229C659 (void);
// 0x00000411 System.Void IronSourceOfferwallAndroid::onOfferwallAdCredited(System.String)
extern void IronSourceOfferwallAndroid_onOfferwallAdCredited_mA21486F1EF4320D352E6E429BD74EF7317DC6237 (void);
// 0x00000412 System.Void IronSourceOfferwallAndroid::onOfferwallAvailable(System.String)
extern void IronSourceOfferwallAndroid_onOfferwallAvailable_m8AF58C75266F108E22E312EC35AFD1464FCAE151 (void);
// 0x00000413 System.Void IronSourceOfferwallAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m85680F1CD47C359155B0352E663DD296C789CD0A (void);
// 0x00000414 System.Void IronSourceOfferwallAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_m0436F19FF1E0A004028A2D0E13972E45E1357C8D (void);
// 0x00000415 System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_0(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_mD962A3BB737307810FB1D3B15E0FCF912D1BA7AB (void);
// 0x00000416 System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_1()
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_mB74A3EFC469676CE25F5FAD4462E01F1CB1CDC8F (void);
// 0x00000417 System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_2()
extern void U3CU3Ec_U3C_ctorU3Eb__0_2_m992C4EC04B54A3ADD596905AC05EE3F0154055D4 (void);
// 0x00000418 System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_3(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_3_mC56346F18033923517A20FE60F324059C956D4FF (void);
// 0x00000419 System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_4(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void U3CU3Ec_U3C_ctorU3Eb__0_4_mE4057E933D5944B753E25B632287D4C430EA214D (void);
// 0x0000041A System.Void IronSourceOfferwallAndroid/<>c::<.ctor>b__0_5(System.Boolean)
extern void U3CU3Ec_U3C_ctorU3Eb__0_5_m9451D49654DBD15FAE064BFA1C3BF05E747CCB3B (void);
// 0x0000041B System.Void IronSourcePlacement::.ctor(System.String,System.String,System.Int32)
extern void IronSourcePlacement__ctor_m3AC84E420A2753B1A248D7EA4A8C1FC290E281BC (void);
// 0x0000041C System.String IronSourcePlacement::getRewardName()
extern void IronSourcePlacement_getRewardName_m49DECEF7708B57ED1C5C402479DD8E1F21D6CB18 (void);
// 0x0000041D System.Int32 IronSourcePlacement::getRewardAmount()
extern void IronSourcePlacement_getRewardAmount_m872D997472B60D9E782F5D30DE02845109BBE909 (void);
// 0x0000041E System.String IronSourcePlacement::getPlacementName()
extern void IronSourcePlacement_getPlacementName_m747D38E22E987B023508667E0026B1DD18D88C2B (void);
// 0x0000041F System.String IronSourcePlacement::ToString()
extern void IronSourcePlacement_ToString_m3878087E0453B7DEEC2B5F76DFD5AEB6020B879D (void);
// 0x00000420 System.Void IronSourceRewardedVideoAndroid::.ctor()
extern void IronSourceRewardedVideoAndroid__ctor_m4C13CF16E6F81842FE78BCEFE43FFB6D68EC186D (void);
// 0x00000421 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdShowFailed_m5D69C3B65BD335DEF78767725B9D81EA184083BB (void);
// 0x00000422 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdShowFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdShowFailed_m91F75B5802AA26AA22C752C7F58E549C2DCF0EFA (void);
// 0x00000423 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdOpened(System.Action)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdOpened_m4F40C18BA6C282987F166A10C03B3063A2446524 (void);
// 0x00000424 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdOpened(System.Action)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdOpened_mA12D2CC45278C72C2D350EFD491D37B5DF1CB464 (void);
// 0x00000425 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdClosed(System.Action)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClosed_m48D28C5ED11FD9B750FF908F1D67B58CC16C4E77 (void);
// 0x00000426 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdClosed(System.Action)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClosed_m27E7469CF51963AE63022673167E2531ACB90EF0 (void);
// 0x00000427 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdStarted(System.Action)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdStarted_m7F3E073DFCE8F5F832C2E3D992D7DBAF34296AAB (void);
// 0x00000428 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdStarted(System.Action)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdStarted_mB159D577193C9233455288742BA185D0CAD0B972 (void);
// 0x00000429 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdEnded(System.Action)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdEnded_m60682675F6243752A50E7CEDA7143BAA16A046F4 (void);
// 0x0000042A System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdEnded(System.Action)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdEnded_m3B966806119BD8235DC5AEDA5ABD2E0C7DCFBE7F (void);
// 0x0000042B System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdRewarded(System.Action`1<IronSourcePlacement>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdRewarded_m4DA50AE560E6C89F89764F7D59324C4FC5CF6408 (void);
// 0x0000042C System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdRewarded(System.Action`1<IronSourcePlacement>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdRewarded_mE47363C6EB9F36AE9856A18355A35931212FF204 (void);
// 0x0000042D System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdClicked(System.Action`1<IronSourcePlacement>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClicked_m5D10549ABBE428C87730A85DB16C5EC7CE35801E (void);
// 0x0000042E System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdClicked(System.Action`1<IronSourcePlacement>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClicked_m16649E5F1187B14B858166FAA7A30895E8F43A6A (void);
// 0x0000042F System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAvailabilityChanged(System.Action`1<System.Boolean>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAvailabilityChanged_mDFE3E47B129F04A20D80E91FE7175E46B16B77EF (void);
// 0x00000430 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAvailabilityChanged(System.Action`1<System.Boolean>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAvailabilityChanged_m809C634F80D18BEFABE8BFB88A17911ECFABD96F (void);
// 0x00000431 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdOpenedDemandOnlyEvent_m2EEFC641A11712D78543B5EE908B77712A88BDA9 (void);
// 0x00000432 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdOpenedDemandOnlyEvent_m54ED8CD47EFC578C82E39CF807BD59DDAE0C2635 (void);
// 0x00000433 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClosedDemandOnlyEvent_mD972879F661DD6B09E1790DC3AAB10E1D222F6CA (void);
// 0x00000434 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClosedDemandOnlyEvent_m1BCF252206F4B9C7E78B4832B31705EFFA642327 (void);
// 0x00000435 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdLoadedDemandOnlyEvent_m4BE710C4EDC6417AF0DD47C5D4E093BD6D24DA0A (void);
// 0x00000436 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdLoadedDemandOnlyEvent_mCDEC321183A8BA15F35A5A3529B3E6F56161B058 (void);
// 0x00000437 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdRewardedDemandOnlyEvent_mFF39EB1EF5B1FEE23ECA34DE219F0E5A90EFE872 (void);
// 0x00000438 System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdRewardedDemandOnlyEvent_mC0E052E691619DCAFBDDDD44372FAF85C264995A (void);
// 0x00000439 System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdShowFailedDemandOnlyEvent_mA392AAFEEC835D33C43D81ACE32C33AE037ED450 (void);
// 0x0000043A System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdShowFailedDemandOnlyEvent_mF67C9C1C104F66E13E62348483E9486361BB517A (void);
// 0x0000043B System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClickedDemandOnlyEvent_mF79F1C444AC8E855DD6A809EA44D854E0BE7309E (void);
// 0x0000043C System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClickedDemandOnlyEvent_m24276C68D0FD3B77B3A64582BAA4049C433B2886 (void);
// 0x0000043D System.Void IronSourceRewardedVideoAndroid::add_OnRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdLoadFailedDemandOnlyEvent_mB1B7D157BC9A436EAFFE0B2A26EB4557F1FAB7AE (void);
// 0x0000043E System.Void IronSourceRewardedVideoAndroid::remove_OnRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
extern void IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdLoadFailedDemandOnlyEvent_m735D981F772166180F5B000D2118B7E964F32460 (void);
// 0x0000043F System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdShowFailed(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdShowFailed_m5DC84567DC1D3E11F9B9EC2B6A79ED9FB16FC9F9 (void);
// 0x00000440 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdClosed()
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdClosed_m6EF6013DC75CE5837D1A017F6BF897657CD6EC2D (void);
// 0x00000441 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdOpened()
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdOpened_m7610CFA746AE454D159862D95BB16DB316AF1868 (void);
// 0x00000442 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdStarted()
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdStarted_mD8C6171DDA763D2DA2FFD480C305EDB5691FB822 (void);
// 0x00000443 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdEnded()
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdEnded_m7104AEDF7EBBEC3745668EF4B2F8A3075C0EB0E7 (void);
// 0x00000444 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdRewarded(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdRewarded_m391298E9C24ADD0B04EE31CE2DA6E05FA308B926 (void);
// 0x00000445 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdClicked(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdClicked_mA722FD1E1FB9B53B2987ABA51EF7B4C3BB03E0C3 (void);
// 0x00000446 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAvailabilityChanged(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAvailabilityChanged_m6EAA670A7E2B9E7C0DE058A56E7B0D95320B5EDB (void);
// 0x00000447 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdShowFailedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdShowFailedDemandOnly_mD8B0284BC2BFD028EF0D32E31C8A78A9A8BA1D02 (void);
// 0x00000448 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdClosedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdClosedDemandOnly_m28D3D91227BBDCAC33C34C1D6D36DE1B726C917A (void);
// 0x00000449 System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdOpenedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdOpenedDemandOnly_mDB2425B23A12DB13CB0F8FE5C0F3BBD55E26A3C6 (void);
// 0x0000044A System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdRewardedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdRewardedDemandOnly_mDC37CBC0099149D21BDAEB0EBABC7EC1B736DEC5 (void);
// 0x0000044B System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdClickedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdClickedDemandOnly_mF10EA26E19274DF5934197ED32D77846370A7A64 (void);
// 0x0000044C System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdLoadedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdLoadedDemandOnly_m35FC1B06E015FE62D1820216E652C460C01794AF (void);
// 0x0000044D System.Void IronSourceRewardedVideoAndroid::onRewardedVideoAdLoadFailedDemandOnly(System.String)
extern void IronSourceRewardedVideoAndroid_onRewardedVideoAdLoadFailedDemandOnly_m4FD3E194C53AC5515C8EFC2E29BC5EA35DE099F0 (void);
// 0x0000044E System.Void IronSourceRewardedVideoAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_mCCD8EBC8601D0C59D051F7E60E258CE531CCE17D (void);
// 0x0000044F System.Void IronSourceRewardedVideoAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mE666C92FFF7A4E7C6BF51166C59EA41CE47EA9F3 (void);
// 0x00000450 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_0(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_mBA8799865C637EA9EAC7D94B08BBB37A6609BC37 (void);
// 0x00000451 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_1()
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_mE4DD9FA895D5088E1B768AD0F4B328BA0C282935 (void);
// 0x00000452 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_2()
extern void U3CU3Ec_U3C_ctorU3Eb__0_2_m4A39A4F487EBF3C395DD6D118A27BB19CDCCC6B9 (void);
// 0x00000453 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_3()
extern void U3CU3Ec_U3C_ctorU3Eb__0_3_mF86C03DBEFC09B90B4B70F855B29748200A8E131 (void);
// 0x00000454 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_4()
extern void U3CU3Ec_U3C_ctorU3Eb__0_4_m9B861CBBCE83EDA8FB96FBBD9A5C660631A461E1 (void);
// 0x00000455 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_5(IronSourcePlacement)
extern void U3CU3Ec_U3C_ctorU3Eb__0_5_m301D5C467D39CEF21A768C705552AA88823B2514 (void);
// 0x00000456 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_6(IronSourcePlacement)
extern void U3CU3Ec_U3C_ctorU3Eb__0_6_m830515B3B7FF71F15B42C96F396387722D7DF659 (void);
// 0x00000457 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_7(System.Boolean)
extern void U3CU3Ec_U3C_ctorU3Eb__0_7_m4F6B70A4E593C005DC8DBF3D256755CB42A50C68 (void);
// 0x00000458 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_8(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_8_m9E860F9C0830A9290F938586017165F35D12810D (void);
// 0x00000459 System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_9(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_9_m720F4239C1CDC0798311CF7889E343E5B35FD770 (void);
// 0x0000045A System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_10(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_10_mC031645525FF69A8316143A91AAF164B90073140 (void);
// 0x0000045B System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_11(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_11_mA1D228493161617CABE34FD3AEDEEB15891A4779 (void);
// 0x0000045C System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_12(System.String,IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_12_m182451A8F77D64F90217C1182E87A49014CBD4E1 (void);
// 0x0000045D System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_13(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__0_13_mC92214983F687BE4EDCD7E49D47CAEB00BF6C1A7 (void);
// 0x0000045E System.Void IronSourceRewardedVideoAndroid/<>c::<.ctor>b__0_14(System.String,IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_14_mBFD3C0CEF1B566ED75D0A7045818DB6D49698845 (void);
// 0x0000045F System.Void IronSourceRewardedVideoEvents::add_onAdShowFailedEvent(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdShowFailedEvent_mA41C26303D32CD8E709B8C867B9E00EAB2208B1D (void);
// 0x00000460 System.Void IronSourceRewardedVideoEvents::remove_onAdShowFailedEvent(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdShowFailedEvent_mD3986845E0AB995A20C438C641039EC07C8AD3BD (void);
// 0x00000461 System.Void IronSourceRewardedVideoEvents::add_onAdOpenedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdOpenedEvent_m3AC8A5AB0ACA12C2B36B26DD6EE063C6F5CFD8EA (void);
// 0x00000462 System.Void IronSourceRewardedVideoEvents::remove_onAdOpenedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdOpenedEvent_mF02B9ED2FD8927D4EFE02B3DC61E17A3A2042B23 (void);
// 0x00000463 System.Void IronSourceRewardedVideoEvents::add_onAdClosedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdClosedEvent_mB3AE13F218C7968A07A0162A18BC9B973CF0CD83 (void);
// 0x00000464 System.Void IronSourceRewardedVideoEvents::remove_onAdClosedEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdClosedEvent_mC73B97666AEF5E177D584AD456BA1ADD6EEA4EED (void);
// 0x00000465 System.Void IronSourceRewardedVideoEvents::add_onAdRewardedEvent(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdRewardedEvent_m61AFF38BFDB3C1AA669B9B13D8378E083B3FB9C6 (void);
// 0x00000466 System.Void IronSourceRewardedVideoEvents::remove_onAdRewardedEvent(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdRewardedEvent_mCB1E4BEB3DDECAE2679AEE800E975F9546450F82 (void);
// 0x00000467 System.Void IronSourceRewardedVideoEvents::add_onAdClickedEvent(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdClickedEvent_m866030CA4633267C4C4A4DD9EC95C3DCB58FAF37 (void);
// 0x00000468 System.Void IronSourceRewardedVideoEvents::remove_onAdClickedEvent(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdClickedEvent_mD0AB46993A4494505FE2FBAE663CFED85B1AAA70 (void);
// 0x00000469 System.Void IronSourceRewardedVideoEvents::add_onAdAvailableEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdAvailableEvent_mAC26BC3EFF5E9198FFDD8BE4C8F2B7AE5BFEC050 (void);
// 0x0000046A System.Void IronSourceRewardedVideoEvents::remove_onAdAvailableEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdAvailableEvent_m6E4A3BE3B02D6BEBA5A6F9B6A6CC8215259F6886 (void);
// 0x0000046B System.Void IronSourceRewardedVideoEvents::add_onAdUnavailableEvent(System.Action)
extern void IronSourceRewardedVideoEvents_add_onAdUnavailableEvent_mCAD1B56DE43B98C955F8680CC8AAC1913B77BAE5 (void);
// 0x0000046C System.Void IronSourceRewardedVideoEvents::remove_onAdUnavailableEvent(System.Action)
extern void IronSourceRewardedVideoEvents_remove_onAdUnavailableEvent_m6AC19CEB3E41D300AA29190E8F4CA97A6F9732A6 (void);
// 0x0000046D System.Void IronSourceRewardedVideoEvents::add_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoEvents_add_onAdLoadFailedEvent_mC182ACFFA8EB75239A333B9B8F82B12DE61C3E22 (void);
// 0x0000046E System.Void IronSourceRewardedVideoEvents::remove_onAdLoadFailedEvent(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoEvents_remove_onAdLoadFailedEvent_mF5BC2BE4F2F277CE9CE28FB4E8392BA622697CE8 (void);
// 0x0000046F System.Void IronSourceRewardedVideoEvents::add_onAdReadyEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_add_onAdReadyEvent_mCCF7143E062BDA79EAC09D9EAA99C9DD39F0735C (void);
// 0x00000470 System.Void IronSourceRewardedVideoEvents::remove_onAdReadyEvent(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoEvents_remove_onAdReadyEvent_mEB0025D4FA09A4D0FF32169CCCAAACC3AD0524F3 (void);
// 0x00000471 System.Void IronSourceRewardedVideoEvents::Awake()
extern void IronSourceRewardedVideoEvents_Awake_mFBE07F49A80846FB989C24B4767A00CEF5C0585E (void);
// 0x00000472 System.Void IronSourceRewardedVideoEvents::registerRewardedVideoEvents()
extern void IronSourceRewardedVideoEvents_registerRewardedVideoEvents_mB7294E7D98DB488139FE5D10304C5F767CCC52D5 (void);
// 0x00000473 System.Void IronSourceRewardedVideoEvents::registerRewardedVideoManualEvents()
extern void IronSourceRewardedVideoEvents_registerRewardedVideoManualEvents_mC0A94F107A104BD804A46EA933925C850908D766 (void);
// 0x00000474 IronSourceError IronSourceRewardedVideoEvents::getErrorFromErrorObject(System.Object)
extern void IronSourceRewardedVideoEvents_getErrorFromErrorObject_m4FA92B279C933CE52A84E55AFBFA66DDBC3A1725 (void);
// 0x00000475 IronSourcePlacement IronSourceRewardedVideoEvents::getPlacementFromObject(System.Object)
extern void IronSourceRewardedVideoEvents_getPlacementFromObject_mE8668B98C1905716A7FA560BB5DECAF997CC4F71 (void);
// 0x00000476 System.Void IronSourceRewardedVideoEvents::.ctor()
extern void IronSourceRewardedVideoEvents__ctor_m72AE72063578914CD70F82C93380D61B3263A6DB (void);
// 0x00000477 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mFBC21C474FD5C70F624EECE2742D25161530943F (void);
// 0x00000478 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_0::<registerRewardedVideoEvents>b__7()
extern void U3CU3Ec__DisplayClass30_0_U3CregisterRewardedVideoEventsU3Eb__7_m4993D860C30CADB72E3B6B0C3FA8F0865D9535CC (void);
// 0x00000479 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_mF9F05729F137CB97649E519F909769C816A8FD18 (void);
// 0x0000047A System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_1::<registerRewardedVideoEvents>b__8()
extern void U3CU3Ec__DisplayClass30_1_U3CregisterRewardedVideoEventsU3Eb__8_mD906726D62D250729346AF2152872F202FC76F52 (void);
// 0x0000047B System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_2::.ctor()
extern void U3CU3Ec__DisplayClass30_2__ctor_mF98CDC964494372851E3BA7C94F205249DB160DE (void);
// 0x0000047C System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_2::<registerRewardedVideoEvents>b__9()
extern void U3CU3Ec__DisplayClass30_2_U3CregisterRewardedVideoEventsU3Eb__9_mC07B532CB6D0108B3A5224BFD6F254348BAB623F (void);
// 0x0000047D System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_3::.ctor()
extern void U3CU3Ec__DisplayClass30_3__ctor_mBA87EDBB733E66EC70799EBDB04BACC2EF554CAC (void);
// 0x0000047E System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_3::<registerRewardedVideoEvents>b__10()
extern void U3CU3Ec__DisplayClass30_3_U3CregisterRewardedVideoEventsU3Eb__10_m4BCD17AB27A84E17435AEDB46BE6FFC99340578C (void);
// 0x0000047F System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_4::.ctor()
extern void U3CU3Ec__DisplayClass30_4__ctor_m54A9527B75311EEAF7AD05D7A2DE8D7DDB5BE114 (void);
// 0x00000480 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_4::<registerRewardedVideoEvents>b__11()
extern void U3CU3Ec__DisplayClass30_4_U3CregisterRewardedVideoEventsU3Eb__11_m555C94F46E53A19A88BC9FA8D11181D814971FC1 (void);
// 0x00000481 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_5::.ctor()
extern void U3CU3Ec__DisplayClass30_5__ctor_m5D484C89B4DEE58F44B6BD3EA5EE0D44BC2E1BB7 (void);
// 0x00000482 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass30_5::<registerRewardedVideoEvents>b__12()
extern void U3CU3Ec__DisplayClass30_5_U3CregisterRewardedVideoEventsU3Eb__12_m3052B84FF31EFF9CBF35FA49919BBF8488749CC2 (void);
// 0x00000483 System.Void IronSourceRewardedVideoEvents/<>c::.cctor()
extern void U3CU3Ec__cctor_mF840959BC4873C406C70D034B9B544C0827BF67A (void);
// 0x00000484 System.Void IronSourceRewardedVideoEvents/<>c::.ctor()
extern void U3CU3Ec__ctor_mC65A730F491F84DA87427301E4BDA58B8BC59961 (void);
// 0x00000485 System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_0(IronSourcePlacement,IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_0_m0CF68F591917CB6AC3670ECE19A5DA245005F513 (void);
// 0x00000486 System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_1(IronSourceError,IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_1_m1625852D533C06F5C1D08D8F08AB458F43A5550C (void);
// 0x00000487 System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_2(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_2_m401F442147E41711F752F3228D9838BC21113EA7 (void);
// 0x00000488 System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_3(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_3_m4490FF2A5EE7C94791FE06EB469E1C223DB49763 (void);
// 0x00000489 System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_4(IronSourcePlacement,IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_4_m4566F98878A5E8542074406EE6C222D4D103F095 (void);
// 0x0000048A System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_5(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_5_m9975EC5EF129F04A72CA532D96AC4B9CC7B4CCCB (void);
// 0x0000048B System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_6()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_6_mAB95D0FBFB93426612C08FEF77D58D861D84D799 (void);
// 0x0000048C System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoEvents>b__30_13()
extern void U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_13_m059C304E334985DA544011F09F9BC195F74C778B (void);
// 0x0000048D System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoManualEvents>b__31_0(IronSourceAdInfo)
extern void U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__31_0_mB06FC738C03209B8437222F001A024EF476B210F (void);
// 0x0000048E System.Void IronSourceRewardedVideoEvents/<>c::<registerRewardedVideoManualEvents>b__31_1(IronSourceError)
extern void U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__31_1_m5768A20A5C56190B110DFA549161AE7891AE55A3 (void);
// 0x0000048F System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m26B492EA621C4D0D7B9757D47C2E831F06EADB13 (void);
// 0x00000490 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass31_0::<registerRewardedVideoManualEvents>b__2()
extern void U3CU3Ec__DisplayClass31_0_U3CregisterRewardedVideoManualEventsU3Eb__2_mB1E71AFCF6A47C24B3FCFB4DA3EB726F758E6008 (void);
// 0x00000491 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass31_1::.ctor()
extern void U3CU3Ec__DisplayClass31_1__ctor_mF9B6BE240C3BD9A2BDC7A83522482D9ACAC2B4E4 (void);
// 0x00000492 System.Void IronSourceRewardedVideoEvents/<>c__DisplayClass31_1::<registerRewardedVideoManualEvents>b__3()
extern void U3CU3Ec__DisplayClass31_1_U3CregisterRewardedVideoManualEventsU3Eb__3_m2162F775D13E9AAE96A66069C51019CA251D3328 (void);
// 0x00000493 System.Void IronSourceRewardedVideoLevelPlayAndroid::.ctor()
extern void IronSourceRewardedVideoLevelPlayAndroid__ctor_m4944F5587E85D05F0AE352D13B6E72C2F9BD0A67 (void);
// 0x00000494 System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdShowFailed_m6DCE670E53544C4588C5E13F75C3B35897BFC146 (void);
// 0x00000495 System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdShowFailed_m392A4FC25E4FECDAA83692FEC842EC17498C5457 (void);
// 0x00000496 System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdOpened(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdOpened_m70CB40E109F8EC4703E05594EBD835B919CD0EDC (void);
// 0x00000497 System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdOpened(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdOpened_mED20E58A386E2DB5CD80C700495FAA18DD8FF6C3 (void);
// 0x00000498 System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdClosed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdClosed_m2F3409F05ADA3716C87CB7DE2DF81C103E43541B (void);
// 0x00000499 System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdClosed(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdClosed_mF68ADB59A94E526C5E10A6B4307DC731C17EAE8D (void);
// 0x0000049A System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdRewarded(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdRewarded_mD5234D18DB5D6D23DFE4FC8C347938B4EB2E3EFC (void);
// 0x0000049B System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdRewarded(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdRewarded_m3F1803596C45034E5024F8C91568A609F1016FCD (void);
// 0x0000049C System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdClicked(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdClicked_m94E06C4FD6F474FC05D67232F6D93493D70DD576 (void);
// 0x0000049D System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdClicked(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdClicked_mDAD55BB5D4CBAFE724B893DEE513EA242EF99CBA (void);
// 0x0000049E System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdAvailable(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdAvailable_m22C853606ABD2D9CA0EDCD12FECAB2D3FE35DAF2 (void);
// 0x0000049F System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdAvailable(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdAvailable_m04DCE733DC3B32413BD98E8D0CFE8A4893266681 (void);
// 0x000004A0 System.Void IronSourceRewardedVideoLevelPlayAndroid::add_OnAdUnavailable(System.Action)
extern void IronSourceRewardedVideoLevelPlayAndroid_add_OnAdUnavailable_m34453045E126AB8FD5181BFEBEDE1C479D676E27 (void);
// 0x000004A1 System.Void IronSourceRewardedVideoLevelPlayAndroid::remove_OnAdUnavailable(System.Action)
extern void IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdUnavailable_m1F20670A4AEE537B444C0708D6F61A7FAE39E673 (void);
// 0x000004A2 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdShowFailed(System.String,System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdShowFailed_m26208DE352679F25C6BCD0215F047FF8EE95FB51 (void);
// 0x000004A3 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdClosed(System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdClosed_m401E111F9691A63443EB7B146883BE722B245594 (void);
// 0x000004A4 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdOpened(System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdOpened_m58A71CE17959758012157891A061CE65A4060655 (void);
// 0x000004A5 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdRewarded(System.String,System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdRewarded_mB4C161681BA049BB9371826F95CA7281C9B723E1 (void);
// 0x000004A6 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdClicked(System.String,System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdClicked_m19E7F85F501BAC3A2CF192D23B2E2A2D6ACC4B3B (void);
// 0x000004A7 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdAvailable(System.String)
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdAvailable_m757A383FD394F69B701687397238C9906BC15A4A (void);
// 0x000004A8 System.Void IronSourceRewardedVideoLevelPlayAndroid::onAdUnavailable()
extern void IronSourceRewardedVideoLevelPlayAndroid_onAdUnavailable_mDF65E0E9D7C7CBDB78D024C81EA9C73A4C7B0FF7 (void);
// 0x000004A9 System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m4BCA76BB846C50D6813F1772BA4F276405CC052B (void);
// 0x000004AA System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_m8BBE574D7632BEC6DAB09ADFF43555521E0850B4 (void);
// 0x000004AB System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_0(IronSourceError,IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_m69AC22159789B59907CFB0B88573B8603C10B761 (void);
// 0x000004AC System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_1(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_m06F2A4262570A51F3C08DA32ED249FC6E7F5C77B (void);
// 0x000004AD System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_2(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_2_mF0EA1F99DE7C0370AD7A57EDEC117C519B8C8BC4 (void);
// 0x000004AE System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_3(IronSourcePlacement,IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_3_m59058E12B1CB7A75A9B19CC6EDF05D1013EF6BB7 (void);
// 0x000004AF System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_4(IronSourcePlacement,IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_4_mBAEC93CE1F4D4F99F341625BA5BE1305D33909F1 (void);
// 0x000004B0 System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_5(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_5_m4FD2E1E0777913D815432A666B7460073F724858 (void);
// 0x000004B1 System.Void IronSourceRewardedVideoLevelPlayAndroid/<>c::<.ctor>b__0_6()
extern void U3CU3Ec_U3C_ctorU3Eb__0_6_m91AEC199ACB71FEE5DD2772BE553123F982A26CA (void);
// 0x000004B2 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::.ctor()
extern void IronSourceRewardedVideoLevelPlayManualAndroid__ctor_mC0747B473E9E6F3094E576E4519377B1B2B5692A (void);
// 0x000004B3 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_add_OnAdLoadFailed_m4F1427B419CB4B0DEDDCD8D2BE2489E835136D70 (void);
// 0x000004B4 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_remove_OnAdLoadFailed_mE8AB4490C63358D9E4CCCCD493A91F681DA798C1 (void);
// 0x000004B5 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::add_OnAdReady(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_add_OnAdReady_m7585E6C4786A220DC10BE5D87EB575395F566217 (void);
// 0x000004B6 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::remove_OnAdReady(System.Action`1<IronSourceAdInfo>)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_remove_OnAdReady_m847D347AB7CEC0E1E2A98D7F3C09208AB85E933C (void);
// 0x000004B7 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::onAdReady(System.String)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_onAdReady_m41C2B32A734F1DB2D649D6FA8150AAB5F7AA5B1C (void);
// 0x000004B8 System.Void IronSourceRewardedVideoLevelPlayManualAndroid::onAdLoadFailed(System.String)
extern void IronSourceRewardedVideoLevelPlayManualAndroid_onAdLoadFailed_m3A147970BB227DE2F9EC54A03977F11B3A016D4D (void);
// 0x000004B9 System.Void IronSourceRewardedVideoLevelPlayManualAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m26F5DB927FF83F70BABCA3B9CFDD63E0ABD929A2 (void);
// 0x000004BA System.Void IronSourceRewardedVideoLevelPlayManualAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mF1565A6F51942F895FE0F5C1A234CCF80EE1C534 (void);
// 0x000004BB System.Void IronSourceRewardedVideoLevelPlayManualAndroid/<>c::<.ctor>b__0_0(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_mB8A1645FF496D78636DDDE08CBD521A63604A917 (void);
// 0x000004BC System.Void IronSourceRewardedVideoLevelPlayManualAndroid/<>c::<.ctor>b__0_1(IronSourceAdInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_mD14802B9F28998C5321B22CDB3AF4CAC93BA4E69 (void);
// 0x000004BD System.Void IronSourceRewardedVideoManualAndroid::.ctor()
extern void IronSourceRewardedVideoManualAndroid__ctor_m003B82F6767C29A4D86D464EC3C2D0D07DE4188A (void);
// 0x000004BE System.Void IronSourceRewardedVideoManualAndroid::add_OnRewardedVideoAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoManualAndroid_add_OnRewardedVideoAdLoadFailed_m474483A76AA154D599F9F668A0C7A365689FA937 (void);
// 0x000004BF System.Void IronSourceRewardedVideoManualAndroid::remove_OnRewardedVideoAdLoadFailed(System.Action`1<IronSourceError>)
extern void IronSourceRewardedVideoManualAndroid_remove_OnRewardedVideoAdLoadFailed_mF7BD36FBA613FF8C93CC3554F5ECCA43A3E732D7 (void);
// 0x000004C0 System.Void IronSourceRewardedVideoManualAndroid::add_OnRewardedVideoAdReady(System.Action)
extern void IronSourceRewardedVideoManualAndroid_add_OnRewardedVideoAdReady_mC271CED21006C50D8B577038CD2AB447F060725E (void);
// 0x000004C1 System.Void IronSourceRewardedVideoManualAndroid::remove_OnRewardedVideoAdReady(System.Action)
extern void IronSourceRewardedVideoManualAndroid_remove_OnRewardedVideoAdReady_mE990D22B7B5325DD077DB154B1FB7E0D71B3EF05 (void);
// 0x000004C2 System.Void IronSourceRewardedVideoManualAndroid::onRewardedVideoAdReady()
extern void IronSourceRewardedVideoManualAndroid_onRewardedVideoAdReady_mC1FCE342802ED7EE992D8A832658E948198C663B (void);
// 0x000004C3 System.Void IronSourceRewardedVideoManualAndroid::onRewardedVideoAdLoadFailed(System.String)
extern void IronSourceRewardedVideoManualAndroid_onRewardedVideoAdLoadFailed_m01279D5D27939276A5C4CDAD49BB7EB297DA4CD8 (void);
// 0x000004C4 System.Void IronSourceRewardedVideoManualAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_mC20AAB18A724C971C317EC0998769D467B1DAE48 (void);
// 0x000004C5 System.Void IronSourceRewardedVideoManualAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_mD16C8762220D52670D033BD199729F58DBB04FD1 (void);
// 0x000004C6 System.Void IronSourceRewardedVideoManualAndroid/<>c::<.ctor>b__0_0(IronSourceError)
extern void U3CU3Ec_U3C_ctorU3Eb__0_0_m3FB16B8090C97782F39815DC4668D44C62981151 (void);
// 0x000004C7 System.Void IronSourceRewardedVideoManualAndroid/<>c::<.ctor>b__0_1()
extern void U3CU3Ec_U3C_ctorU3Eb__0_1_m2EE2DF468DA626DB0E993E6ED6B8B73695D6F86D (void);
// 0x000004C8 System.Void IronSourceSegment::.ctor()
extern void IronSourceSegment__ctor_mE574CFEC106EDDC03C569D27C4895B5EB966605D (void);
// 0x000004C9 System.Void IronSourceSegment::setCustom(System.String,System.String)
extern void IronSourceSegment_setCustom_m0C47A244F67C01230CB5BB939E1A306B14C397BB (void);
// 0x000004CA System.Collections.Generic.Dictionary`2<System.String,System.String> IronSourceSegment::getSegmentAsDict()
extern void IronSourceSegment_getSegmentAsDict_mC40C238CDDE9A62366ED9A5A30D912790A94A657 (void);
// 0x000004CB System.Void IronSourceSegment/<>c::.cctor()
extern void U3CU3Ec__cctor_m6163CD352D8DEFB692DF15F7F2D192A108CB73F5 (void);
// 0x000004CC System.Void IronSourceSegment/<>c::.ctor()
extern void U3CU3Ec__ctor_mD4A8B181D66382B2A89897C054802C45FB5FF632 (void);
// 0x000004CD System.String IronSourceSegment/<>c::<getSegmentAsDict>b__10_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_0_m1B8B186D0FF7EB726E8F6CB8512A685BC03CCE3E (void);
// 0x000004CE System.String IronSourceSegment/<>c::<getSegmentAsDict>b__10_1(System.Linq.IGrouping`2<System.String,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_1_m82BCB5EA31512718DD5B28CEBB409BA9166448C8 (void);
// 0x000004CF System.String IronSourceSegment/<>c::<getSegmentAsDict>b__10_2(System.Linq.IGrouping`2<System.String,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_2_m19DC4B2EA7357C8A53DB6902607A678C1F267D6E (void);
// 0x000004D0 System.Void IronSourceSegmentAndroid::add_OnSegmentRecieved(System.Action`1<System.String>)
extern void IronSourceSegmentAndroid_add_OnSegmentRecieved_mDDEA7DD5A9D62E8C08F7A047B18E21697527ABBA (void);
// 0x000004D1 System.Void IronSourceSegmentAndroid::remove_OnSegmentRecieved(System.Action`1<System.String>)
extern void IronSourceSegmentAndroid_remove_OnSegmentRecieved_mB3AD982751ECEF1656B46CC19FD801872A7E0063 (void);
// 0x000004D2 System.Void IronSourceSegmentAndroid::.ctor()
extern void IronSourceSegmentAndroid__ctor_m8CF1656B06CA2F87B1486CE6EBDFBBDE41F71459 (void);
// 0x000004D3 System.Void IronSourceSegmentAndroid::onSegmentRecieved(System.String)
extern void IronSourceSegmentAndroid_onSegmentRecieved_m19D892C8AA7166213ACE588F06AF0F93CC13AD6E (void);
// 0x000004D4 System.Void IronSourceSegmentAndroid/<>c::.cctor()
extern void U3CU3Ec__cctor_m339A561A90EFE055467EB5EE2546538402D3E76C (void);
// 0x000004D5 System.Void IronSourceSegmentAndroid/<>c::.ctor()
extern void U3CU3Ec__ctor_m12C62B0AE205D489D946B88B7DBA0D7E42682DD1 (void);
// 0x000004D6 System.Void IronSourceSegmentAndroid/<>c::<.ctor>b__3_0(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_m1DE5600622F06D33338F909D011FCB200A918012 (void);
// 0x000004D7 IronSourceError IronSourceUtils::getErrorFromErrorObject(System.Object)
extern void IronSourceUtils_getErrorFromErrorObject_m4BF0C110C0643B35577B6729A7535522C960EC6B (void);
// 0x000004D8 IronSourcePlacement IronSourceUtils::getPlacementFromObject(System.Object)
extern void IronSourceUtils_getPlacementFromObject_mD2226B29CA168FC85356404C56482F25AC7EF566 (void);
// 0x000004D9 System.Void IronSourceUtils::.ctor()
extern void IronSourceUtils__ctor_m1C07CA369CEFE295D8FBBC624CD41E18FD077048 (void);
// 0x000004DA System.Void IUnityBanner::add_OnBannerAdLoaded(System.Action)
// 0x000004DB System.Void IUnityBanner::remove_OnBannerAdLoaded(System.Action)
// 0x000004DC System.Void IUnityBanner::add_OnBannerAdLeftApplication(System.Action)
// 0x000004DD System.Void IUnityBanner::remove_OnBannerAdLeftApplication(System.Action)
// 0x000004DE System.Void IUnityBanner::add_OnBannerAdScreenDismissed(System.Action)
// 0x000004DF System.Void IUnityBanner::remove_OnBannerAdScreenDismissed(System.Action)
// 0x000004E0 System.Void IUnityBanner::add_OnBannerAdScreenPresented(System.Action)
// 0x000004E1 System.Void IUnityBanner::remove_OnBannerAdScreenPresented(System.Action)
// 0x000004E2 System.Void IUnityBanner::add_OnBannerAdClicked(System.Action)
// 0x000004E3 System.Void IUnityBanner::remove_OnBannerAdClicked(System.Action)
// 0x000004E4 System.Void IUnityBanner::add_OnBannerAdLoadFailed(System.Action`1<IronSourceError>)
// 0x000004E5 System.Void IUnityBanner::remove_OnBannerAdLoadFailed(System.Action`1<IronSourceError>)
// 0x000004E6 System.Void IUnityImpressionData::add_OnImpressionDataReady(System.Action`1<IronSourceImpressionData>)
// 0x000004E7 System.Void IUnityImpressionData::remove_OnImpressionDataReady(System.Action`1<IronSourceImpressionData>)
// 0x000004E8 System.Void IUnityImpressionData::add_OnImpressionSuccess(System.Action`1<IronSourceImpressionData>)
// 0x000004E9 System.Void IUnityImpressionData::remove_OnImpressionSuccess(System.Action`1<IronSourceImpressionData>)
// 0x000004EA System.Void IUnityInitialization::add_OnSdkInitializationCompletedEvent(System.Action)
// 0x000004EB System.Void IUnityInitialization::remove_OnSdkInitializationCompletedEvent(System.Action)
// 0x000004EC System.Void IUnityInterstitial::add_OnInterstitialAdShowFailed(System.Action`1<IronSourceError>)
// 0x000004ED System.Void IUnityInterstitial::remove_OnInterstitialAdShowFailed(System.Action`1<IronSourceError>)
// 0x000004EE System.Void IUnityInterstitial::add_OnInterstitialAdLoadFailed(System.Action`1<IronSourceError>)
// 0x000004EF System.Void IUnityInterstitial::remove_OnInterstitialAdLoadFailed(System.Action`1<IronSourceError>)
// 0x000004F0 System.Void IUnityInterstitial::add_OnInterstitialAdReady(System.Action)
// 0x000004F1 System.Void IUnityInterstitial::remove_OnInterstitialAdReady(System.Action)
// 0x000004F2 System.Void IUnityInterstitial::add_OnInterstitialAdOpened(System.Action)
// 0x000004F3 System.Void IUnityInterstitial::remove_OnInterstitialAdOpened(System.Action)
// 0x000004F4 System.Void IUnityInterstitial::add_OnInterstitialAdClosed(System.Action)
// 0x000004F5 System.Void IUnityInterstitial::remove_OnInterstitialAdClosed(System.Action)
// 0x000004F6 System.Void IUnityInterstitial::add_OnInterstitialAdShowSucceeded(System.Action)
// 0x000004F7 System.Void IUnityInterstitial::remove_OnInterstitialAdShowSucceeded(System.Action)
// 0x000004F8 System.Void IUnityInterstitial::add_OnInterstitialAdClicked(System.Action)
// 0x000004F9 System.Void IUnityInterstitial::remove_OnInterstitialAdClicked(System.Action)
// 0x000004FA System.Void IUnityInterstitial::add_OnInterstitialAdRewarded(System.Action)
// 0x000004FB System.Void IUnityInterstitial::remove_OnInterstitialAdRewarded(System.Action)
// 0x000004FC System.Void IUnityInterstitial::add_OnInterstitialAdReadyDemandOnly(System.Action`1<System.String>)
// 0x000004FD System.Void IUnityInterstitial::remove_OnInterstitialAdReadyDemandOnly(System.Action`1<System.String>)
// 0x000004FE System.Void IUnityInterstitial::add_OnInterstitialAdOpenedDemandOnly(System.Action`1<System.String>)
// 0x000004FF System.Void IUnityInterstitial::remove_OnInterstitialAdOpenedDemandOnly(System.Action`1<System.String>)
// 0x00000500 System.Void IUnityInterstitial::add_OnInterstitialAdClosedDemandOnly(System.Action`1<System.String>)
// 0x00000501 System.Void IUnityInterstitial::remove_OnInterstitialAdClosedDemandOnly(System.Action`1<System.String>)
// 0x00000502 System.Void IUnityInterstitial::add_OnInterstitialAdLoadFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
// 0x00000503 System.Void IUnityInterstitial::remove_OnInterstitialAdLoadFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
// 0x00000504 System.Void IUnityInterstitial::add_OnInterstitialAdClickedDemandOnly(System.Action`1<System.String>)
// 0x00000505 System.Void IUnityInterstitial::remove_OnInterstitialAdClickedDemandOnly(System.Action`1<System.String>)
// 0x00000506 System.Void IUnityInterstitial::add_OnInterstitialAdShowFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
// 0x00000507 System.Void IUnityInterstitial::remove_OnInterstitialAdShowFailedDemandOnly(System.Action`2<System.String,IronSourceError>)
// 0x00000508 System.Void IUnityLevelPlayBanner::add_OnAdLoaded(System.Action`1<IronSourceAdInfo>)
// 0x00000509 System.Void IUnityLevelPlayBanner::remove_OnAdLoaded(System.Action`1<IronSourceAdInfo>)
// 0x0000050A System.Void IUnityLevelPlayBanner::add_OnAdLeftApplication(System.Action`1<IronSourceAdInfo>)
// 0x0000050B System.Void IUnityLevelPlayBanner::remove_OnAdLeftApplication(System.Action`1<IronSourceAdInfo>)
// 0x0000050C System.Void IUnityLevelPlayBanner::add_OnAdScreenDismissed(System.Action`1<IronSourceAdInfo>)
// 0x0000050D System.Void IUnityLevelPlayBanner::remove_OnAdScreenDismissed(System.Action`1<IronSourceAdInfo>)
// 0x0000050E System.Void IUnityLevelPlayBanner::add_OnAdScreenPresented(System.Action`1<IronSourceAdInfo>)
// 0x0000050F System.Void IUnityLevelPlayBanner::remove_OnAdScreenPresented(System.Action`1<IronSourceAdInfo>)
// 0x00000510 System.Void IUnityLevelPlayBanner::add_OnAdClicked(System.Action`1<IronSourceAdInfo>)
// 0x00000511 System.Void IUnityLevelPlayBanner::remove_OnAdClicked(System.Action`1<IronSourceAdInfo>)
// 0x00000512 System.Void IUnityLevelPlayBanner::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000513 System.Void IUnityLevelPlayBanner::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000514 System.Void IUnityLevelPlayInterstitial::add_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
// 0x00000515 System.Void IUnityLevelPlayInterstitial::remove_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
// 0x00000516 System.Void IUnityLevelPlayInterstitial::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000517 System.Void IUnityLevelPlayInterstitial::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000518 System.Void IUnityLevelPlayInterstitial::add_OnAdReady(System.Action`1<IronSourceAdInfo>)
// 0x00000519 System.Void IUnityLevelPlayInterstitial::remove_OnAdReady(System.Action`1<IronSourceAdInfo>)
// 0x0000051A System.Void IUnityLevelPlayInterstitial::add_OnAdOpened(System.Action`1<IronSourceAdInfo>)
// 0x0000051B System.Void IUnityLevelPlayInterstitial::remove_OnAdOpened(System.Action`1<IronSourceAdInfo>)
// 0x0000051C System.Void IUnityLevelPlayInterstitial::add_OnAdClosed(System.Action`1<IronSourceAdInfo>)
// 0x0000051D System.Void IUnityLevelPlayInterstitial::remove_OnAdClosed(System.Action`1<IronSourceAdInfo>)
// 0x0000051E System.Void IUnityLevelPlayInterstitial::add_OnAdShowSucceeded(System.Action`1<IronSourceAdInfo>)
// 0x0000051F System.Void IUnityLevelPlayInterstitial::remove_OnAdShowSucceeded(System.Action`1<IronSourceAdInfo>)
// 0x00000520 System.Void IUnityLevelPlayInterstitial::add_OnAdClicked(System.Action`1<IronSourceAdInfo>)
// 0x00000521 System.Void IUnityLevelPlayInterstitial::remove_OnAdClicked(System.Action`1<IronSourceAdInfo>)
// 0x00000522 System.Void IUnityLevelPlayRewardedVideo::add_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
// 0x00000523 System.Void IUnityLevelPlayRewardedVideo::remove_OnAdShowFailed(System.Action`2<IronSourceError,IronSourceAdInfo>)
// 0x00000524 System.Void IUnityLevelPlayRewardedVideo::add_OnAdOpened(System.Action`1<IronSourceAdInfo>)
// 0x00000525 System.Void IUnityLevelPlayRewardedVideo::remove_OnAdOpened(System.Action`1<IronSourceAdInfo>)
// 0x00000526 System.Void IUnityLevelPlayRewardedVideo::add_OnAdClosed(System.Action`1<IronSourceAdInfo>)
// 0x00000527 System.Void IUnityLevelPlayRewardedVideo::remove_OnAdClosed(System.Action`1<IronSourceAdInfo>)
// 0x00000528 System.Void IUnityLevelPlayRewardedVideo::add_OnAdRewarded(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
// 0x00000529 System.Void IUnityLevelPlayRewardedVideo::remove_OnAdRewarded(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
// 0x0000052A System.Void IUnityLevelPlayRewardedVideo::add_OnAdClicked(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
// 0x0000052B System.Void IUnityLevelPlayRewardedVideo::remove_OnAdClicked(System.Action`2<IronSourcePlacement,IronSourceAdInfo>)
// 0x0000052C System.Void IUnityLevelPlayRewardedVideo::add_OnAdAvailable(System.Action`1<IronSourceAdInfo>)
// 0x0000052D System.Void IUnityLevelPlayRewardedVideo::remove_OnAdAvailable(System.Action`1<IronSourceAdInfo>)
// 0x0000052E System.Void IUnityLevelPlayRewardedVideo::add_OnAdUnavailable(System.Action)
// 0x0000052F System.Void IUnityLevelPlayRewardedVideo::remove_OnAdUnavailable(System.Action)
// 0x00000530 System.Void IUnityLevelPlayRewardedVideoManual::add_OnAdReady(System.Action`1<IronSourceAdInfo>)
// 0x00000531 System.Void IUnityLevelPlayRewardedVideoManual::remove_OnAdReady(System.Action`1<IronSourceAdInfo>)
// 0x00000532 System.Void IUnityLevelPlayRewardedVideoManual::add_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000533 System.Void IUnityLevelPlayRewardedVideoManual::remove_OnAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000534 System.Void IUnityOfferwall::add_OnOfferwallShowFailed(System.Action`1<IronSourceError>)
// 0x00000535 System.Void IUnityOfferwall::remove_OnOfferwallShowFailed(System.Action`1<IronSourceError>)
// 0x00000536 System.Void IUnityOfferwall::add_OnOfferwallOpened(System.Action)
// 0x00000537 System.Void IUnityOfferwall::remove_OnOfferwallOpened(System.Action)
// 0x00000538 System.Void IUnityOfferwall::add_OnOfferwallClosed(System.Action)
// 0x00000539 System.Void IUnityOfferwall::remove_OnOfferwallClosed(System.Action)
// 0x0000053A System.Void IUnityOfferwall::add_OnGetOfferwallCreditsFailed(System.Action`1<IronSourceError>)
// 0x0000053B System.Void IUnityOfferwall::remove_OnGetOfferwallCreditsFailed(System.Action`1<IronSourceError>)
// 0x0000053C System.Void IUnityOfferwall::add_OnOfferwallAdCredited(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
// 0x0000053D System.Void IUnityOfferwall::remove_OnOfferwallAdCredited(System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
// 0x0000053E System.Void IUnityOfferwall::add_OnOfferwallAvailable(System.Action`1<System.Boolean>)
// 0x0000053F System.Void IUnityOfferwall::remove_OnOfferwallAvailable(System.Action`1<System.Boolean>)
// 0x00000540 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdShowFailed(System.Action`1<IronSourceError>)
// 0x00000541 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdShowFailed(System.Action`1<IronSourceError>)
// 0x00000542 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdOpened(System.Action)
// 0x00000543 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdOpened(System.Action)
// 0x00000544 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdClosed(System.Action)
// 0x00000545 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdClosed(System.Action)
// 0x00000546 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdStarted(System.Action)
// 0x00000547 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdStarted(System.Action)
// 0x00000548 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdEnded(System.Action)
// 0x00000549 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdEnded(System.Action)
// 0x0000054A System.Void IUnityRewardedVideo::add_OnRewardedVideoAdRewarded(System.Action`1<IronSourcePlacement>)
// 0x0000054B System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdRewarded(System.Action`1<IronSourcePlacement>)
// 0x0000054C System.Void IUnityRewardedVideo::add_OnRewardedVideoAdClicked(System.Action`1<IronSourcePlacement>)
// 0x0000054D System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdClicked(System.Action`1<IronSourcePlacement>)
// 0x0000054E System.Void IUnityRewardedVideo::add_OnRewardedVideoAvailabilityChanged(System.Action`1<System.Boolean>)
// 0x0000054F System.Void IUnityRewardedVideo::remove_OnRewardedVideoAvailabilityChanged(System.Action`1<System.Boolean>)
// 0x00000550 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000551 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdOpenedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000552 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000553 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdClosedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000554 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000555 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdLoadedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000556 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000557 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdRewardedDemandOnlyEvent(System.Action`1<System.String>)
// 0x00000558 System.Void IUnityRewardedVideo::add_OnRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
// 0x00000559 System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdShowFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
// 0x0000055A System.Void IUnityRewardedVideo::add_OnRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
// 0x0000055B System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdClickedDemandOnlyEvent(System.Action`1<System.String>)
// 0x0000055C System.Void IUnityRewardedVideo::add_OnRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
// 0x0000055D System.Void IUnityRewardedVideo::remove_OnRewardedVideoAdLoadFailedDemandOnlyEvent(System.Action`2<System.String,IronSourceError>)
// 0x0000055E System.Void IUnityRewardedVideoManual::add_OnRewardedVideoAdReady(System.Action)
// 0x0000055F System.Void IUnityRewardedVideoManual::remove_OnRewardedVideoAdReady(System.Action)
// 0x00000560 System.Void IUnityRewardedVideoManual::add_OnRewardedVideoAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000561 System.Void IUnityRewardedVideoManual::remove_OnRewardedVideoAdLoadFailed(System.Action`1<IronSourceError>)
// 0x00000562 System.Void IUnitySegment::add_OnSegmentRecieved(System.Action`1<System.String>)
// 0x00000563 System.Void IUnitySegment::remove_OnSegmentRecieved(System.Action`1<System.String>)
// 0x00000564 System.Void UnsupportedPlatformAgent::.ctor()
extern void UnsupportedPlatformAgent__ctor_m1954CFC5B92BDD5D22ADC3E079C0BAE2CF1BB6C7 (void);
// 0x00000565 System.Void UnsupportedPlatformAgent::start()
extern void UnsupportedPlatformAgent_start_m3097EDD90F06DDCD7A4D5D41215051B4BC7D1CC0 (void);
// 0x00000566 System.Void UnsupportedPlatformAgent::onApplicationPause(System.Boolean)
extern void UnsupportedPlatformAgent_onApplicationPause_m47FC64902BA27D678E51B757FE0B4C2E67FE1CE9 (void);
// 0x00000567 System.Void UnsupportedPlatformAgent::setMediationSegment(System.String)
extern void UnsupportedPlatformAgent_setMediationSegment_m1946D551F1860B01E50AD8D27712F5E34619E4A4 (void);
// 0x00000568 System.String UnsupportedPlatformAgent::getAdvertiserId()
extern void UnsupportedPlatformAgent_getAdvertiserId_m93E929931E6F07A4D05BF316275334EA8591975C (void);
// 0x00000569 System.Void UnsupportedPlatformAgent::validateIntegration()
extern void UnsupportedPlatformAgent_validateIntegration_m02E893E5DC374E2BD749E9A82D0A3F3B358C990B (void);
// 0x0000056A System.Void UnsupportedPlatformAgent::shouldTrackNetworkState(System.Boolean)
extern void UnsupportedPlatformAgent_shouldTrackNetworkState_mC315832CD80680374BB06E815991F991CD9DF200 (void);
// 0x0000056B System.Boolean UnsupportedPlatformAgent::setDynamicUserId(System.String)
extern void UnsupportedPlatformAgent_setDynamicUserId_m311E373ADF1E9164CB3D3130E4927817B545F439 (void);
// 0x0000056C System.Void UnsupportedPlatformAgent::setAdaptersDebug(System.Boolean)
extern void UnsupportedPlatformAgent_setAdaptersDebug_m534ED1FFE98627073258FB4462ECA5B4178CC7B0 (void);
// 0x0000056D System.Void UnsupportedPlatformAgent::setMetaData(System.String,System.String)
extern void UnsupportedPlatformAgent_setMetaData_mC61828B357BE984F72307C3CC9692BC793B0CD70 (void);
// 0x0000056E System.Void UnsupportedPlatformAgent::setMetaData(System.String,System.String[])
extern void UnsupportedPlatformAgent_setMetaData_m70A8CAFBA4B6D39F971C05525A450186D08B2613 (void);
// 0x0000056F System.Nullable`1<System.Int32> UnsupportedPlatformAgent::getConversionValue()
extern void UnsupportedPlatformAgent_getConversionValue_mBF7AF8F77BD0591AFBD2F40D61983C4640FA33C2 (void);
// 0x00000570 System.Void UnsupportedPlatformAgent::setManualLoadRewardedVideo(System.Boolean)
extern void UnsupportedPlatformAgent_setManualLoadRewardedVideo_mED1E5C92E2DDE77F79CD27CD4075FA1508C0C578 (void);
// 0x00000571 System.Void UnsupportedPlatformAgent::setNetworkData(System.String,System.String)
extern void UnsupportedPlatformAgent_setNetworkData_m80FBB82DF70F7E74FBE23840E073E8FC4A0F970B (void);
// 0x00000572 System.Void UnsupportedPlatformAgent::SetPauseGame(System.Boolean)
extern void UnsupportedPlatformAgent_SetPauseGame_m59F45FF0684BC8EF16B31D135C49402E8820C1DA (void);
// 0x00000573 System.Void UnsupportedPlatformAgent::setUserId(System.String)
extern void UnsupportedPlatformAgent_setUserId_mDD0958B087FC28EC598E1A156C3D44279E60B02E (void);
// 0x00000574 System.Void UnsupportedPlatformAgent::init(System.String)
extern void UnsupportedPlatformAgent_init_m8894B78C226F7E506C8D43AF6BF871E19C30B0F9 (void);
// 0x00000575 System.Void UnsupportedPlatformAgent::init(System.String,System.String[])
extern void UnsupportedPlatformAgent_init_m827520E4CC5F44B403FA0770B6DB732A14E84381 (void);
// 0x00000576 System.Void UnsupportedPlatformAgent::initISDemandOnly(System.String,System.String[])
extern void UnsupportedPlatformAgent_initISDemandOnly_m3CAC9FA50476B9FB02AAF7FFD1F7849729AE3D0F (void);
// 0x00000577 System.Void UnsupportedPlatformAgent::loadRewardedVideo()
extern void UnsupportedPlatformAgent_loadRewardedVideo_m37246CB7A6C36943A340898B88C31962C94F47EA (void);
// 0x00000578 System.Void UnsupportedPlatformAgent::showRewardedVideo()
extern void UnsupportedPlatformAgent_showRewardedVideo_m52F4003EBD31A5A265B4607F62A09000182847F7 (void);
// 0x00000579 System.Void UnsupportedPlatformAgent::showRewardedVideo(System.String)
extern void UnsupportedPlatformAgent_showRewardedVideo_m4FE0C1511AB77E6B611244E82F1B3FFF87691A67 (void);
// 0x0000057A System.Boolean UnsupportedPlatformAgent::isRewardedVideoAvailable()
extern void UnsupportedPlatformAgent_isRewardedVideoAvailable_mD9541F3B32491BCB5B13CFDC976B67EDDCF6851E (void);
// 0x0000057B System.Boolean UnsupportedPlatformAgent::isRewardedVideoPlacementCapped(System.String)
extern void UnsupportedPlatformAgent_isRewardedVideoPlacementCapped_mE05CBA278B4DA3E9CF46405F8EDAF188269D7EBE (void);
// 0x0000057C IronSourcePlacement UnsupportedPlatformAgent::getPlacementInfo(System.String)
extern void UnsupportedPlatformAgent_getPlacementInfo_m4B11B182FF44D5F74CB819A07D26A5D7859EF303 (void);
// 0x0000057D System.Void UnsupportedPlatformAgent::setRewardedVideoServerParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void UnsupportedPlatformAgent_setRewardedVideoServerParams_m93FD58562D93AE669B4E01EB13A492D799F819A0 (void);
// 0x0000057E System.Void UnsupportedPlatformAgent::clearRewardedVideoServerParams()
extern void UnsupportedPlatformAgent_clearRewardedVideoServerParams_mC706C9C87110130861D851FBD0F12BCBC274B8F1 (void);
// 0x0000057F System.Void UnsupportedPlatformAgent::showISDemandOnlyRewardedVideo(System.String)
extern void UnsupportedPlatformAgent_showISDemandOnlyRewardedVideo_m5B209B423ACEDB077BEC2E36F9CA4E5D2A8A678B (void);
// 0x00000580 System.Void UnsupportedPlatformAgent::loadISDemandOnlyRewardedVideo(System.String)
extern void UnsupportedPlatformAgent_loadISDemandOnlyRewardedVideo_mC1D34F2BD041A797CD391C31DE431FCE71BEB686 (void);
// 0x00000581 System.Boolean UnsupportedPlatformAgent::isISDemandOnlyRewardedVideoAvailable(System.String)
extern void UnsupportedPlatformAgent_isISDemandOnlyRewardedVideoAvailable_mE407C75F4CBECF7BBBF96ED3E7A93916162A5E23 (void);
// 0x00000582 System.Void UnsupportedPlatformAgent::loadInterstitial()
extern void UnsupportedPlatformAgent_loadInterstitial_mEF0107BF78E0CE8BF2882E32310C837C9A728BC4 (void);
// 0x00000583 System.Void UnsupportedPlatformAgent::showInterstitial()
extern void UnsupportedPlatformAgent_showInterstitial_m7FC068E48CDFB16027F015BBEEB7D51C5F2395E6 (void);
// 0x00000584 System.Void UnsupportedPlatformAgent::showInterstitial(System.String)
extern void UnsupportedPlatformAgent_showInterstitial_m786089CBD50430D389F404CBC8187DF4D9421716 (void);
// 0x00000585 System.Boolean UnsupportedPlatformAgent::isInterstitialReady()
extern void UnsupportedPlatformAgent_isInterstitialReady_m2115D114C6DE84973CA33ACAB70BD0F1CEA0F8E4 (void);
// 0x00000586 System.Boolean UnsupportedPlatformAgent::isInterstitialPlacementCapped(System.String)
extern void UnsupportedPlatformAgent_isInterstitialPlacementCapped_mE8EB1F70953BBCEA68CF0B5152C413B6F8338426 (void);
// 0x00000587 System.Void UnsupportedPlatformAgent::loadISDemandOnlyInterstitial(System.String)
extern void UnsupportedPlatformAgent_loadISDemandOnlyInterstitial_m22EAE0BE41D7F0E2EB2BE5C2A50D5E0A0A939AA4 (void);
// 0x00000588 System.Void UnsupportedPlatformAgent::showISDemandOnlyInterstitial(System.String)
extern void UnsupportedPlatformAgent_showISDemandOnlyInterstitial_m74D8FB5E88E9C601C2B77AB2BC8261FB583823A8 (void);
// 0x00000589 System.Boolean UnsupportedPlatformAgent::isISDemandOnlyInterstitialReady(System.String)
extern void UnsupportedPlatformAgent_isISDemandOnlyInterstitialReady_m2C847F6F9CEC2F18890D03E9FC9CFDA33122FA69 (void);
// 0x0000058A System.Void UnsupportedPlatformAgent::showOfferwall()
extern void UnsupportedPlatformAgent_showOfferwall_m4028E72CE1EC7465FE41B6CDEECC552EE766DCC0 (void);
// 0x0000058B System.Void UnsupportedPlatformAgent::showOfferwall(System.String)
extern void UnsupportedPlatformAgent_showOfferwall_mB0FEBDC6CC1574E69C0F7538D777F893773E7E0E (void);
// 0x0000058C System.Void UnsupportedPlatformAgent::getOfferwallCredits()
extern void UnsupportedPlatformAgent_getOfferwallCredits_m4D447F02F71CDDCB5CF3CB9B783C04EEF7EA2FBA (void);
// 0x0000058D System.Boolean UnsupportedPlatformAgent::isOfferwallAvailable()
extern void UnsupportedPlatformAgent_isOfferwallAvailable_m38CEDEE28D5111170EB084AC37AE98A13E5C3D79 (void);
// 0x0000058E System.Void UnsupportedPlatformAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition)
extern void UnsupportedPlatformAgent_loadBanner_mC85FDCA0F8CF82C110EE69EB49520D7720DEDA58 (void);
// 0x0000058F System.Void UnsupportedPlatformAgent::loadBanner(IronSourceBannerSize,IronSourceBannerPosition,System.String)
extern void UnsupportedPlatformAgent_loadBanner_mC17481B70077C2DFC40718AE6B8225BA8078574E (void);
// 0x00000590 System.Void UnsupportedPlatformAgent::destroyBanner()
extern void UnsupportedPlatformAgent_destroyBanner_mCEE250A186ED8EAAF3BA7E0EC05B0A0A6B9E26CF (void);
// 0x00000591 System.Void UnsupportedPlatformAgent::displayBanner()
extern void UnsupportedPlatformAgent_displayBanner_m2D81508D492A2F1C07AA7935925820659FE2CF78 (void);
// 0x00000592 System.Void UnsupportedPlatformAgent::hideBanner()
extern void UnsupportedPlatformAgent_hideBanner_mEB1B66D23137D575BBD24E874EBE6153295D7065 (void);
// 0x00000593 System.Boolean UnsupportedPlatformAgent::isBannerPlacementCapped(System.String)
extern void UnsupportedPlatformAgent_isBannerPlacementCapped_mB4EB76D4DC15053F8B1C8321F9F640BF59458199 (void);
// 0x00000594 System.Void UnsupportedPlatformAgent::setSegment(IronSourceSegment)
extern void UnsupportedPlatformAgent_setSegment_m0570F63815B7DA0E6D6A0BF8626B299BB1F80A16 (void);
// 0x00000595 System.Void UnsupportedPlatformAgent::setConsent(System.Boolean)
extern void UnsupportedPlatformAgent_setConsent_mEC44C1C8759B255B214EC1DCC342D48D0AB5944A (void);
// 0x00000596 System.Void UnsupportedPlatformAgent::loadConsentViewWithType(System.String)
extern void UnsupportedPlatformAgent_loadConsentViewWithType_mEAF07F7B0A86AEFD3CAFF48BC0C7374906CAD6E4 (void);
// 0x00000597 System.Void UnsupportedPlatformAgent::showConsentViewWithType(System.String)
extern void UnsupportedPlatformAgent_showConsentViewWithType_m6891DBEDDF9665E70BE9B17172609032AE63D5A4 (void);
// 0x00000598 System.Void UnsupportedPlatformAgent::setAdRevenueData(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void UnsupportedPlatformAgent_setAdRevenueData_m2CB71B75BABEDD60372EB30D0FB469031C0DFF97 (void);
// 0x00000599 System.Void AdsControl::.ctor()
extern void AdsControl__ctor_m3B4E137BD5032EBA13A3CB658E93FEA58E2045B2 (void);
// 0x0000059A AdsControl AdsControl::get_Instance()
extern void AdsControl_get_Instance_m67FEBB609D578763D6D8F9EC96CC6EBB35ECF815 (void);
// 0x0000059B System.Void IAPManager::.ctor()
extern void IAPManager__ctor_mA8A65B484AE753D49283D3326A929A43B2EBFE1B (void);
// 0x0000059C System.Void TRAN_KHUONG_DUY.ClickedButton::Pressed()
extern void ClickedButton_Pressed_m6672B382E1A57B446E88F788B6654ABE8434EAB6 (void);
// 0x0000059D System.Void TRAN_KHUONG_DUY.ClickedButton::Released()
extern void ClickedButton_Released_m3779E4B3058322425B9A93AC70ABBA8219F5839C (void);
// 0x0000059E System.Void TRAN_KHUONG_DUY.ClickedButton::.ctor()
extern void ClickedButton__ctor_mBDB9C0F4CEA0330F48C2015C723D12805F30B847 (void);
// 0x0000059F System.Void TRAN_KHUONG_DUY.ConstantsInGame::.ctor()
extern void ConstantsInGame__ctor_m715D73CB85D86E949C74FDA1AA27B302A42DB92A (void);
// 0x000005A0 System.Void TRAN_KHUONG_DUY.FingerBehaviour::Start()
extern void FingerBehaviour_Start_m0993FA1DF147556CD51FE572785334E2811105E1 (void);
// 0x000005A1 System.Void TRAN_KHUONG_DUY.FingerBehaviour::Update()
extern void FingerBehaviour_Update_m694541BA98B18AC42CFD6E999D43C86206C45C1D (void);
// 0x000005A2 System.Void TRAN_KHUONG_DUY.FingerBehaviour::CheckDrawingString()
extern void FingerBehaviour_CheckDrawingString_m0FAF5F2AC3170BF12D26836394E1DF3ADFE194F1 (void);
// 0x000005A3 System.Void TRAN_KHUONG_DUY.FingerBehaviour::.ctor()
extern void FingerBehaviour__ctor_mFB68AFDB7291DF3559F3F8E6950F5F8369BF8AE7 (void);
// 0x000005A4 TRAN_KHUONG_DUY.GameManager TRAN_KHUONG_DUY.GameManager::get_Instance()
extern void GameManager_get_Instance_mFF43CEFAA8DD05AC28DC954832316583DB40ED37 (void);
// 0x000005A5 System.Void TRAN_KHUONG_DUY.GameManager::set_Instance(TRAN_KHUONG_DUY.GameManager)
extern void GameManager_set_Instance_mE2E0260BFD7623A07453124E2A25A64430A1602C (void);
// 0x000005A6 System.Void TRAN_KHUONG_DUY.GameManager::add_OnShowResults(TRAN_KHUONG_DUY.GameManager/ShowResults)
extern void GameManager_add_OnShowResults_m61B53094395BE13547639EB13CE23504C5FFF417 (void);
// 0x000005A7 System.Void TRAN_KHUONG_DUY.GameManager::remove_OnShowResults(TRAN_KHUONG_DUY.GameManager/ShowResults)
extern void GameManager_remove_OnShowResults_mA4542D9C64F67D9AFC95061F180786427B37B812 (void);
// 0x000005A8 System.Void TRAN_KHUONG_DUY.GameManager::add_OnHint(TRAN_KHUONG_DUY.GameManager/Hint)
extern void GameManager_add_OnHint_mDE03FCABEE43577C3BFF90718191F46A2F3D8A68 (void);
// 0x000005A9 System.Void TRAN_KHUONG_DUY.GameManager::remove_OnHint(TRAN_KHUONG_DUY.GameManager/Hint)
extern void GameManager_remove_OnHint_mC6F378E3E31B3E6E6A2260BD60FFAFA4FD222387 (void);
// 0x000005AA System.Void TRAN_KHUONG_DUY.GameManager::add_OnUndo(TRAN_KHUONG_DUY.GameManager/Undo)
extern void GameManager_add_OnUndo_mD5F8A710FF84EE19A70DF87F571A6327F69132DB (void);
// 0x000005AB System.Void TRAN_KHUONG_DUY.GameManager::remove_OnUndo(TRAN_KHUONG_DUY.GameManager/Undo)
extern void GameManager_remove_OnUndo_m93EC59D1CF52393C9179672049C6717C1D36F394 (void);
// 0x000005AC System.Int32 TRAN_KHUONG_DUY.GameManager::GetCoinValue()
extern void GameManager_GetCoinValue_mF276FCEA5AC789E74B6F220E022B4637457FF267 (void);
// 0x000005AD System.Int32 TRAN_KHUONG_DUY.GameManager::GetScoreValue()
extern void GameManager_GetScoreValue_mA06870D5B2290D412061C01318BF092DC193EE66 (void);
// 0x000005AE System.Boolean TRAN_KHUONG_DUY.GameManager::get_EndGame()
extern void GameManager_get_EndGame_m70D5BFEB9B697DF71ED6196B42C86807FD3972D8 (void);
// 0x000005AF System.Void TRAN_KHUONG_DUY.GameManager::set_EndGame(System.Boolean)
extern void GameManager_set_EndGame_mD7EAC8317EF8D2AA70AA7AED040F1998ED462E2D (void);
// 0x000005B0 System.Int32 TRAN_KHUONG_DUY.GameManager::get_BGMStatus()
extern void GameManager_get_BGMStatus_m2BE6B513BD8A149E10BC2C339326B8A53A2FDFF4 (void);
// 0x000005B1 System.Void TRAN_KHUONG_DUY.GameManager::set_BGMStatus(System.Int32)
extern void GameManager_set_BGMStatus_m36F8639FB0D8B28A2170CBF0D3DCF3E96C3B0A86 (void);
// 0x000005B2 System.Int32 TRAN_KHUONG_DUY.GameManager::get_SFXStatus()
extern void GameManager_get_SFXStatus_m9F985C08C88025BD8B04AB48BA61D6D8D103DAE1 (void);
// 0x000005B3 System.Void TRAN_KHUONG_DUY.GameManager::set_SFXStatus(System.Int32)
extern void GameManager_set_SFXStatus_m9639AEEA3960CE0466ACDFC75DD7B39DE010624D (void);
// 0x000005B4 GameServices TRAN_KHUONG_DUY.GameManager::GetGameServices()
extern void GameManager_GetGameServices_mA3523E31780C918FC16A72EDDE421A1DAF1DC28B (void);
// 0x000005B5 System.Void TRAN_KHUONG_DUY.GameManager::Awake()
extern void GameManager_Awake_m83273263F4E53E63CCB3A71E37AF95276A0113AF (void);
// 0x000005B6 System.Void TRAN_KHUONG_DUY.GameManager::OnEnable()
extern void GameManager_OnEnable_m728AE872D7850F32C3E1587113360B91B2DBCD1D (void);
// 0x000005B7 System.Void TRAN_KHUONG_DUY.GameManager::OnDisable()
extern void GameManager_OnDisable_m35F03FB5E9826875E864428957ABD7AB33D9B5E2 (void);
// 0x000005B8 System.Void TRAN_KHUONG_DUY.GameManager::Instance_AdsReward(System.String)
extern void GameManager_Instance_AdsReward_mA05EBC0D44A2558FFE6489953F93CFA6396659FA (void);
// 0x000005B9 System.Void TRAN_KHUONG_DUY.GameManager::Start()
extern void GameManager_Start_mE4356C55B4DF45C9C50BD159F7BF8E9B70A8F5F0 (void);
// 0x000005BA System.Void TRAN_KHUONG_DUY.GameManager::CheckSetup()
extern void GameManager_CheckSetup_m99D209DE7A976701C835979D5ECB3C18A2255D71 (void);
// 0x000005BB System.Void TRAN_KHUONG_DUY.GameManager::SettingsBtn_Onclick()
extern void GameManager_SettingsBtn_Onclick_mB6B47D0AECF752CD5EFD662478D3AB17DEA3F6A9 (void);
// 0x000005BC System.Void TRAN_KHUONG_DUY.GameManager::TurnOffCoinShop()
extern void GameManager_TurnOffCoinShop_m374F834E206F9A23D7DC10688B7CEDAA75D28641 (void);
// 0x000005BD System.Void TRAN_KHUONG_DUY.GameManager::MoreGameBtn_Onclick()
extern void GameManager_MoreGameBtn_Onclick_m061CC449908926D9BAD920A33A1593336AA4C767 (void);
// 0x000005BE System.Void TRAN_KHUONG_DUY.GameManager::ShopBtn_Onclick(System.Boolean)
extern void GameManager_ShopBtn_Onclick_m51C4CAF0D447571F24F2B9BE1324518A75EB5179 (void);
// 0x000005BF System.Void TRAN_KHUONG_DUY.GameManager::EmailBtn_Onclick()
extern void GameManager_EmailBtn_Onclick_m8B0EA8A7B192EEBBEC201329118FD1A321DC24EB (void);
// 0x000005C0 System.Void TRAN_KHUONG_DUY.GameManager::GameCenterBtn_Onclick()
extern void GameManager_GameCenterBtn_Onclick_m2BB2D10A23C216D344907E727E96B7EBE5C5A211 (void);
// 0x000005C1 System.Void TRAN_KHUONG_DUY.GameManager::BGMButton_Onclick()
extern void GameManager_BGMButton_Onclick_mCFD29C7587824FDA20095FE857A2CFE5283A698A (void);
// 0x000005C2 System.Void TRAN_KHUONG_DUY.GameManager::SFXButton_Onclick()
extern void GameManager_SFXButton_Onclick_m2FC3846AFBF81466D3F039B1FBD5D9A18420B2E4 (void);
// 0x000005C3 System.Void TRAN_KHUONG_DUY.GameManager::BackgroundButton_Onclick(System.Int32)
extern void GameManager_BackgroundButton_Onclick_m58D156DF91B1E77B0B52EAC7883739319156381D (void);
// 0x000005C4 System.Void TRAN_KHUONG_DUY.GameManager::PlayButton_Onclick()
extern void GameManager_PlayButton_Onclick_mFD750494E57EB6FD758265CA96D768653D5F5D8F (void);
// 0x000005C5 System.Void TRAN_KHUONG_DUY.GameManager::SpinWheelButton_OnClick()
extern void GameManager_SpinWheelButton_OnClick_m6D573061038EE4C9FD8721E2CDA9B1E58E1F1400 (void);
// 0x000005C6 System.Void TRAN_KHUONG_DUY.GameManager::BackToHomeButton_Onclick()
extern void GameManager_BackToHomeButton_Onclick_m212EC43E26D281A8E0302DA235B8533E5CD65650 (void);
// 0x000005C7 System.Void TRAN_KHUONG_DUY.GameManager::BackToHomeFromSetting()
extern void GameManager_BackToHomeFromSetting_m1EC30011B446387EF96F565A5B43A86558339001 (void);
// 0x000005C8 System.Void TRAN_KHUONG_DUY.GameManager::BackToHome()
extern void GameManager_BackToHome_mA158A9D4EA7C52DBB65F856CC4C070D4045EA681 (void);
// 0x000005C9 System.Void TRAN_KHUONG_DUY.GameManager::PackageButton_Onclick(System.String,System.String,TRAN_KHUONG_DUY.LevelContent[])
extern void GameManager_PackageButton_Onclick_mBF9C23087A577749AF46B252BC28D24A0A29C2DD (void);
// 0x000005CA System.Void TRAN_KHUONG_DUY.GameManager::BackToPackageButton_Onclick()
extern void GameManager_BackToPackageButton_Onclick_m091DFE8337E588B206B6E6FACB2BDFEDCACE65EC (void);
// 0x000005CB System.Void TRAN_KHUONG_DUY.GameManager::StartLevel(TRAN_KHUONG_DUY.CURRENTGROUP,System.String,System.Int32,System.String)
extern void GameManager_StartLevel_m62C9D4EB6D8EF6D378FD5DDA38867B7685F30A8C (void);
// 0x000005CC System.Void TRAN_KHUONG_DUY.GameManager::BackToLevelButton_Onclick()
extern void GameManager_BackToLevelButton_Onclick_mCCB981E86601F1692445C96D5BD6EE1331825B95 (void);
// 0x000005CD System.Void TRAN_KHUONG_DUY.GameManager::CheckResults(System.String)
extern void GameManager_CheckResults_m6C42D9AC462E8BAE120E453AB15DFA87885E3814 (void);
// 0x000005CE System.Collections.IEnumerator TRAN_KHUONG_DUY.GameManager::Finishing()
extern void GameManager_Finishing_m16FF31FC9763877CE5D997674D3DF12E6B14F8CB (void);
// 0x000005CF System.Void TRAN_KHUONG_DUY.GameManager::AddMoreStars(System.Int32)
extern void GameManager_AddMoreStars_mE2DF1703F0975D8F643FAD4B11264C904E3A5B0A (void);
// 0x000005D0 System.Void TRAN_KHUONG_DUY.GameManager::PrepareToShowResult()
extern void GameManager_PrepareToShowResult_m401FC28CD4D73A961A3589AFA5978FFE5262402E (void);
// 0x000005D1 System.Void TRAN_KHUONG_DUY.GameManager::NextBtn_Onclick()
extern void GameManager_NextBtn_Onclick_mD295CBA171805780907A81DB39BC0FF7771BC348 (void);
// 0x000005D2 System.Void TRAN_KHUONG_DUY.GameManager::Reshuffle()
extern void GameManager_Reshuffle_mDF494C9B1F003E50DA74F87CC2122FF80773772C (void);
// 0x000005D3 System.Void TRAN_KHUONG_DUY.GameManager::UndoBtn_Onclick()
extern void GameManager_UndoBtn_Onclick_m861C98379C10F7FB48E06D4C9F0244ED9532A027 (void);
// 0x000005D4 System.Void TRAN_KHUONG_DUY.GameManager::Update()
extern void GameManager_Update_m6690E419C0D4F81E8FC91E7CCBE5C56720AC58AC (void);
// 0x000005D5 System.Void TRAN_KHUONG_DUY.GameManager::ResetBtn_Onclick()
extern void GameManager_ResetBtn_Onclick_m53C825D52BF7FDC06358B38FABF7548918B62598 (void);
// 0x000005D6 System.Void TRAN_KHUONG_DUY.GameManager::FreeBtn_Onclick()
extern void GameManager_FreeBtn_Onclick_m95E700315932DEBB8749729B01745771DFA46B09 (void);
// 0x000005D7 System.Collections.IEnumerator TRAN_KHUONG_DUY.GameManager::AdsCountdown()
extern void GameManager_AdsCountdown_mA882DC01772EBD01ECF4AAFBDF2036648E579490 (void);
// 0x000005D8 System.Void TRAN_KHUONG_DUY.GameManager::HintBtn_Onclick()
extern void GameManager_HintBtn_Onclick_m1F3390180FA04818F139683A827BA36D20E31FF8 (void);
// 0x000005D9 System.Void TRAN_KHUONG_DUY.GameManager::MoreCoin(System.Int32)
extern void GameManager_MoreCoin_m96D1934B1E8386F0B999A42BCE1DD3DC30144F2D (void);
// 0x000005DA System.Void TRAN_KHUONG_DUY.GameManager::LevelBtn_Onclick()
extern void GameManager_LevelBtn_Onclick_m18017CC8655D87BA1DE67815811076ADFC5D1D6D (void);
// 0x000005DB System.Void TRAN_KHUONG_DUY.GameManager::ReplayBtn_Onclick()
extern void GameManager_ReplayBtn_Onclick_mC8D3BEC2FF973E41BDA05DA8ECB14E1607508239 (void);
// 0x000005DC System.Void TRAN_KHUONG_DUY.GameManager::NextBtn2_Onclick()
extern void GameManager_NextBtn2_Onclick_m9F40CF492F61CC3F542EBEECF2F2A7D1065CF252 (void);
// 0x000005DD System.Void TRAN_KHUONG_DUY.GameManager::ResetGame()
extern void GameManager_ResetGame_m0F34F856D0E4177EDDA1D6365BC5010A00BF0B4E (void);
// 0x000005DE System.Void TRAN_KHUONG_DUY.GameManager::SetCoinValue(System.Int32)
extern void GameManager_SetCoinValue_m7BB3E359AD998B3F29031CCFF1C378F237B57B59 (void);
// 0x000005DF System.Void TRAN_KHUONG_DUY.GameManager::LoadData(packageManager/PackageLevel)
extern void GameManager_LoadData_m11B0D99E053EB2FDC8F1BD1D59980D99F57EFB60 (void);
// 0x000005E0 System.Void TRAN_KHUONG_DUY.GameManager::LoadData(System.String)
extern void GameManager_LoadData_mCEE5E3CA29E86482A71DF7ABF0B8282A67E7FDAC (void);
// 0x000005E1 System.Void TRAN_KHUONG_DUY.GameManager::.ctor()
extern void GameManager__ctor_mA77D6031CDC66527C11386004CF37C8DC8D33549 (void);
// 0x000005E2 System.Void TRAN_KHUONG_DUY.GameManager/ShowResults::.ctor(System.Object,System.IntPtr)
extern void ShowResults__ctor_mC11277F074C6464789BC1E5FCAE134CE494AD19B (void);
// 0x000005E3 System.Void TRAN_KHUONG_DUY.GameManager/ShowResults::Invoke(System.String)
extern void ShowResults_Invoke_m0DAB539AAC15ED81DEF361CB4B7E3D6F9202A926 (void);
// 0x000005E4 System.IAsyncResult TRAN_KHUONG_DUY.GameManager/ShowResults::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ShowResults_BeginInvoke_m7D9294481FB6A5FED7AAEC9BE7609E0788DB731F (void);
// 0x000005E5 System.Void TRAN_KHUONG_DUY.GameManager/ShowResults::EndInvoke(System.IAsyncResult)
extern void ShowResults_EndInvoke_mEA58240266B7C7A1644563D4C71D1BDF8FCA77E6 (void);
// 0x000005E6 System.Void TRAN_KHUONG_DUY.GameManager/Hint::.ctor(System.Object,System.IntPtr)
extern void Hint__ctor_m4A245A2E8CDCEC35CBC57A776A17492E51F6496C (void);
// 0x000005E7 System.Void TRAN_KHUONG_DUY.GameManager/Hint::Invoke()
extern void Hint_Invoke_m0748B11D7813F833B169C903D973FDA7B4584F9B (void);
// 0x000005E8 System.IAsyncResult TRAN_KHUONG_DUY.GameManager/Hint::BeginInvoke(System.AsyncCallback,System.Object)
extern void Hint_BeginInvoke_m085C03474A6081E92E9F836D14CCAF22DF46E49A (void);
// 0x000005E9 System.Void TRAN_KHUONG_DUY.GameManager/Hint::EndInvoke(System.IAsyncResult)
extern void Hint_EndInvoke_mB2689A0C58023A0EF28DB332B649D456B050A383 (void);
// 0x000005EA System.Void TRAN_KHUONG_DUY.GameManager/Undo::.ctor(System.Object,System.IntPtr)
extern void Undo__ctor_m13025DA3077EC422DAE0C63659823957FA9D126D (void);
// 0x000005EB System.Boolean TRAN_KHUONG_DUY.GameManager/Undo::Invoke()
extern void Undo_Invoke_mBA15DE1C9CC3E86BE1ADE4B8F9BAF11E7A366EE0 (void);
// 0x000005EC System.IAsyncResult TRAN_KHUONG_DUY.GameManager/Undo::BeginInvoke(System.AsyncCallback,System.Object)
extern void Undo_BeginInvoke_mA1E8CDDE7ECA6F7B5CD2E806A93985FDB4015292 (void);
// 0x000005ED System.Boolean TRAN_KHUONG_DUY.GameManager/Undo::EndInvoke(System.IAsyncResult)
extern void Undo_EndInvoke_m12A4BEFF722F713F6F10C95171154F408143436D (void);
// 0x000005EE System.Void TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::.ctor(System.Int32)
extern void U3CFinishingU3Ed__128__ctor_mC186DB5CBD25F91F512FF5EE1B5FFD35852715FA (void);
// 0x000005EF System.Void TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::System.IDisposable.Dispose()
extern void U3CFinishingU3Ed__128_System_IDisposable_Dispose_mB0E7522B331A366DB1108D98D16CEF82189A4897 (void);
// 0x000005F0 System.Boolean TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::MoveNext()
extern void U3CFinishingU3Ed__128_MoveNext_m39ADBC8A5A373644ADB2CA44ADC369CC01687B9E (void);
// 0x000005F1 System.Object TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFinishingU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B82B678020C8B97C5E10B24E4E624C74862D445 (void);
// 0x000005F2 System.Void TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::System.Collections.IEnumerator.Reset()
extern void U3CFinishingU3Ed__128_System_Collections_IEnumerator_Reset_m8C81EF676183FE0D07A35A5C12ECDB933C3BA079 (void);
// 0x000005F3 System.Object TRAN_KHUONG_DUY.GameManager/<Finishing>d__128::System.Collections.IEnumerator.get_Current()
extern void U3CFinishingU3Ed__128_System_Collections_IEnumerator_get_Current_m5649F9632B3F95C905EDB667872694C3E8667F2D (void);
// 0x000005F4 System.Void TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::.ctor(System.Int32)
extern void U3CAdsCountdownU3Ed__137__ctor_m78FF6A5E90FEB51C9E3AA4720642CCB6FA7F44FA (void);
// 0x000005F5 System.Void TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::System.IDisposable.Dispose()
extern void U3CAdsCountdownU3Ed__137_System_IDisposable_Dispose_m9C52EEACC112F106B71D7D8BEAEB3C90614CEC3B (void);
// 0x000005F6 System.Boolean TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::MoveNext()
extern void U3CAdsCountdownU3Ed__137_MoveNext_mA03F1428D658B5E882D81DD1758423F75D6A8926 (void);
// 0x000005F7 System.Object TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAdsCountdownU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD5A5A6DA1C710CE14C726987AD2C79C92D6546A (void);
// 0x000005F8 System.Void TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::System.Collections.IEnumerator.Reset()
extern void U3CAdsCountdownU3Ed__137_System_Collections_IEnumerator_Reset_m5D30870097FD28FFAEB0228F03ECB4111633B87B (void);
// 0x000005F9 System.Object TRAN_KHUONG_DUY.GameManager/<AdsCountdown>d__137::System.Collections.IEnumerator.get_Current()
extern void U3CAdsCountdownU3Ed__137_System_Collections_IEnumerator_get_Current_m05D27619178E258D881D7D0C01A2E4545AB1FB29 (void);
// 0x000005FA System.Void TRAN_KHUONG_DUY.Packages::.ctor()
extern void Packages__ctor_mACC8AAF216F0A509F5E744E0D5132FE6B3A9BE91 (void);
// 0x000005FB System.Void TRAN_KHUONG_DUY.PackageLevels::.ctor()
extern void PackageLevels__ctor_m89A881DBA35D076277D41144EB7273F05A7CCAB6 (void);
// 0x000005FC T[] TRAN_KHUONG_DUY.JsonHelper::FromJson(System.String)
// 0x000005FD System.String TRAN_KHUONG_DUY.JsonHelper::ToJson(T[])
// 0x000005FE System.String TRAN_KHUONG_DUY.JsonHelper::ToJson(T[],System.Boolean)
// 0x000005FF System.Void TRAN_KHUONG_DUY.JsonHelper/Wrapper`1::.ctor()
// 0x00000600 UnityEngine.Vector3 TRAN_KHUONG_DUY.LetterScript::get_Destination()
extern void LetterScript_get_Destination_m423DEA0555A2A0EE53714AFB9345EDAD2D5335C5 (void);
// 0x00000601 System.Void TRAN_KHUONG_DUY.LetterScript::set_Destination(UnityEngine.Vector3)
extern void LetterScript_set_Destination_m89A873B230BDA58EC8C26F477BA51853E98B5508 (void);
// 0x00000602 System.Single TRAN_KHUONG_DUY.LetterScript::get_OffsetDistanceOnAxis_X()
extern void LetterScript_get_OffsetDistanceOnAxis_X_m31AA0CA3F4E1F980D043351AC9A7A11B0D89C3B6 (void);
// 0x00000603 System.Void TRAN_KHUONG_DUY.LetterScript::set_OffsetDistanceOnAxis_X(System.Single)
extern void LetterScript_set_OffsetDistanceOnAxis_X_mE070F0865855090A609E2F5E68B0E1CC8749006D (void);
// 0x00000604 System.Boolean TRAN_KHUONG_DUY.LetterScript::get_IsPicking()
extern void LetterScript_get_IsPicking_mD2560AB8A300129340858446C473A5EB4E559D5D (void);
// 0x00000605 System.Void TRAN_KHUONG_DUY.LetterScript::set_IsPicking(System.Boolean)
extern void LetterScript_set_IsPicking_m7915AF8E13737AAFEBD6B991D9DF8DB8773456E7 (void);
// 0x00000606 System.Boolean TRAN_KHUONG_DUY.LetterScript::get_IsSelected()
extern void LetterScript_get_IsSelected_m34378399E161B8D75CB212DA60D6363FC30A8224 (void);
// 0x00000607 System.Void TRAN_KHUONG_DUY.LetterScript::set_IsSelected(System.Boolean)
extern void LetterScript_set_IsSelected_m58064D7E92E9BBEC18C2DEAF0BF1B7647607F2C8 (void);
// 0x00000608 System.Boolean TRAN_KHUONG_DUY.LetterScript::get_IsBouncing()
extern void LetterScript_get_IsBouncing_mD313CC4E07404C4C62F9E28E37EF3EC0ECB1D987 (void);
// 0x00000609 System.Void TRAN_KHUONG_DUY.LetterScript::set_IsBouncing(System.Boolean)
extern void LetterScript_set_IsBouncing_m88629307A8200FF30B60EFD7C45F4A0DB0E8818C (void);
// 0x0000060A System.Boolean TRAN_KHUONG_DUY.LetterScript::get_Undo()
extern void LetterScript_get_Undo_m8818C62CB0B1DB731BAD454A705B056DEECDE638 (void);
// 0x0000060B System.Void TRAN_KHUONG_DUY.LetterScript::set_Undo(System.Boolean)
extern void LetterScript_set_Undo_m10AC0C21C7EAAB31255E95CB53D002047A251D96 (void);
// 0x0000060C System.Boolean TRAN_KHUONG_DUY.LetterScript::get_IsMovingHorizontal()
extern void LetterScript_get_IsMovingHorizontal_mA05CE2B5658B02B62995165461C036B8A34F1028 (void);
// 0x0000060D System.String TRAN_KHUONG_DUY.LetterScript::get_Alphabet()
extern void LetterScript_get_Alphabet_m26D0CACA0A908ABFBE90136D4EB5E98A2742D151 (void);
// 0x0000060E System.Void TRAN_KHUONG_DUY.LetterScript::OnEnable()
extern void LetterScript_OnEnable_m57702334F8F5014B8B1723CDE15B3120B2FEF528 (void);
// 0x0000060F System.Void TRAN_KHUONG_DUY.LetterScript::CheckTheme()
extern void LetterScript_CheckTheme_m0837D7858EF55AD54BBE7600CF85857B2BC47203 (void);
// 0x00000610 System.Void TRAN_KHUONG_DUY.LetterScript::CheckLetterAbove()
extern void LetterScript_CheckLetterAbove_mFC62E8320221913772231CBA1B810FCF9E7B6886 (void);
// 0x00000611 System.Void TRAN_KHUONG_DUY.LetterScript::OnDisable()
extern void LetterScript_OnDisable_mBB750F0B844F853759159CEE5553062180E1F0A1 (void);
// 0x00000612 System.Void TRAN_KHUONG_DUY.LetterScript::Start()
extern void LetterScript_Start_m6FEA155A30C117EB84A63A2B95DCB478C92FC5FF (void);
// 0x00000613 System.Void TRAN_KHUONG_DUY.LetterScript::Update()
extern void LetterScript_Update_mD94E1456E8300C7F2A038CA70E824C928CBE26A1 (void);
// 0x00000614 System.Void TRAN_KHUONG_DUY.LetterScript::Appear()
extern void LetterScript_Appear_m9EB79BE2203F6DF24049A907598715DD8A9C30DD (void);
// 0x00000615 System.Void TRAN_KHUONG_DUY.LetterScript::Set(TRAN_KHUONG_DUY.Letter)
extern void LetterScript_Set_m5EC55B30864E9BDEAF2B6D480621228CF4CE6207 (void);
// 0x00000616 System.Void TRAN_KHUONG_DUY.LetterScript::HighLightLetter()
extern void LetterScript_HighLightLetter_m278332F59D9D87D9E057BC3D3C4EA28BD798A9F1 (void);
// 0x00000617 System.Void TRAN_KHUONG_DUY.LetterScript::NormalLetter()
extern void LetterScript_NormalLetter_m62A94D5AD20A6D2C91AF137BFA21458208FE5390 (void);
// 0x00000618 System.Void TRAN_KHUONG_DUY.LetterScript::ChangeState(System.Boolean)
extern void LetterScript_ChangeState_m3D0E8E3DD3857A06240D833D9657CDCE8580D8D6 (void);
// 0x00000619 System.Boolean TRAN_KHUONG_DUY.LetterScript::CheckValidity(UnityEngine.Collider2D)
extern void LetterScript_CheckValidity_m2657E051302DC77A90D1B965E3214B37E1E42921 (void);
// 0x0000061A System.Void TRAN_KHUONG_DUY.LetterScript::Bound()
extern void LetterScript_Bound_m24EBA9AB841E509C7AC25D5EEC30CD86A98FCE70 (void);
// 0x0000061B System.Void TRAN_KHUONG_DUY.LetterScript::FallingDown()
extern void LetterScript_FallingDown_mB976412474CE8075DD0E42A5C20B267FE2D6A9DA (void);
// 0x0000061C System.Void TRAN_KHUONG_DUY.LetterScript::.ctor()
extern void LetterScript__ctor_mB03A25E28FD05A455A353C180CD0B8E589444B70 (void);
// 0x0000061D System.Void TRAN_KHUONG_DUY.LevelGenerator::InitEssentialArrays()
extern void LevelGenerator_InitEssentialArrays_m22EE088CE2CF1F05FBE68D825D46959AC2D9133C (void);
// 0x0000061E System.Void TRAN_KHUONG_DUY.LevelGenerator::InitLevel(TRAN_KHUONG_DUY.CURRENTGROUP,System.String,System.String[])
extern void LevelGenerator_InitLevel_mFBDABC7458B586E15815E01FAA94424C93351B99 (void);
// 0x0000061F System.Void TRAN_KHUONG_DUY.LevelGenerator::LoadHintData(TRAN_KHUONG_DUY.MiniLetterScript[])
extern void LevelGenerator_LoadHintData_mAEB8211D46BF771D2B183C78DFD93404998833F3 (void);
// 0x00000620 System.Void TRAN_KHUONG_DUY.LevelGenerator::CreateNewWordInDictionary()
extern void LevelGenerator_CreateNewWordInDictionary_mC9A97EE795223AD20B29DD6F8D141F4CC19C0545 (void);
// 0x00000621 System.Void TRAN_KHUONG_DUY.LevelGenerator::ShowLetters(TRAN_KHUONG_DUY.LetterScript[])
extern void LevelGenerator_ShowLetters_m12807F757DCAC730B188788C9B2D99ABBEA481DD (void);
// 0x00000622 System.Void TRAN_KHUONG_DUY.LevelGenerator::GenerateLevel(TRAN_KHUONG_DUY.CURRENTGROUP,System.Boolean&,System.String,UnityEngine.SpriteRenderer[],TRAN_KHUONG_DUY.MiniLetterScript[])
extern void LevelGenerator_GenerateLevel_mD3C2E766C0EFD0C5FA25867AA43452D82EDF1648 (void);
// 0x00000623 System.Void TRAN_KHUONG_DUY.LevelGenerator::GenerateMainSection(System.Int32,TRAN_KHUONG_DUY.CURRENTGROUP,System.Int32,System.Single&)
extern void LevelGenerator_GenerateMainSection_m931F6EDC1120FE2AE8E93F08A5EDAB224B8267B7 (void);
// 0x00000624 TRAN_KHUONG_DUY.Letter[] TRAN_KHUONG_DUY.LevelGenerator::SelectAlphabetForLetter(System.Int32,TRAN_KHUONG_DUY.LetterScript[]&)
extern void LevelGenerator_SelectAlphabetForLetter_m9C12A5EC4D26072EDEF5F1226F77697063B8F0E4 (void);
// 0x00000625 System.Void TRAN_KHUONG_DUY.LevelGenerator::GenerateMiniSection(UnityEngine.SpriteRenderer[],TRAN_KHUONG_DUY.MiniLetterScript[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void LevelGenerator_GenerateMiniSection_m4F7DB19E3A82FFC4F442BD76AC3CE6AB823F9738 (void);
// 0x00000626 System.Void TRAN_KHUONG_DUY.LevelGenerator::ArrangeMiniSection(TRAN_KHUONG_DUY.Letter[],UnityEngine.SpriteRenderer[],TRAN_KHUONG_DUY.MiniLetterScript[],UnityEngine.Sprite,UnityEngine.Sprite,System.Single,UnityEngine.Transform)
extern void LevelGenerator_ArrangeMiniSection_mFF5CACB47ED7A39E702E1AA35451591EFF6A146D (void);
// 0x00000627 System.Void TRAN_KHUONG_DUY.LevelGenerator::OnEnable()
extern void LevelGenerator_OnEnable_m775644E8CEEB8212C660DA8C3436B6E7559BA469 (void);
// 0x00000628 System.Void TRAN_KHUONG_DUY.LevelGenerator::OnDisable()
extern void LevelGenerator_OnDisable_mAF8FBC5DF0ECEDB8A251C54F5FBA958C0B3F81F8 (void);
// 0x00000629 System.Void TRAN_KHUONG_DUY.LevelGenerator::ShowResults(System.String)
extern void LevelGenerator_ShowResults_m72622F93E1E924FAD528ED32AAF6215E8D953696 (void);
// 0x0000062A System.Void TRAN_KHUONG_DUY.LevelGenerator::ShowHint()
extern void LevelGenerator_ShowHint_mA782412416965B128ABF10101888BAB34C820F87 (void);
// 0x0000062B System.Boolean TRAN_KHUONG_DUY.LevelGenerator::Undo()
extern void LevelGenerator_Undo_m408EEEDA4832A17E9080FCA97556AB497FB9F653 (void);
// 0x0000062C System.Void TRAN_KHUONG_DUY.LevelGenerator::RemoveUndoActions()
extern void LevelGenerator_RemoveUndoActions_m3AEA330A2A9C072FB9E583B9AC9603EFA7343B43 (void);
// 0x0000062D System.Void TRAN_KHUONG_DUY.LevelGenerator::CloseLevel()
extern void LevelGenerator_CloseLevel_m67D7B6A8F666A2A04C9431579E59E22EF5D30839 (void);
// 0x0000062E System.Void TRAN_KHUONG_DUY.LevelGenerator::.ctor()
extern void LevelGenerator__ctor_mDE485869E3B183CF4708A8D6A042C3933E95A441 (void);
// 0x0000062F System.Void TRAN_KHUONG_DUY.LetterGroups::.ctor()
extern void LetterGroups__ctor_mB1AFF3FA6BD6604339C2A1E1AB66D2CA77BA173E (void);
// 0x00000630 System.Void TRAN_KHUONG_DUY.WordInDictionary::.ctor()
extern void WordInDictionary__ctor_m1325ECAB116AE749774AF041A72A9A27F0482D45 (void);
// 0x00000631 System.Void TRAN_KHUONG_DUY.UndoAction::.ctor()
extern void UndoAction__ctor_m968C2C1E4DB73DBA0AD378BB98AB0F4EADA5361F (void);
// 0x00000632 System.Void TRAN_KHUONG_DUY.LevelScript::Start()
extern void LevelScript_Start_m755921CA4933193F9C1140E0FEE93800C1B667C3 (void);
// 0x00000633 System.Void TRAN_KHUONG_DUY.LevelScript::SetUp(System.Int32,System.Int32,TRAN_KHUONG_DUY.CURRENTGROUP,System.String)
extern void LevelScript_SetUp_m82E3D91FE28C799ED30A54C40E54A0818FAB39FB (void);
// 0x00000634 System.Void TRAN_KHUONG_DUY.LevelScript::PrepareToPlay(System.Int32)
extern void LevelScript_PrepareToPlay_m0A7991BDCFA83B22BD0F36D37DB1268B2AF91E14 (void);
// 0x00000635 System.Void TRAN_KHUONG_DUY.LevelScript::UpdateInformation(System.Int32,System.Int32)
extern void LevelScript_UpdateInformation_m326D6D72603BA36B0DBBE5D410E1E59BE5AC971C (void);
// 0x00000636 System.Void TRAN_KHUONG_DUY.LevelScript::.ctor()
extern void LevelScript__ctor_m83FD9EE4B2C9F309B2A2C0E6843C0D8AB42E46A6 (void);
// 0x00000637 System.Char TRAN_KHUONG_DUY.MiniLetterScript::get_Alphabet()
extern void MiniLetterScript_get_Alphabet_mAF9BACA4601F5803C349D3EFF0D830748BC4E9CE (void);
// 0x00000638 System.Void TRAN_KHUONG_DUY.MiniLetterScript::set_Alphabet(System.Char)
extern void MiniLetterScript_set_Alphabet_mAA1DCC62EC0C7C224E92199D117E4A584E1F62D0 (void);
// 0x00000639 System.Boolean TRAN_KHUONG_DUY.MiniLetterScript::get_IsHint()
extern void MiniLetterScript_get_IsHint_m67301755D5D1DC612E7C4B49D1DDFE5FCB3E96EC (void);
// 0x0000063A System.Void TRAN_KHUONG_DUY.MiniLetterScript::set_IsHint(System.Boolean)
extern void MiniLetterScript_set_IsHint_m6D32C56EDD188771042F7F34159E55A92429168B (void);
// 0x0000063B System.Void TRAN_KHUONG_DUY.MiniLetterScript::OnEnable()
extern void MiniLetterScript_OnEnable_m22550F7A2485555A4261D7FD8A564F305DB05AEB (void);
// 0x0000063C System.Void TRAN_KHUONG_DUY.MiniLetterScript::SpriteIsNull()
extern void MiniLetterScript_SpriteIsNull_m6E4C41717B5D572171F1FB738895B268645ABCCD (void);
// 0x0000063D System.Void TRAN_KHUONG_DUY.MiniLetterScript::SetLetterSprite(UnityEngine.Sprite)
extern void MiniLetterScript_SetLetterSprite_m751FBE102A08F55611D4A18A8E3F9B3771701A34 (void);
// 0x0000063E System.Void TRAN_KHUONG_DUY.MiniLetterScript::PlayHintAnimation()
extern void MiniLetterScript_PlayHintAnimation_m67F0EC107CC023262DB182EE99DBB90762E0931F (void);
// 0x0000063F System.Void TRAN_KHUONG_DUY.MiniLetterScript::PlayResultAnimation(System.Single)
extern void MiniLetterScript_PlayResultAnimation_mFD44C034C462994341ED9F56069073DF90A49B55 (void);
// 0x00000640 System.Void TRAN_KHUONG_DUY.MiniLetterScript::PlayResult()
extern void MiniLetterScript_PlayResult_m7E43C28A0E525F0EA611F3E460E4A35CB943FF97 (void);
// 0x00000641 System.Void TRAN_KHUONG_DUY.MiniLetterScript::.ctor()
extern void MiniLetterScript__ctor_m7C3E98B46B7AF49011F9A7A8CECD6B7C2D58371A (void);
// 0x00000642 System.Void TRAN_KHUONG_DUY.Package::Start()
extern void Package_Start_m05456B7C9891A342275E8A2EB5FC0890060D7F60 (void);
// 0x00000643 System.Void TRAN_KHUONG_DUY.Package::CheckPackage()
extern void Package_CheckPackage_m9C5E60D70DE3E99CB692E2C8CEC68A22DBD13D62 (void);
// 0x00000644 System.Void TRAN_KHUONG_DUY.Package::OnEnable()
extern void Package_OnEnable_m0A25FFE1A7A903F91A668F7457112243134348D1 (void);
// 0x00000645 System.Void TRAN_KHUONG_DUY.Package::LoadData()
extern void Package_LoadData_mB6A2264733EECE58EFBD00D7FAA974D54D3C19BC (void);
// 0x00000646 System.Void TRAN_KHUONG_DUY.Package::PackageButton_Onclick()
extern void Package_PackageButton_Onclick_mFAA50383AD79CFB324560A29447C310F5BE7395C (void);
// 0x00000647 System.Void TRAN_KHUONG_DUY.Package::.ctor()
extern void Package__ctor_m98B90CE4A5721AF1A174AB7A21679E787C007D6A (void);
// 0x00000648 System.Void TRAN_KHUONG_DUY.LevelStatus::.ctor(System.Int32)
extern void LevelStatus__ctor_mDD0D80D4EE2D69B62BFACDF2C40F743F519BA891 (void);
// 0x00000649 System.Void TRAN_KHUONG_DUY.LevelContent::.ctor()
extern void LevelContent__ctor_mED5A4FD44C9E6A0A859A178996C76D318F57101E (void);
// 0x0000064A TRAN_KHUONG_DUY.SoundManager TRAN_KHUONG_DUY.SoundManager::get_Instance()
extern void SoundManager_get_Instance_mCC7EE761B79A602131C88064AA2D4AFEAD4B91F5 (void);
// 0x0000064B System.Void TRAN_KHUONG_DUY.SoundManager::set_Instance(TRAN_KHUONG_DUY.SoundManager)
extern void SoundManager_set_Instance_m14C1BD8449BBCB59FFAB0AD31D4A33475BE8C899 (void);
// 0x0000064C System.Void TRAN_KHUONG_DUY.SoundManager::Awake()
extern void SoundManager_Awake_mE86193E80847AFADE46D84BD7506A19402E50CAE (void);
// 0x0000064D System.Void TRAN_KHUONG_DUY.SoundManager::.ctor()
extern void SoundManager__ctor_m0406710AE0BEE3838B9A026C0B122017D39F7DE2 (void);
// 0x0000064E System.Void TRAN_KHUONG_DUY.WordsNumberInLevel::.ctor()
extern void WordsNumberInLevel__ctor_m4612ADD9D6D7A2B88E7ADA0AF9214DDDC1A261C3 (void);
// 0x0000064F System.Void TRAN_KHUONG_DUY.OrderOfLetters::.ctor()
extern void OrderOfLetters__ctor_m5FA667584720B51C9B411A2615150669E511626E (void);
// 0x00000650 System.Void TRAN_KHUONG_DUY.Words::.ctor()
extern void Words__ctor_m0E6B2965FD6A1B8178FE197276F26F385F6C36F2 (void);
// 0x00000651 System.String TRAN_KHUONG_DUY.Letter::get_Alphabet()
extern void Letter_get_Alphabet_mFCDE8C49726E815ECB83D1E41C2E2F188FE5EF56 (void);
// 0x00000652 System.Void TRAN_KHUONG_DUY.Letter::set_Alphabet(System.String)
extern void Letter_set_Alphabet_m7D597FCFD8F39F11BC93B32E1A4A0F47CE36DA05 (void);
// 0x00000653 System.Void TRAN_KHUONG_DUY.Letter::.ctor()
extern void Letter__ctor_mD2B1B65CFE4B1EDE6F162048CFB6D446FD4F6DDF (void);
// 0x00000654 System.Boolean EasyUI.PickerWheelUI.PickerWheel::get_IsSpinning()
extern void PickerWheel_get_IsSpinning_m8566FB6996408B59818B6A59242E03349455E7E0 (void);
// 0x00000655 System.Void EasyUI.PickerWheelUI.PickerWheel::Start()
extern void PickerWheel_Start_mF70DA1A610F8648971E2F62D10C6F7B2E13E8F9C (void);
// 0x00000656 System.Void EasyUI.PickerWheelUI.PickerWheel::SetupAudio()
extern void PickerWheel_SetupAudio_m6C41A2E64D8C9E236C3BAF61BD5FAB1789DD10C7 (void);
// 0x00000657 System.Void EasyUI.PickerWheelUI.PickerWheel::Generate()
extern void PickerWheel_Generate_m0A1D2E636CD26A4CDC9F9F36883A720D381B9290 (void);
// 0x00000658 System.Void EasyUI.PickerWheelUI.PickerWheel::DrawPiece(System.Int32)
extern void PickerWheel_DrawPiece_mE4D45F862EC1463EEC4E548228B9977770B5E3DB (void);
// 0x00000659 UnityEngine.GameObject EasyUI.PickerWheelUI.PickerWheel::InstantiatePiece()
extern void PickerWheel_InstantiatePiece_mED11A91F893DD308A0A85F9C6E90E39587FFF09B (void);
// 0x0000065A System.Void EasyUI.PickerWheelUI.PickerWheel::Spin()
extern void PickerWheel_Spin_m62C45E943DCE976B38A38E1FE9E628DEEB23FF22 (void);
// 0x0000065B System.Void EasyUI.PickerWheelUI.PickerWheel::FixedUpdate()
extern void PickerWheel_FixedUpdate_mAFCE99193C86C11AE2C4713D67EEB3D515AFE462 (void);
// 0x0000065C System.Void EasyUI.PickerWheelUI.PickerWheel::OnSpinStart(UnityEngine.Events.UnityAction)
extern void PickerWheel_OnSpinStart_m58DA7AB899C52BEC398A6C67B5F368BECE58CE5C (void);
// 0x0000065D System.Void EasyUI.PickerWheelUI.PickerWheel::OnSpinEnd(UnityEngine.Events.UnityAction`1<EasyUI.PickerWheelUI.WheelPiece>)
extern void PickerWheel_OnSpinEnd_m42B4A69CB04854C24996F0C32C94A7115DE97F66 (void);
// 0x0000065E System.Int32 EasyUI.PickerWheelUI.PickerWheel::GetRandomPieceIndex()
extern void PickerWheel_GetRandomPieceIndex_m6FB4D76132EF2AD2B7F3004AB1A318EBC20A7112 (void);
// 0x0000065F System.Void EasyUI.PickerWheelUI.PickerWheel::CalculateWeightsAndIndices()
extern void PickerWheel_CalculateWeightsAndIndices_m096196FE5B0031709A884BD792911A0D79973FEE (void);
// 0x00000660 System.Void EasyUI.PickerWheelUI.PickerWheel::OnValidate()
extern void PickerWheel_OnValidate_m9B2518985190068EF0FD9130C0A083F869F2CBED (void);
// 0x00000661 System.Void EasyUI.PickerWheelUI.PickerWheel::.ctor()
extern void PickerWheel__ctor_mAD463207B32A3D5498D2EED4E8B3FE9AAFC32F8F (void);
// 0x00000662 System.Void EasyUI.PickerWheelUI.PickerWheel/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m311E100160BFC87F2EEF7091A159F3724A58DA2A (void);
// 0x00000663 System.Void EasyUI.PickerWheelUI.PickerWheel/<>c__DisplayClass34_0::<Spin>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CSpinU3Eb__0_m6D318D6918290275BB1111ED6C0B15116EBB00A7 (void);
// 0x00000664 System.Void EasyUI.PickerWheelUI.PickerWheel/<>c__DisplayClass34_0::<Spin>b__1()
extern void U3CU3Ec__DisplayClass34_0_U3CSpinU3Eb__1_m95D3C94EC381916690AF2B0D4E672EAD4003FFDD (void);
// 0x00000665 System.Void EasyUI.PickerWheelUI.WheelPiece::.ctor()
extern void WheelPiece__ctor_m5D96EFF31440009CB109207C19E886535BBF162C (void);
// 0x00000666 System.Object IronSourceJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m8FE82986ED46C15633CE71CEA565F9436B6474A4 (void);
// 0x00000667 System.String IronSourceJSON.Json::Serialize(System.Object)
extern void Json_Serialize_mC01A3EE8555C42F6DA02F3BFBF75C3E8F7D2342B (void);
// 0x00000668 System.Void IronSourceJSON.Json/Parser::.ctor(System.String)
extern void Parser__ctor_mF6E2F8B708BC8DA7BDB80DAD1DF622355BF50536 (void);
// 0x00000669 System.Object IronSourceJSON.Json/Parser::Parse(System.String)
extern void Parser_Parse_m550577AC93E71023378D27EA1361383BA16504CE (void);
// 0x0000066A System.Void IronSourceJSON.Json/Parser::Dispose()
extern void Parser_Dispose_mC88EEDDFE3CDB836F3AF49F721D3614158DED3A1 (void);
// 0x0000066B System.Collections.Generic.Dictionary`2<System.String,System.Object> IronSourceJSON.Json/Parser::ParseObject()
extern void Parser_ParseObject_mF3509D092287719C782B830F60B511483B9E570B (void);
// 0x0000066C System.Collections.Generic.List`1<System.Object> IronSourceJSON.Json/Parser::ParseArray()
extern void Parser_ParseArray_m3F732244DC13726B9DB99A9A335D4E8A9B31A575 (void);
// 0x0000066D System.Object IronSourceJSON.Json/Parser::ParseValue()
extern void Parser_ParseValue_m1BDF43E4AD2A64690AAFB6DBA800472F2A94272B (void);
// 0x0000066E System.Object IronSourceJSON.Json/Parser::ParseByToken(IronSourceJSON.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mF0830ED1965E050AE0195353B26ADB01C87E18F1 (void);
// 0x0000066F System.String IronSourceJSON.Json/Parser::ParseString()
extern void Parser_ParseString_m4913878D0B3878423C25644610F690615E7724C2 (void);
// 0x00000670 System.Object IronSourceJSON.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mDE9009CD828B692F4E5683708FEC450BF88D465F (void);
// 0x00000671 System.Void IronSourceJSON.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m3D91DD801FB915B39B1A5B133CB159DA4A6D2CF1 (void);
// 0x00000672 System.Char IronSourceJSON.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m631004C7D090E106B50F8CDFA62CB254B8C5553A (void);
// 0x00000673 System.Char IronSourceJSON.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m93C329D94339309A4654DF11D6C06D4D1ABBDBDF (void);
// 0x00000674 System.String IronSourceJSON.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m0FDD048038CF96F085F527E30FB94893225C2725 (void);
// 0x00000675 IronSourceJSON.Json/Parser/TOKEN IronSourceJSON.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m2CF12E0515A498D7CD696E6065BB8F5401182EB4 (void);
// 0x00000676 System.Void IronSourceJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_m75AAE9DA26CF48B1C8608E850915E74AC135BD0F (void);
// 0x00000677 System.String IronSourceJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m074256116E009BD598CCC76D76661DFADCA49C09 (void);
// 0x00000678 System.Void IronSourceJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m402A1869F9E3A33F621B305ED4C2322D6C1B16A7 (void);
// 0x00000679 System.Void IronSourceJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m76665B596B2DA337CFBAFD5374CBC170E29AE684 (void);
// 0x0000067A System.Void IronSourceJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mCC39C55C650C20A797385B9B3737F6DF08ECB30D (void);
// 0x0000067B System.Void IronSourceJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m5FB343C30BD22AB9DE403EB8135FF8C9A58DBF4C (void);
// 0x0000067C System.Void IronSourceJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m1ADF12DA620629116FC15C5FF3E60019B8409AA0 (void);
// 0x0000067D System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::get_Extras()
extern void MediationExtras_get_Extras_mAB9A0A22B862FF49296A158D6763B460A6B7B9E6 (void);
// 0x0000067E System.Void GoogleMobileAds.Api.Mediation.MediationExtras::set_Extras(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void MediationExtras_set_Extras_m35AC2360D2D08879D82F2C913F063420B918316B (void);
// 0x0000067F System.Void GoogleMobileAds.Api.Mediation.MediationExtras::.ctor()
extern void MediationExtras__ctor_mB2F2CF61A73BEA4E6FA458627EAB360A4C7A1097 (void);
// 0x00000680 System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_AndroidMediationExtraBuilderClassName()
// 0x00000681 System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_IOSMediationExtraBuilderClassName()
// 0x00000682 System.Void GoogleMobileAds.Api.Mediation.UnityAds.UnityAds::SetConsentMetaData(System.String,System.Boolean)
extern void UnityAds_SetConsentMetaData_mCC02C38C3D0E413053F907E25CCFA2444AAFD41A (void);
// 0x00000683 System.Void GoogleMobileAds.Api.Mediation.UnityAds.UnityAds::.ctor()
extern void UnityAds__ctor_m73520DF33582A597E7C17A0EA7B736598FBAA52D (void);
// 0x00000684 System.Void GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::SetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.Boolean)
extern void AdColonyAppOptions_SetPrivacyFrameworkRequired_m899246793F23C79B48400B17D1E47B071F59BF50 (void);
// 0x00000685 System.Boolean GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::GetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptions_GetPrivacyFrameworkRequired_m8F23B81FF0339D573B74F400D04658BF34A64226 (void);
// 0x00000686 System.Void GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::SetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.String)
extern void AdColonyAppOptions_SetPrivacyConsentString_mB8F86BF10FFAAA8B5E1D1F8718D68DF30D3E1C00 (void);
// 0x00000687 System.String GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::GetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptions_GetPrivacyConsentString_mBF71FB1DE0E6659D8E46F03B3310B18CEEAE13C5 (void);
// 0x00000688 System.Void GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::SetUserId(System.String)
extern void AdColonyAppOptions_SetUserId_m6DA0BB0E74DCBDECF3EB4A95EDF909CC5A6424FD (void);
// 0x00000689 System.String GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::GetUserId()
extern void AdColonyAppOptions_GetUserId_mA4FCFDE8CDEC821907053D772DC3058A8EA650EC (void);
// 0x0000068A System.Void GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::SetTestMode(System.Boolean)
extern void AdColonyAppOptions_SetTestMode_m4FA88F46305C59AC6A1D459BC1AF286053F9C983 (void);
// 0x0000068B System.Boolean GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::IsTestMode()
extern void AdColonyAppOptions_IsTestMode_m05873AC4A3DAD12FBF44ED1272604A0095F4F138 (void);
// 0x0000068C System.Void GoogleMobileAds.Api.Mediation.AdColony.AdColonyAppOptions::.ctor()
extern void AdColonyAppOptions__ctor_m8CB3020802C8BDFE13F020DA3152A071B9DD475A (void);
// 0x0000068D GoogleMobileAds.Mediation.UnityAds.Common.IUnityAdsClient GoogleMobileAds.Mediation.UnityAds.UnityAdsClientFactory::CreateUnityAdsClient()
extern void UnityAdsClientFactory_CreateUnityAdsClient_mF071B0990B9C1508D15F7549471C02941B2BB385 (void);
// 0x0000068E System.Void GoogleMobileAds.Mediation.UnityAds.UnityAdsClientFactory::.ctor()
extern void UnityAdsClientFactory__ctor_m7E9388805A0BA1D0855648555EC9BD51DFD77084 (void);
// 0x0000068F System.Void GoogleMobileAds.Mediation.UnityAds.Android.UnityAdsClient::.ctor()
extern void UnityAdsClient__ctor_m04AC52C256D7370D5755468724EE6BB5FDA0F7C1 (void);
// 0x00000690 GoogleMobileAds.Mediation.UnityAds.Android.UnityAdsClient GoogleMobileAds.Mediation.UnityAds.Android.UnityAdsClient::get_Instance()
extern void UnityAdsClient_get_Instance_mFF6141BD16378E611E8BAFA7C054D08AA0ABC138 (void);
// 0x00000691 System.Void GoogleMobileAds.Mediation.UnityAds.Android.UnityAdsClient::SetConsentMetaData(System.String,System.Boolean)
extern void UnityAdsClient_SetConsentMetaData_m1FEDB70441D40525079213BEDA1BC7D7593C277E (void);
// 0x00000692 System.Void GoogleMobileAds.Mediation.UnityAds.Android.UnityAdsClient::.cctor()
extern void UnityAdsClient__cctor_m1C34659E50F98214104455B11C61E95AE6513F2D (void);
// 0x00000693 System.Void GoogleMobileAds.Mediation.UnityAds.Common.DummyClient::.ctor()
extern void DummyClient__ctor_mF3A882B7C36D888AE9E4DAF9EC6FAE625532176C (void);
// 0x00000694 System.Void GoogleMobileAds.Mediation.UnityAds.Common.DummyClient::SetConsentMetaData(System.String,System.Boolean)
extern void DummyClient_SetConsentMetaData_mF53639A6FCA9BEFCEED398984AADA0CD65409254 (void);
// 0x00000695 System.Void GoogleMobileAds.Mediation.UnityAds.Common.IUnityAdsClient::SetConsentMetaData(System.String,System.Boolean)
// 0x00000696 System.Void GoogleMobileAds.Mediation.UnityAds.Api.UnityAds::SetConsentMetaData(System.String,System.Boolean)
extern void UnityAds_SetConsentMetaData_m01F69568A14C744AC2A596DA7471245D1F39A650 (void);
// 0x00000697 System.Void GoogleMobileAds.Mediation.UnityAds.Api.UnityAds::.ctor()
extern void UnityAds__ctor_m18403C1C51C4344B6FDA1D299A9ECD4D136717C1 (void);
// 0x00000698 System.Void GoogleMobileAds.Mediation.UnityAds.Api.UnityAds::.cctor()
extern void UnityAds__cctor_mE90A66BBDC7DA9F965DA77F0A4B6D77AE584A0D2 (void);
// 0x00000699 GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient GoogleMobileAds.Mediation.AdColony.AdColonyAppOptionsClientFactory::getAdColonyAppOptionsInstance()
extern void AdColonyAppOptionsClientFactory_getAdColonyAppOptionsInstance_m3E6F164A142C016DE03675AA6DDC03815F1B7251 (void);
// 0x0000069A System.Void GoogleMobileAds.Mediation.AdColony.AdColonyAppOptionsClientFactory::.ctor()
extern void AdColonyAppOptionsClientFactory__ctor_m893B6AE174DA6EAC2771CEE21EE6AD32BDFCDFA7 (void);
// 0x0000069B System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::.ctor()
extern void AdColonyAppOptionsClient__ctor_mAB8C747F85A2778B5E13525BBEF172B98DAAFAF6 (void);
// 0x0000069C GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::get_Instance()
extern void AdColonyAppOptionsClient_get_Instance_mD38CC038DEE25F3CDFC97B9AE006609050722DE6 (void);
// 0x0000069D System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::SetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.Boolean)
extern void AdColonyAppOptionsClient_SetPrivacyFrameworkRequired_m68E50A0535D274E76A8F29451660FAC595F5236C (void);
// 0x0000069E System.Boolean GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::GetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptionsClient_GetPrivacyFrameworkRequired_mEC6847DA1A7946AFBFD611F60DBC625A56CDC54A (void);
// 0x0000069F System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::SetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.String)
extern void AdColonyAppOptionsClient_SetPrivacyConsentString_mB4A2C05636036BA640984FA38B62467DCD04A2AF (void);
// 0x000006A0 System.String GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::GetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptionsClient_GetPrivacyConsentString_m5BDF4DCC1B8D5B7B774A2C075B955278260A5E8D (void);
// 0x000006A1 System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::SetUserId(System.String)
extern void AdColonyAppOptionsClient_SetUserId_m8DFD4E97EE468F0A21E117EA4051B8D0BEF8EA76 (void);
// 0x000006A2 System.String GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::GetUserId()
extern void AdColonyAppOptionsClient_GetUserId_mAA3059B8D0E55C5F57934EC8EFA8670070422062 (void);
// 0x000006A3 System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::SetTestMode(System.Boolean)
extern void AdColonyAppOptionsClient_SetTestMode_mECD4634B7EB6BFD4BD819E6B7BD4FBAF076F8ED7 (void);
// 0x000006A4 System.Boolean GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::IsTestMode()
extern void AdColonyAppOptionsClient_IsTestMode_m78A18C36D69660DFF3EB2D577570A5DD8C320C7D (void);
// 0x000006A5 System.String GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::GetAdColonyPrivacyFrameworkString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptionsClient_GetAdColonyPrivacyFrameworkString_mAE46A6D0AE0959E1884A79F0A9A96476DDE035B7 (void);
// 0x000006A6 System.Void GoogleMobileAds.Mediation.AdColony.Android.AdColonyAppOptionsClient::.cctor()
extern void AdColonyAppOptionsClient__cctor_mAD13D6DA6D40FD1B0A04E43FF2E4BEE1CF955F99 (void);
// 0x000006A7 System.Void GoogleMobileAds.Mediation.AdColony.Common.DummyClient::.ctor()
extern void DummyClient__ctor_m0BA06464ADC9CCBE47A6FAECBDEF81AB9BF2E976 (void);
// 0x000006A8 System.Void GoogleMobileAds.Mediation.AdColony.Common.DummyClient::SetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.Boolean)
extern void DummyClient_SetPrivacyFrameworkRequired_mBA78A98F352B41396F96DD9C399FA95D0A61B4DB (void);
// 0x000006A9 System.Boolean GoogleMobileAds.Mediation.AdColony.Common.DummyClient::GetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void DummyClient_GetPrivacyFrameworkRequired_m4EE050EEE6C8F8ED11D8D5C740C18A915C7A2356 (void);
// 0x000006AA System.Void GoogleMobileAds.Mediation.AdColony.Common.DummyClient::SetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.String)
extern void DummyClient_SetPrivacyConsentString_mF59C5AEE0CA310A0AAF0815FB2D1453194A18053 (void);
// 0x000006AB System.String GoogleMobileAds.Mediation.AdColony.Common.DummyClient::GetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void DummyClient_GetPrivacyConsentString_mBA8D20D00910501657CBA0DA55B1BC95E0D19BCE (void);
// 0x000006AC System.Void GoogleMobileAds.Mediation.AdColony.Common.DummyClient::SetUserId(System.String)
extern void DummyClient_SetUserId_mAABCF8BEFDDE79D228A33CC292D9907295DFBFC2 (void);
// 0x000006AD System.String GoogleMobileAds.Mediation.AdColony.Common.DummyClient::GetUserId()
extern void DummyClient_GetUserId_mFF9ACE865165A5C49173BAFB02DB61B715BB2721 (void);
// 0x000006AE System.Void GoogleMobileAds.Mediation.AdColony.Common.DummyClient::SetTestMode(System.Boolean)
extern void DummyClient_SetTestMode_m64C9904859BF57BA58181739D20499FABED129B9 (void);
// 0x000006AF System.Boolean GoogleMobileAds.Mediation.AdColony.Common.DummyClient::IsTestMode()
extern void DummyClient_IsTestMode_mFD4AA5547F59B6DFF81F55999FA3F40A42CDE92F (void);
// 0x000006B0 System.Void GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::SetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.Boolean)
// 0x000006B1 System.Boolean GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::GetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
// 0x000006B2 System.Void GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::SetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.String)
// 0x000006B3 System.String GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::GetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
// 0x000006B4 System.Void GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::SetUserId(System.String)
// 0x000006B5 System.String GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::GetUserId()
// 0x000006B6 System.Void GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::SetTestMode(System.Boolean)
// 0x000006B7 System.Boolean GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient::IsTestMode()
// 0x000006B8 GoogleMobileAds.Mediation.AdColony.Common.IAdColonyAppOptionsClient GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::GetAdColonyAppOptionsClient()
extern void AdColonyAppOptions_GetAdColonyAppOptionsClient_m7CF66B5EE6EEFE87EC9D3588F59B572E1CCB1AB2 (void);
// 0x000006B9 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::SetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.Boolean)
extern void AdColonyAppOptions_SetPrivacyFrameworkRequired_m6B5427F457EE53E5659FE59BAFBB0DC689EF7523 (void);
// 0x000006BA System.Boolean GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::GetPrivacyFrameworkRequired(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptions_GetPrivacyFrameworkRequired_m4144D0D0C3430B26F08C51570ACAC7A5E4257A4C (void);
// 0x000006BB System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::SetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework,System.String)
extern void AdColonyAppOptions_SetPrivacyConsentString_mD16CBD1F1215A95910DD58DE32FB58498F0BB145 (void);
// 0x000006BC System.String GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::GetPrivacyConsentString(GoogleMobileAds.Mediation.AdColony.Api.AdColonyPrivacyFramework)
extern void AdColonyAppOptions_GetPrivacyConsentString_mB4B226537F135BE3738636A3DFB3E061E99B7F15 (void);
// 0x000006BD System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::SetUserId(System.String)
extern void AdColonyAppOptions_SetUserId_mB8E9F286CC58CE277B15606CF6AD7D2CF3F8DA6F (void);
// 0x000006BE System.String GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::GetUserId()
extern void AdColonyAppOptions_GetUserId_m06CFFB4C0CD4D4CFEFCAB6F448FF48F1D4F45620 (void);
// 0x000006BF System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::SetTestMode(System.Boolean)
extern void AdColonyAppOptions_SetTestMode_mC89F24D793F99C60ADF68868B913D6DBC10A7592 (void);
// 0x000006C0 System.Boolean GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::IsTestMode()
extern void AdColonyAppOptions_IsTestMode_m4E51DA965BC326B36668E37EB286E4AE65C7E301 (void);
// 0x000006C1 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::.ctor()
extern void AdColonyAppOptions__ctor_m73398EDF508F5385800D8F8FED39FDF642E365A3 (void);
// 0x000006C2 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyAppOptions::.cctor()
extern void AdColonyAppOptions__cctor_m3D42793B4281B8FBBD10752406EDB77AC704456F (void);
// 0x000006C3 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyMediationExtras::.ctor()
extern void AdColonyMediationExtras__ctor_m359AD4F47D86CDFCEB3C8C1CF91B18E2E4E0277B (void);
// 0x000006C4 System.String GoogleMobileAds.Mediation.AdColony.Api.AdColonyMediationExtras::get_AndroidMediationExtraBuilderClassName()
extern void AdColonyMediationExtras_get_AndroidMediationExtraBuilderClassName_m1A5973D015E1E620A4184D3428EFD42756AB95D8 (void);
// 0x000006C5 System.String GoogleMobileAds.Mediation.AdColony.Api.AdColonyMediationExtras::get_IOSMediationExtraBuilderClassName()
extern void AdColonyMediationExtras_get_IOSMediationExtraBuilderClassName_m1693436157396C5DD5A4E375A2DF126EAF8BCCDD (void);
// 0x000006C6 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyMediationExtras::SetShowPrePopup(System.Boolean)
extern void AdColonyMediationExtras_SetShowPrePopup_mDB6769E78764E96E668A9607AF6B870DEB2946C9 (void);
// 0x000006C7 System.Void GoogleMobileAds.Mediation.AdColony.Api.AdColonyMediationExtras::SetShowPostPopup(System.Boolean)
extern void AdColonyMediationExtras_SetShowPostPopup_m361B18B90B76AE9014C930E30E93F420F055BC14 (void);
// 0x000006C8 System.Void com.adjust.sdk.JSONNode::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3 (void);
// 0x000006C9 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3 (void);
// 0x000006CA System.Void com.adjust.sdk.JSONNode::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B (void);
// 0x000006CB com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF (void);
// 0x000006CC System.Void com.adjust.sdk.JSONNode::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D (void);
// 0x000006CD System.String com.adjust.sdk.JSONNode::get_Value()
extern void JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F (void);
// 0x000006CE System.Void com.adjust.sdk.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC (void);
// 0x000006CF System.Int32 com.adjust.sdk.JSONNode::get_Count()
extern void JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25 (void);
// 0x000006D0 System.Void com.adjust.sdk.JSONNode::Add(com.adjust.sdk.JSONNode)
extern void JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99 (void);
// 0x000006D1 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71 (void);
// 0x000006D2 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA (void);
// 0x000006D3 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(com.adjust.sdk.JSONNode)
extern void JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1 (void);
// 0x000006D4 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_Childs()
extern void JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482 (void);
// 0x000006D5 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_DeepChilds()
extern void JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5 (void);
// 0x000006D6 System.String com.adjust.sdk.JSONNode::ToString()
extern void JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1 (void);
// 0x000006D7 System.String com.adjust.sdk.JSONNode::ToString(System.String)
extern void JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2 (void);
// 0x000006D8 System.Int32 com.adjust.sdk.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7 (void);
// 0x000006D9 System.Void com.adjust.sdk.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A (void);
// 0x000006DA System.Single com.adjust.sdk.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6 (void);
// 0x000006DB System.Void com.adjust.sdk.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C (void);
// 0x000006DC System.Double com.adjust.sdk.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752 (void);
// 0x000006DD System.Void com.adjust.sdk.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76 (void);
// 0x000006DE System.Boolean com.adjust.sdk.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F (void);
// 0x000006DF System.Void com.adjust.sdk.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD (void);
// 0x000006E0 com.adjust.sdk.JSONArray com.adjust.sdk.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3 (void);
// 0x000006E1 com.adjust.sdk.JSONClass com.adjust.sdk.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700 (void);
// 0x000006E2 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m9105117784C6916F6B646D36A1370FB58A9C6762 (void);
// 0x000006E3 System.String com.adjust.sdk.JSONNode::op_Implicit(com.adjust.sdk.JSONNode)
extern void JSONNode_op_Implicit_m8DDFBC3FCEA2F2087B7A132A483F724B1529B183 (void);
// 0x000006E4 System.Boolean com.adjust.sdk.JSONNode::op_Equality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Equality_mF53AB65ABCF70E4C7D035DF059648FED12577634 (void);
// 0x000006E5 System.Boolean com.adjust.sdk.JSONNode::op_Inequality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m31F67DC83671EE7A334E8A1A0445AC08CFDD9BD5 (void);
// 0x000006E6 System.Boolean com.adjust.sdk.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE (void);
// 0x000006E7 System.Int32 com.adjust.sdk.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5 (void);
// 0x000006E8 System.String com.adjust.sdk.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m56A8C20A29A7EE378C0A55BB6CD46467F0347950 (void);
// 0x000006E9 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Parse(System.String)
extern void JSONNode_Parse_mD6D2A7EBFFEDF36552C27F27CB53F9DBA6C9D12E (void);
// 0x000006EA System.Void com.adjust.sdk.JSONNode::Serialize(System.IO.BinaryWriter)
extern void JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F (void);
// 0x000006EB System.Void com.adjust.sdk.JSONNode::SaveToStream(System.IO.Stream)
extern void JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27 (void);
// 0x000006EC System.Void com.adjust.sdk.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1 (void);
// 0x000006ED System.Void com.adjust.sdk.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71 (void);
// 0x000006EE System.String com.adjust.sdk.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B (void);
// 0x000006EF com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Deserialize(System.IO.BinaryReader)
extern void JSONNode_Deserialize_m5C0519240162A7247E99053C69876E590587B38C (void);
// 0x000006F0 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_mEB55B68104BA51A066485FBBC1CB6A1D16DAA504 (void);
// 0x000006F1 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m7368A19FFACEDA51ED6E39DE7919FDF11836F030 (void);
// 0x000006F2 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mE4C615EAEC9DD685E6659BC69C77CDF7A6FE1CFB (void);
// 0x000006F3 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromStream(System.IO.Stream)
extern void JSONNode_LoadFromStream_mD78A68A7F71EE78FB1E12311DBFFFAA3E6AF8B69 (void);
// 0x000006F4 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromBase64(System.String)
extern void JSONNode_LoadFromBase64_m33C55652F013772F68C8CA8B54CEE7EBE297B08C (void);
// 0x000006F5 System.Void com.adjust.sdk.JSONNode::.ctor()
extern void JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA (void);
// 0x000006F6 System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__17__ctor_mF797A836A6E3340B1BAAAF50AEEC10DFA89DAE73 (void);
// 0x000006F7 System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m94877C8D1DA5BE6EA246389F10F4C9C7AF366A74 (void);
// 0x000006F8 System.Boolean com.adjust.sdk.JSONNode/<get_Childs>d__17::MoveNext()
extern void U3Cget_ChildsU3Ed__17_MoveNext_mE6F86033D5481E4036487E92E0F64B9840CB794B (void);
// 0x000006F9 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m54B690F8FA694EE02627B97D48386D9095E229E4 (void);
// 0x000006FA System.Void com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m0366D0A88F5A1FBA81AE3F732F855C0CAE045788 (void);
// 0x000006FB System.Object com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_m38B1B4A7F33CF7834BB44D72C8FED6220D1C75BD (void);
// 0x000006FC System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m071E822DD6D0A22882CDAA62B63D9660FF1776C0 (void);
// 0x000006FD System.Collections.IEnumerator com.adjust.sdk.JSONNode/<get_Childs>d__17::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mA892F96D61A7E4D40CBA9B2CF48F34079F726849 (void);
// 0x000006FE System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::.ctor(System.Int32)
extern void U3Cget_DeepChildsU3Ed__19__ctor_m046016CC55A4C7B9C1FE5AE10DD72E4050826BAC (void);
// 0x000006FF System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.IDisposable.Dispose()
extern void U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mDCF0787A78165575446D46E5C1CD1A8A10FF7018 (void);
// 0x00000700 System.Boolean com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::MoveNext()
extern void U3Cget_DeepChildsU3Ed__19_MoveNext_m9F51E57FD6F93E690C029855DAA59D591E773022 (void);
// 0x00000701 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::<>m__Finally1()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m44FF129641F4B9A288B22CC405CFDB01813AF413 (void);
// 0x00000702 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::<>m__Finally2()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m50CD9A7591C11FE67D4AE24614BD3B979B030C7F (void);
// 0x00000703 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB46DB5D77ECC4B15D6B4BA4ED0A03F21CE95D25D (void);
// 0x00000704 System.Void com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_m2862E593874F11BCC026302EFC96AFDE00AB5A06 (void);
// 0x00000705 System.Object com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mABF1413C57FA5A3C1651355AB4A5E4C0402E27DA (void);
// 0x00000706 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mA1AC5268F717D42575E2B41A69ED09A0C60071B0 (void);
// 0x00000707 System.Collections.IEnumerator com.adjust.sdk.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m481F9219BE5FB9B09E8DFC02714ED34393641102 (void);
// 0x00000708 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503 (void);
// 0x00000709 System.Void com.adjust.sdk.JSONArray::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5 (void);
// 0x0000070A com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454 (void);
// 0x0000070B System.Void com.adjust.sdk.JSONArray::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE (void);
// 0x0000070C System.Int32 com.adjust.sdk.JSONArray::get_Count()
extern void JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B (void);
// 0x0000070D System.Void com.adjust.sdk.JSONArray::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B (void);
// 0x0000070E com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF (void);
// 0x0000070F com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(com.adjust.sdk.JSONNode)
extern void JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A (void);
// 0x00000710 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray::get_Childs()
extern void JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874 (void);
// 0x00000711 System.Collections.IEnumerator com.adjust.sdk.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635 (void);
// 0x00000712 System.String com.adjust.sdk.JSONArray::ToString()
extern void JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB (void);
// 0x00000713 System.String com.adjust.sdk.JSONArray::ToString(System.String)
extern void JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B (void);
// 0x00000714 System.Void com.adjust.sdk.JSONArray::Serialize(System.IO.BinaryWriter)
extern void JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF (void);
// 0x00000715 System.Void com.adjust.sdk.JSONArray::.ctor()
extern void JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E (void);
// 0x00000716 System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__13__ctor_m6474546C7E49FF6A548EFAB592D82CD4C52A3E2B (void);
// 0x00000717 System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_mE5E9FC725A9C7CB1A97AE4BCF844F2F773000283 (void);
// 0x00000718 System.Boolean com.adjust.sdk.JSONArray/<get_Childs>d__13::MoveNext()
extern void U3Cget_ChildsU3Ed__13_MoveNext_m93338AB8CC1520C49B28F7BA37D0216F8DFE3894 (void);
// 0x00000719 System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_mC2A2DBA3A3B18AB12F36800AD2129AF9CE3B4865 (void);
// 0x0000071A com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m939A44A15A892D3328145314D6F3BD3180E600F0 (void);
// 0x0000071B System.Void com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m9BBCABEA39F9133BC5E7188E8E6346538077025F (void);
// 0x0000071C System.Object com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_mAAA548D9FF3C67A7AEF373A19FFAB5C9C3A97C5E (void);
// 0x0000071D System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m3C0A4675A2FFA0A86616539694B4392F7A86B4D6 (void);
// 0x0000071E System.Collections.IEnumerator com.adjust.sdk.JSONArray/<get_Childs>d__13::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_m208AD3C417C7CAABDDEABD4EE49670B1E6403926 (void);
// 0x0000071F System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__14__ctor_m0695479939904B620F4C38973708FCF84941755E (void);
// 0x00000720 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_mDCC768ED829C36509C1AE08EC89EDA457099995C (void);
// 0x00000721 System.Boolean com.adjust.sdk.JSONArray/<GetEnumerator>d__14::MoveNext()
extern void U3CGetEnumeratorU3Ed__14_MoveNext_mE03BAD590A4A72ADD88274F87F144FA61C677CD2 (void);
// 0x00000722 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m4F72145277B7B879E05365A241A55E1FA2DCC478 (void);
// 0x00000723 System.Object com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC44ED8F1DA1967025AB91E813D3F5E8CAB273B4A (void);
// 0x00000724 System.Void com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m5FF5CA385FB75BA70CEF94B6FAE71C700ABA5A6A (void);
// 0x00000725 System.Object com.adjust.sdk.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_m4BE072CF6B24FCA546848441F442FA00F8049498 (void);
// 0x00000726 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.String)
extern void JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93 (void);
// 0x00000727 System.Void com.adjust.sdk.JSONClass::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E (void);
// 0x00000728 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.Int32)
extern void JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551 (void);
// 0x00000729 System.Void com.adjust.sdk.JSONClass::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71 (void);
// 0x0000072A System.Int32 com.adjust.sdk.JSONClass::get_Count()
extern void JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C (void);
// 0x0000072B System.Void com.adjust.sdk.JSONClass::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38 (void);
// 0x0000072C com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.String)
extern void JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680 (void);
// 0x0000072D com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.Int32)
extern void JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680 (void);
// 0x0000072E com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(com.adjust.sdk.JSONNode)
extern void JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877 (void);
// 0x0000072F System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass::get_Childs()
extern void JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925 (void);
// 0x00000730 System.Collections.IEnumerator com.adjust.sdk.JSONClass::GetEnumerator()
extern void JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD (void);
// 0x00000731 System.String com.adjust.sdk.JSONClass::ToString()
extern void JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37 (void);
// 0x00000732 System.String com.adjust.sdk.JSONClass::ToString(System.String)
extern void JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027 (void);
// 0x00000733 System.Void com.adjust.sdk.JSONClass::Serialize(System.IO.BinaryWriter)
extern void JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8 (void);
// 0x00000734 System.Void com.adjust.sdk.JSONClass::.ctor()
extern void JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446 (void);
// 0x00000735 System.Void com.adjust.sdk.JSONClass/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m54EAB9A9ED470825466B363E8EF656B51E99F746 (void);
// 0x00000736 System.Boolean com.adjust.sdk.JSONClass/<>c__DisplayClass12_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,com.adjust.sdk.JSONNode>)
extern void U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_m919E74F0EFF672D2C2B58FE1D21F705C7A3DD3B8 (void);
// 0x00000737 System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__14__ctor_m2A7CD01CA103381FBF4FB8D70B1FD26083894793 (void);
// 0x00000738 System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_m31D2FA345366CAF0D63CB243DCBE67745AA52783 (void);
// 0x00000739 System.Boolean com.adjust.sdk.JSONClass/<get_Childs>d__14::MoveNext()
extern void U3Cget_ChildsU3Ed__14_MoveNext_m3C0CFA00C04821A51C6A3C0A5808DFDE9ECE5A5E (void);
// 0x0000073A System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m9FA71F2E785602FDD64125BD5BE3FCE3561532D7 (void);
// 0x0000073B com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerator<com.adjust.sdk.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB0226A6E5AC71AEBA38F19684C7758964A705388 (void);
// 0x0000073C System.Void com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m65E72C29D884A8DEDBBAAD79C3035EFB6A106ACB (void);
// 0x0000073D System.Object com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m78FA739F5BE9ACDFE18DD77640515DA4B2BF4576 (void);
// 0x0000073E System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerable<com.adjust.sdk.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mF561773E4A39039E3DAFEE4082F0FF2E39DE7F2C (void);
// 0x0000073F System.Collections.IEnumerator com.adjust.sdk.JSONClass/<get_Childs>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m80BE9E5871675B635E6737823E81E269BDFF7359 (void);
// 0x00000740 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__15__ctor_m71795B1B109474BBF1C793AE265C8218EE46A317 (void);
// 0x00000741 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_m92BA0C0FE94BF8EA461E8D2985DCADF217B29B3A (void);
// 0x00000742 System.Boolean com.adjust.sdk.JSONClass/<GetEnumerator>d__15::MoveNext()
extern void U3CGetEnumeratorU3Ed__15_MoveNext_mB78E9C334CD336419DCE6EAFEF6FFE51FD51947E (void);
// 0x00000743 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_m909727106AE5FE5AA12C727A654659A639A11F9F (void);
// 0x00000744 System.Object com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D7E2674085894FC3011FFCE567CFA21DA8C30D0 (void);
// 0x00000745 System.Void com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m9877E5F955C30C1CE2C2FEB43B2608137528BCE4 (void);
// 0x00000746 System.Object com.adjust.sdk.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_mDB2FE16291B3B81162572DC31EE732BA957BD162 (void);
// 0x00000747 System.String com.adjust.sdk.JSONData::get_Value()
extern void JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425 (void);
// 0x00000748 System.Void com.adjust.sdk.JSONData::set_Value(System.String)
extern void JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2 (void);
// 0x00000749 System.Void com.adjust.sdk.JSONData::.ctor(System.String)
extern void JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5 (void);
// 0x0000074A System.Void com.adjust.sdk.JSONData::.ctor(System.Single)
extern void JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2 (void);
// 0x0000074B System.Void com.adjust.sdk.JSONData::.ctor(System.Double)
extern void JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE (void);
// 0x0000074C System.Void com.adjust.sdk.JSONData::.ctor(System.Boolean)
extern void JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350 (void);
// 0x0000074D System.Void com.adjust.sdk.JSONData::.ctor(System.Int32)
extern void JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F (void);
// 0x0000074E System.String com.adjust.sdk.JSONData::ToString()
extern void JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580 (void);
// 0x0000074F System.String com.adjust.sdk.JSONData::ToString(System.String)
extern void JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD (void);
// 0x00000750 System.Void com.adjust.sdk.JSONData::Serialize(System.IO.BinaryWriter)
extern void JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91 (void);
// 0x00000751 System.Void com.adjust.sdk.JSONLazyCreator::.ctor(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator__ctor_mF3FA6877067F4F9086006389F0910D541AA27059 (void);
// 0x00000752 System.Void com.adjust.sdk.JSONLazyCreator::.ctor(com.adjust.sdk.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m8C48C27CBB58E5F6990AFCA554803B7E191A3D46 (void);
// 0x00000753 System.Void com.adjust.sdk.JSONLazyCreator::Set(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Set_m9FCCD3D9059234381ACD61296F3B33ABB5392630 (void);
// 0x00000754 com.adjust.sdk.JSONNode com.adjust.sdk.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m8A8A9C17EDC287BFF118DDED313F4107F4072A22 (void);
// 0x00000755 System.Void com.adjust.sdk.JSONLazyCreator::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_set_Item_m19485C7273CDACCE9F7B1834D56FB74E19777E12 (void);
// 0x00000756 com.adjust.sdk.JSONNode com.adjust.sdk.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_mF8E012F6005145846304C594EACF1584CA5DDA9A (void);
// 0x00000757 System.Void com.adjust.sdk.JSONLazyCreator::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_set_Item_m33B036AAA008F596ADB3F455D2021D1EF025B49A (void);
// 0x00000758 System.Void com.adjust.sdk.JSONLazyCreator::Add(com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Add_m59C51EC772277124A17D87DA5C74707E3EFD6AFB (void);
// 0x00000759 System.Void com.adjust.sdk.JSONLazyCreator::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONLazyCreator_Add_mBACDDB1A14FCE6CEA2674C9B4EDA816F21DF9E29 (void);
// 0x0000075A System.Boolean com.adjust.sdk.JSONLazyCreator::op_Equality(com.adjust.sdk.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m9A8BAD40A9286520054534AE702678831750A10B (void);
// 0x0000075B System.Boolean com.adjust.sdk.JSONLazyCreator::op_Inequality(com.adjust.sdk.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_mDFBBDA95E69B946B8DD0C0807083A0A34746A7C4 (void);
// 0x0000075C System.Boolean com.adjust.sdk.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m586E6D9E990915F11F72E099E203D60723BEBC48 (void);
// 0x0000075D System.Int32 com.adjust.sdk.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m9CC90F8845B9E57BB6913BF83FB3C5D89FA61A35 (void);
// 0x0000075E System.String com.adjust.sdk.JSONLazyCreator::ToString()
extern void JSONLazyCreator_ToString_m49AE6E6F07C738C9FB3F7ABC86EBBA8C0828996C (void);
// 0x0000075F System.String com.adjust.sdk.JSONLazyCreator::ToString(System.String)
extern void JSONLazyCreator_ToString_mD794E0057AC95367420ED3D100817F019BD77A91 (void);
// 0x00000760 System.Int32 com.adjust.sdk.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m3D1F120A6A54087B34B638943D2168F10CF58BA8 (void);
// 0x00000761 System.Void com.adjust.sdk.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m78FF9DC114F9731546230F760A5048069378B32F (void);
// 0x00000762 System.Single com.adjust.sdk.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1BF100B3129E3B67B00EB12BAB1C599198E60D87 (void);
// 0x00000763 System.Void com.adjust.sdk.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mE15D7CE3B2B38E9E5EDC079D339A8AB5FAF674D6 (void);
// 0x00000764 System.Double com.adjust.sdk.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m40DD7A463621DD10526570D5430173F7AE7771B5 (void);
// 0x00000765 System.Void com.adjust.sdk.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m275BB96935FAE5F2CD8A8328EAC9FE887B5A3377 (void);
// 0x00000766 System.Boolean com.adjust.sdk.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mAF53CC7D86CFCE9E85413B126CFD79470BCD6B30 (void);
// 0x00000767 System.Void com.adjust.sdk.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m05997EDED19E2297BFE7D45A1062CB7CA52F1ED8 (void);
// 0x00000768 com.adjust.sdk.JSONArray com.adjust.sdk.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mAEBD138BCA37CA30F180A221CC7872FE240BFA62 (void);
// 0x00000769 com.adjust.sdk.JSONClass com.adjust.sdk.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m52BB6F39E712E7E3ED988842537FEFF804B543C7 (void);
// 0x0000076A com.adjust.sdk.JSONNode com.adjust.sdk.JSON::Parse(System.String)
extern void JSON_Parse_m64D44E2E2DCDC73C4FC5A08B5D13F92209F2482E (void);
// 0x0000076B System.Void com.adjust.sdk.AdjustAndroid::Start(com.adjust.sdk.AdjustConfig)
extern void AdjustAndroid_Start_m1D39F038C9A56C36B88B6D1145B9F69E01C7E7C7 (void);
// 0x0000076C System.Void com.adjust.sdk.AdjustAndroid::TrackEvent(com.adjust.sdk.AdjustEvent)
extern void AdjustAndroid_TrackEvent_m1ACD3DE44F09700192A24C5A326E73E76673C9F2 (void);
// 0x0000076D System.Boolean com.adjust.sdk.AdjustAndroid::IsEnabled()
extern void AdjustAndroid_IsEnabled_m0F99D443225B532431D92E5BDDC011DFDC7AD3DF (void);
// 0x0000076E System.Void com.adjust.sdk.AdjustAndroid::SetEnabled(System.Boolean)
extern void AdjustAndroid_SetEnabled_m804BC54C758D85422C2D408FAFBF0D53B746423C (void);
// 0x0000076F System.Void com.adjust.sdk.AdjustAndroid::SetOfflineMode(System.Boolean)
extern void AdjustAndroid_SetOfflineMode_mAB3976F1FF8690D22BFF4A6ADEAAE8C47524B7A7 (void);
// 0x00000770 System.Void com.adjust.sdk.AdjustAndroid::SendFirstPackages()
extern void AdjustAndroid_SendFirstPackages_mC6C37B30223758423D5557CF2FA8674801D326CC (void);
// 0x00000771 System.Void com.adjust.sdk.AdjustAndroid::SetDeviceToken(System.String)
extern void AdjustAndroid_SetDeviceToken_mF30818E7C4F0F5DD856C9E35394E9FB9ADD87E20 (void);
// 0x00000772 System.String com.adjust.sdk.AdjustAndroid::GetAdid()
extern void AdjustAndroid_GetAdid_m0525482D8E3F1167CD14B2A12D2E2D7E9FBD8AA5 (void);
// 0x00000773 System.Void com.adjust.sdk.AdjustAndroid::GdprForgetMe()
extern void AdjustAndroid_GdprForgetMe_mD1AECB214CA8BFB9783A5E3F70F5E80914F08437 (void);
// 0x00000774 System.Void com.adjust.sdk.AdjustAndroid::DisableThirdPartySharing()
extern void AdjustAndroid_DisableThirdPartySharing_m55E805701C85CBBA00F7580AAE75967EA31D9061 (void);
// 0x00000775 com.adjust.sdk.AdjustAttribution com.adjust.sdk.AdjustAndroid::GetAttribution()
extern void AdjustAndroid_GetAttribution_mD7681342B5DDF0B2A2C9BCE18327E583189CAF9B (void);
// 0x00000776 System.Void com.adjust.sdk.AdjustAndroid::AddSessionPartnerParameter(System.String,System.String)
extern void AdjustAndroid_AddSessionPartnerParameter_mFAB6A72387CDDE7C9212D10D49CF488FB26185E6 (void);
// 0x00000777 System.Void com.adjust.sdk.AdjustAndroid::AddSessionCallbackParameter(System.String,System.String)
extern void AdjustAndroid_AddSessionCallbackParameter_m3886354DCD87FDCFFAFA69A19C4E97E59D31C824 (void);
// 0x00000778 System.Void com.adjust.sdk.AdjustAndroid::RemoveSessionPartnerParameter(System.String)
extern void AdjustAndroid_RemoveSessionPartnerParameter_mE310C1452E7AA2779CEA5B6C8B70410104D87966 (void);
// 0x00000779 System.Void com.adjust.sdk.AdjustAndroid::RemoveSessionCallbackParameter(System.String)
extern void AdjustAndroid_RemoveSessionCallbackParameter_m7D835C9CD309FAB5C5F48ECC068EFD813B90B349 (void);
// 0x0000077A System.Void com.adjust.sdk.AdjustAndroid::ResetSessionPartnerParameters()
extern void AdjustAndroid_ResetSessionPartnerParameters_m85FA68D4FA76A036E67BDA3371169BEFBAD46CC1 (void);
// 0x0000077B System.Void com.adjust.sdk.AdjustAndroid::ResetSessionCallbackParameters()
extern void AdjustAndroid_ResetSessionCallbackParameters_m2319AAC0995822B275A8A731F88866F960BCF09A (void);
// 0x0000077C System.Void com.adjust.sdk.AdjustAndroid::AppWillOpenUrl(System.String)
extern void AdjustAndroid_AppWillOpenUrl_mE323FAC42F2AE9B649BFA44CC7EC253361733AD7 (void);
// 0x0000077D System.Void com.adjust.sdk.AdjustAndroid::TrackAdRevenue(System.String,System.String)
extern void AdjustAndroid_TrackAdRevenue_mE1FF768B0E1885910BEF4C4CB0316852B0774881 (void);
// 0x0000077E System.Void com.adjust.sdk.AdjustAndroid::TrackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void AdjustAndroid_TrackAdRevenue_m17E49555E6663E2C1FDB7723ED2ACF8AF7BB2523 (void);
// 0x0000077F System.Void com.adjust.sdk.AdjustAndroid::TrackPlayStoreSubscription(com.adjust.sdk.AdjustPlayStoreSubscription)
extern void AdjustAndroid_TrackPlayStoreSubscription_m0D0FD86FD0258755FB8555589C707BE28406B7BE (void);
// 0x00000780 System.Void com.adjust.sdk.AdjustAndroid::TrackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void AdjustAndroid_TrackThirdPartySharing_m2F3BB31D3B60EE3D70D07494921949F090837613 (void);
// 0x00000781 System.Void com.adjust.sdk.AdjustAndroid::TrackMeasurementConsent(System.Boolean)
extern void AdjustAndroid_TrackMeasurementConsent_m9A2C735602AB9605791360DC02AFDC569CEC7672 (void);
// 0x00000782 System.Void com.adjust.sdk.AdjustAndroid::OnPause()
extern void AdjustAndroid_OnPause_mEAB3D744D7FA078BE7ABB84342EEA9FCAE7A32B5 (void);
// 0x00000783 System.Void com.adjust.sdk.AdjustAndroid::OnResume()
extern void AdjustAndroid_OnResume_m6E0115176F1BF8238B46D009A2DEC66D1DDA1C97 (void);
// 0x00000784 System.Void com.adjust.sdk.AdjustAndroid::SetReferrer(System.String)
extern void AdjustAndroid_SetReferrer_m595DF146979F63B8867E574DC6FCB4F4B9695DB0 (void);
// 0x00000785 System.Void com.adjust.sdk.AdjustAndroid::GetGoogleAdId(System.Action`1<System.String>)
extern void AdjustAndroid_GetGoogleAdId_mDA64F7291B15B14F9B91E8812282463A73A02A40 (void);
// 0x00000786 System.String com.adjust.sdk.AdjustAndroid::GetAmazonAdId()
extern void AdjustAndroid_GetAmazonAdId_m7B737E67E9B95631CDBD4436109243F1AD78C5DA (void);
// 0x00000787 System.String com.adjust.sdk.AdjustAndroid::GetSdkVersion()
extern void AdjustAndroid_GetSdkVersion_m543983FDFEFD66BB6CCA9C9B5FD60799547CDA3E (void);
// 0x00000788 System.Void com.adjust.sdk.AdjustAndroid::VerifyPlayStorePurchase(com.adjust.sdk.AdjustPlayStorePurchase,System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>)
extern void AdjustAndroid_VerifyPlayStorePurchase_m9F1EE974114C4562DF39A8DCF93EFAB894B2A3A6 (void);
// 0x00000789 System.Void com.adjust.sdk.AdjustAndroid::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustAndroid_SetTestOptions_m320168C98A3232EE9383FAC4A22BD11193367CC1 (void);
// 0x0000078A System.Boolean com.adjust.sdk.AdjustAndroid::IsAppSecretSet(com.adjust.sdk.AdjustConfig)
extern void AdjustAndroid_IsAppSecretSet_m95C969EBDF9C163B3E3D665A8CD34481B4DB283D (void);
// 0x0000078B System.Void com.adjust.sdk.AdjustAndroid::.ctor()
extern void AdjustAndroid__ctor_m0035BEC1E1826590313CEAF840550DF637DF1268 (void);
// 0x0000078C System.Void com.adjust.sdk.AdjustAndroid::.cctor()
extern void AdjustAndroid__cctor_mA53D2A12B2657A0A62B72B55988B4EAA3E7A1C1A (void);
// 0x0000078D System.Void com.adjust.sdk.AdjustAndroid/AttributionChangeListener::.ctor(System.Action`1<com.adjust.sdk.AdjustAttribution>)
extern void AttributionChangeListener__ctor_mA6205D1D3A3CBA8F7FD10B413B0FD68FFFA548B8 (void);
// 0x0000078E System.Void com.adjust.sdk.AdjustAndroid/AttributionChangeListener::onAttributionChanged(UnityEngine.AndroidJavaObject)
extern void AttributionChangeListener_onAttributionChanged_m8EEEC19136B64A9315CCB266EF17028994504EB8 (void);
// 0x0000078F System.Void com.adjust.sdk.AdjustAndroid/DeferredDeeplinkListener::.ctor(System.Action`1<System.String>)
extern void DeferredDeeplinkListener__ctor_m9AFF50C9019C550FE14B914675EE675B36EE0582 (void);
// 0x00000790 System.Boolean com.adjust.sdk.AdjustAndroid/DeferredDeeplinkListener::launchReceivedDeeplink(UnityEngine.AndroidJavaObject)
extern void DeferredDeeplinkListener_launchReceivedDeeplink_mE6D35176E2C2AADC10C881FF8F3927AB528C5F3B (void);
// 0x00000791 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingSucceededListener::.ctor(System.Action`1<com.adjust.sdk.AdjustEventSuccess>)
extern void EventTrackingSucceededListener__ctor_m2A4ED2670A9A651245F8D5E344BDBFB5B3E3D43D (void);
// 0x00000792 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingSucceededListener::onFinishedEventTrackingSucceeded(UnityEngine.AndroidJavaObject)
extern void EventTrackingSucceededListener_onFinishedEventTrackingSucceeded_m202DFEA1323BD584F04E02C0A8323870A1959460 (void);
// 0x00000793 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingFailedListener::.ctor(System.Action`1<com.adjust.sdk.AdjustEventFailure>)
extern void EventTrackingFailedListener__ctor_m7D5E6FA8F17AAC33B50DFC25A1ACA5183FCE6545 (void);
// 0x00000794 System.Void com.adjust.sdk.AdjustAndroid/EventTrackingFailedListener::onFinishedEventTrackingFailed(UnityEngine.AndroidJavaObject)
extern void EventTrackingFailedListener_onFinishedEventTrackingFailed_m4FB36E7E27EF57535C67940BC6F148293CF366C0 (void);
// 0x00000795 System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingSucceededListener::.ctor(System.Action`1<com.adjust.sdk.AdjustSessionSuccess>)
extern void SessionTrackingSucceededListener__ctor_mD524E8A2ACB053F1100FC8BD428D97E256EEAAF1 (void);
// 0x00000796 System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingSucceededListener::onFinishedSessionTrackingSucceeded(UnityEngine.AndroidJavaObject)
extern void SessionTrackingSucceededListener_onFinishedSessionTrackingSucceeded_m641A6B81E4C67260BB84A35FC9803D11AA33102A (void);
// 0x00000797 System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingFailedListener::.ctor(System.Action`1<com.adjust.sdk.AdjustSessionFailure>)
extern void SessionTrackingFailedListener__ctor_mEC4AC95C995754842BC483D6D7AA0381A508BA6B (void);
// 0x00000798 System.Void com.adjust.sdk.AdjustAndroid/SessionTrackingFailedListener::onFinishedSessionTrackingFailed(UnityEngine.AndroidJavaObject)
extern void SessionTrackingFailedListener_onFinishedSessionTrackingFailed_m2494A8E23D515011E6014DEB2630F9F3A88F8D69 (void);
// 0x00000799 System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::.ctor(System.Action`1<System.String>)
extern void DeviceIdsReadListener__ctor_mDF6EF7D21335FCB0B244E975C68B5A7CC8F1E39D (void);
// 0x0000079A System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::onGoogleAdIdRead(System.String)
extern void DeviceIdsReadListener_onGoogleAdIdRead_m82DB269D37125FB1A6A3667454CF5B408AC179DD (void);
// 0x0000079B System.Void com.adjust.sdk.AdjustAndroid/DeviceIdsReadListener::onGoogleAdIdRead(UnityEngine.AndroidJavaObject)
extern void DeviceIdsReadListener_onGoogleAdIdRead_m46B72AE61C8F5B9408B0FA1374F0D34557C6D16A (void);
// 0x0000079C System.Void com.adjust.sdk.AdjustAndroid/VerificationInfoListener::.ctor(System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>)
extern void VerificationInfoListener__ctor_m6008AE03F8DF0F524F2FCBD37BACAAB1A9656F8C (void);
// 0x0000079D System.Void com.adjust.sdk.AdjustAndroid/VerificationInfoListener::onVerificationFinished(UnityEngine.AndroidJavaObject)
extern void VerificationInfoListener_onVerificationFinished_m172D0806BF4776A2B565AF3870F52F107BF1E94E (void);
// 0x0000079E System.Void com.adjust.sdk.Adjust::Awake()
extern void Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8 (void);
// 0x0000079F System.Void com.adjust.sdk.Adjust::OnApplicationPause(System.Boolean)
extern void Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012 (void);
// 0x000007A0 System.Void com.adjust.sdk.Adjust::start(com.adjust.sdk.AdjustConfig)
extern void Adjust_start_mF24352A04B12F9A3D5314851E1F476DD4BCBF0E3 (void);
// 0x000007A1 System.Void com.adjust.sdk.Adjust::trackEvent(com.adjust.sdk.AdjustEvent)
extern void Adjust_trackEvent_m788CBA9B9C606FE179B1582368C6A8A171425E36 (void);
// 0x000007A2 System.Void com.adjust.sdk.Adjust::setEnabled(System.Boolean)
extern void Adjust_setEnabled_mEF3633C6A6BBC4F91FCDB4F934A281DDE96712F1 (void);
// 0x000007A3 System.Boolean com.adjust.sdk.Adjust::isEnabled()
extern void Adjust_isEnabled_m254951D8A14448BE51FD7527AC80CC3950E6EE4B (void);
// 0x000007A4 System.Void com.adjust.sdk.Adjust::setOfflineMode(System.Boolean)
extern void Adjust_setOfflineMode_mDA3666A20F780FFDD8BCC07F9DB4D3215823C360 (void);
// 0x000007A5 System.Void com.adjust.sdk.Adjust::setDeviceToken(System.String)
extern void Adjust_setDeviceToken_mC1407160399AC998FF11E556CBFA3B38950B67A8 (void);
// 0x000007A6 System.Void com.adjust.sdk.Adjust::gdprForgetMe()
extern void Adjust_gdprForgetMe_m7A0CFD6A9B4137418F35E5AA201B6410737359C0 (void);
// 0x000007A7 System.Void com.adjust.sdk.Adjust::disableThirdPartySharing()
extern void Adjust_disableThirdPartySharing_m302224B75CA744974396F66A08AA1FE587873BE5 (void);
// 0x000007A8 System.Void com.adjust.sdk.Adjust::appWillOpenUrl(System.String)
extern void Adjust_appWillOpenUrl_m1E13932CE37598AA3C42C4509D2323491569A6F7 (void);
// 0x000007A9 System.Void com.adjust.sdk.Adjust::sendFirstPackages()
extern void Adjust_sendFirstPackages_mCFE8665FB15B08EC04CB5BCF14C2923E5189883F (void);
// 0x000007AA System.Void com.adjust.sdk.Adjust::addSessionPartnerParameter(System.String,System.String)
extern void Adjust_addSessionPartnerParameter_m4AC3D2786FFF94A176E93DEBF049FCC6BB71B3E6 (void);
// 0x000007AB System.Void com.adjust.sdk.Adjust::addSessionCallbackParameter(System.String,System.String)
extern void Adjust_addSessionCallbackParameter_mCCA2594D1EAADD151C56F4537823EBBFE3EA645F (void);
// 0x000007AC System.Void com.adjust.sdk.Adjust::removeSessionPartnerParameter(System.String)
extern void Adjust_removeSessionPartnerParameter_mA50E1FF8D276CA300DFBC2D8C52E1C60194BD98C (void);
// 0x000007AD System.Void com.adjust.sdk.Adjust::removeSessionCallbackParameter(System.String)
extern void Adjust_removeSessionCallbackParameter_m9D480CA9958869ACA6205BCA9CE84D235B4E45B1 (void);
// 0x000007AE System.Void com.adjust.sdk.Adjust::resetSessionPartnerParameters()
extern void Adjust_resetSessionPartnerParameters_m788D347452CE9472C4B4BFCB581272C33A903459 (void);
// 0x000007AF System.Void com.adjust.sdk.Adjust::resetSessionCallbackParameters()
extern void Adjust_resetSessionCallbackParameters_m092D40CDF59B8BE4FB165BB105F914E67E61D7B4 (void);
// 0x000007B0 System.Void com.adjust.sdk.Adjust::trackAdRevenue(System.String,System.String)
extern void Adjust_trackAdRevenue_mE7017F85963C48E260AE4A390B6E03C1365CEC8F (void);
// 0x000007B1 System.Void com.adjust.sdk.Adjust::trackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void Adjust_trackAdRevenue_m7F1739F079028F6AFA4F42629B803A152F0BB9AE (void);
// 0x000007B2 System.Void com.adjust.sdk.Adjust::trackAppStoreSubscription(com.adjust.sdk.AdjustAppStoreSubscription)
extern void Adjust_trackAppStoreSubscription_m3BE586B120A00A43CD00D754363EE0548488EB5B (void);
// 0x000007B3 System.Void com.adjust.sdk.Adjust::trackPlayStoreSubscription(com.adjust.sdk.AdjustPlayStoreSubscription)
extern void Adjust_trackPlayStoreSubscription_mB28584B6B95978DD627EF386DE383E258A55C394 (void);
// 0x000007B4 System.Void com.adjust.sdk.Adjust::trackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void Adjust_trackThirdPartySharing_mDBB4F949AA7D3532188DB67890C38CF006E5C3AE (void);
// 0x000007B5 System.Void com.adjust.sdk.Adjust::trackMeasurementConsent(System.Boolean)
extern void Adjust_trackMeasurementConsent_m28091C4476B42F91B98E2864860109C8B2FF7F3C (void);
// 0x000007B6 System.Void com.adjust.sdk.Adjust::requestTrackingAuthorizationWithCompletionHandler(System.Action`1<System.Int32>,System.String)
extern void Adjust_requestTrackingAuthorizationWithCompletionHandler_m0F4258A1D04183560AE8A0DB926BD1DD063330D2 (void);
// 0x000007B7 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32)
extern void Adjust_updateConversionValue_mB83B123737964C3CDD68E5B94588575C973EEADF (void);
// 0x000007B8 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32,System.Action`1<System.String>,System.String)
extern void Adjust_updateConversionValue_m22B9DD112274E04D0CE5267196B8FC850FF32BF0 (void);
// 0x000007B9 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32,System.String,System.Boolean,System.Action`1<System.String>,System.String)
extern void Adjust_updateConversionValue_m7619FC95F34018A0C8B115B114417776FB016C2A (void);
// 0x000007BA System.Void com.adjust.sdk.Adjust::checkForNewAttStatus()
extern void Adjust_checkForNewAttStatus_mE8CA001423E1EFFB7D610102747D76F5B2DE02AF (void);
// 0x000007BB System.Int32 com.adjust.sdk.Adjust::getAppTrackingAuthorizationStatus()
extern void Adjust_getAppTrackingAuthorizationStatus_m2CD6E53C3C56055BEF58BCA99C30417FE4231006 (void);
// 0x000007BC System.String com.adjust.sdk.Adjust::getAdid()
extern void Adjust_getAdid_m2285DFA62339E5ED400D271E8E661FD0174600A6 (void);
// 0x000007BD com.adjust.sdk.AdjustAttribution com.adjust.sdk.Adjust::getAttribution()
extern void Adjust_getAttribution_m3B7BBB4900BDCC0B92D6A103FE178D9E808F2188 (void);
// 0x000007BE System.String com.adjust.sdk.Adjust::getWinAdid()
extern void Adjust_getWinAdid_m528B95DD3250ED9D5BCAB2B58C3B8CD7F6B940F6 (void);
// 0x000007BF System.String com.adjust.sdk.Adjust::getIdfa()
extern void Adjust_getIdfa_mB068686DAF448330241C2367A2767891EADA60CB (void);
// 0x000007C0 System.String com.adjust.sdk.Adjust::getSdkVersion()
extern void Adjust_getSdkVersion_m8F7D96C6A76363F65E34D362C4912D3D5DA34E7D (void);
// 0x000007C1 System.Void com.adjust.sdk.Adjust::setReferrer(System.String)
extern void Adjust_setReferrer_m223FB53F552416333FC5B55D1E6F3D5DD5F9270B (void);
// 0x000007C2 System.Void com.adjust.sdk.Adjust::getGoogleAdId(System.Action`1<System.String>)
extern void Adjust_getGoogleAdId_m9F83C2BEA8B17987BC6BD492C1593444D427CB06 (void);
// 0x000007C3 System.String com.adjust.sdk.Adjust::getAmazonAdId()
extern void Adjust_getAmazonAdId_mCF6657242C0F74B3D50BD6C412C91311CFE96688 (void);
// 0x000007C4 System.String com.adjust.sdk.Adjust::getLastDeeplink()
extern void Adjust_getLastDeeplink_mF3BC9E789AEB1C8FB8C54BEDF98F3D04049D2D66 (void);
// 0x000007C5 System.Void com.adjust.sdk.Adjust::verifyAppStorePurchase(com.adjust.sdk.AdjustAppStorePurchase,System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>,System.String)
extern void Adjust_verifyAppStorePurchase_mE3B63AAEDB2569792D5167200AEFE589E69AE2FC (void);
// 0x000007C6 System.Void com.adjust.sdk.Adjust::verifyPlayStorePurchase(com.adjust.sdk.AdjustPlayStorePurchase,System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>)
extern void Adjust_verifyPlayStorePurchase_m769B3D59149BABD106127A3D5E9C84F67CFA3865 (void);
// 0x000007C7 System.Boolean com.adjust.sdk.Adjust::IsEditor()
extern void Adjust_IsEditor_m01CB66E062E93F971F53080A97634DFA78C2A184 (void);
// 0x000007C8 System.Void com.adjust.sdk.Adjust::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Adjust_SetTestOptions_m5313B20E29B0029F24CF33ECE44DC008CDD6F360 (void);
// 0x000007C9 System.Void com.adjust.sdk.Adjust::.ctor()
extern void Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B (void);
// 0x000007CA System.Void com.adjust.sdk.AdjustAdRevenue::.ctor(System.String)
extern void AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8 (void);
// 0x000007CB System.Void com.adjust.sdk.AdjustAdRevenue::setRevenue(System.Double,System.String)
extern void AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7 (void);
// 0x000007CC System.Void com.adjust.sdk.AdjustAdRevenue::setAdImpressionsCount(System.Int32)
extern void AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115 (void);
// 0x000007CD System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueNetwork(System.String)
extern void AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25 (void);
// 0x000007CE System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueUnit(System.String)
extern void AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856 (void);
// 0x000007CF System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenuePlacement(System.String)
extern void AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B (void);
// 0x000007D0 System.Void com.adjust.sdk.AdjustAdRevenue::addCallbackParameter(System.String,System.String)
extern void AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C (void);
// 0x000007D1 System.Void com.adjust.sdk.AdjustAdRevenue::addPartnerParameter(System.String,System.String)
extern void AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3 (void);
// 0x000007D2 System.Void com.adjust.sdk.AdjustAppStorePurchase::.ctor(System.String,System.String,System.String)
extern void AdjustAppStorePurchase__ctor_mC1D24DD9ABED88BF92A169ABE33573DAB8FFF404 (void);
// 0x000007D3 System.Void com.adjust.sdk.AdjustAppStoreSubscription::.ctor(System.String,System.String,System.String,System.String)
extern void AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536 (void);
// 0x000007D4 System.Void com.adjust.sdk.AdjustAppStoreSubscription::setTransactionDate(System.String)
extern void AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C (void);
// 0x000007D5 System.Void com.adjust.sdk.AdjustAppStoreSubscription::setSalesRegion(System.String)
extern void AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B (void);
// 0x000007D6 System.Void com.adjust.sdk.AdjustAppStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70 (void);
// 0x000007D7 System.Void com.adjust.sdk.AdjustAppStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019 (void);
// 0x000007D8 System.String com.adjust.sdk.AdjustAttribution::get_adid()
extern void AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159 (void);
// 0x000007D9 System.Void com.adjust.sdk.AdjustAttribution::set_adid(System.String)
extern void AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6 (void);
// 0x000007DA System.String com.adjust.sdk.AdjustAttribution::get_network()
extern void AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3 (void);
// 0x000007DB System.Void com.adjust.sdk.AdjustAttribution::set_network(System.String)
extern void AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20 (void);
// 0x000007DC System.String com.adjust.sdk.AdjustAttribution::get_adgroup()
extern void AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416 (void);
// 0x000007DD System.Void com.adjust.sdk.AdjustAttribution::set_adgroup(System.String)
extern void AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB (void);
// 0x000007DE System.String com.adjust.sdk.AdjustAttribution::get_campaign()
extern void AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750 (void);
// 0x000007DF System.Void com.adjust.sdk.AdjustAttribution::set_campaign(System.String)
extern void AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9 (void);
// 0x000007E0 System.String com.adjust.sdk.AdjustAttribution::get_creative()
extern void AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36 (void);
// 0x000007E1 System.Void com.adjust.sdk.AdjustAttribution::set_creative(System.String)
extern void AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183 (void);
// 0x000007E2 System.String com.adjust.sdk.AdjustAttribution::get_clickLabel()
extern void AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D (void);
// 0x000007E3 System.Void com.adjust.sdk.AdjustAttribution::set_clickLabel(System.String)
extern void AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE (void);
// 0x000007E4 System.String com.adjust.sdk.AdjustAttribution::get_trackerName()
extern void AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52 (void);
// 0x000007E5 System.Void com.adjust.sdk.AdjustAttribution::set_trackerName(System.String)
extern void AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876 (void);
// 0x000007E6 System.String com.adjust.sdk.AdjustAttribution::get_trackerToken()
extern void AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A (void);
// 0x000007E7 System.Void com.adjust.sdk.AdjustAttribution::set_trackerToken(System.String)
extern void AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F (void);
// 0x000007E8 System.String com.adjust.sdk.AdjustAttribution::get_costType()
extern void AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6 (void);
// 0x000007E9 System.Void com.adjust.sdk.AdjustAttribution::set_costType(System.String)
extern void AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0 (void);
// 0x000007EA System.Nullable`1<System.Double> com.adjust.sdk.AdjustAttribution::get_costAmount()
extern void AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182 (void);
// 0x000007EB System.Void com.adjust.sdk.AdjustAttribution::set_costAmount(System.Nullable`1<System.Double>)
extern void AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647 (void);
// 0x000007EC System.String com.adjust.sdk.AdjustAttribution::get_costCurrency()
extern void AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999 (void);
// 0x000007ED System.Void com.adjust.sdk.AdjustAttribution::set_costCurrency(System.String)
extern void AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51 (void);
// 0x000007EE System.String com.adjust.sdk.AdjustAttribution::get_fbInstallReferrer()
extern void AdjustAttribution_get_fbInstallReferrer_m730BCBF4BD7687B6ABA49F85E1E3592944782A68 (void);
// 0x000007EF System.Void com.adjust.sdk.AdjustAttribution::set_fbInstallReferrer(System.String)
extern void AdjustAttribution_set_fbInstallReferrer_m03CE43EE59FB3D653CB09AB9BD1DE86EE11D292D (void);
// 0x000007F0 System.Void com.adjust.sdk.AdjustAttribution::.ctor()
extern void AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307 (void);
// 0x000007F1 System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.String)
extern void AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A (void);
// 0x000007F2 System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9 (void);
// 0x000007F3 System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment)
extern void AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395 (void);
// 0x000007F4 System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment,System.Boolean)
extern void AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7 (void);
// 0x000007F5 System.Void com.adjust.sdk.AdjustConfig::setLogLevel(com.adjust.sdk.AdjustLogLevel)
extern void AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F (void);
// 0x000007F6 System.Void com.adjust.sdk.AdjustConfig::setDefaultTracker(System.String)
extern void AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724 (void);
// 0x000007F7 System.Void com.adjust.sdk.AdjustConfig::setExternalDeviceId(System.String)
extern void AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF (void);
// 0x000007F8 System.Void com.adjust.sdk.AdjustConfig::setLaunchDeferredDeeplink(System.Boolean)
extern void AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F (void);
// 0x000007F9 System.Void com.adjust.sdk.AdjustConfig::setSendInBackground(System.Boolean)
extern void AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317 (void);
// 0x000007FA System.Void com.adjust.sdk.AdjustConfig::setEventBufferingEnabled(System.Boolean)
extern void AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982 (void);
// 0x000007FB System.Void com.adjust.sdk.AdjustConfig::setCoppaCompliantEnabled(System.Boolean)
extern void AdjustConfig_setCoppaCompliantEnabled_m43149C9F256F85E6149011100CEC777326B818DF (void);
// 0x000007FC System.Void com.adjust.sdk.AdjustConfig::setNeedsCost(System.Boolean)
extern void AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD (void);
// 0x000007FD System.Void com.adjust.sdk.AdjustConfig::setDelayStart(System.Double)
extern void AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6 (void);
// 0x000007FE System.Void com.adjust.sdk.AdjustConfig::setUserAgent(System.String)
extern void AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D (void);
// 0x000007FF System.Void com.adjust.sdk.AdjustConfig::setIsDeviceKnown(System.Boolean)
extern void AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA (void);
// 0x00000800 System.Void com.adjust.sdk.AdjustConfig::setUrlStrategy(System.String)
extern void AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263 (void);
// 0x00000801 System.Void com.adjust.sdk.AdjustConfig::setAppSecret(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64)
extern void AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE (void);
// 0x00000802 System.Void com.adjust.sdk.AdjustConfig::setDeferredDeeplinkDelegate(System.Action`1<System.String>,System.String)
extern void AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750 (void);
// 0x00000803 System.Action`1<System.String> com.adjust.sdk.AdjustConfig::getDeferredDeeplinkDelegate()
extern void AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574 (void);
// 0x00000804 System.Void com.adjust.sdk.AdjustConfig::setAttributionChangedDelegate(System.Action`1<com.adjust.sdk.AdjustAttribution>,System.String)
extern void AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054 (void);
// 0x00000805 System.Action`1<com.adjust.sdk.AdjustAttribution> com.adjust.sdk.AdjustConfig::getAttributionChangedDelegate()
extern void AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E (void);
// 0x00000806 System.Void com.adjust.sdk.AdjustConfig::setEventSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustEventSuccess>,System.String)
extern void AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00 (void);
// 0x00000807 System.Action`1<com.adjust.sdk.AdjustEventSuccess> com.adjust.sdk.AdjustConfig::getEventSuccessDelegate()
extern void AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE (void);
// 0x00000808 System.Void com.adjust.sdk.AdjustConfig::setEventFailureDelegate(System.Action`1<com.adjust.sdk.AdjustEventFailure>,System.String)
extern void AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919 (void);
// 0x00000809 System.Action`1<com.adjust.sdk.AdjustEventFailure> com.adjust.sdk.AdjustConfig::getEventFailureDelegate()
extern void AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73 (void);
// 0x0000080A System.Void com.adjust.sdk.AdjustConfig::setSessionSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustSessionSuccess>,System.String)
extern void AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED (void);
// 0x0000080B System.Action`1<com.adjust.sdk.AdjustSessionSuccess> com.adjust.sdk.AdjustConfig::getSessionSuccessDelegate()
extern void AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1 (void);
// 0x0000080C System.Void com.adjust.sdk.AdjustConfig::setSessionFailureDelegate(System.Action`1<com.adjust.sdk.AdjustSessionFailure>,System.String)
extern void AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC (void);
// 0x0000080D System.Action`1<com.adjust.sdk.AdjustSessionFailure> com.adjust.sdk.AdjustConfig::getSessionFailureDelegate()
extern void AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66 (void);
// 0x0000080E System.Void com.adjust.sdk.AdjustConfig::setAllowiAdInfoReading(System.Boolean)
extern void AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C (void);
// 0x0000080F System.Void com.adjust.sdk.AdjustConfig::setAllowAdServicesInfoReading(System.Boolean)
extern void AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1 (void);
// 0x00000810 System.Void com.adjust.sdk.AdjustConfig::setAllowIdfaReading(System.Boolean)
extern void AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA (void);
// 0x00000811 System.Void com.adjust.sdk.AdjustConfig::deactivateSKAdNetworkHandling()
extern void AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E (void);
// 0x00000812 System.Void com.adjust.sdk.AdjustConfig::setLinkMeEnabled(System.Boolean)
extern void AdjustConfig_setLinkMeEnabled_mC3B85AB4A602F3BB59B8B4B7FA973D9F2B8EB55E (void);
// 0x00000813 System.Void com.adjust.sdk.AdjustConfig::setConversionValueUpdatedDelegate(System.Action`1<System.Int32>,System.String)
extern void AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4 (void);
// 0x00000814 System.Action`1<System.Int32> com.adjust.sdk.AdjustConfig::getConversionValueUpdatedDelegate()
extern void AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23 (void);
// 0x00000815 System.Void com.adjust.sdk.AdjustConfig::setSkad4ConversionValueUpdatedDelegate(System.Action`3<System.Int32,System.String,System.Boolean>,System.String)
extern void AdjustConfig_setSkad4ConversionValueUpdatedDelegate_mBDC7D976A8BD22E4680DA70B9EC5EE7ECC4A45E4 (void);
// 0x00000816 System.Action`3<System.Int32,System.String,System.Boolean> com.adjust.sdk.AdjustConfig::getSkad4ConversionValueUpdatedDelegate()
extern void AdjustConfig_getSkad4ConversionValueUpdatedDelegate_m791D25B57223A51EDE3A28E916F1A6AB43EC2FFF (void);
// 0x00000817 System.Void com.adjust.sdk.AdjustConfig::setAttConsentWaitingInterval(System.Int32)
extern void AdjustConfig_setAttConsentWaitingInterval_m207E02DC9D926C771965BB2270A18A914B1B1DA3 (void);
// 0x00000818 System.Void com.adjust.sdk.AdjustConfig::setProcessName(System.String)
extern void AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53 (void);
// 0x00000819 System.Void com.adjust.sdk.AdjustConfig::setReadMobileEquipmentIdentity(System.Boolean)
extern void AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C (void);
// 0x0000081A System.Void com.adjust.sdk.AdjustConfig::setPreinstallTrackingEnabled(System.Boolean)
extern void AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA (void);
// 0x0000081B System.Void com.adjust.sdk.AdjustConfig::setPreinstallFilePath(System.String)
extern void AdjustConfig_setPreinstallFilePath_mF70F4E2F50F2E73E7EAF1DEAB6351F6AB6EB728A (void);
// 0x0000081C System.Void com.adjust.sdk.AdjustConfig::setPlayStoreKidsAppEnabled(System.Boolean)
extern void AdjustConfig_setPlayStoreKidsAppEnabled_m6786F76DFEE24836BA51A2FA1B798FB2AEA86484 (void);
// 0x0000081D System.Void com.adjust.sdk.AdjustConfig::setLogDelegate(System.Action`1<System.String>)
extern void AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C (void);
// 0x0000081E System.String com.adjust.sdk.AdjustEnvironmentExtension::ToLowercaseString(com.adjust.sdk.AdjustEnvironment)
extern void AdjustEnvironmentExtension_ToLowercaseString_mAEDC5B0CBA386D07FB258ED1BDFC83CB4394D49B (void);
// 0x0000081F System.Void com.adjust.sdk.AdjustEvent::.ctor(System.String)
extern void AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03 (void);
// 0x00000820 System.Void com.adjust.sdk.AdjustEvent::setRevenue(System.Double,System.String)
extern void AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49 (void);
// 0x00000821 System.Void com.adjust.sdk.AdjustEvent::addCallbackParameter(System.String,System.String)
extern void AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC (void);
// 0x00000822 System.Void com.adjust.sdk.AdjustEvent::addPartnerParameter(System.String,System.String)
extern void AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B (void);
// 0x00000823 System.Void com.adjust.sdk.AdjustEvent::setCallbackId(System.String)
extern void AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98 (void);
// 0x00000824 System.Void com.adjust.sdk.AdjustEvent::setTransactionId(System.String)
extern void AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254 (void);
// 0x00000825 System.Void com.adjust.sdk.AdjustEvent::setProductId(System.String)
extern void AdjustEvent_setProductId_mA5A287AD1813C5E813F12A097C4587AD23B2C8AA (void);
// 0x00000826 System.Void com.adjust.sdk.AdjustEvent::setReceipt(System.String,System.String)
extern void AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD (void);
// 0x00000827 System.Void com.adjust.sdk.AdjustEvent::setReceipt(System.String)
extern void AdjustEvent_setReceipt_m44238E44C88E2249BF57CB4775FF867EAEA76497 (void);
// 0x00000828 System.Void com.adjust.sdk.AdjustEvent::setPurchaseToken(System.String)
extern void AdjustEvent_setPurchaseToken_m921AABC8958704EE95BB091E047E4DF47E4E4F3F (void);
// 0x00000829 System.String com.adjust.sdk.AdjustEventFailure::get_Adid()
extern void AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9 (void);
// 0x0000082A System.Void com.adjust.sdk.AdjustEventFailure::set_Adid(System.String)
extern void AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC (void);
// 0x0000082B System.String com.adjust.sdk.AdjustEventFailure::get_Message()
extern void AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C (void);
// 0x0000082C System.Void com.adjust.sdk.AdjustEventFailure::set_Message(System.String)
extern void AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472 (void);
// 0x0000082D System.String com.adjust.sdk.AdjustEventFailure::get_Timestamp()
extern void AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C (void);
// 0x0000082E System.Void com.adjust.sdk.AdjustEventFailure::set_Timestamp(System.String)
extern void AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F (void);
// 0x0000082F System.String com.adjust.sdk.AdjustEventFailure::get_EventToken()
extern void AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F (void);
// 0x00000830 System.Void com.adjust.sdk.AdjustEventFailure::set_EventToken(System.String)
extern void AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E (void);
// 0x00000831 System.String com.adjust.sdk.AdjustEventFailure::get_CallbackId()
extern void AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F (void);
// 0x00000832 System.Void com.adjust.sdk.AdjustEventFailure::set_CallbackId(System.String)
extern void AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD (void);
// 0x00000833 System.Boolean com.adjust.sdk.AdjustEventFailure::get_WillRetry()
extern void AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138 (void);
// 0x00000834 System.Void com.adjust.sdk.AdjustEventFailure::set_WillRetry(System.Boolean)
extern void AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9 (void);
// 0x00000835 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventFailure::get_JsonResponse()
extern void AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A (void);
// 0x00000836 System.Void com.adjust.sdk.AdjustEventFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE (void);
// 0x00000837 System.Void com.adjust.sdk.AdjustEventFailure::.ctor()
extern void AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827 (void);
// 0x00000838 System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14 (void);
// 0x00000839 System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.String)
extern void AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284 (void);
// 0x0000083A System.Void com.adjust.sdk.AdjustEventFailure::BuildJsonResponseFromString(System.String)
extern void AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A (void);
// 0x0000083B System.String com.adjust.sdk.AdjustEventFailure::GetJsonResponse()
extern void AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD (void);
// 0x0000083C System.String com.adjust.sdk.AdjustEventSuccess::get_Adid()
extern void AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5 (void);
// 0x0000083D System.Void com.adjust.sdk.AdjustEventSuccess::set_Adid(System.String)
extern void AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD (void);
// 0x0000083E System.String com.adjust.sdk.AdjustEventSuccess::get_Message()
extern void AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2 (void);
// 0x0000083F System.Void com.adjust.sdk.AdjustEventSuccess::set_Message(System.String)
extern void AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F (void);
// 0x00000840 System.String com.adjust.sdk.AdjustEventSuccess::get_Timestamp()
extern void AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10 (void);
// 0x00000841 System.Void com.adjust.sdk.AdjustEventSuccess::set_Timestamp(System.String)
extern void AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B (void);
// 0x00000842 System.String com.adjust.sdk.AdjustEventSuccess::get_EventToken()
extern void AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E (void);
// 0x00000843 System.Void com.adjust.sdk.AdjustEventSuccess::set_EventToken(System.String)
extern void AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64 (void);
// 0x00000844 System.String com.adjust.sdk.AdjustEventSuccess::get_CallbackId()
extern void AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983 (void);
// 0x00000845 System.Void com.adjust.sdk.AdjustEventSuccess::set_CallbackId(System.String)
extern void AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9 (void);
// 0x00000846 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventSuccess::get_JsonResponse()
extern void AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75 (void);
// 0x00000847 System.Void com.adjust.sdk.AdjustEventSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90 (void);
// 0x00000848 System.Void com.adjust.sdk.AdjustEventSuccess::.ctor()
extern void AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C (void);
// 0x00000849 System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B (void);
// 0x0000084A System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.String)
extern void AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75 (void);
// 0x0000084B System.Void com.adjust.sdk.AdjustEventSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C (void);
// 0x0000084C System.String com.adjust.sdk.AdjustEventSuccess::GetJsonResponse()
extern void AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B (void);
// 0x0000084D System.String com.adjust.sdk.AdjustLogLevelExtension::ToLowercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToLowercaseString_mEF9C47460E6774026C495F7646A4369476C53588 (void);
// 0x0000084E System.String com.adjust.sdk.AdjustLogLevelExtension::ToUppercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToUppercaseString_m457BEEAE7375DBA0C92F1180B69A432CE360A133 (void);
// 0x0000084F System.Void com.adjust.sdk.AdjustPlayStorePurchase::.ctor(System.String,System.String)
extern void AdjustPlayStorePurchase__ctor_m7B9ABD9ED2899FDF05BDF64D3E52047C489915F2 (void);
// 0x00000850 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern void AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1 (void);
// 0x00000851 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::setPurchaseTime(System.String)
extern void AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4 (void);
// 0x00000852 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4 (void);
// 0x00000853 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4 (void);
// 0x00000854 System.Int32 com.adjust.sdk.AdjustPurchaseVerificationInfo::get_code()
extern void AdjustPurchaseVerificationInfo_get_code_m8FB046D3E105D4581BDA3A4C60BD5FE7FE0608BF (void);
// 0x00000855 System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_code(System.Int32)
extern void AdjustPurchaseVerificationInfo_set_code_mC3B02737B3A0F2A350DFF882C39A2829D2E7A8EC (void);
// 0x00000856 System.String com.adjust.sdk.AdjustPurchaseVerificationInfo::get_message()
extern void AdjustPurchaseVerificationInfo_get_message_mF44C745A7801EFE4681CA685163736AAD65BD093 (void);
// 0x00000857 System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_message(System.String)
extern void AdjustPurchaseVerificationInfo_set_message_m2CF4884928E67730C04532244236225CF728AEB6 (void);
// 0x00000858 System.String com.adjust.sdk.AdjustPurchaseVerificationInfo::get_verificationStatus()
extern void AdjustPurchaseVerificationInfo_get_verificationStatus_mBD654E0CF645A3B31439DFD66DEEF7A18469FF97 (void);
// 0x00000859 System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_verificationStatus(System.String)
extern void AdjustPurchaseVerificationInfo_set_verificationStatus_mAD3AC1434552BFC186F6B8A51F2116F04CE9D692 (void);
// 0x0000085A System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::.ctor()
extern void AdjustPurchaseVerificationInfo__ctor_mFD144FB4E4ACF2B998F0FA447CD249C2850972F1 (void);
// 0x0000085B System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::.ctor(System.String)
extern void AdjustPurchaseVerificationInfo__ctor_m8C6412DCE35238BF47E8E4202598F74CE085668E (void);
// 0x0000085C System.String com.adjust.sdk.AdjustSessionFailure::get_Adid()
extern void AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986 (void);
// 0x0000085D System.Void com.adjust.sdk.AdjustSessionFailure::set_Adid(System.String)
extern void AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7 (void);
// 0x0000085E System.String com.adjust.sdk.AdjustSessionFailure::get_Message()
extern void AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071 (void);
// 0x0000085F System.Void com.adjust.sdk.AdjustSessionFailure::set_Message(System.String)
extern void AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835 (void);
// 0x00000860 System.String com.adjust.sdk.AdjustSessionFailure::get_Timestamp()
extern void AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9 (void);
// 0x00000861 System.Void com.adjust.sdk.AdjustSessionFailure::set_Timestamp(System.String)
extern void AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E (void);
// 0x00000862 System.Boolean com.adjust.sdk.AdjustSessionFailure::get_WillRetry()
extern void AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7 (void);
// 0x00000863 System.Void com.adjust.sdk.AdjustSessionFailure::set_WillRetry(System.Boolean)
extern void AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF (void);
// 0x00000864 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionFailure::get_JsonResponse()
extern void AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337 (void);
// 0x00000865 System.Void com.adjust.sdk.AdjustSessionFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E (void);
// 0x00000866 System.Void com.adjust.sdk.AdjustSessionFailure::.ctor()
extern void AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577 (void);
// 0x00000867 System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2 (void);
// 0x00000868 System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.String)
extern void AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F (void);
// 0x00000869 System.Void com.adjust.sdk.AdjustSessionFailure::BuildJsonResponseFromString(System.String)
extern void AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6 (void);
// 0x0000086A System.String com.adjust.sdk.AdjustSessionFailure::GetJsonResponse()
extern void AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351 (void);
// 0x0000086B System.String com.adjust.sdk.AdjustSessionSuccess::get_Adid()
extern void AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414 (void);
// 0x0000086C System.Void com.adjust.sdk.AdjustSessionSuccess::set_Adid(System.String)
extern void AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573 (void);
// 0x0000086D System.String com.adjust.sdk.AdjustSessionSuccess::get_Message()
extern void AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA (void);
// 0x0000086E System.Void com.adjust.sdk.AdjustSessionSuccess::set_Message(System.String)
extern void AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99 (void);
// 0x0000086F System.String com.adjust.sdk.AdjustSessionSuccess::get_Timestamp()
extern void AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813 (void);
// 0x00000870 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Timestamp(System.String)
extern void AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA (void);
// 0x00000871 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionSuccess::get_JsonResponse()
extern void AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595 (void);
// 0x00000872 System.Void com.adjust.sdk.AdjustSessionSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC (void);
// 0x00000873 System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor()
extern void AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A (void);
// 0x00000874 System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D (void);
// 0x00000875 System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.String)
extern void AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542 (void);
// 0x00000876 System.Void com.adjust.sdk.AdjustSessionSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE (void);
// 0x00000877 System.String com.adjust.sdk.AdjustSessionSuccess::GetJsonResponse()
extern void AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D (void);
// 0x00000878 System.Void com.adjust.sdk.AdjustThirdPartySharing::.ctor(System.Nullable`1<System.Boolean>)
extern void AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82 (void);
// 0x00000879 System.Void com.adjust.sdk.AdjustThirdPartySharing::addGranularOption(System.String,System.String,System.String)
extern void AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77 (void);
// 0x0000087A System.Void com.adjust.sdk.AdjustThirdPartySharing::addPartnerSharingSetting(System.String,System.String,System.Boolean)
extern void AdjustThirdPartySharing_addPartnerSharingSetting_m46C4F5606AF8CE842EFA05FD126197ACCEC911E1 (void);
// 0x0000087B System.String com.adjust.sdk.AdjustUrlStrategyExtension::ToLowerCaseString(com.adjust.sdk.AdjustUrlStrategy)
extern void AdjustUrlStrategyExtension_ToLowerCaseString_mC501B171FABC8E81E217A019B01F9D079D4DC7A0 (void);
// 0x0000087C System.Int32 com.adjust.sdk.AdjustUtils::ConvertLogLevel(System.Nullable`1<com.adjust.sdk.AdjustLogLevel>)
extern void AdjustUtils_ConvertLogLevel_mF7D0CB4C0B08008E37686670B7361871B737A53F (void);
// 0x0000087D System.Int32 com.adjust.sdk.AdjustUtils::ConvertBool(System.Nullable`1<System.Boolean>)
extern void AdjustUtils_ConvertBool_mBFC3BC841A003201C7056448C67C35625379E786 (void);
// 0x0000087E System.Double com.adjust.sdk.AdjustUtils::ConvertDouble(System.Nullable`1<System.Double>)
extern void AdjustUtils_ConvertDouble_m328F7E087047FA52AEF1D681FCCD32D80791B749 (void);
// 0x0000087F System.Int32 com.adjust.sdk.AdjustUtils::ConvertInt(System.Nullable`1<System.Int32>)
extern void AdjustUtils_ConvertInt_mE9AACF8054BA25B7605B3F8727091ED4F41CF37C (void);
// 0x00000880 System.Int64 com.adjust.sdk.AdjustUtils::ConvertLong(System.Nullable`1<System.Int64>)
extern void AdjustUtils_ConvertLong_m7B66091ED09C4DA947FB5C61D5AC40762100FAF4 (void);
// 0x00000881 System.String com.adjust.sdk.AdjustUtils::ConvertListToJson(System.Collections.Generic.List`1<System.String>)
extern void AdjustUtils_ConvertListToJson_m0834067B90DD8AA9713B0A395933C806BDB84689 (void);
// 0x00000882 System.String com.adjust.sdk.AdjustUtils::GetJsonResponseCompact(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_GetJsonResponseCompact_mB1763C6F6A17665BAA0534CE919BCFB7D7D491F6 (void);
// 0x00000883 System.String com.adjust.sdk.AdjustUtils::GetJsonString(com.adjust.sdk.JSONNode,System.String)
extern void AdjustUtils_GetJsonString_m7E4ABC127B656F2CF1D6D5C2973CCDC9345477A1 (void);
// 0x00000884 System.Void com.adjust.sdk.AdjustUtils::WriteJsonResponseDictionary(com.adjust.sdk.JSONClass,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_WriteJsonResponseDictionary_m45C6F803D1190D8144D7E3441A4CF870606463ED (void);
// 0x00000885 System.String com.adjust.sdk.AdjustUtils::TryGetValue(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String)
extern void AdjustUtils_TryGetValue_m3BF1818C3435B2DD8794C6BF52073DE2D50A57E9 (void);
// 0x00000886 System.Int32 com.adjust.sdk.AdjustUtils::GetSkad4ConversionValue(System.String)
extern void AdjustUtils_GetSkad4ConversionValue_mF1B95F499F7AECC0987FA3A4DD57E10F9582741E (void);
// 0x00000887 System.String com.adjust.sdk.AdjustUtils::GetSkad4CoarseValue(System.String)
extern void AdjustUtils_GetSkad4CoarseValue_m6A96D9597EAAD2D606A7B8730683A1870E324FCA (void);
// 0x00000888 System.Boolean com.adjust.sdk.AdjustUtils::GetSkad4LockWindow(System.String)
extern void AdjustUtils_GetSkad4LockWindow_mE9E55E3A5B683CDF1BF463568133655A4BEEA39C (void);
// 0x00000889 UnityEngine.AndroidJavaObject com.adjust.sdk.AdjustUtils::TestOptionsMap2AndroidJavaObject(System.Collections.Generic.Dictionary`2<System.String,System.String>,UnityEngine.AndroidJavaObject)
extern void AdjustUtils_TestOptionsMap2AndroidJavaObject_m898CB8FB47E84D854197B546786A2AD9C160FCAB (void);
// 0x0000088A System.Void com.adjust.sdk.AdjustUtils::.ctor()
extern void AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2 (void);
// 0x0000088B System.Void com.adjust.sdk.AdjustUtils::.cctor()
extern void AdjustUtils__cctor_m4489DD780E5669549E8C7EDAF985BDEC7AC456E1 (void);
static Il2CppMethodPointer s_methodPointers[2187] = 
{
	ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514,
	ExampleGUI_ShowGUI_m3A21CF5132CCE304624990EEB9311B27FC974279,
	ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0,
	ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73,
	ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D,
	ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE,
	ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C,
	ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059,
	ExampleGUI_DeferredDeeplinkCallback_m0490F45A5F0CAB4623C4DE98C9056D175511544B,
	ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D,
	U3CU3Ec__cctor_m8004B56DF658186326E479AFFA0822C993DD72ED,
	U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6,
	U3CU3Ec_U3COnGUIU3Eb__6_0_m44C8CA7B767C4F1F34EBEA0E08A25658315F0219,
	AdsManager_GetRewardedAdInfo_m4930CED0217C9D5530AEDEC77D8CFCEF51F0FB33,
	AdsManager_add_OnAdRewardSuccess_m4658B080D07C49D78A2EC7A21F099BDECB4AE95D,
	AdsManager_remove_OnAdRewardSuccess_m7014CABED0E7AC7CF6B629B1B6CF1498FD01CCCD,
	AdsManager_Awake_m41E841D8C091F00F7AB09B5403D9C3BD4BED7EBB,
	AdsManager_Start_m00CAFF84B03355B41A941006E80DB2F1D49E3EF3,
	AdsManager_SetInterstitialEvents_m5BF06DEF0FFCF9591FC9FF64E463429855FAF7C9,
	AdsManager_SetRewardedEvents_m6D6B5755B2BEE37BB817051FF3A036F2117FD3B1,
	AdsManager_OnDisable_m6C06FADBF21F3490168ABAE34676F4854A48A47C,
	AdsManager_Interstitial_OnAdPaid_mE1BB2946B860F37A6FDC26C037B7A9B7D5F994A2,
	AdsManager_Interstitial_OnAdImpressionRecorded_m83F85AC7A2E1F3A2BB990FB81CADB9D0B74FC4B1,
	AdsManager_Interstitial_OnAdFullScreenContentOpened_m4DAA50CD9F49E35ABAAA2FC21753A7FE470B6248,
	AdsManager_Interstitial_OnAdFullScreenContentFailed_mAD8110C843F051C1BFF4382BD8CACF4293BB2EB3,
	AdsManager_Interstitial_OnAdFullScreenContentClosed_m20FFA8DCA618C20153B1DA7CA6120770A6B69EF9,
	AdsManager_Interstitial_OnAdClicked_m63AD53CF5E333C912918AE3B204F329E06C23B24,
	AdsManager_Rewarded_OnAdPaid_m69FFD13FBDA650184D36DF69B65946DA65E0870B,
	AdsManager_Rewarded_OnAdImpressionRecorded_m38F857DCC44071139F06C9F7564B42A23AD869C8,
	AdsManager_Rewarded_OnAdFullScreenContentOpened_m7088C2C989C4A71DFD0C7C88D16A24AEAA309C31,
	AdsManager_Rewarded_OnAdFullScreenContentFailed_mC01D81197F5C50F54B41E1A67273E7FC907C0455,
	AdsManager_Rewarded_OnAdFullScreenContentClosed_m6DEDB1D092E615C99FA69BB6759B097B3D86EADD,
	AdsManager_Rewarded_OnAdClicked_m56129EFA6EE079FF344060A7316B8C0ACB81EB2C,
	AdsManager_LoadBannerAds_m76B46F4AEF2ECBD3E76711588D4445842CEAC9DA,
	AdsManager_LoadInterstitialAds_mA12DE7ED1810B0C535E6B02904F420C6C9F088EC,
	AdsManager_LoadRewardedAds_m2546F5FC35D817F97A6BDC8C8C3400287E2EA9BE,
	AdsManager_RequestRewardedAds_m2C6C742352D6BB34ED2F6E67F3212F72DF96D223,
	AdsManager_OnGetReward_mB1C6846FBB3ECC86581EF12D9677D0D72F3E3623,
	AdsManager_RequestInterstitial_mFA0DD2D687719943460CDD053C44AE096661FC93,
	AdsManager__ctor_m6A4EB92F72E9B7F28CC4E62F87F54F89CBAD6B3C,
	AdsManager__cctor_m9183945BB8BEA85401F1952C993E3FD6333D0A92,
	AdsManager_U3CStartU3Eb__18_0_mD5CBA755AFE5045A563116519089A6E4C1D47B71,
	AdsManager_U3CLoadInterstitialAdsU3Eb__35_0_m9FD502D1A32728CE332F1A8E4BD1811E5F0B2AF3,
	AdsManager_U3CLoadRewardedAdsU3Eb__36_0_m2FD4A41592D38EB3CCF9E259DC66D0BC7677C650,
	AdsRewardSuccessDelegate__ctor_m5D24EFB03154EC18222127814D6217678206B2DC,
	AdsRewardSuccessDelegate_Invoke_m73D12F2CB60B95B30447C1D442483C7F7955A059,
	AdsRewardSuccessDelegate_BeginInvoke_m2D3F8251156FA7D4117514522E573A813006C17D,
	AdsRewardSuccessDelegate_EndInvoke_mD42D874EE40FA61E68DB6C03DB86A0E966AC34B8,
	AuthenticationPage_Update_mF9E17C6FB363D3323CAD217432C3F9C3B4DA7F43,
	AuthenticationPage_SetLabel_mD00348C4952CF200CDBC9B0785E4B7CBEC097953,
	AuthenticationPage__ctor_mB01D55AAA281991663012BEEA31C4D96FAE6F3A9,
	BonusCard_InitCard_m2DC0665E3456C3514D80406CDCAA62E736242CD0,
	BonusCard_isBonusClaimed_m4C9BAFFA5BDC108ED12B39D9038B64E11CFC6348,
	BonusCard_ReadyToClaim_m0D80D2226DEAE045C7B7683CC2D1FAD1F926264B,
	BonusCard_Claim_mD08DB9690D726AD60903E36B68582FBB5FD89BE9,
	BonusCard_GetBonusCoin_mFF2164E9743D42079253A5347129BAC02E98D48B,
	BonusCard__ctor_m96A5469B1886D33204EE34C0DA5741B4B092F4B8,
	CoinShop_Start_mD6A479F750CFB256AEC73C3202070C47873B0510,
	CoinShop_SetCoinLabel_mA4F5A28D23F1CBFDFDD7F5116D0CA615B43789D7,
	CoinShop_Buy_m7590780DCDBA63E33F4611E21B6F9F07F118FFAB,
	CoinShop_Loading_m43720D02C7558970FB6C222256F3C6F2FF82EB3E,
	CoinShop_OnSuccessPurchasing_m92357969D036D22DC8EF31C8DAB90437F96A9FE9,
	CoinShop_OnfailedPurchasing_m6F03F22C92226DFA6FCE49306F6E1FC84BAB3050,
	CoinShop__ctor_mFAB356CFFCB176E7F7A1E54A2032C2B6CC1BCCE5,
	U3CLoadingU3Ed__5__ctor_m9C90B91C82E928EF7A853E50760B569601E12B58,
	U3CLoadingU3Ed__5_System_IDisposable_Dispose_mE6C0D3D9053E2216B69107491C01A677250D45AF,
	U3CLoadingU3Ed__5_MoveNext_m3990C8AD3522A51B23E3C23911108510F60D49A3,
	U3CLoadingU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDA4AB795ECCA2AF707571E07DE0B3F1A73055F12,
	U3CLoadingU3Ed__5_System_Collections_IEnumerator_Reset_m1DF7BCA5CD420A31D590D3401433484F8E55B721,
	U3CLoadingU3Ed__5_System_Collections_IEnumerator_get_Current_m6C801CE56B8729FAE8DC7D053DB23FB6F5102ADB,
	DailyLoginPage_Start_mFE687C3075DBD5D36B40106D9A1A1FDEEEA9C724,
	DailyLoginPage_Init_mAF13C77A99CA228974C86C97D124C45A989702C2,
	DailyLoginPage_CheckClaimButton_m5CEA81F9D629862292A1FC9BB53FDDBDE8250119,
	DailyLoginPage__ctor_m76FC80E8AD0F958EB74C75905A0C799DB53D5199,
	U3CU3Ec__DisplayClass9_0__ctor_m6804D13585A893E5B3C65E8C739957059B71FFDB,
	U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__0_mC7C88E37AE8D8144D38096C7C34AB7AB40E9771B,
	U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__1_m59B3E43571A52CA52EC1B5DD185CA69A21C3F5B6,
	U3CU3Ec__DisplayClass9_0_U3CStartU3Eb__2_m945C3D0CD0718F3D6637E5CC9F94B8C8E10D8AEB,
	RewardData__ctor_m56FA7E6414745F0980B560439A3FD1E8F76F5725,
	ForceApplicationPage_Start_mDEE93BC8AFCDBC2A9BD0E9BBE4C7DC1294778E48,
	ForceApplicationPage_CheckVersion_m0D0C26E0D14498CFA0CDB83F4C3A9B1D3D2BA1E1,
	ForceApplicationPage__ctor_mE3788C569CDF901D1C109AADCA5CE2C55CE5F06A,
	ForceApplicationPage_U3CStartU3Eb__4_0_mE7D1D61B8438C29D08881B30F181F454C96F4E27,
	U3CCheckVersionU3Ed__5__ctor_mEF2AF7730ABDC5BA06BCED81A228B80929E98ACB,
	U3CCheckVersionU3Ed__5_System_IDisposable_Dispose_m69B4900DD1658211E6A938A2498DED794CF103F8,
	U3CCheckVersionU3Ed__5_MoveNext_m94A69DBD46F83AEB945C05441D4C2FB101B46C10,
	U3CCheckVersionU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0386B7CDBC92D99AE0682B6D8FC452D476BAB10,
	U3CCheckVersionU3Ed__5_System_Collections_IEnumerator_Reset_m26429745C43250B887E1030FB31AB2085E1FD749,
	U3CCheckVersionU3Ed__5_System_Collections_IEnumerator_get_Current_mD78592F687BB2ECD39F70D10E527CB19942BA292,
	GameServices_GetJarakWaktu_m8F16953B9F5148FA4B1CFA8096D18DB767406A32,
	GameServices_Awake_m231631DEB029E8BA32ECF46F138327730724399C,
	GameServices_CheckAppVersion_mA9038BA1C1B69C7F29FEC8D8C907281A291F94C6,
	GameServices_Instance_SignedIn_m095C581AE22F22A20FCDFBED4A5FCE9D54C970BD,
	GameServices_Instance_SignInFailed_mA0E8F7925C3D2791A9F49815FC0A9952DBB2E14E,
	GameServices_GetNewVersion_m06BB42215BDAB16FCEA68858CDA1AFE4D62B8432,
	GameServices_Start_mCB5B371E22FA9C7F37D1AA6C8E7B662967BFA4AA,
	GameServices_SetHighscoreButton_mB6816C39B3F261F815C350C2629A68DD1618A0CC,
	GameServices_OnDisable_m01A32EB39C83E2ED3492FB20F4C5D47C3A106401,
	GameServices_AndroidNotificationCenter_OnNotificationReceived_mA7F538569F1B0189AAC48B52501C76663A71FEBB,
	GameServices_SetNewLeaderboard_m6EE5310DF5817050CD9B91938292DDC45993E0A0,
	GameServices_ChangeName_m31A855B0BD1CB281CAE5B7BE03F2859D1525CEAD,
	GameServices_CheckTimeSave_m0A6F576ACB8154D336C3F0052AFC4E9AE6E6A682,
	GameServices_RequestNotification_mBBC0B2272F8EAA00C4F34F425372CCB86D0CEDD9,
	GameServices_RequestNotificationPermission_m011CED5BDF110C78256628064C964E99BFAF1F76,
	GameServices_CreateNotification_m7E4D5C9E12261FA472F48B13F0DF6580B5C2D712,
	GameServices_OnPlayGamesSignedIn_m015EFA423FC417027273D6509B95CD1F2F9A8FFD,
	GameServices_OnGetCallbacks_m95043980ED50E6B328F663FC5D857D87AD7944E3,
	GameServices_SignInWithGooglePlayGamesAsync_mBE34B181A5D81CFB95735E9AE04F22046BE83256,
	GameServices_SignInAnonymouslyAsync_mE8BD0F882604907A6301D1FA7F54FEBDAD7FE287,
	GameServices__ctor_mEF5841AC407E60DBD0C708E03B7BEA0B7C3F07F3,
	GameServices_U3CStartU3Eb__18_0_m6A3796982BFC67408936DCFE6F207DA0AC11BEB3,
	GameServices_U3CStartU3Eb__18_1_m3160889901018D9193D3B6F2D0D97998916268DE,
	GameServices_U3CSetNewLeaderboardU3Eb__22_0_mF55CE4E010995001F3989D395FC4ADA2C20B97E3,
	U3CAwakeU3Ed__13_MoveNext_mCF7E898FE5CEEDBFBA0B2181063C660EEB7F150C,
	U3CAwakeU3Ed__13_SetStateMachine_m87671A5980EB6DD5A90613EF13F034A439D707F1,
	U3CU3Ec__cctor_mD3D1474B891E3824A99F67B8E9E15B11256F1F9E,
	U3CU3Ec__ctor_m4F9B0E11F42B95E7F9C449D550561CE95A6CB928,
	U3CU3Ec_U3CCheckAppVersionU3Eb__14_0_m1EE8660BD5FD3777E0EEFC5D501DD61C7ABA5916,
	U3CCheckAppVersionU3Ed__14__ctor_m1EA04C2C47777509E49989F63DCA1CD88D0259AB,
	U3CCheckAppVersionU3Ed__14_System_IDisposable_Dispose_m325A0BDA6EE5AA4A6D2541D785F0067BEB1C5F85,
	U3CCheckAppVersionU3Ed__14_MoveNext_m04899718E75C7FCB5E2DE58877F14642ECEA8C52,
	U3CCheckAppVersionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB21710A43D6257D31873B8075B2646FC1447D2,
	U3CCheckAppVersionU3Ed__14_System_Collections_IEnumerator_Reset_m053061EFFB5925A8206D669B20A190CAEE69985F,
	U3CCheckAppVersionU3Ed__14_System_Collections_IEnumerator_get_Current_mAE810789CB809B98459A3DFAA9EEBB073A3ADD70,
	U3CChangeNameU3Ed__23_MoveNext_m715380D2F220ECA84F1FD6C7768EA280FF87F06E,
	U3CChangeNameU3Ed__23_SetStateMachine_mEBC90E9B5632FF9FE2DC69AB44A82A0511006400,
	U3CRequestNotificationPermissionU3Ed__26__ctor_m6E9F7D0F834646C32C6EBB0155F7B11242E9AA70,
	U3CRequestNotificationPermissionU3Ed__26_System_IDisposable_Dispose_m461754E59EF20BB1A4A84D6080E71F64AC57FB15,
	U3CRequestNotificationPermissionU3Ed__26_MoveNext_mAF5299AA530FE588CD39865AB1735CF942A9AB29,
	U3CRequestNotificationPermissionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m527E69A8E393AA639DEB91380D6C763804D2BEA9,
	U3CRequestNotificationPermissionU3Ed__26_System_Collections_IEnumerator_Reset_mA53E92EAE38D26E1160682E7799905731A4FFB8A,
	U3CRequestNotificationPermissionU3Ed__26_System_Collections_IEnumerator_get_Current_m743282682E50888566AFF733BDC850F6DBC6FC13,
	U3COnPlayGamesSignedInU3Ed__28_MoveNext_m58E4F170537D3683633F8C5A21D8754E132CB4F7,
	U3COnPlayGamesSignedInU3Ed__28_SetStateMachine_mE675A3114870098867823FD27648010FAC1EE263,
	U3COnGetCallbacksU3Ed__29_MoveNext_m348948E06A523F4B57F9A23D4FCEC79176C2B7CB,
	U3COnGetCallbacksU3Ed__29_SetStateMachine_m846EB23EF52841941A1983569EFA2942BE2A3999,
	U3CSignInWithGooglePlayGamesAsyncU3Ed__30_MoveNext_m0A04B2BCA035997FD36186DBFE1BAC72993E9C68,
	U3CSignInWithGooglePlayGamesAsyncU3Ed__30_SetStateMachine_mF2A1FA66B49F9C79DF43A2E6970D8BDD74CB9F99,
	U3CSignInAnonymouslyAsyncU3Ed__31_MoveNext_mCA66806BE6EBDDF2ED3B120696DB58C656A15D3C,
	U3CSignInAnonymouslyAsyncU3Ed__31_SetStateMachine_mF2072EE052257698D917DD162BF4DB7CADD8A9CB,
	DataServer__ctor_m7045FF9E088140546AEDF9D77172739ED54E8B16,
	GamesonData__ctor_m0967DE5A98F9C729AFF306FF28C373D0DCCD285F,
	HomePage_OnEnable_mDD26B1F57CCE8B74FEE064461C2832CAC1A30D73,
	HomePage_OnDisable_mA70DC8E3819A32692FFF5C8E716DB8506F0D8F46,
	HomePage_Start_m2B172C37BA2990DA79B559EF8D61322CC9635E91,
	HomePage_Instance_OnAdRewardSuccess_m6D0460BC5B620141BF0FDF50CFAD2FFE2E6D3FF1,
	HomePage_CheckHourly_mBF73BAA8C58766832A1F27B531362122D0945E69,
	HomePage_Update_m5E1D4387780D6ED369ADF41FC4556296A219AA25,
	HomePage__ctor_m95C69629EC5C5B408499B7AA3C69CFBD7CF70A1E,
	HomePage_U3CStartU3Eb__18_0_m43A230BD690EA07A027A52EA9F53139D54A31E0B,
	HomePage_U3CStartU3Eb__18_1_mFA5F6FECD9E571C17D97E2B0946EB7E51F878548,
	HomePage_U3CStartU3Eb__18_2_m091E5F713EAE8C20B1200879F98DDF352F906188,
	HomePage_U3CStartU3Eb__18_6_m659006878A679F42D1B7683E165DAD009D219B3F,
	U3CU3Ec__cctor_mDC3B104D0105E60CAFE67313847A93623A6E2C8C,
	U3CU3Ec__ctor_m3055D1B38AEB34340AA53D51536246EF9FDA398F,
	U3CU3Ec_U3CStartU3Eb__18_3_m5EB1FDBD269868EE1D66F7A7B6701C74B99D2839,
	U3CU3Ec_U3CStartU3Eb__18_4_m1DE0C7D74DB536D0B4B1743AA64282A975FB25BA,
	U3CU3Ec_U3CStartU3Eb__18_5_m0E97550454914FE3B4F742D94E83709ACF9B79DD,
	U3CU3Ec_U3CStartU3Eb__18_7_m16C474CC910CC026F986A48CCB9831BA4665A4B8,
	leaderboardCard_SetLeaderbordData_mBD492F0F8B17531080C9F7F2C5651188188AFC47,
	leaderboardCard__ctor_mC486DD947FB65B270361F147F0FD882B5DF8BE79,
	LeaderboardPage_Start_m280B175DE3774A56F9D54233669E43D244975F3C,
	LeaderboardPage_OnEnable_mCF02776FBCBDA89D8026417BFF1CAA6F7E11E387,
	LeaderboardPage_GetLeaderboardBucketData_m76ABD728A190EFB413C61F2DBD1C975B80D331A6,
	LeaderboardPage_Setleaderboard_m1325F5230846E581D8D4BD7C3C826B1CC41B93E6,
	LeaderboardPage__ctor_mD9FE83FEFC7FA5FCE42C645EC47AA6100AF0B179,
	LeaderboardPage_U3CStartU3Eb__8_1_m9365A95D2FE8BC9AB4ABDF89C040D7B501F748F6,
	LeaderboardPage_U3CStartU3Eb__8_2_m541E63BBC5800509D17576A4ABBAEE2F62C64F37,
	U3CU3Ec__cctor_m5F913BEB1DBE713CF41B9D0B88AF578FF487E1B4,
	U3CU3Ec__ctor_m680A3AD472AF45C7CBB52A4F562E2E64514DE414,
	U3CU3Ec_U3CStartU3Eb__8_0_m41E5C7E7495128D667E3C9021FF5D03259AE99F3,
	LockLevel_Start_m28E89770A180FC69D20C149518CBAF74781F2A31,
	LockLevel_CheckLockLevel_m533E7B09FCB92798D9CFFE4973C8D3F9C74A5D59,
	LockLevel__ctor_m07E4121E67EE8586E2E6FF1F0B963CD970445047,
	LockLevel_U3CStartU3Eb__4_0_mE172E8E1A4F34F04D23B0295F7D45043FCCAF386,
	LogManager_Awake_m2BF00F6A757025035C6F9A5D07EB89E2DC623751,
	LogManager_AddLog_mF66D82A38A5343E6969F9DC327059C9FB49D521F,
	LogManager__ctor_m37558B87A7D080BB5F3ABF8487FF1900BCD0136D,
	packageCard_OnEnable_m78130AC8225BA066B902230E1E700DE40D9F1269,
	packageCard_SetCard_mC6FDF80B360469AE7809E5C593A8293F209595A9,
	packageCard_BuyPackage_mE578764F9F031B9B2E1632C5148398A70C3245BA,
	packageCard__ctor_m0EEAEADF7AC371247BA03098F562EE5E48D14184,
	packageCard_U3COnEnableU3Eb__18_0_m7DF897B7F7FA994811ABE1DCCD52A719B31FA015,
	packageCard_U3COnEnableU3Eb__18_1_m27622B3F0C50C9498158F829D45797F8CDC0AAAF,
	packageCard_U3CSetCardU3Eb__19_0_mDD65B3B8238FC3712D35E26F98E6D5F5A6B6AE44,
	packageManager_Awake_m2E2E54DDA0E59AADD45A54F292F2438E5F8D3458,
	packageManager_GetPackageLevel_m54161E5C2FA19691A8B343AD0D1F9FB7FE7ECE73,
	packageManager_SaveData_m809351C0600AAAE3C89852844C7023868D35BECB,
	packageManager_LoadData_mD45912A020D814A01295CA6B503828F19FC9862C,
	packageManager_HapusData_mEFFC2FCAC63A1E6F0290B8C027410C123CE570E2,
	packageManager_GetPackageName_m4ABE2F1F7D454FEA5AC8A33D651C1776E89A80D0,
	packageManager_GetMaxPackageList_mD92F69AA1DA750EA4197B192C6E083B27476F7BB,
	packageManager_AddNewPackage_m34FA68F41D418A64DD55FF1CAA014CBDCCFA5126,
	packageManager__ctor_m9D602D64F26EE1D636DDF2B52788FFBFA78BD669,
	PackageLevel_UpdateData_mF90B10607F4580F01F9E08E5F354848699B7CDCB,
	PackageLevel__ctor_m5528328C1BA781187E657A2562873D4C32C8343F,
	SaveFile__ctor_m3518A8BB29A19126C7299991BBD61759E6638EE0,
	SaveFile__ctor_m9E56B22855EDB49B5EE1AE4269D70669F5C24771,
	U3CU3Ec__DisplayClass8_0__ctor_m28754AB3BA483A061F107F994D996FC297A722DC,
	U3CU3Ec__DisplayClass8_0_U3CGetPackageLevelU3Eb__0_m82D0ACAA612C699712BB64D2E25EAC1CFE992260,
	ResultPrefab_OnEnable_m4C5EBCB8DF7DB574E81988EC942658FEC6EC5C3D,
	ResultPrefab_SetLabel_m0AA213B4B6E1C48BB11482D3F8FA69353EBF226B,
	ResultPrefab__ctor_m8A48EC4B633196196926E4A78F93B1543DF0911E,
	U3CSetLabelU3Ed__3__ctor_m34F77AE44F246B34F28A68A5C13589E6F27318F1,
	U3CSetLabelU3Ed__3_System_IDisposable_Dispose_m56BB7C00BD8D8D8D81F34C2C45DA99F1C501C4AF,
	U3CSetLabelU3Ed__3_MoveNext_m701C74074D5FA5DA71FF7CCC7634FA460846DB50,
	U3CSetLabelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7954407A6770F7870DE7C06E242F2688ECF2436B,
	U3CSetLabelU3Ed__3_System_Collections_IEnumerator_Reset_mFB4DBEE9C4DA0912165048EDDF688F47A5C8042F,
	U3CSetLabelU3Ed__3_System_Collections_IEnumerator_get_Current_mAAFC1A456194A6FC40D769E18AD958D44FFD5E7D,
	SettingPage_Start_mBACD95B247C12F6E75C1D3B52386089DA8B5F2D9,
	SettingPage_ResetGameData_m5FE7B350E8C034CA76AC4CCE0EA3524844A3B855,
	SettingPage__ctor_m9721B199B8B7847FB9178D83194E58A615E3F7B6,
	SettingPage_U3CStartU3Eb__5_0_mC7215AE82B2A64487F2DA47AFF0E209404689B94,
	SettingPage_U3CStartU3Eb__5_1_mE061C6E84FDC653D062FFB92C223C79539722DFC,
	SpinWheelPage_Start_mE954A16E7CE061F92C87E0DB264B37A4BC44190A,
	SpinWheelPage_OnEnable_m779FDD1996C9A8A0842E83CC7253F6E450DD911D,
	SpinWheelPage_OnDisable_mE583894A7564408AD896CEF2CBC85A5E4FE49826,
	SpinWheelPage_OnRewardedAdLoadSuccess_m0A447507082BB43E4142399AF7F567C5782D576A,
	SpinWheelPage_OnEndSpin_m175B7ADFFDD43227353CBB976F94D216E7B25DFA,
	SpinWheelPage_ShowResult_m87307DEF121BC016ED7E7969475AA706197DBDD5,
	SpinWheelPage_HandleSpin_m9FD2142228F2C318CFF0A7EE9372CCE9AD24E2EB,
	SpinWheelPage__ctor_mBCF404349DF84F3C0EF08B3CD743D38457CC131B,
	U3CU3Ec__cctor_m2D81B0E56587B3E4CB8041240E4762450CA5430C,
	U3CU3Ec__ctor_mA1DB08B42717BD1F1CC9FBD73BCBEF0F161C7B29,
	U3CU3Ec_U3CStartU3Eb__7_0_m4EB083235910A85C7D0C704EFC91547F210C2D7E,
	SplashscreenPage_Start_mDD117CEE39934E0ABC3008C79C9AEA1370A39375,
	SplashscreenPage_Video_loopPointReached_m8DC9F24DBC2FDB9B9AD9C87ED74C940E791C4ADB,
	SplashscreenPage_PindahScene_mC2499C7062A0572B630DE765F9865CD7FEB9E415,
	SplashscreenPage__ctor_m29E8B801D62C146D44F25320EEB52F7CDEC4EFC2,
	SwipeMenu_Start_mF49B3B2AF9955E8429EFABBC7F8C7A4C9D73ABE5,
	SwipeMenu_Update_mAE33874564EBE84862BA97BE8AEBDCD1D5007A9A,
	SwipeMenu__ctor_mF71FD4B4351FB13F62DE12BC7C9C7F164D7B8DD2,
	Tema__ctor_m5C1B87E5B589340531E9EFBE77403FAEF61EF468,
	ThemeCard_Start_mDAC5E62ABAF8C10B155F6FB87FE0B1C63480CA2E,
	ThemeCard_ApplyTheme_m43F90DA46A831FC3657B5B0E760775547AB4C0D7,
	ThemeCard__ctor_m88E49A14A83AD3A2C3B25C897F7B4D049D282D18,
	ThemeCard_U3CStartU3Eb__3_0_mED45853553FDCCC392A48CB355BECEA12A7BF464,
	ThemeManager_Awake_m2AEE036E797A06A326764D45E47A50CE9CB8195E,
	ThemeManager_GetTheme_m53E959D7B2187A9BE4B3817F64FA208A8E3E7962,
	ThemeManager_SetTheme_mFE31D6B059A1FE23E41D613F5F656A22ED31BFD7,
	ThemeManager__ctor_m1B15A16D3C500F39D01E5B302126CC3BFC819FF7,
	ThemeManager_U3CSetThemeU3Eb__6_0_mE4758C847ADA64CFD0A1E26D7E914A6F386C6EEA,
	ThemeHolder__ctor_mC9B7A9F932C2D6027861A4EFF4E3CB3063468EDF,
	WidgetHabisHint_Start_m6FB19BFF38636A1398570FBD5177F98CC10A8EDB,
	WidgetHabisHint__ctor_m5128894F2B8DCD274E18E8E07D331D3FC3330BF9,
	WidgetHabisHint_U3CStartU3Eb__1_0_m3BD6D272FD3C175FAE628EB2879666A6C870A32C,
	WidgetKurangCoin_Start_m27DB080A4CE73E6A7D8530E6EE8BEC56D211DBE6,
	WidgetKurangCoin__ctor_m7EF6B49FF8C5E748D2AEAC4B130A629D39A489D7,
	WidgetKurangCoin_U3CStartU3Eb__2_0_mCA79C96E3D33E5174B33205212BC2BA29DE5ED32,
	WidgetKurangCoin_U3CStartU3Eb__2_1_m307CB24C7C0A576670AE5C8BBF41D10BADF6EA2A,
	IronSourceDemoScript_Start_m77BBBF6AE3F0DF2428C73C5D2D5F45FDF4EFB8BC,
	IronSourceDemoScript_OnEnable_m8946831B21535385ACF7C8E15325DF8608E47D34,
	IronSourceDemoScript_OnApplicationPause_m2FF515EB862B1AE0BD7F86418D31B9F970DF59C6,
	IronSourceDemoScript_OnGUI_mE2E5B983419B49DB74ED30F283AA6D5202F5DB4C,
	IronSourceDemoScript_SdkInitializationCompletedEvent_m849CAE6B3FC42F029D1126497086EB1A68D1D663,
	IronSourceDemoScript_ReardedVideoOnAdOpenedEvent_mAA90C5DB459623CB82CF4C1757BA9982A0020F0D,
	IronSourceDemoScript_ReardedVideoOnAdClosedEvent_m5097FECA2AB4269F5162340D618B456EC7802D1A,
	IronSourceDemoScript_ReardedVideoOnAdAvailable_mB8298C68A77BFDBE4E7F2642EDB817FB234ACCB5,
	IronSourceDemoScript_ReardedVideoOnAdUnavailable_mFDA7DB7C9514E6A2A98FBE8A694AC271756ABC1C,
	IronSourceDemoScript_ReardedVideoOnAdShowFailedEvent_mD8A88D647F5096B156A8A8E4D323D5B8A168BBBF,
	IronSourceDemoScript_ReardedVideoOnAdRewardedEvent_mF8F69DE0CF85726566FFF69075337A856AEC3347,
	IronSourceDemoScript_ReardedVideoOnAdClickedEvent_m4F99DDF14AEC2020478A08F6A3A1B7E911AACCDD,
	IronSourceDemoScript_RewardedVideoAvailabilityChangedEvent_m8D6D030DD7DFC0AD4E22B96DAE1107018642955D,
	IronSourceDemoScript_RewardedVideoAdOpenedEvent_mCA1EBEC4D751D2E0E99766AA4C468E09958C04E7,
	IronSourceDemoScript_RewardedVideoAdRewardedEvent_m1A57ED0C0318246EF3F82D592D2FF71A9479D8E1,
	IronSourceDemoScript_RewardedVideoAdClosedEvent_mBD15C7A41C09B50A91A836C3E21FF5237D77E448,
	IronSourceDemoScript_RewardedVideoAdStartedEvent_m15ADE4BB5E28B6996F707DE3A45AFDDCD46D8F70,
	IronSourceDemoScript_RewardedVideoAdEndedEvent_mBBBCFBCB0BF6E31D925E8C55FEF143B74944E6CC,
	IronSourceDemoScript_RewardedVideoAdShowFailedEvent_m12220F16740A72B2362B6C6A70232B22E6C9976D,
	IronSourceDemoScript_RewardedVideoAdClickedEvent_mD609F9E25702627A6A3BB3813DA8C2F654F421D4,
	IronSourceDemoScript_RewardedVideoAdLoadedDemandOnlyEvent_m516EDB8DFEA9D89459C638DE070E9E80DE6E635B,
	IronSourceDemoScript_RewardedVideoAdLoadFailedDemandOnlyEvent_mB25510FCA9F14003FA47BDFCF3F022ED0CA16EA0,
	IronSourceDemoScript_RewardedVideoAdOpenedDemandOnlyEvent_mDD764123153CC8133ECBC85E569B3B2BD4295C87,
	IronSourceDemoScript_RewardedVideoAdRewardedDemandOnlyEvent_m7E67AE25E69510F382C4F0233FAB27525CF426D9,
	IronSourceDemoScript_RewardedVideoAdClosedDemandOnlyEvent_m077D2168926533A87F1DC33D4A8280D597D3CC32,
	IronSourceDemoScript_RewardedVideoAdShowFailedDemandOnlyEvent_mB1D85D07EFD5C99B7D91215FF91828AC7C00E463,
	IronSourceDemoScript_RewardedVideoAdClickedDemandOnlyEvent_mF46D7047786F04D06E33BD702CAEC29BB49E7F4A,
	IronSourceDemoScript_InterstitialOnAdReadyEvent_m2D145827D504F5B16F4E60C3C1A9738033B43053,
	IronSourceDemoScript_InterstitialOnAdLoadFailed_m5B05AFE22E3073F324C16E226AA25F18CA206D0B,
	IronSourceDemoScript_InterstitialOnAdOpenedEvent_m5EF442105B865E25655FD85B2CE82CEA176BB3ED,
	IronSourceDemoScript_InterstitialOnAdClickedEvent_m4CBCF8854B78C2EFF3A8FA9F2DC1465742442E1B,
	IronSourceDemoScript_InterstitialOnAdShowSucceededEvent_mEE8AFD2DC75F164289653B67D9F8B2A4BA90B43F,
	IronSourceDemoScript_InterstitialOnAdShowFailedEvent_m25479BB7773BB4C872700856551E14035CBEB388,
	IronSourceDemoScript_InterstitialOnAdClosedEvent_mA32A648AA1FF5FD94E481B8C6F4FE03526996937,
	IronSourceDemoScript_InterstitialAdReadyEvent_m7540A41C2D8BC4BCBAF2F6D1150D3893B7E65796,
	IronSourceDemoScript_InterstitialAdLoadFailedEvent_m49260AD6F3985A12BE62243D54382FB6F12F71F8,
	IronSourceDemoScript_InterstitialAdShowSucceededEvent_m8DC6A0EF9FF7D3BC35602DE70FF07D1368550779,
	IronSourceDemoScript_InterstitialAdShowFailedEvent_m044C2F8F84E3920F764B5C3B1BC05643C1F43303,
	IronSourceDemoScript_InterstitialAdClickedEvent_m53B12D24C9AE85ACA81294CE11630EF167896B62,
	IronSourceDemoScript_InterstitialAdOpenedEvent_m7A11AF2D87DE4EF38304AC7AA59CC98D7AE1D05D,
	IronSourceDemoScript_InterstitialAdClosedEvent_m9F67DAB55D1CBB561095B97FACAE2D7426F9CCFF,
	IronSourceDemoScript_InterstitialAdReadyDemandOnlyEvent_m8D6E6D0B5A6A7BCDD87661C33EE0A0111402AB8E,
	IronSourceDemoScript_InterstitialAdLoadFailedDemandOnlyEvent_m98CA7BF6A5D70BD8C6A85FEBB7B869F6F975AFFF,
	IronSourceDemoScript_InterstitialAdShowFailedDemandOnlyEvent_mC7EF101695BEC4C1215C4DB46CB9E8F818071B13,
	IronSourceDemoScript_InterstitialAdClickedDemandOnlyEvent_m5D260FF2ABA91E3F6550F334A3D02E7CE740A96E,
	IronSourceDemoScript_InterstitialAdOpenedDemandOnlyEvent_m56D5E479F14569874DF9EA09CA785E809C8D2681,
	IronSourceDemoScript_InterstitialAdClosedDemandOnlyEvent_mE3AB61CBEE02C43F9311D4E4E185A0329850D2AE,
	IronSourceDemoScript_BannerOnAdLoadedEvent_m80D002600F688EDFD2C98F404016ECE8C245D926,
	IronSourceDemoScript_BannerOnAdLoadFailedEvent_mB1406D5A0CC8FCA60BDE7FCD56FE9206DD6F0240,
	IronSourceDemoScript_BannerOnAdClickedEvent_mC9F5264E62FC4748A37022A8EA81498A5E729EAE,
	IronSourceDemoScript_BannerOnAdScreenPresentedEvent_mBF9C0A5EFE1BBD2C7717CFCFA69B4EF9B07BB5AB,
	IronSourceDemoScript_BannerOnAdScreenDismissedEvent_m8B60FE3ABF24B4CA33E7B4871F22813BADEEC437,
	IronSourceDemoScript_BannerOnAdLeftApplicationEvent_m4871D7055E2C609856B5B651DC8B9CFA6EC0F171,
	IronSourceDemoScript_BannerAdLoadedEvent_m72DF5D965B6B049B99738E946EC79F6884D20A31,
	IronSourceDemoScript_BannerAdLoadFailedEvent_m583267AF7FF9B15DB1ED470688C92418E9651E3D,
	IronSourceDemoScript_BannerAdClickedEvent_mA16CEF59D8B2727C8F624427599FAE37F3ABAA7C,
	IronSourceDemoScript_BannerAdScreenPresentedEvent_mB196ADD5DBC96B406CE2B692B6A7AC337AEED1EA,
	IronSourceDemoScript_BannerAdScreenDismissedEvent_m0702C693A37BC01E7F308B1DD4F9E1D24E6C4D38,
	IronSourceDemoScript_BannerAdLeftApplicationEvent_mCBE6E143B9DB8747A75FEC6CBD672CE5A117ACE2,
	IronSourceDemoScript_OfferwallOpenedEvent_m5F3F46BF3CA43F00EAD3F7BA321B668B239B837C,
	IronSourceDemoScript_OfferwallClosedEvent_m4346BD782B909613779E96541C9D7D14FA8D17A7,
	IronSourceDemoScript_OfferwallShowFailedEvent_m5B8BE354AF2EF7BB715110A748AB2F6D45560D66,
	IronSourceDemoScript_OfferwallAdCreditedEvent_mE706AA91C365D2496B44BFA7A02663BEE4A39760,
	IronSourceDemoScript_GetOfferwallCreditsFailedEvent_mA4C78B923B8179668F1FB2DE7CB05C6AE0B478E6,
	IronSourceDemoScript_OfferwallAvailableEvent_m5912223F9C4E4C9A6F07F2A82150E7C886286064,
	IronSourceDemoScript_ImpressionSuccessEvent_m1A555AF2B938562B3D493A2CF13B3BAC76324811,
	IronSourceDemoScript_ImpressionDataReadyEvent_mF582B7C042903AE0932DB4296587E9487F108E07,
	IronSourceDemoScript__ctor_mDD3BB0AECC3B37BFFBA8DDB8EFD3D381893E20CB,
	AndroidAgent__ctor_m2F1A50D8D921A298DB2D6BA6CE5806F8603EFD4F,
	AndroidAgent_getBridge_m0A292F3DDA49DBF47DAE3F002B77003A3E1D7BEE,
	AndroidAgent_initEventsDispatcher_mFF2C86FB1A82FF9F04D609820CDFA9CA59FFBC7A,
	AndroidAgent_onApplicationPause_m6EB98993705760C1F6999C692F25DB5D6B4A5CDB,
	AndroidAgent_setMediationSegment_m3A0480804838E5E8ECC2BC5B8B5680A6105B9C70,
	AndroidAgent_getAdvertiserId_m3710C4C861EF0FF0F39063DA54BEFFEAF3CE030A,
	AndroidAgent_validateIntegration_mE08D2E6349AE514ABB189490FAE6DD951A8C327E,
	AndroidAgent_shouldTrackNetworkState_m8797D28C0404D921C99163A3343D89490FC9DE94,
	AndroidAgent_setDynamicUserId_m2ECDDD36B92F7747B6528E51C37BB9E4D42D4EBC,
	AndroidAgent_setAdaptersDebug_mE7FD44DDEF492001F0F52576F1589F28F4B9DEF5,
	AndroidAgent_setMetaData_m065F83A2898C3CA9E804D074316ABACC362482E8,
	AndroidAgent_setMetaData_m72A54E1A81C43E256EB7BDC40E15E4EFBDC218D1,
	AndroidAgent_getConversionValue_mEDF33AC51809A034B51713043D5572B4B34D9240,
	AndroidAgent_setManualLoadRewardedVideo_m80960FB9EA98BF1DE19AC8F0FB3A68CBAD9895E5,
	AndroidAgent_setNetworkData_m384413180ABF267A646072E19A80F46A2C92072E,
	AndroidAgent_SetPauseGame_m5C5B7470DCFBB7E55799773388D303B2F71D532C,
	AndroidAgent_setUserId_mEDEFB903A8927A6D7CD852EDBC69F8E975DF4DC0,
	AndroidAgent_init_m5518C696AD17BD78BFBE888EB2D6A753EEF32BA8,
	AndroidAgent_init_m68BB3FA729DDAC86BC809D42D4B97F66C5A5B4A2,
	AndroidAgent_initISDemandOnly_m4741E6CAE778A2B01D5DD467AE8D9CF714B27BF2,
	AndroidAgent_loadRewardedVideo_m2AECD4A36B197F5725E25E2A1D01D43A2886D149,
	AndroidAgent_showRewardedVideo_m6F0B3BC254FA12DA162E77145A15D660384BF631,
	AndroidAgent_showRewardedVideo_mE300B80BA2F3EDA6652B3B24B86D4861508A1AA4,
	AndroidAgent_isRewardedVideoAvailable_mFBD236A6085B36A46E3566F7B7BC0580DEEAB0BD,
	AndroidAgent_isRewardedVideoPlacementCapped_m7A9480436E644353B9410CC6B9FC2F84EF426387,
	AndroidAgent_getPlacementInfo_m8F93E027CA731571B7D30EDC6E067281F6EF3065,
	AndroidAgent_setRewardedVideoServerParams_m7235399F36B0D265ADFA44D96038B48BFD9ED55C,
	AndroidAgent_clearRewardedVideoServerParams_mEB34661A68B6BAAA955D1292E7B8DBF33127689E,
	AndroidAgent_showISDemandOnlyRewardedVideo_m8877C5FCCB0FE1A314605BAF49C8F462EA8C53DB,
	AndroidAgent_loadISDemandOnlyRewardedVideo_mCBC4DB9E07A597F4E2889FCE1CF6C161EDE4B51F,
	AndroidAgent_isISDemandOnlyRewardedVideoAvailable_m9C8F0CFB56E5C155C6DFE4ED161C79E6472A9153,
	AndroidAgent_loadInterstitial_m21905E46FD67D875EFFE454A67D29E956441FD8B,
	AndroidAgent_showInterstitial_m4A7D3D84077A51BE807150D94F86CC4FE104B092,
	AndroidAgent_showInterstitial_m6FB72A001E84E21A0E215AC2B757CC88D85E4B83,
	AndroidAgent_isInterstitialReady_m51C44AB312CF8632C45A69C605A8BB79E56B2EE5,
	AndroidAgent_isInterstitialPlacementCapped_m12972293AB0894E88ADC862DD212617879EF11B1,
	AndroidAgent_loadISDemandOnlyInterstitial_m01237D1EDFB1FF359820AAB67936F83514C99EAD,
	AndroidAgent_showISDemandOnlyInterstitial_m22FDF895CEE08619BE6CD4F08B0B01677B698EC6,
	AndroidAgent_isISDemandOnlyInterstitialReady_mAE5E73422969E3165B9744D1183A8891BB28464C,
	AndroidAgent_showOfferwall_mD5A0802243C5E038945E3E9B146E2B8B9DE077E8,
	AndroidAgent_showOfferwall_m43C6328874CE364DA3EEDBAC887CBE285C9DA355,
	AndroidAgent_getOfferwallCredits_m4733426D27DA45621DE45CC0B55793CBA88F4A8F,
	AndroidAgent_isOfferwallAvailable_m53AE0C27A803FC14EE460DCD77C660F052A045B1,
	AndroidAgent_loadBanner_mF2B2F9AA79ED6B1655B15FB856ABC74528594E7F,
	AndroidAgent_loadBanner_mC3E441ACADA0DCA0E5736F0D16D5B3C0F66A3818,
	AndroidAgent_destroyBanner_mCCBFE09EBC2124EE181F809186BD428D04118CBD,
	AndroidAgent_displayBanner_m7B7FB2A3D813AE3B0665C25DD833A806136FF79B,
	AndroidAgent_hideBanner_m39208475B2396C5BD2B49E0AF73B8BC5A1FA3CA5,
	AndroidAgent_isBannerPlacementCapped_mEAD2A063BF068156B01DA9E5663F1E7BD79D56F2,
	AndroidAgent_setSegment_m3776055F5BC0F962D828BEB75534EDCF0BBE67AC,
	AndroidAgent_setConsent_mD5D3EEE4AAA4CC6251760222C84CCB5CEDBC5D63,
	AndroidAgent_loadConsentViewWithType_m226A2F1C1FA3F8B23AA13E03205D840E351285F8,
	AndroidAgent_showConsentViewWithType_mD106E1957A62FF6A2744D877E7E8F8C31F3ABC0F,
	AndroidAgent_setAdRevenueData_m879D0D032F29178EE457A000BD506FD4C0A2B98E,
	AndroidAgent__cctor_m7177F073688A780EFA2F13786FF39B5CBA697DE6,
	IronSource__ctor_mCD8F82382069D7C032538FE2FE4291227D7C32B3,
	IronSource_get_Agent_m263B42666F99FAD3CCC93BBCF79EE3E85D485566,
	IronSource_pluginVersion_mE6F3D9B41DECA6F53E668929809A46FA096CF732,
	IronSource_unityVersion_m5312B4562DD63B0B17536089D1F92D362FD82AD8,
	IronSource_setUnsupportedPlatform_m3C90507AB0F3118C57D1DF37C893598D339C4F92,
	IronSource_onApplicationPause_m7F0FED1D5D5C76B1446294878864374AF4AC7315,
	IronSource_setMediationSegment_m511F2EF8432A9F65BF274821099D452152A72847,
	IronSource_getAdvertiserId_mC04AD8567F617BF37858BC884BDFD3BD2E93F44F,
	IronSource_validateIntegration_mAA95A52D8F37C0D5A2DC530F73865E40FF07DB67,
	IronSource_shouldTrackNetworkState_mB5FF34F60168FF821571775E654BC5FDD34DBF13,
	IronSource_setDynamicUserId_mEDDDA71582DB31D68C30D420589A549E6E5942D2,
	IronSource_setAdaptersDebug_mCAEACE5C50C8D61CF867CDD2783E9F94BB881D0D,
	IronSource_setMetaData_mE670A504A91308D04F9EEA81EAE8473CD99DB804,
	IronSource_setMetaData_mFD139A925E30253CAB1CC237CD057BFE6E275DEE,
	IronSource_getConversionValue_m35E84D2BCCE4F62DA953898B53655588D6A1B986,
	IronSource_setManualLoadRewardedVideo_m5DED3093B53CBBBDF92FEF516BEE793C8DC3BE87,
	IronSource_setNetworkData_mB3C6BEFDB0F29CEF39064FCB79797C3B0BAD66A3,
	IronSource_SetPauseGame_m3F590CC02DB95FB5E5D17C82AF2EBDE7C27CCCC1,
	IronSource_setUserId_m4475193FBF49B08AD0D5618D8005BB04E931DE5C,
	IronSource_init_mC35175772BE07A91B6CD7FD09E744FC96BA8520E,
	IronSource_init_mC764BC165DE9127E939FF7DA044367690B4DD3DA,
	IronSource_initISDemandOnly_mD621820D8DE879B90CDEA47D6086E09C4BB3F907,
	IronSource_loadManualRewardedVideo_m30443CD7B65D40B1D506C615EF1A724B010CED3F,
	IronSource_loadRewardedVideo_m6420CED59C78ECF65D94743DDA66766DC414B798,
	IronSource_showRewardedVideo_m5BF8747147A5AB0C167BBBCED99B300DB0F257AD,
	IronSource_showRewardedVideo_m9213D17AB1D6653ECB61DD7418030C7776896302,
	IronSource_getPlacementInfo_mCF44F07E63A48BF07FE8CB4F3861AB726773E840,
	IronSource_isRewardedVideoAvailable_m3E4AF1257B0B1F70940E99DB8A8FBC8E0B5A41BB,
	IronSource_isRewardedVideoPlacementCapped_mE0B04B25FCAB7B1729128AD4C986D2F770D1AF7E,
	IronSource_setRewardedVideoServerParams_mA9C2AE88E92ABE4E8062AE06491A9204D1BE7688,
	IronSource_clearRewardedVideoServerParams_m2C815B918801933E3191564227B81B008711D8CC,
	IronSource_showISDemandOnlyRewardedVideo_mC5A0E1909AD1E45E4754EBE2AF7B34BB95039F2D,
	IronSource_loadISDemandOnlyRewardedVideo_m522FF1BB75B2F1C64DED1F27FA9C0DF2875D0427,
	IronSource_isISDemandOnlyRewardedVideoAvailable_m71030208B1C3E6BA2CA95688F612EC1C61A5616E,
	IronSource_loadInterstitial_m39088A580601883B6379FF62DA10597DC2C28694,
	IronSource_showInterstitial_m3ADEF617BDE29F84C81F21FF802B040AD6C94C4C,
	IronSource_showInterstitial_mA4E2434AE0A6D8A203E918A28E74ED12EFF13EE0,
	IronSource_isInterstitialReady_m58079B2E013BC54CE46B257F59E7073722DC5765,
	IronSource_isInterstitialPlacementCapped_m2D45FBD7ED41EC08B9F447937144F919DF1A2D2D,
	IronSource_loadISDemandOnlyInterstitial_m77F67456C869D60DF73DD3E67D6D22572E6C81F8,
	IronSource_showISDemandOnlyInterstitial_mAF78884276FD07D7C889983A7D8CC9106EC2DCB3,
	IronSource_isISDemandOnlyInterstitialReady_mE2AC65C5A0AC2F4E84B0E533C0EE0514ECF0B369,
	IronSource_showOfferwall_mCC6506DAC941DDC6BDB129DE8CDDC911A86FDA4C,
	IronSource_showOfferwall_mECEA5E201AFB0ABF2421C762328BBBB51D5B5A5E,
	IronSource_getOfferwallCredits_m8E03240FF37A04A5F656011201C1119CD82CDE75,
	IronSource_isOfferwallAvailable_mD2CE4B92C8E643570FB0B2856F61437BBE9D3A61,
	IronSource_loadBanner_m03C65D66F8966F461CA14957430B10C74DCB3DA6,
	IronSource_loadBanner_m89535BCB06C68314DCB235D711D83B5E31E204D5,
	IronSource_destroyBanner_mB66FBD0FD978EBF8FF29CB9436152BB6AB710C05,
	IronSource_displayBanner_mA7F41E7675B6D3723B8E7952027DF1D83F78DFDC,
	IronSource_hideBanner_m1E057C27EECC6DA5C438A95EAA251D86F1851163,
	IronSource_isBannerPlacementCapped_mA556F2F7474259098D57D3567C6752F84DA16D23,
	IronSource_setSegment_m42254418A0802B073A4E46AA6572E8FBD653D30E,
	IronSource_setConsent_m7BF5F76ACAC26533651B45DF82D9EB1A70EA38F6,
	IronSource_loadConsentViewWithType_m4576A4E53A31AFCA9542A3BF03416497F81197E4,
	IronSource_showConsentViewWithType_mB18B211CAA1D43315E1DE9DA1D06F44202295538,
	IronSource_setAdRevenueData_mD94656C154410DB9040D4BDEA1C0BB9EF7513AC0,
	IronSource__cctor_m3E55470F854938EE38046A445A225A3E17280C10,
	IronSourceAdInfo__ctor_mF7D8AF83461D224E8C08A74C66E71FB07648BC95,
	IronSourceAdInfo_ToString_m623348DE306F85979183DCFAE50A14EF0309D592,
	IronSourceBannerAndroid_add_OnBannerAdLoaded_mDF5C33697B5715B87F6A3464B1D986B116C3B257,
	IronSourceBannerAndroid_remove_OnBannerAdLoaded_m2BF2743E161EC5E4A8836DDD64EB12534CA5306C,
	IronSourceBannerAndroid_add_OnBannerAdLeftApplication_mF02C0A9E80ECE42311720E76211CFC5B1B2DBDE9,
	IronSourceBannerAndroid_remove_OnBannerAdLeftApplication_m0C86C8F7C04ACD8860B861CF9031DEE8034C1D05,
	IronSourceBannerAndroid_add_OnBannerAdScreenDismissed_mF894FA3B2EB7A76878B135CAE7E09E5516E5B5C1,
	IronSourceBannerAndroid_remove_OnBannerAdScreenDismissed_mFADB9036AFEEB3A67B08CA52206032E2E5AC5419,
	IronSourceBannerAndroid_add_OnBannerAdScreenPresented_m20849CEF8A9A61E3C79D003E1728CF97072DF7BA,
	IronSourceBannerAndroid_remove_OnBannerAdScreenPresented_mAA130FB7E0C5CE53A74617305CD1EC090C5015FF,
	IronSourceBannerAndroid_add_OnBannerAdClicked_mC32FE194C4AE994DEBC4CEA34960C4D550F85BDF,
	IronSourceBannerAndroid_remove_OnBannerAdClicked_m65000F5AD6D4A4B4C0FDEF8635BF1164D51F8A63,
	IronSourceBannerAndroid_add_OnBannerAdLoadFailed_m0FBEC248687E62048447C1AABEB96112A1B6261F,
	IronSourceBannerAndroid_remove_OnBannerAdLoadFailed_m03721B3E054E9366CDA223D1140C137C0DAEBEC9,
	IronSourceBannerAndroid__ctor_m046B28BC9EC7E766448B0E2ED7B527903A17F060,
	IronSourceBannerAndroid_onBannerAdLoaded_m584C812249D7D01EA7AC159D93B4540313890823,
	IronSourceBannerAndroid_onBannerAdLoadFailed_m86E49994A320BC2BBBC63DDA296C8CF1F4150819,
	IronSourceBannerAndroid_onBannerAdClicked_m98C279D56D3641DE033911E905723355C09E9756,
	IronSourceBannerAndroid_onBannerAdScreenPresented_mF5C8622256201B97258A91B2D01531471A9F7A86,
	IronSourceBannerAndroid_onBannerAdScreenDismissed_m450C5AA743D1F1D6B8F7B2EC10C0D1A42E275CF1,
	IronSourceBannerAndroid_onBannerAdLeftApplication_mD700DF6065DE2E2DE5193FBDC9750FA3CC4BECA7,
	U3CU3Ec__cctor_m5211CDE90E7FB460727CD8060454F38E1C99A0EF,
	U3CU3Ec__ctor_mBFD7A70F3704CB9D33A24218E35B3ED9AA5DA7E4,
	U3CU3Ec_U3C_ctorU3Eb__18_0_mA9D1628CE95A035959D388DC09CF4D4C9D2BAAA1,
	U3CU3Ec_U3C_ctorU3Eb__18_1_mEDAD022E38444B18A1F8A31C5750A5587FCE7824,
	U3CU3Ec_U3C_ctorU3Eb__18_2_m5F1FC9EE84572B40D3BD0F25969079F3779F9975,
	U3CU3Ec_U3C_ctorU3Eb__18_3_m6F87CC805517B6142802989A198A77E5755CFF2F,
	U3CU3Ec_U3C_ctorU3Eb__18_4_m1E7C89203B4B2400FA115B32CDAF8C31FB47BE33,
	U3CU3Ec_U3C_ctorU3Eb__18_5_m148561F53AE393AC9CE3DE54878905AA7BD1C21C,
	IronSourceBannerEvents_add_onAdLoadedEvent_mAB7C723AEBDD230032F2C0E7A3181E766F9B6782,
	IronSourceBannerEvents_remove_onAdLoadedEvent_mAA8ECCC8241A0D08A6D9B0046B670C25D11DFB94,
	IronSourceBannerEvents_add_onAdLeftApplicationEvent_m364538208BEEF7C3ED2F45326789074552D1801A,
	IronSourceBannerEvents_remove_onAdLeftApplicationEvent_m389EA33DA3F4CF2C3E5B484FF99D6C1D50C9865E,
	IronSourceBannerEvents_add_onAdScreenDismissedEvent_m0E7948623E369E5E3CD05DC4D063B271A9DD3877,
	IronSourceBannerEvents_remove_onAdScreenDismissedEvent_m12DA29BEFB70CEA0A85965C7353EBEB464D71797,
	IronSourceBannerEvents_add_onAdScreenPresentedEvent_m9219387E02653C21FE739D020CBBA2B9E81C4FC8,
	IronSourceBannerEvents_remove_onAdScreenPresentedEvent_mE38F36C5B00793A655E6E7EABE8A4F2D5CCC8405,
	IronSourceBannerEvents_add_onAdClickedEvent_mDE542295FDD50DAE96B0E7AB0C2FE717431D2519,
	IronSourceBannerEvents_remove_onAdClickedEvent_mE3505586962B6AA519EB0F305CC091B47F962E89,
	IronSourceBannerEvents_add_onAdLoadFailedEvent_m20245573E61E5D57AAAA1CB50EC7B1990F6B698F,
	IronSourceBannerEvents_remove_onAdLoadFailedEvent_m793EB4010C773B21B70D983A6A13F3A3B3A580CA,
	IronSourceBannerEvents_Awake_mDF073F75C563D3944C06ECF35A9E240CE5DA18A8,
	IronSourceBannerEvents_registerBannerEvents_m299B4CE3E629F5DDB071711AE79E1A5CFEED0835,
	IronSourceBannerEvents_getErrorFromErrorObject_m556ED5C825602D48726CA81B573AFAFCCBEB970D,
	IronSourceBannerEvents_getPlacementFromObject_m55A925AFA6A74551CD777EA8552ED936C72C74E8,
	IronSourceBannerEvents__ctor_mC266A7D537841B189D398F4FDB66827C58E4D9F7,
	U3CU3Ec__DisplayClass20_0__ctor_m2517437E4858DE1CF59F79E3CCA0CB7B5C039924,
	U3CU3Ec__DisplayClass20_0_U3CregisterBannerEventsU3Eb__6_mAA654A109960A234B9B35ECF0CE7478E043B3101,
	U3CU3Ec__DisplayClass20_1__ctor_m1A47472451937CF8013DA93B8A3F84BAF997E89D,
	U3CU3Ec__DisplayClass20_1_U3CregisterBannerEventsU3Eb__7_mB218B5DBF1F7A693C7659E494D1D17BF40DA367F,
	U3CU3Ec__DisplayClass20_2__ctor_mE7762ED8A14B6A7359798AF0FBCED6F957B9C9D0,
	U3CU3Ec__DisplayClass20_2_U3CregisterBannerEventsU3Eb__8_m5C27A65340A0D9310B62B7591581BD4732848B4E,
	U3CU3Ec__DisplayClass20_3__ctor_m5A7B50AB8AA48F2FA9FF8604E14A5A76CDCDB5BF,
	U3CU3Ec__DisplayClass20_3_U3CregisterBannerEventsU3Eb__9_m404E2A857A16C98E490DC0D4F00EF9A98F3C5BE6,
	U3CU3Ec__DisplayClass20_4__ctor_mC82F5474D6C5D6391AFCBC3106E80787CE99F70C,
	U3CU3Ec__DisplayClass20_4_U3CregisterBannerEventsU3Eb__10_mC892FE1F96F7DA45D97D7780D039A62A2ED2E77F,
	U3CU3Ec__DisplayClass20_5__ctor_mA9193729A3045A09C106B620E9DE17E4B50EF27B,
	U3CU3Ec__DisplayClass20_5_U3CregisterBannerEventsU3Eb__11_m06E54B7136D2B36A19943B554336E264D2F55F91,
	U3CU3Ec__cctor_mD339A4CAFC5F796EB1B714B5E2767C99CDC7EEDA,
	U3CU3Ec__ctor_mE836590DDEE358CB1B3F2FD4F8E920101D1343CA,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_0_mD4594D6839FD8D45637413FDD7B1C6F10BD6B9D7,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_1_m8D1ACE2A3539A2711348B076E1EC0DAF20CE2739,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_2_m4EDC1C5641309446B7E72F07997DF5C2D2AEB2A4,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_3_m7F4C9335A1205B5F2AFA8BD87C67DAF8D627AB7E,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_4_mF7407AB7F3115DD76DC841261202EB39EE35E104,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__20_5_m777C96B012685E3A88AF31D2AE54A0967A927012,
	IronSourceBannerLevelPlayAndroid_add_OnAdLoaded_m8D0C1C08B64E35487A3C7E5E09ABCCA0F7ECFCE7,
	IronSourceBannerLevelPlayAndroid_remove_OnAdLoaded_m0F4E7C083A5FEAAC377501D23C913FA016F8E37D,
	IronSourceBannerLevelPlayAndroid_add_OnAdLeftApplication_mAE8E0216AB45D618B87E7ED5B6D9694A2861AFAD,
	IronSourceBannerLevelPlayAndroid_remove_OnAdLeftApplication_m4054FCCDDAC1DBDBEC4647146DCB389C7402AF58,
	IronSourceBannerLevelPlayAndroid_add_OnAdScreenDismissed_m7A57A3315A9A6A4B5B31D6CFB208BC2EAEB1F427,
	IronSourceBannerLevelPlayAndroid_remove_OnAdScreenDismissed_m18B2FA386F8E29EA5CB8F250BD68F0211489F1C1,
	IronSourceBannerLevelPlayAndroid_add_OnAdScreenPresented_m9C5EBA20F672A33187FD8F74A27A36A83CFE793B,
	IronSourceBannerLevelPlayAndroid_remove_OnAdScreenPresented_m76A36E7298826F585B748CF537891C819D14302B,
	IronSourceBannerLevelPlayAndroid_add_OnAdClicked_mABF09F6553B809AFE05B6F3815EF2CDCE4AC3057,
	IronSourceBannerLevelPlayAndroid_remove_OnAdClicked_mA7C4C7EB81F9B16555A0CABA593C6A58808979F6,
	IronSourceBannerLevelPlayAndroid_add_OnAdLoadFailed_m9E64BDDD38DBCBF9037352A72BF886B389031DB0,
	IronSourceBannerLevelPlayAndroid_remove_OnAdLoadFailed_mFD0D76AC648F48F27C7E2C7FFF901C1D612A4AD0,
	IronSourceBannerLevelPlayAndroid__ctor_m9065B57B265EC9D60F65C21E4C4EBA59EB144269,
	IronSourceBannerLevelPlayAndroid_onAdLoaded_mEEB52915F8014FC0E18697A82672CC3635DC4464,
	IronSourceBannerLevelPlayAndroid_onAdLoadFailed_m202B6BA11EAA5BE6B79D878543E2AE421E523A12,
	IronSourceBannerLevelPlayAndroid_onAdClicked_mD48EB773C7AD76360B926A7F3D4F3E92F15CB045,
	IronSourceBannerLevelPlayAndroid_onAdScreenPresented_m00934215E8F282C17D37187A4CF0600247D9C073,
	IronSourceBannerLevelPlayAndroid_onAdScreenDismissed_m958010DC3285FE02595B7B7964064C68032061D9,
	IronSourceBannerLevelPlayAndroid_onAdLeftApplication_m95AA7B0145781994AB4AB96304FE266EA495B744,
	U3CU3Ec__cctor_m1A0E84DF521F89B62BCCCC2C1C071C9E18D1BFA2,
	U3CU3Ec__ctor_mACD1B0DA8066DDCA825BDAC2691A81B0BF0C3F56,
	U3CU3Ec_U3C_ctorU3Eb__18_0_m683D6190D8D7387F12659738C19177593B3F046E,
	U3CU3Ec_U3C_ctorU3Eb__18_1_m43B9EFF4E586D5F50AC5D387E9DDE6261FD6C335,
	U3CU3Ec_U3C_ctorU3Eb__18_2_mC4E4EDB598DFFAF8CB90B7349AAC79DB84D934C3,
	U3CU3Ec_U3C_ctorU3Eb__18_3_m3015019EE3B07B250BE1FBC71FEE7549B8DAB96A,
	U3CU3Ec_U3C_ctorU3Eb__18_4_mBF9AEC6824556206CF36FBD2F9166F181A00E7EC,
	U3CU3Ec_U3C_ctorU3Eb__18_5_mDE3FABD48D1210EEB765E21BEDFFE18AEE1610A4,
	IronSourceConfig_get_Instance_m0378F040E15F2A50A09D6FB92BC8EF574567AD66,
	IronSourceConfig__ctor_mE3DE536BB1303D6358FCB9E67903E797C52A7999,
	IronSourceConfig_setLanguage_m1B96A863FC8FEB5E966153C42A009F7EAE954C6B,
	IronSourceConfig_setClientSideCallbacks_m403FACCCA99A9B395CF62210EF543BFE43D42D54,
	IronSourceConfig_setRewardedVideoCustomParams_m8F9337882315A174B87CDC49559808AAEBD3E914,
	IronSourceConfig_setOfferwallCustomParams_m6A7A28411A5014E4B951DE2249173162FB7DB5F4,
	IronSourceConfig__cctor_m1A5AC5F7A51F07730D169FACF3E7F742795E1B1E,
	IronSourceError_getErrorCode_m52FDA5DC9A5907273FA8295C0126098D72D53583,
	IronSourceError_getDescription_m5B0FEA9AB9AA406E2EC2582B81FFB8F1AD99F5CB,
	IronSourceError_getCode_mECF7AA882A931B515CBB012FB97D5D8F9B3D7194,
	IronSourceError__ctor_mA09EE012D497AB24FD88631DEBD67A9FBB801E83,
	IronSourceError_ToString_m4C41B343FE87831C5DEB35AD4A33A66E9CF374E1,
	IronSourceEvents_add_onSdkInitializationCompletedEvent_m471FCAE6872A1E5A9AC33A8AC594D81C8615E4AC,
	IronSourceEvents_remove_onSdkInitializationCompletedEvent_m91D5D675910C3CBC3B0F0DC97E1A2B18839EF860,
	IronSourceEvents_add_onRewardedVideoAdShowFailedEvent_m29722602CE97A6FDF8523AC431E189C2048F16E4,
	IronSourceEvents_remove_onRewardedVideoAdShowFailedEvent_m73E0FE05A1CEBBFA062FBE224F27D5374446A731,
	IronSourceEvents_add_onRewardedVideoAdOpenedEvent_mB55F555C6F8D9EAE02B2E5D4CDE14128CBC77B73,
	IronSourceEvents_remove_onRewardedVideoAdOpenedEvent_m8D54F4E940A98474E897E02805BEC2B7D6A5F772,
	IronSourceEvents_add_onRewardedVideoAdClosedEvent_m959D5DD7F99D48A65FDC3441C934EB3D4E7300E4,
	IronSourceEvents_remove_onRewardedVideoAdClosedEvent_mEB76930F842DB23E0B7EE650D19F3C183E81C36A,
	IronSourceEvents_add_onRewardedVideoAdStartedEvent_m6620D9C25FFCDF3DE9A90C29E882DD2C280A2763,
	IronSourceEvents_remove_onRewardedVideoAdStartedEvent_m0B5E8A2A39DAD8DA0962D509A040948662C62004,
	IronSourceEvents_add_onRewardedVideoAdEndedEvent_m3250118B6571BB74D01A028F606D1206167BADA1,
	IronSourceEvents_remove_onRewardedVideoAdEndedEvent_mCB3FD0F8A3A964DF972F87B124C0CFBB413C17F8,
	IronSourceEvents_add_onRewardedVideoAdRewardedEvent_mA71DC1C296BDAFB7EF00BC2937C0D8838E93AFAD,
	IronSourceEvents_remove_onRewardedVideoAdRewardedEvent_m357D37267736CC1C2D77718D7685A32208B46329,
	IronSourceEvents_add_onRewardedVideoAdClickedEvent_m8B8BEA58A629E567AFF177241995FDF24F830A59,
	IronSourceEvents_remove_onRewardedVideoAdClickedEvent_m3C0B966592CD46F8576B099166726F6137613A0D,
	IronSourceEvents_add_onRewardedVideoAvailabilityChangedEvent_m5A19B1A1EBFDC945BA01B41D2CE0BA9BE5734B72,
	IronSourceEvents_remove_onRewardedVideoAvailabilityChangedEvent_mD4A3B6C153DA2992A8C6985132F7810B6A4C1718,
	IronSourceEvents_add_onRewardedVideoAdLoadFailedEvent_m3A6F6307B15CA482926E295A75C566E141622B4B,
	IronSourceEvents_remove_onRewardedVideoAdLoadFailedEvent_mB9C1F50B6C29D9791BBC293CCCA891F953A94AD6,
	IronSourceEvents_add_onRewardedVideoAdReadyEvent_mD6E4535916F412CA974342BC5E1BBB5B6C17155B,
	IronSourceEvents_remove_onRewardedVideoAdReadyEvent_m72ABF85DAEDAD5C0F49E55951C2664342AE74BE2,
	IronSourceEvents_add_onRewardedVideoAdOpenedDemandOnlyEvent_mEC82D0E4958FBB13D533D4C0679E29B2D6549F10,
	IronSourceEvents_remove_onRewardedVideoAdOpenedDemandOnlyEvent_mA32E0DB6FEEA1485DDFE6A1B718E289C8030D804,
	IronSourceEvents_add_onRewardedVideoAdClosedDemandOnlyEvent_m3C67F506D142671A261C04A10AC3F9AB0AEA081A,
	IronSourceEvents_remove_onRewardedVideoAdClosedDemandOnlyEvent_mB08D7D65220F81075C72419BD473855FF2A4C5ED,
	IronSourceEvents_add_onRewardedVideoAdLoadedDemandOnlyEvent_m0D48658F2FD93FC2E3131CE20136099A34CCCE6D,
	IronSourceEvents_remove_onRewardedVideoAdLoadedDemandOnlyEvent_m8E0833F6DEB0FC8DAC856408437CA7350144498C,
	IronSourceEvents_add_onRewardedVideoAdRewardedDemandOnlyEvent_mFCEE5A8D8368EEBCBF7191665AE7ECD67418A980,
	IronSourceEvents_remove_onRewardedVideoAdRewardedDemandOnlyEvent_mE5EC2B6BE2A60A199144DA0F244599107592DB99,
	IronSourceEvents_add_onRewardedVideoAdShowFailedDemandOnlyEvent_m4DF230EC01A9F727A11251AC6B2F8539ADE29ED1,
	IronSourceEvents_remove_onRewardedVideoAdShowFailedDemandOnlyEvent_m4498C29227026D44338E6E4E37083BF5481421D9,
	IronSourceEvents_add_onRewardedVideoAdClickedDemandOnlyEvent_mA9C688E33BB7D9B92295EBF945BF8768ED90C4F3,
	IronSourceEvents_remove_onRewardedVideoAdClickedDemandOnlyEvent_m6104623178966A79B4039CDEE64F9615C12355DC,
	IronSourceEvents_add_onRewardedVideoAdLoadFailedDemandOnlyEvent_m4B826C2CCD77AA6F70B04D9964102BB3A1EFE5C6,
	IronSourceEvents_remove_onRewardedVideoAdLoadFailedDemandOnlyEvent_mC253AFA347960767255B15EF643B0FEA194786BB,
	IronSourceEvents_add_onInterstitialAdReadyEvent_m34F94121A366099D40030542EABDDAF33BC7FE7C,
	IronSourceEvents_remove_onInterstitialAdReadyEvent_m1870C568C60D032D143D8DA1F1222D4207FF33FA,
	IronSourceEvents_add_onInterstitialAdLoadFailedEvent_m24CF2CD7A6382B1EE2F14998784AC3F6632479AE,
	IronSourceEvents_remove_onInterstitialAdLoadFailedEvent_m3C04C71B599FC0A451242EFBBA9ABD9CD14D7493,
	IronSourceEvents_add_onInterstitialAdOpenedEvent_m00C0BC496463FE263851C412124530A40EA09A36,
	IronSourceEvents_remove_onInterstitialAdOpenedEvent_m91690127DBA1BF88842E0A3CF474FEB9E492BC88,
	IronSourceEvents_add_onInterstitialAdClosedEvent_mD6847B3D2C68F8079CC790C13FAD39052E15273F,
	IronSourceEvents_remove_onInterstitialAdClosedEvent_mE9B906EB9F8F06E59ADD4DABDD5EBBF11AC5CF57,
	IronSourceEvents_add_onInterstitialAdShowSucceededEvent_m097D856DB436ACEA9CC31EE08B90B31464D673A3,
	IronSourceEvents_remove_onInterstitialAdShowSucceededEvent_m68F162EEAF81CF1BF0636B01943E13639B958291,
	IronSourceEvents_add_onInterstitialAdShowFailedEvent_mF176DC5A365A7C77CC9707D2146E96FAB21D16E0,
	IronSourceEvents_remove_onInterstitialAdShowFailedEvent_m5664BE553972DAEC65AC31F1EFA98276ECF5FE2E,
	IronSourceEvents_add_onInterstitialAdClickedEvent_m78608D4D8D8A967090F89B172BA5D080ACB6D714,
	IronSourceEvents_remove_onInterstitialAdClickedEvent_m3D4F92373DD7D9950D8CEE254E159DB723630078,
	IronSourceEvents_add_onInterstitialAdReadyDemandOnlyEvent_m2D877B96503ABECE2D8D0B2A92A24E5A65EBFD22,
	IronSourceEvents_remove_onInterstitialAdReadyDemandOnlyEvent_m4EB1DC2ADBAD6A56F7AD2D1CEC531F2DC2CCDFA9,
	IronSourceEvents_add_onInterstitialAdOpenedDemandOnlyEvent_mEE95BFD58A4DA7278AB65094ED4CAC3D2A32C27F,
	IronSourceEvents_remove_onInterstitialAdOpenedDemandOnlyEvent_mA2527D6E6FE0265C3A99CDF5048F15CE7FD0A6AB,
	IronSourceEvents_add_onInterstitialAdClosedDemandOnlyEvent_mEE673AAE31EEE663A13E21DD557D095C4660DFF6,
	IronSourceEvents_remove_onInterstitialAdClosedDemandOnlyEvent_m6AA19068C126F8B6D6A4AB39EADAC377BDAB3B88,
	IronSourceEvents_add_onInterstitialAdLoadFailedDemandOnlyEvent_m9F00538F8DF6D450EC86F26B0F735D908CB8B04D,
	IronSourceEvents_remove_onInterstitialAdLoadFailedDemandOnlyEvent_mE7C58951819292C37BA18E816E5FD2CD7174624A,
	IronSourceEvents_add_onInterstitialAdClickedDemandOnlyEvent_m215930C2F54D9368B1F0C49DA0D9E4CC75D3E916,
	IronSourceEvents_remove_onInterstitialAdClickedDemandOnlyEvent_m1EB5EA8245922CFCDB42A6B5D0F1BAD3DD55C5B7,
	IronSourceEvents_add_onInterstitialAdShowFailedDemandOnlyEvent_m802B30D3869C47BC6024EAC4D132A6ED3ADD631E,
	IronSourceEvents_remove_onInterstitialAdShowFailedDemandOnlyEvent_m5BB86A5CF3E509D2FB92CCF2330AB95D2D6A8665,
	IronSourceEvents_add_onOfferwallAvailableEvent_mABE61BE1908998144A168AD58666025107D40D74,
	IronSourceEvents_remove_onOfferwallAvailableEvent_m1AB4884508986007B674ACE8DF82BD251A90BB99,
	IronSourceEvents_add_onOfferwallOpenedEvent_m26EB82756530ECF5CCD44B6715ED88FAAAC153D0,
	IronSourceEvents_remove_onOfferwallOpenedEvent_mBF7745ACC3B197ECC0C28423B4C83421A78BA6D2,
	IronSourceEvents_add_onOfferwallAdCreditedEvent_m625FE6E328C52D10FC5C9E8F759363510D5E8273,
	IronSourceEvents_remove_onOfferwallAdCreditedEvent_m0657B0EC312EFC2AC432DC6C56CD7FAA807F9147,
	IronSourceEvents_add_onGetOfferwallCreditsFailedEvent_m677F48F28418CD6E1A6014C6C1B8076E0934C545,
	IronSourceEvents_remove_onGetOfferwallCreditsFailedEvent_mAAA2A15BCA447C3928E8A8B228123D389DEE5528,
	IronSourceEvents_add_onOfferwallClosedEvent_m031BB82A7474309857A686074FE8B715B194C7A3,
	IronSourceEvents_remove_onOfferwallClosedEvent_m66799C27234453F140B9CC2846894EEF2D95DD43,
	IronSourceEvents_add_onOfferwallShowFailedEvent_m2B47334DF4BB7C140970479B8D19CCC0D084BD60,
	IronSourceEvents_remove_onOfferwallShowFailedEvent_m8419C9F07B707125B5816BFEA8FBAEE721E07EEC,
	IronSourceEvents_add_onBannerAdLoadedEvent_m5AC72619E274F9F242C060EFE25475FB5A8C2730,
	IronSourceEvents_remove_onBannerAdLoadedEvent_m18EFCD0F6B0D380EE81D5A4CB2B95E6E6B79626C,
	IronSourceEvents_add_onBannerAdLeftApplicationEvent_m710AA72BED846467CE545277882E0DC15474BD42,
	IronSourceEvents_remove_onBannerAdLeftApplicationEvent_mBB047FC8EA03E3C1E56149EA35E1A2AD221ABD35,
	IronSourceEvents_add_onBannerAdScreenDismissedEvent_m38B8276614DD95132A504FF6C2C3621E8CCDD027,
	IronSourceEvents_remove_onBannerAdScreenDismissedEvent_m3FFBB4F494DF1439DADA50638021FB119D922337,
	IronSourceEvents_add_onBannerAdScreenPresentedEvent_m99C9D07939297D3B847AD13A5A05AC5798310FF0,
	IronSourceEvents_remove_onBannerAdScreenPresentedEvent_m98B8F5DADD79B17DE9819CCAF2E49795122652BF,
	IronSourceEvents_add_onBannerAdClickedEvent_mA997638A3550C426CF2BFEEE5890F773C057F6C1,
	IronSourceEvents_remove_onBannerAdClickedEvent_mFC885813154EFF3F0677E1D81D100D342CA5C56C,
	IronSourceEvents_add_onBannerAdLoadFailedEvent_m9AA6305404DB70F0C187B59FD2D6DCA83D0D144F,
	IronSourceEvents_remove_onBannerAdLoadFailedEvent_mAD3C8E30972196A50CCA5521D18133DF32FE3C29,
	IronSourceEvents_add_onSegmentReceivedEvent_m6998D836B150F5A65BCFA8EEA54EAE8D7CC3E2B2,
	IronSourceEvents_remove_onSegmentReceivedEvent_m6753C63ED4D5AFFD58840D2D343430D7BAEC3EFE,
	IronSourceEvents_add_onImpressionSuccessEvent_m047522F303674EF194FEB9F8A03CC9457EDDE53F,
	IronSourceEvents_remove_onImpressionSuccessEvent_m0461F3E2D5A62A4BCD4538B35B567A16B590CF3D,
	IronSourceEvents_add_onImpressionDataReadyEvent_mCDB20226BEEF3502327CC2E5DFF014175E0661BB,
	IronSourceEvents_remove_onImpressionDataReadyEvent_m8EA6BD62BEC2C682BD314EF1B630D3C87EDBFADB,
	IronSourceEvents_Awake_m0CB3C1DF700B1BD58C8813A0653F43C8471B39E0,
	IronSourceEvents_registerInitializationEvents_m0440A12C16761B186092C978E3C77D92AAFCCAA8,
	IronSourceEvents_registerBannerEvents_m797C8E569208993EE2D451731E7BDACB20CA9733,
	IronSourceEvents_registerInterstitialEvents_m5E613746944E0EFEEB7FD6D5ACA60D6A99960B9E,
	IronSourceEvents_registerInterstitialDemandOnlyEvents_m08D5A94CBFB61A5444C52CD4E93C3CCCF8A92C3F,
	IronSourceEvents_registerOfferwallEvents_mF8243F660BE299D772D70F8A01E6F836C9FCBC95,
	IronSourceEvents_registerSegmentEvents_m3D00BB865B5D40C1E00376B56291B91836E5496B,
	IronSourceEvents_registerImpressionDataEvents_m8B7756CC432FE7DD754786D20DCCC594A0D5DDFF,
	IronSourceEvents_registerRewardedVideoDemandOnlyEvents_mF2B83116D195B43F9D8A127B0213F1E0D6C6D177,
	IronSourceEvents_registerRewardedVideoEvents_mD992ECAFB586F4198F11E8249AF6357EA60CB812,
	IronSourceEvents_registerRewardedVideoManualEvents_m37914E35D820D14202C42232D19BB9E4161D3A38,
	IronSourceEvents_add__onConsentViewDidFailToLoadWithErrorEvent_m7E83CDADEFD2BFED62D3627CB462E48E7A53F130,
	IronSourceEvents_remove__onConsentViewDidFailToLoadWithErrorEvent_mB7CCDA70650FDAB369DF961F3338A9DC1EFE8854,
	IronSourceEvents_add_onConsentViewDidFailToLoadWithErrorEvent_m7AF225A27FA8F204DAFB841FBA5F1F5370F2ABF6,
	IronSourceEvents_remove_onConsentViewDidFailToLoadWithErrorEvent_m7D9F5092EDE934B8EF3605B75C0376DDBB4E03D6,
	IronSourceEvents_onConsentViewDidFailToLoadWithError_mD7994A50F8CAB78E71DE22F3FCFDD14D9AC182DE,
	IronSourceEvents_add__onConsentViewDidFailToShowWithErrorEvent_mBADA58A3A8DD457A76F7EF3CDA062C115A7431F6,
	IronSourceEvents_remove__onConsentViewDidFailToShowWithErrorEvent_m955580FA39FB6334B09958CA3987E6AC818F1E44,
	IronSourceEvents_add_onConsentViewDidFailToShowWithErrorEvent_m714366B0D0F10BC22383A7AAE182B2D7C0FD90BC,
	IronSourceEvents_remove_onConsentViewDidFailToShowWithErrorEvent_m69C51056180A357D572FB98D1476F69AFB783A7A,
	IronSourceEvents_onConsentViewDidFailToShowWithError_mB67F917F6ECCC1AB1FF1B82343E36E81986E73DD,
	IronSourceEvents_add__onConsentViewDidAcceptEvent_mFC0762C460226A61102E0ADE2DB6673A67CFB4A1,
	IronSourceEvents_remove__onConsentViewDidAcceptEvent_mFADA8AF86FCFA9FFDCC28733F1FB74B37A301C09,
	IronSourceEvents_add_onConsentViewDidAcceptEvent_mD858F0C13086EF25D39A1FC3ECCF198E1DABB37C,
	IronSourceEvents_remove_onConsentViewDidAcceptEvent_mD755AEC6E7B01AD08DCA6D7553A7720993EABE2D,
	IronSourceEvents_onConsentViewDidAccept_m456D33695A84A713912C46E0C41262E5E21CC022,
	IronSourceEvents_add__onConsentViewDidDismissEvent_m3F7A4DA3B6FA218C21EF115E99E26AF12222D039,
	IronSourceEvents_remove__onConsentViewDidDismissEvent_m96ABDD24E844C9F0A5C8E609A774EDA41F761AB9,
	IronSourceEvents_add_onConsentViewDidDismissEvent_m6ADA373C67B8C73693388D8CEB8AB3F4F0EA535C,
	IronSourceEvents_remove_onConsentViewDidDismissEvent_mD76ACB56866CEAF43733F2372FDC947E4D99E36B,
	IronSourceEvents_onConsentViewDidDismiss_m41190FB7B040AC2E390775276AA102A8E2077682,
	IronSourceEvents_add__onConsentViewDidLoadSuccessEvent_m091FA7804E1529C0FC10E923ECA9E9E59F8CC923,
	IronSourceEvents_remove__onConsentViewDidLoadSuccessEvent_m9724FEB1CB26470BB9A85CF61D40CE4789B293F2,
	IronSourceEvents_add_onConsentViewDidLoadSuccessEvent_mF450ADA3183ABEB3193B0CAC819E72A84B990233,
	IronSourceEvents_remove_onConsentViewDidLoadSuccessEvent_m969EB641479A20FD91FC86E68C8109FFCAF12E83,
	IronSourceEvents_onConsentViewDidLoadSuccess_mF6046C66B99CCD582F189FB382B1574AF663B0F9,
	IronSourceEvents_add__onConsentViewDidShowSuccessEvent_mE7A70EA11EF021350802F9E18B39C2CFC0C16068,
	IronSourceEvents_remove__onConsentViewDidShowSuccessEvent_m689A5DD54D76363486ACB300B69629EEE40B2E76,
	IronSourceEvents_add_onConsentViewDidShowSuccessEvent_mE2CB53F9E07E8FB957E2822BEBC769FAABFC9D78,
	IronSourceEvents_remove_onConsentViewDidShowSuccessEvent_mA20C70BB78000506E991D35F86A1713A5B822711,
	IronSourceEvents_onConsentViewDidShowSuccess_mDD064CF212AC73248A0C9B37C3C08606181F77A0,
	IronSourceEvents_getErrorFromErrorObject_mFD793978EFB2037D980106D7A18F385A86880B96,
	IronSourceEvents_getPlacementFromObject_m95E8B3F58AB072286914C92B282350B12F797E72,
	IronSourceEvents_InvokeEvent_mC492E94F6DFF61B20560DE716E1116E5CBB99239,
	IronSourceEvents__ctor_mA551D770B0AADFBA26A1C7F3AFA0B9C304654F5F,
	U3CU3Ec__cctor_m09FE091D42C3C439D661E1E724D0AA5E7EF03CB2,
	U3CU3Ec__ctor_m4E5FB9428004D80CB5FACA7C5193AB7575109A62,
	U3CU3Ec_U3CregisterInitializationEventsU3Eb__151_0_m267F582A13C7915F968FE82BB0FDE7F93CBB2992,
	U3CU3Ec_U3CregisterInitializationEventsU3Eb__151_1_m003CFDE9E63B60438D38CFB3C10D1A854440B555,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_0_mB9495556BBCFE038D77BCD61A35FEE0C0EBB57C4,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_6_m097161BCC8F9E9A58E37DD3FCAF572F144FDC307,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_1_m26F63B1DFEF3F0354953E83F75BD38D0650CCE96,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_7_mA6CE38103BB13E711C910BE79C0DB6E86DC4617F,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_2_m7F691D164F04E9C61BA7B166C5C8FEC5440FDC56,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_3_mDD9F856FED30B26C3AC3C4BB0C4089DF185C79D3,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_9_m4847192AE0E7AB3F33E43FD4712876F109ADA5CA,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_4_m4FEDA56B8A8544543A8D272A82A82E81AAD753B9,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_10_m8137E8D7C89BF1038FCB479A269A140A2B43C505,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_5_m6C943703F9A2956B68E997C51C60AD3F241B9CAB,
	U3CU3Ec_U3CregisterBannerEventsU3Eb__152_11_mF276D5137121A098BC1CE0658C95F26213757A33,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_0_m4A7558CFFEDFDA653EE01CB0CFAA5C0A0277BCDC,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_7_mE5691673171522EA00B0AB3A0AD9DFE9163CC55D,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_1_m3582828D0EE6D54E1DE4128B119FCE86CA78051D,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_8_m39C211CCF95564D07FF032D3E45FDE658CA0013F,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_2_m64A6AF2DB383B2B1573FB1A6D435D9AD6C7BD0DC,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_9_m0B734F5A33D69E91DFB6949344A2DD99C6AD3713,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_3_m5366E23B48A8B057DBC6BB605272827E38B27BB1,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_10_m2D87627E100EDF679EED31F0F7658C96B067E91E,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_4_mFE9E6CF4C0CCC9C0FDDD3459A977EFEAA6B41984,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_5_m493F560BEE792C3A5E06E559A74AFEDD7FBCEF43,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_6_mC640ED8B3DB5FB3DAA56EAB63E774FA0FF342ED7,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__153_13_mD8149772CCE65E73140A99285C125B1613AC42B8,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_0_mE4E4688C33184433DCE90917B3D243F22A383EFB,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_1_m045296B7F88EE69D8A439BF4F7F587774AD4A2F1,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_2_m472AC489ACAD14C883925657A0F424E9D7780F89,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_3_m8076D52F6653BD7765485135A4A815C9754671E9,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_4_m33635C0D043B9211B1FFF4582EDDB536C3DA1651,
	U3CU3Ec_U3CregisterInterstitialDemandOnlyEventsU3Eb__154_5_mBBA8A6FD3AB82417B649F645FCEACB447192ABE7,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_0_m7B217917CEFC90024E18C4034FBF3AD4151F8D0B,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_6_m1766D299108457E9FAB4D48E43AE7F232743D1B1,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_1_m9D710421B7D0C0CED535D373A7E9EFD913C86007,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_2_m5DED9F01D4AB1FE0B0A8208B65202C4E952C30E3,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_8_m8B984ABE766FC1E1BB67D49A258F376DD5C8EB5E,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_3_m39E3B4DB6FB9FD1E8573E39BCBB7E46D4730C2E2,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_4_m7380989133E4F025EF316EB0C12ED8452ADD157E,
	U3CU3Ec_U3CregisterOfferwallEventsU3Eb__155_5_mB57C982CC5AB6613E2FD8AEDC49824E6DFCAD7F7,
	U3CU3Ec_U3CregisterSegmentEventsU3Eb__156_0_m2CA1A54497EFA1A07D7BB1C9E953335147D3E4F7,
	U3CU3Ec_U3CregisterImpressionDataEventsU3Eb__157_0_m2C40397002ACAF526EDAF4594438671D7C467EBC,
	U3CU3Ec_U3CregisterImpressionDataEventsU3Eb__157_1_mDDB8B55669C3FA2E577219435EF2ADD0097E434F,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_0_m666939FCE07FD70F37016499DD06C20C033B592B,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_1_m5F50069909A896E515A75AE0357D4A12C636184D,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_2_mC284C8D0B62E6DD0F3F46CAA9982D59D5B52FA90,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_3_m81F286DCC5D7AE2420123F4126E5B23571674FCD,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_4_m7A2A14D5CE45B23B4716251730D1E94E4A1303CE,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_5_m465E6BFB04BB10FA1FF6D3C3BF3B314D91FE3F1B,
	U3CU3Ec_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__158_6_m6E54808B4C194B0CFE80A5F063DE2E775FC04CEF,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_0_m4741C869EF9E6E8DFA9C2F61E58D4D295D2B4F26,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_1_mEDD845C21C41D43EDE9D06AF96126ADCB79F46FA,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_2_m83FDA9435D3ED62A0899394207736907181953B3,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_10_m7A397EE7CFBBB0B2AC782508CFDB1B16AC746A64,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_3_mA2CF9C376F308E8E75634F77FF3BB8E487AC51F2,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_11_mB3DAA5B0BE0750D86234960D034E57103D4040F0,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_4_mE17FDF09605C42C78FB55B592EA472EAB8D946DC,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_12_m6E6F4C9ABE718600B51B05C5230BD2EAA25F11CB,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_5_m8CB264ADF9AF9330E15D120269E9B956495327A1,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_13_mCC6E6DF4149B482DD2328C7244F04D76C9A6CE69,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_6_m855A0D5E61791C28B851F47DB5B84C10BC251B45,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__159_7_m6B288B00DF7E57824879A73CAB60C2A88B59BFCC,
	U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_0_m10BF938092BD468EE729CA25AA0C02DE670E73A5,
	U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_2_mEA999A8DBCBC66E8965964A3D3D9BB551E7E8266,
	U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__160_1_m9A20CEC5FE8D7284AD3BBD09C62E9D7C1363C2BE,
	U3CU3Ec__DisplayClass152_0__ctor_m6A355C1D8CD82BB75B41F08C3683EC12076D61AF,
	U3CU3Ec__DisplayClass152_0_U3CregisterBannerEventsU3Eb__8_m5BFF953BFEBB79A771614CBDA1F752B3EE828E84,
	U3CU3Ec__DisplayClass153_0__ctor_m178C1936B695056979383263963ABB868749E29E,
	U3CU3Ec__DisplayClass153_0_U3CregisterInterstitialEventsU3Eb__11_m96D91C841D4E1DCF5A5A7F6207D22B0AD9B7DA5E,
	U3CU3Ec__DisplayClass153_1__ctor_mA3D2A8C0B1AF540E4A2530A068C0E0164DD1361C,
	U3CU3Ec__DisplayClass153_1_U3CregisterInterstitialEventsU3Eb__12_mB67F9D98B59A93B859E4E6CAEC135DC5890872BA,
	U3CU3Ec__DisplayClass154_0__ctor_mBFDAC64F658FC6FF2C5E887169C3442B52E2A9B9,
	U3CU3Ec__DisplayClass154_0_U3CregisterInterstitialDemandOnlyEventsU3Eb__6_m5C4BBCABC6BE1A014B3CAA98267CD46BF762B16F,
	U3CU3Ec__DisplayClass154_1__ctor_m58C641D058DCFA292409B0DF75BB1E3B52A9A236,
	U3CU3Ec__DisplayClass154_1_U3CregisterInterstitialDemandOnlyEventsU3Eb__7_mEB99D3D1945B54D38A5711ACC668818EDDEDD611,
	U3CU3Ec__DisplayClass154_2__ctor_m4C41BB1B094EE8B29CBDB22B6F17F232B1E62297,
	U3CU3Ec__DisplayClass154_2_U3CregisterInterstitialDemandOnlyEventsU3Eb__8_m5E3D6A02ABB26419FF103E1EDD094DF656D35890,
	U3CU3Ec__DisplayClass154_3__ctor_mC1CDF832459744D578CF619D34A6E3578C75BA28,
	U3CU3Ec__DisplayClass154_3_U3CregisterInterstitialDemandOnlyEventsU3Eb__9_mC77635684E0278E1E7D7B0D8AB5A91EF296A5C02,
	U3CU3Ec__DisplayClass154_4__ctor_mFF01C71E7F449A3DD9F0B4E60FB2B3B9B555956A,
	U3CU3Ec__DisplayClass154_4_U3CregisterInterstitialDemandOnlyEventsU3Eb__10_m4C0325E6B2F6E58FABC8BF6D5A95B37CA18BB523,
	U3CU3Ec__DisplayClass154_5__ctor_mD4A1CF8BF2DAEE01EF648D8097776C47280458A6,
	U3CU3Ec__DisplayClass154_5_U3CregisterInterstitialDemandOnlyEventsU3Eb__11_mEE31B6A2B4039573A33E76D6BB69FB256DB041FA,
	U3CU3Ec__DisplayClass155_0__ctor_mE6AEB4A8BB3C5F8C876C7D236136EE9C04EB506A,
	U3CU3Ec__DisplayClass155_0_U3CregisterOfferwallEventsU3Eb__7_m6188260F4318F082D4C0673F223E985677845F76,
	U3CU3Ec__DisplayClass155_1__ctor_mBBC3D9C2C5297CEF23EB9ACB2FD6BC9AED4B5743,
	U3CU3Ec__DisplayClass155_1_U3CregisterOfferwallEventsU3Eb__9_m64FBBEC8BBFB05A2225D0EF8CE1E0C90B37314EC,
	U3CU3Ec__DisplayClass155_2__ctor_m1141FD53B4B3CEC6A55FBE50D640E5D1A1838778,
	U3CU3Ec__DisplayClass155_2_U3CregisterOfferwallEventsU3Eb__10_m5B3FDA7B6C7C4CF1A21218A6811B61C287B776CC,
	U3CU3Ec__DisplayClass155_3__ctor_m01E13F6D2BCFFEEFCD0D2595D55C2F3E8C2DB3D1,
	U3CU3Ec__DisplayClass155_3_U3CregisterOfferwallEventsU3Eb__11_m0863C0242E65468B620A344BDB16EE3420BCE1FB,
	U3CU3Ec__DisplayClass156_0__ctor_mDA5E79798A3D4CCCBEABBF9F68D7EE82B90D4259,
	U3CU3Ec__DisplayClass156_0_U3CregisterSegmentEventsU3Eb__1_m7D371671B267D0E36F44A570ECADB45B6BE13BB5,
	U3CU3Ec__DisplayClass157_0__ctor_m041E77209555597242A828A9EC8C908F739638FD,
	U3CU3Ec__DisplayClass157_0_U3CregisterImpressionDataEventsU3Eb__2_m7484AED40082B8A3DF3DEAECDAD59C39C0C59512,
	U3CU3Ec__DisplayClass158_0__ctor_m26C1AC241009DAEC5E3C75CC8C6A3C8C9754DC9A,
	U3CU3Ec__DisplayClass158_0_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__7_m6B01FA57A255D963CD093D9F90C3E5815C9C4537,
	U3CU3Ec__DisplayClass158_1__ctor_m131F3F614940EF9F42495715B55D83A73606BDDD,
	U3CU3Ec__DisplayClass158_1_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__8_m50F8967A8986E45C89B21BEFF5F1376789C086E2,
	U3CU3Ec__DisplayClass158_2__ctor_m75EFAF8524D81979F34F640E7348D66808278B8B,
	U3CU3Ec__DisplayClass158_2_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__9_m910FF67C933109973CFACE20DAE6DE17996B8CED,
	U3CU3Ec__DisplayClass158_3__ctor_mB646E057F4FBDE93E26350FB3DE97AA8DFDD74FE,
	U3CU3Ec__DisplayClass158_3_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__10_mA40E042247CD77C85233709DF43B2D45F58D417B,
	U3CU3Ec__DisplayClass158_4__ctor_m0BDCA311035FB051EF91E579A50F3B1AB4F789CA,
	U3CU3Ec__DisplayClass158_4_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__11_m29263CCD5FAA266416E898F9E6EC295839994C8E,
	U3CU3Ec__DisplayClass158_5__ctor_mEA6E43556D224EBA8EB390CE614E6BFBB9BE7606,
	U3CU3Ec__DisplayClass158_5_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__12_m8972E3D27914CFCBAD53F9846D8A3A4EE906254E,
	U3CU3Ec__DisplayClass158_6__ctor_mC31FEA67D2A752E5A9D10A0E5271AD5207859AFC,
	U3CU3Ec__DisplayClass158_6_U3CregisterRewardedVideoDemandOnlyEventsU3Eb__13_mBD03F556F8C83418AC8A4E525511BB6E04FAD044,
	U3CU3Ec__DisplayClass159_0__ctor_mC1136824EE160773F4F3AB9A79842D8C2412DDEB,
	U3CU3Ec__DisplayClass159_0_U3CregisterRewardedVideoEventsU3Eb__8_m329EC8C67BDF8A81586D42B3FB9A40FD618A8135,
	U3CU3Ec__DisplayClass159_1__ctor_m1DDC6A4752829C86CFEB377ACB3FB537D9BD7924,
	U3CU3Ec__DisplayClass159_1_U3CregisterRewardedVideoEventsU3Eb__9_m5758472D6C8CD34852FE7BE3DB62BC8740C4B967,
	U3CU3Ec__DisplayClass159_2__ctor_mD6653B25320E22BC2D0C531E4EFE9D1D377F05D1,
	U3CU3Ec__DisplayClass159_2_U3CregisterRewardedVideoEventsU3Eb__14_m4C37FE706E0D48F921F5639C55E2624F9E6B2854,
	U3CU3Ec__DisplayClass159_3__ctor_mD0D0275433E36531D74B8564419F8F90CAFBDDA3,
	U3CU3Ec__DisplayClass159_3_U3CregisterRewardedVideoEventsU3Eb__15_m8DB25B22DD06DA3114BBF9A0F1525C8AEA536362,
	U3CU3Ec__DisplayClass160_0__ctor_m65FE38ECE69357A32C429247E8B003932464FCC4,
	U3CU3Ec__DisplayClass160_0_U3CregisterRewardedVideoManualEventsU3Eb__3_m82218F491C9EB9001E9056C4FCAAD0E695BE2286,
	IronSourceEventsDispatcher_executeAction_m36717C759E39796E1B872179DC1F07729235ED9D,
	IronSourceEventsDispatcher_Update_m91047BCF6ADDB0CE7126D4B573F899938071F894,
	IronSourceEventsDispatcher_removeFromParent_m0CD4DCB5A160BCCE0EC99B5AC0102FB3C974F927,
	IronSourceEventsDispatcher_initialize_m653E787D78AB21D66620C755C2256674C5C9D934,
	IronSourceEventsDispatcher_isCreated_m1B163868049A565CB45A7F8D702DD9B83D9FB770,
	IronSourceEventsDispatcher_Awake_mE82E33B01D9824575A5BCC0B5B1C42F1BC97E4D2,
	IronSourceEventsDispatcher_OnDisable_mEFC008C61043801393F1A4650DB274E2FF359A8D,
	IronSourceEventsDispatcher__ctor_m382478BE3147BCCB476866C7930B8DF8945592DA,
	IronSourceEventsDispatcher__cctor_mB2D7F32F1E19F6EF0C342F6544323756D1D93C3A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	dataSource_get_MOPUB_m47E06E361A32AE39794C7A89264FD31C5DDA4AB2,
	IronSourceAdUnits_get_REWARDED_VIDEO_mA0E5A6A8356BC1F538996BDE9FC07C1B51D86057,
	IronSourceAdUnits_get_INTERSTITIAL_m1515DA902042C6D1B5E243D6385F740672D7EEFA,
	IronSourceAdUnits_get_OFFERWALL_m8687E1E2BA632B49C3B555680D1D3CC6EB02165F,
	IronSourceAdUnits_get_BANNER_mCB493DAD554E65E501179B44EE1BD9652ECCBDE1,
	IronSourceBannerSize__ctor_mFB14AA94EE7FB60C982443D564B3907AB0077F06,
	IronSourceBannerSize__ctor_m8CA67299323746F72DBEB070AEE991E0BD99F794,
	IronSourceBannerSize__ctor_m026976835E1345597A5196F6A46136E494EA3A70,
	IronSourceBannerSize_SetAdaptive_m899E84CD99F0434C7296AE50E239A116C784858C,
	IronSourceBannerSize_IsAdaptiveEnabled_m003E2079AF51947BB79259443AEF81063D521E96,
	IronSourceBannerSize_get_Description_mF8C8EA3AA5791A341E4AE11699DFC702DC110FD2,
	IronSourceBannerSize_get_Width_mCE75041D93CB0F822A6DF0CCFFD044C1640CF70A,
	IronSourceBannerSize_get_Height_m3342A6235EB2B68B0F6DE323D2FCEF8D59ACCBEF,
	IronSourceBannerSize__cctor_m8A4C2198BD3EC84B4F88014F9D2B3ADFA90AA5CD,
	IronSourceImpressionData__ctor_m8A9655672C03A50586C8C082AE9F4AE7112684FA,
	IronSourceImpressionData_ToString_m31EFDF4D06DDC7C45ACB24A41D5DD3D21C324750,
	IronSourceImpressionDataAndroid_add_OnImpressionSuccess_m3399492A2C1F73621FD554ABFD129839957C1492,
	IronSourceImpressionDataAndroid_remove_OnImpressionSuccess_mF351CD3EC83142D2A913716B5EE2A22971F2CF33,
	IronSourceImpressionDataAndroid_add_OnImpressionDataReady_mE3F8F15265245FD703E708CD364D0D99F7A16414,
	IronSourceImpressionDataAndroid_remove_OnImpressionDataReady_m8571DE3278B1C1719BA66F8E2045B5D3AB76A1FC,
	IronSourceImpressionDataAndroid__ctor_m98E1B0B8B4A116995C0909BC8004A8CA70406693,
	IronSourceImpressionDataAndroid_onImpressionSuccess_m45BF76217DDA3C564FDE8FC7C3ADFA7C928EC017,
	IronSourceImpressionDataAndroid_onImpressionDataReady_mA96415A0C5810D870EE93A38B690A0392030AEDB,
	U3CU3Ec__cctor_m493CCF82AF29E03D97A6878263A993D2C8F64C10,
	U3CU3Ec__ctor_m91C1A6267A5D37E31C27DAECC44619B84508F37E,
	U3CU3Ec_U3C_ctorU3Eb__6_0_mE0B10BB184C402ED6582235B342C754DBE6081D1,
	U3CU3Ec_U3C_ctorU3Eb__6_1_mC38AD512107A207631DCA1B45DF8B3BD090F35BF,
	IronSourceInitializationAndroid_add_OnSdkInitializationCompletedEvent_m6B7AED378A4719382B64892590CF9425D3CBB662,
	IronSourceInitializationAndroid_remove_OnSdkInitializationCompletedEvent_mBBEEEE8898BFA0B74F5354B985AE527C57EC6E48,
	IronSourceInitializationAndroid__ctor_m7E5E2D060E9245221B232F119F7BD4D4655AC036,
	IronSourceInitializationAndroid_onSdkInitializationCompleted_mD833C6AF2C5F6107F6BD5B2BC966F8206A2D59FE,
	U3CU3Ec__cctor_mB464ADDA936D38152F7AAB1ACD9BC07782701AEF,
	U3CU3Ec__ctor_mA1E596EC5B0D6DDF7D1D68D79E3130B41D231634,
	U3CU3Ec_U3C_ctorU3Eb__3_0_m20C2D3C6498B4F204EA3DE5C13C82A07C5D33C1D,
	IronSourceInitilizer_Initilize_m8EAF5195C6D5479A1DA01FD06B63492CFC349A5B,
	IronSourceInitilizer__ctor_mA332DD98DB4565F71AB6CC7BB7026AF382BDF8D7,
	IronSourceInterstitialAndroid__ctor_m4A8A6104D3D43915FF89DFF53792FA9217CE10A2,
	IronSourceInterstitialAndroid_add_OnInterstitialAdShowFailed_mA1EE744F54B74834E229C7D5EE78FFE263280A90,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdShowFailed_mBBCB251CA2CDEC72CF85274781F4531E948B2AC9,
	IronSourceInterstitialAndroid_add_OnInterstitialAdLoadFailed_mFD989BA0F515E2210EF58F5945939B340AE86571,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdLoadFailed_mDF2DCDC425F759C9045A116399D80DCA01334C4E,
	IronSourceInterstitialAndroid_add_OnInterstitialAdReady_m899724818295CBD5A61BDC38BC98ECF14A84E76E,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdReady_mA3971C5364E1546F36DDDE73B30537A3E9AD9C8B,
	IronSourceInterstitialAndroid_add_OnInterstitialAdOpened_m09F1E193364681953777B8A29B1D9C640EB0C3CC,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdOpened_m914ED38FA0311900B2C40DE6E300AFC57D58E408,
	IronSourceInterstitialAndroid_add_OnInterstitialAdClosed_m31BDD7D11918E5CBC22C6EECBC6BFE7F27B2BAA5,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdClosed_mF1878DD36CCD2539844805379133738A2918C93C,
	IronSourceInterstitialAndroid_add_OnInterstitialAdShowSucceeded_mAFD13301C4C87792F073825B290D5FB19A242C3D,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdShowSucceeded_m6BB20F1FB7BF4BFB5C94BB97AB6F7E55E6D60F50,
	IronSourceInterstitialAndroid_add_OnInterstitialAdClicked_mD1B0380BFFD9720765402615820283D2033A4404,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdClicked_m0A931020B0897D361AA70EDD5107D870684F5B31,
	IronSourceInterstitialAndroid_add_OnInterstitialAdRewarded_m1354BD1EEF257E119EE3793C092BACCB644C30A2,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdRewarded_m55BC59327C46B26A7283B97D35C65B535943F50C,
	IronSourceInterstitialAndroid_add_OnInterstitialAdReadyDemandOnly_mD620FC42994EC546E9D3F16811BBFE883BF4726E,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdReadyDemandOnly_m9DBC04A573A1189B634D8F68744AC799F6A9AABE,
	IronSourceInterstitialAndroid_add_OnInterstitialAdOpenedDemandOnly_mE296BE8FCDD52C7715FF10B4EE3A4E5149B084FE,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdOpenedDemandOnly_mDEBAFB541845193AA7C80958B7E6CD456421E859,
	IronSourceInterstitialAndroid_add_OnInterstitialAdClosedDemandOnly_m05A9B1328274BB4707B595CD2CBECCB2835FB7CC,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdClosedDemandOnly_m34630D2B0D73FC7C966226A19277EC4453D7488F,
	IronSourceInterstitialAndroid_add_OnInterstitialAdLoadFailedDemandOnly_mB3AA4E9F6D1259AAA4C7FDCF0A8E5FA9C730897D,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdLoadFailedDemandOnly_m7FA513D9D3DF827BB085790C7E329C43DB326BA3,
	IronSourceInterstitialAndroid_add_OnInterstitialAdClickedDemandOnly_mC38D598385FF6B83192A28AD6C3804DCC807E256,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdClickedDemandOnly_mBCB300371D3CD601FCA7F98BA1F4667362B57A38,
	IronSourceInterstitialAndroid_add_OnInterstitialAdShowFailedDemandOnly_m44F8EF5A1721E4522BC04D5A8A4898BD98EA0E8D,
	IronSourceInterstitialAndroid_remove_OnInterstitialAdShowFailedDemandOnly_mD035C9B5C734113127E53CE4C91A4005D48B3B90,
	IronSourceInterstitialAndroid_onInterstitialAdShowFailed_m4FEBBE144213CCCF22211767197B202B45EBCB9D,
	IronSourceInterstitialAndroid_onInterstitialAdReady_mCA02398737E86A45B3DEECE90A240269C76F3AB3,
	IronSourceInterstitialAndroid_onInterstitialAdOpened_m19CF785A56B24A721C9AD623AEAF8B00892F6E52,
	IronSourceInterstitialAndroid_onInterstitialAdClosed_m023BC6888D68F446FF4681721E5F8992AEFCCC96,
	IronSourceInterstitialAndroid_onInterstitialAdShowSucceeded_m7521B6EF0B4A49A7D1DDD2F1C9871D942BCDC5DB,
	IronSourceInterstitialAndroid_onInterstitialAdClicked_m6FB4B00694B26981EC5FBF2E527FA1395E6D0A98,
	IronSourceInterstitialAndroid_onInterstitialAdLoadFailed_mDD1BFEAC16F33540918599F85ED02FDE75F42988,
	IronSourceInterstitialAndroid_onInterstitialAdRewarded_m369797B2CAE1DE627F4D8AA6E71C5A9C460436A6,
	IronSourceInterstitialAndroid_onInterstitialAdClickedDemandOnly_m0481955C954EF1BA9490B59CB0D100D80D7CA246,
	IronSourceInterstitialAndroid_onInterstitialAdClosedDemandOnly_m5B76588151E01C87F8DCB366C6C862809C84153E,
	IronSourceInterstitialAndroid_onInterstitialAdOpenedDemandOnly_m31A2AF5B3705CEE1DB7CBB0925B52829E576C995,
	IronSourceInterstitialAndroid_onInterstitialAdReadyDemandOnly_m53A3CC1C14475BA1FB5CE9926A75AE75488E3255,
	IronSourceInterstitialAndroid_onInterstitialAdLoadFailedDemandOnly_mA40C8F70C6E54CA7E23FA1E8F38D2B05947E8FF1,
	IronSourceInterstitialAndroid_onInterstitialAdShowFailedDemandOnly_mD57969AFA8D8E169E0014DCC59E513022B59B057,
	U3CU3Ec__cctor_mE77A583537674ABC48BA7DD4E3719F71816F0D47,
	U3CU3Ec__ctor_m562D7BF420B3ADEDDCEADCA73782A047D7AE6D82,
	U3CU3Ec_U3C_ctorU3Eb__0_0_m0DB812B92882D29B0F1A6D859C5F4DB953A707EB,
	U3CU3Ec_U3C_ctorU3Eb__0_1_m77B9D57CBCFE6298E392DF70A858A9A0532455EE,
	U3CU3Ec_U3C_ctorU3Eb__0_2_m2EC460FB7659DF110A84D717DAABD7F632D5BA65,
	U3CU3Ec_U3C_ctorU3Eb__0_3_m263A1286C46BB79E20FCD599530DFA62C388E6FF,
	U3CU3Ec_U3C_ctorU3Eb__0_4_mA45436BA889438FD6495457EB3A03DEEDE8D63BA,
	U3CU3Ec_U3C_ctorU3Eb__0_5_m6AE31EFCE6A6672E2B1850D64688FDBA746C5E55,
	U3CU3Ec_U3C_ctorU3Eb__0_6_mC1EEC8BAECB2C7255A5920EF9E546C6CFCA91E0C,
	U3CU3Ec_U3C_ctorU3Eb__0_7_m07C4EA13548ADA97224CB1057607BEDAC5DCCCBD,
	U3CU3Ec_U3C_ctorU3Eb__0_8_m1655F6F1703AF3FBA92C83D54EE8D0DA500CD90E,
	U3CU3Ec_U3C_ctorU3Eb__0_9_m4485296FF3508B90B43BB66F64EA24600CA36686,
	U3CU3Ec_U3C_ctorU3Eb__0_10_mB9DBE8F5A3E9DD1BC1B0FCB0778102A996353B8C,
	U3CU3Ec_U3C_ctorU3Eb__0_11_m99065CDEA2707CDB445B5F84FA10EA139E60F0A2,
	U3CU3Ec_U3C_ctorU3Eb__0_12_m3830E0FCD4CFEB68C2EA06F0CCF0A10994DF7E08,
	U3CU3Ec_U3C_ctorU3Eb__0_13_m21A7BD3BC0FAB79B29F925D6C2FEF6866218E5CF,
	IronSourceInterstitialEvents_add_onAdReadyEvent_mAC86194F45F7E7AB77073640E4ECEFC9236E93E9,
	IronSourceInterstitialEvents_remove_onAdReadyEvent_mF838F0C283E45E752259F77D6962694CCD3D80CC,
	IronSourceInterstitialEvents_add_onAdLoadFailedEvent_m8A64B3148241AB6CE98D50C21B7E6C91DD8D7FBC,
	IronSourceInterstitialEvents_remove_onAdLoadFailedEvent_m89229DFEC32D77A5C697B48435D9268D4275C98C,
	IronSourceInterstitialEvents_add_onAdOpenedEvent_m29EBD9E72926592F221ED242B476AF1B8A55597D,
	IronSourceInterstitialEvents_remove_onAdOpenedEvent_m0C5E81F12E50E7645ADC1B68372305E1F7D7C6F3,
	IronSourceInterstitialEvents_add_onAdClosedEvent_mF3A5888FC5A45C7FA1CDB42F0F4DD7F6530D31FD,
	IronSourceInterstitialEvents_remove_onAdClosedEvent_m4808088E893C9B690B0B454704A97B6334CE80FB,
	IronSourceInterstitialEvents_add_onAdShowSucceededEvent_mB1A08240F65DBB4B50267C968D5CA5CC3E0B3490,
	IronSourceInterstitialEvents_remove_onAdShowSucceededEvent_mE053C5C2EC888A96992E68538DE2ECC7EB1E9048,
	IronSourceInterstitialEvents_add_onAdShowFailedEvent_mCB00D02E0BB50F0CDF7C3CEADD09F200593F9C04,
	IronSourceInterstitialEvents_remove_onAdShowFailedEvent_mD40AFC63586F8D09CC4E3827AA8856A19B5899DD,
	IronSourceInterstitialEvents_add_onAdClickedEvent_mD40F90BDA270CEBCD8D843AFBEBA45DB1CAA22F6,
	IronSourceInterstitialEvents_remove_onAdClickedEvent_m3FC1E797ABDFA8E1A86BF73F6CF680164152B5C8,
	IronSourceInterstitialEvents_Awake_m644207F3532D065D87013515DDAED03839CECF40,
	IronSourceInterstitialEvents_registerInterstitialEvents_mD3CB3019CA61E1AE823913A1516860C24089EF8F,
	IronSourceInterstitialEvents_getErrorFromErrorObject_mC6D1790E1EBAF37257227AD55E926BB9B3CD4511,
	IronSourceInterstitialEvents_getPlacementFromObject_mDE0ECD343EA317A05CC7562FFB951A7CBB5400D9,
	IronSourceInterstitialEvents__ctor_m634331C3B79329F8EA8BE17C65A4655D6714C3A5,
	U3CU3Ec__DisplayClass23_0__ctor_m1C094D3FA404FDA6FDDA4BDD71BE423C943381EB,
	U3CU3Ec__DisplayClass23_0_U3CregisterInterstitialEventsU3Eb__7_mD80D20F287879F11C21E62CEF90ECAAF19E220F0,
	U3CU3Ec__DisplayClass23_1__ctor_m4DA0B4DF18C51B3603DB2038209EBE09EED748DE,
	U3CU3Ec__DisplayClass23_1_U3CregisterInterstitialEventsU3Eb__8_m243C8062418FB8C074E524C9FA35808CF59C4D25,
	U3CU3Ec__DisplayClass23_2__ctor_mE3454DA88A3CAF22DBC823D105AB00B8DC14B2B0,
	U3CU3Ec__DisplayClass23_2_U3CregisterInterstitialEventsU3Eb__9_m37F77F5433F6A17B2A9D325FF363D408671A8918,
	U3CU3Ec__DisplayClass23_3__ctor_m90009FD1DCB3587D3B764148FCB1827378A053D6,
	U3CU3Ec__DisplayClass23_3_U3CregisterInterstitialEventsU3Eb__10_mD3C53BD3DB291581BC0269551EEF15968E8FA54B,
	U3CU3Ec__DisplayClass23_4__ctor_m30F241858F676299B827419E16680BFE893C609E,
	U3CU3Ec__DisplayClass23_4_U3CregisterInterstitialEventsU3Eb__11_mAA7CF3684A3BADA16C7FC974C1ECEF19048D3FAC,
	U3CU3Ec__DisplayClass23_5__ctor_mDC4333569D60555DC64584F4ED1BEF96692AD059,
	U3CU3Ec__DisplayClass23_5_U3CregisterInterstitialEventsU3Eb__12_m936336AAC063D9768BA844593CBC9D0684B0CF00,
	U3CU3Ec__DisplayClass23_6__ctor_m54112CAB4E3D1D6DB2DAF54CB7CE2370B03629E3,
	U3CU3Ec__DisplayClass23_6_U3CregisterInterstitialEventsU3Eb__13_m191F35EE6421B347FA437A2D980DA7E3EC82748C,
	U3CU3Ec__cctor_m4046FA9D5FDB2C18E21C3D2BCCC031FF50D2D39A,
	U3CU3Ec__ctor_m5876A49F87AECDD5F8EED6FACA7BBAC6C5FFC90F,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_0_mC3D86AA78AE4D55B10AA3A61F707594B5863A727,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_1_m0FFE23191644F6B8CE33B0882FCD0EBCD21BC2F5,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_2_m6EA8C05848C332C070E681AD129D2ECFCB9F9BEF,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_3_m4794066A6DA6ED97190E543B0138178BF41C8546,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_4_mE8D3266BEF8BD9C3BDDA4BC9CC09E04274C17081,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_5_m3C4876BB26B25ADADCEFCDC904660CFE5FA857EE,
	U3CU3Ec_U3CregisterInterstitialEventsU3Eb__23_6_m96B79E7ECAF568091B2E7C46EB9566866C11689B,
	IronSourceInterstitialLevelPlayAndroid__ctor_m4D66678392C50640BED08C8BFFF35B3F23D105CD,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdShowFailed_mBBB3198C565F27B74F1A62EC95C0EE4738EE808D,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdShowFailed_m0A33710EF3DF87B2079258DDB6D119BEDF51AEAD,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdLoadFailed_m8A7FE8F651E2491B2B6581F34174B1CDD52015B5,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdLoadFailed_m7F843E96A99A0FC8178AC4C867C0CBC9967874BF,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdReady_mACC99F83BC597A3B12E3DFC27262380819B9DE77,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdReady_mC6990FF33F1862E78C888202F4644E6E2795F1E7,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdOpened_m8BF1404E9A683AA999AC8B921F8EFAA3B317988B,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdOpened_mC0747F7C2FE6CAE38FAD822E1565A6F31813474F,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdClosed_m7F7CE9DAF62DB15F1A40D934D66040368B92BBC3,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdClosed_m1150737A24A7D0CD91747EB46E6FF375666232CF,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdShowSucceeded_mDB7A4AAB128DE1C8178AF9F5EAEDB828640257C2,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdShowSucceeded_m228612E1AC3E37A894DF232E7D012FC96C677F85,
	IronSourceInterstitialLevelPlayAndroid_add_OnAdClicked_mA25B563E7FEC2EEDC6052C8B48911A3CB973C0A9,
	IronSourceInterstitialLevelPlayAndroid_remove_OnAdClicked_m9446764F768D848221B8CECBD1DFA41C743CEB98,
	IronSourceInterstitialLevelPlayAndroid_onAdShowFailed_m5A0082D189A9254B5E26496D1E3B46C99C1ECEC9,
	IronSourceInterstitialLevelPlayAndroid_onAdReady_m8BAB3976328D11FE80EA7FE4625BAA0274222746,
	IronSourceInterstitialLevelPlayAndroid_onAdOpened_mBCC32BDAE741AE7CCD2804E4A58E31EBF638B567,
	IronSourceInterstitialLevelPlayAndroid_onAdClosed_mF2DCF3348B5EE6A8D32A186465D080A34D3F7BEC,
	IronSourceInterstitialLevelPlayAndroid_onAdShowSucceeded_m7812ED2F0A7C0B023E03CD57BD51B2E121FD1DB3,
	IronSourceInterstitialLevelPlayAndroid_onAdClicked_m0A78B8FF2AC090802CDB6421CD8B48A50FEC1EE3,
	IronSourceInterstitialLevelPlayAndroid_onAdLoadFailed_mE143A820508F7C0E3DD0D504558752EBD6C4C09B,
	U3CU3Ec__cctor_mBE48F3A7C83B413BBBBE59E41CC8CF9B67DB394D,
	U3CU3Ec__ctor_mBD63DC0F65F506CD1E7F4D41A6965FDB1D089B3D,
	U3CU3Ec_U3C_ctorU3Eb__0_0_m8C830AEA0A269E8A43E03F9488BF0BA0389D691A,
	U3CU3Ec_U3C_ctorU3Eb__0_1_mBEC849AA094EBF0948A1FF1A993E4C6508C1176A,
	U3CU3Ec_U3C_ctorU3Eb__0_2_m98B1AF9A48ACB9E505B83341A7E8A6B00A03BBB1,
	U3CU3Ec_U3C_ctorU3Eb__0_3_mAAE9356002E1DA43080A75929AC8B18CBDA19DFA,
	U3CU3Ec_U3C_ctorU3Eb__0_4_m7A402326D6784F6997381821D720B3B13BFE6250,
	U3CU3Ec_U3C_ctorU3Eb__0_5_m4D9C171DF4E7E19675CF282ADEE1E3DC7C29FCDC,
	U3CU3Ec_U3C_ctorU3Eb__0_6_m64684466919D66890E4D04A2F107D03934E78E93,
	IronSourceMediationSettings__ctor_mF5C76171B0F547C34638A7DF59ACFC1F2029D09F,
	IronSourceMediationSettings__cctor_m302FAD38ED27A70D3CED684383F050D1F5D0F404,
	IronSourceOfferwallAndroid__ctor_m72002BC554C2C5C28766EE83742D888DBF2771DC,
	IronSourceOfferwallAndroid_add_OnOfferwallShowFailed_m03FE762044E07CE83592A205CB6897424E9DA2D0,
	IronSourceOfferwallAndroid_remove_OnOfferwallShowFailed_m26C85AAA7B279E66BDFC6D70124F0C01D800E41A,
	IronSourceOfferwallAndroid_add_OnOfferwallOpened_m9E0870D63167DD18C1CD64F7CFC6936F646CEEC4,
	IronSourceOfferwallAndroid_remove_OnOfferwallOpened_m5B6B2CCACC2A4CD8CD43F1498C7EEB1C7F892563,
	IronSourceOfferwallAndroid_add_OnOfferwallClosed_m5F4E1278ACAA1F6DFE97A05840AA9EB8B191AD9A,
	IronSourceOfferwallAndroid_remove_OnOfferwallClosed_m07BF664AE49BD7D7D89BA7F1AB259A59549BDCD3,
	IronSourceOfferwallAndroid_add_OnGetOfferwallCreditsFailed_m757A4494C6F763B05B18FF58A9D36231C18B62D2,
	IronSourceOfferwallAndroid_remove_OnGetOfferwallCreditsFailed_m054230D0DF27A4D5976C138AF5698D62B3441A07,
	IronSourceOfferwallAndroid_add_OnOfferwallAdCredited_m01FE6E0DE70271C8567238C30675981CCE338DB5,
	IronSourceOfferwallAndroid_remove_OnOfferwallAdCredited_m9FD3EE48155C30AF9FBE37D557712FCC63A333E1,
	IronSourceOfferwallAndroid_add_OnOfferwallAvailable_m38BFF08E770E756393CBEE765DF45CC441B0CEFB,
	IronSourceOfferwallAndroid_remove_OnOfferwallAvailable_mDAC0CA382F4102EFC9B2E6C21E74F81271C416A1,
	IronSourceOfferwallAndroid_onOfferwallOpened_m2757B5FB4D9B7B8CB4C80DE47CFBE30232BA395B,
	IronSourceOfferwallAndroid_onOfferwallShowFailed_mAFA3D6A84D98E1EFACBE1DE7357D268FDBB13B11,
	IronSourceOfferwallAndroid_onOfferwallClosed_m8EF0D9A8DBE7DAAA90CB63B375581C9EBF07B70F,
	IronSourceOfferwallAndroid_onGetOfferwallCreditsFailed_m303296D3330D432E9FAA5C36DC3A06ED7229C659,
	IronSourceOfferwallAndroid_onOfferwallAdCredited_mA21486F1EF4320D352E6E429BD74EF7317DC6237,
	IronSourceOfferwallAndroid_onOfferwallAvailable_m8AF58C75266F108E22E312EC35AFD1464FCAE151,
	U3CU3Ec__cctor_m85680F1CD47C359155B0352E663DD296C789CD0A,
	U3CU3Ec__ctor_m0436F19FF1E0A004028A2D0E13972E45E1357C8D,
	U3CU3Ec_U3C_ctorU3Eb__0_0_mD962A3BB737307810FB1D3B15E0FCF912D1BA7AB,
	U3CU3Ec_U3C_ctorU3Eb__0_1_mB74A3EFC469676CE25F5FAD4462E01F1CB1CDC8F,
	U3CU3Ec_U3C_ctorU3Eb__0_2_m992C4EC04B54A3ADD596905AC05EE3F0154055D4,
	U3CU3Ec_U3C_ctorU3Eb__0_3_mC56346F18033923517A20FE60F324059C956D4FF,
	U3CU3Ec_U3C_ctorU3Eb__0_4_mE4057E933D5944B753E25B632287D4C430EA214D,
	U3CU3Ec_U3C_ctorU3Eb__0_5_m9451D49654DBD15FAE064BFA1C3BF05E747CCB3B,
	IronSourcePlacement__ctor_m3AC84E420A2753B1A248D7EA4A8C1FC290E281BC,
	IronSourcePlacement_getRewardName_m49DECEF7708B57ED1C5C402479DD8E1F21D6CB18,
	IronSourcePlacement_getRewardAmount_m872D997472B60D9E782F5D30DE02845109BBE909,
	IronSourcePlacement_getPlacementName_m747D38E22E987B023508667E0026B1DD18D88C2B,
	IronSourcePlacement_ToString_m3878087E0453B7DEEC2B5F76DFD5AEB6020B879D,
	IronSourceRewardedVideoAndroid__ctor_m4C13CF16E6F81842FE78BCEFE43FFB6D68EC186D,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdShowFailed_m5D69C3B65BD335DEF78767725B9D81EA184083BB,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdShowFailed_m91F75B5802AA26AA22C752C7F58E549C2DCF0EFA,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdOpened_m4F40C18BA6C282987F166A10C03B3063A2446524,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdOpened_mA12D2CC45278C72C2D350EFD491D37B5DF1CB464,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClosed_m48D28C5ED11FD9B750FF908F1D67B58CC16C4E77,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClosed_m27E7469CF51963AE63022673167E2531ACB90EF0,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdStarted_m7F3E073DFCE8F5F832C2E3D992D7DBAF34296AAB,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdStarted_mB159D577193C9233455288742BA185D0CAD0B972,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdEnded_m60682675F6243752A50E7CEDA7143BAA16A046F4,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdEnded_m3B966806119BD8235DC5AEDA5ABD2E0C7DCFBE7F,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdRewarded_m4DA50AE560E6C89F89764F7D59324C4FC5CF6408,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdRewarded_mE47363C6EB9F36AE9856A18355A35931212FF204,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClicked_m5D10549ABBE428C87730A85DB16C5EC7CE35801E,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClicked_m16649E5F1187B14B858166FAA7A30895E8F43A6A,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAvailabilityChanged_mDFE3E47B129F04A20D80E91FE7175E46B16B77EF,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAvailabilityChanged_m809C634F80D18BEFABE8BFB88A17911ECFABD96F,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdOpenedDemandOnlyEvent_m2EEFC641A11712D78543B5EE908B77712A88BDA9,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdOpenedDemandOnlyEvent_m54ED8CD47EFC578C82E39CF807BD59DDAE0C2635,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClosedDemandOnlyEvent_mD972879F661DD6B09E1790DC3AAB10E1D222F6CA,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClosedDemandOnlyEvent_m1BCF252206F4B9C7E78B4832B31705EFFA642327,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdLoadedDemandOnlyEvent_m4BE710C4EDC6417AF0DD47C5D4E093BD6D24DA0A,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdLoadedDemandOnlyEvent_mCDEC321183A8BA15F35A5A3529B3E6F56161B058,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdRewardedDemandOnlyEvent_mFF39EB1EF5B1FEE23ECA34DE219F0E5A90EFE872,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdRewardedDemandOnlyEvent_mC0E052E691619DCAFBDDDD44372FAF85C264995A,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdShowFailedDemandOnlyEvent_mA392AAFEEC835D33C43D81ACE32C33AE037ED450,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdShowFailedDemandOnlyEvent_mF67C9C1C104F66E13E62348483E9486361BB517A,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdClickedDemandOnlyEvent_mF79F1C444AC8E855DD6A809EA44D854E0BE7309E,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdClickedDemandOnlyEvent_m24276C68D0FD3B77B3A64582BAA4049C433B2886,
	IronSourceRewardedVideoAndroid_add_OnRewardedVideoAdLoadFailedDemandOnlyEvent_mB1B7D157BC9A436EAFFE0B2A26EB4557F1FAB7AE,
	IronSourceRewardedVideoAndroid_remove_OnRewardedVideoAdLoadFailedDemandOnlyEvent_m735D981F772166180F5B000D2118B7E964F32460,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdShowFailed_m5DC84567DC1D3E11F9B9EC2B6A79ED9FB16FC9F9,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdClosed_m6EF6013DC75CE5837D1A017F6BF897657CD6EC2D,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdOpened_m7610CFA746AE454D159862D95BB16DB316AF1868,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdStarted_mD8C6171DDA763D2DA2FFD480C305EDB5691FB822,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdEnded_m7104AEDF7EBBEC3745668EF4B2F8A3075C0EB0E7,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdRewarded_m391298E9C24ADD0B04EE31CE2DA6E05FA308B926,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdClicked_mA722FD1E1FB9B53B2987ABA51EF7B4C3BB03E0C3,
	IronSourceRewardedVideoAndroid_onRewardedVideoAvailabilityChanged_m6EAA670A7E2B9E7C0DE058A56E7B0D95320B5EDB,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdShowFailedDemandOnly_mD8B0284BC2BFD028EF0D32E31C8A78A9A8BA1D02,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdClosedDemandOnly_m28D3D91227BBDCAC33C34C1D6D36DE1B726C917A,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdOpenedDemandOnly_mDB2425B23A12DB13CB0F8FE5C0F3BBD55E26A3C6,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdRewardedDemandOnly_mDC37CBC0099149D21BDAEB0EBABC7EC1B736DEC5,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdClickedDemandOnly_mF10EA26E19274DF5934197ED32D77846370A7A64,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdLoadedDemandOnly_m35FC1B06E015FE62D1820216E652C460C01794AF,
	IronSourceRewardedVideoAndroid_onRewardedVideoAdLoadFailedDemandOnly_m4FD3E194C53AC5515C8EFC2E29BC5EA35DE099F0,
	U3CU3Ec__cctor_mCCD8EBC8601D0C59D051F7E60E258CE531CCE17D,
	U3CU3Ec__ctor_mE666C92FFF7A4E7C6BF51166C59EA41CE47EA9F3,
	U3CU3Ec_U3C_ctorU3Eb__0_0_mBA8799865C637EA9EAC7D94B08BBB37A6609BC37,
	U3CU3Ec_U3C_ctorU3Eb__0_1_mE4DD9FA895D5088E1B768AD0F4B328BA0C282935,
	U3CU3Ec_U3C_ctorU3Eb__0_2_m4A39A4F487EBF3C395DD6D118A27BB19CDCCC6B9,
	U3CU3Ec_U3C_ctorU3Eb__0_3_mF86C03DBEFC09B90B4B70F855B29748200A8E131,
	U3CU3Ec_U3C_ctorU3Eb__0_4_m9B861CBBCE83EDA8FB96FBBD9A5C660631A461E1,
	U3CU3Ec_U3C_ctorU3Eb__0_5_m301D5C467D39CEF21A768C705552AA88823B2514,
	U3CU3Ec_U3C_ctorU3Eb__0_6_m830515B3B7FF71F15B42C96F396387722D7DF659,
	U3CU3Ec_U3C_ctorU3Eb__0_7_m4F6B70A4E593C005DC8DBF3D256755CB42A50C68,
	U3CU3Ec_U3C_ctorU3Eb__0_8_m9E860F9C0830A9290F938586017165F35D12810D,
	U3CU3Ec_U3C_ctorU3Eb__0_9_m720F4239C1CDC0798311CF7889E343E5B35FD770,
	U3CU3Ec_U3C_ctorU3Eb__0_10_mC031645525FF69A8316143A91AAF164B90073140,
	U3CU3Ec_U3C_ctorU3Eb__0_11_mA1D228493161617CABE34FD3AEDEEB15891A4779,
	U3CU3Ec_U3C_ctorU3Eb__0_12_m182451A8F77D64F90217C1182E87A49014CBD4E1,
	U3CU3Ec_U3C_ctorU3Eb__0_13_mC92214983F687BE4EDCD7E49D47CAEB00BF6C1A7,
	U3CU3Ec_U3C_ctorU3Eb__0_14_mBFD3C0CEF1B566ED75D0A7045818DB6D49698845,
	IronSourceRewardedVideoEvents_add_onAdShowFailedEvent_mA41C26303D32CD8E709B8C867B9E00EAB2208B1D,
	IronSourceRewardedVideoEvents_remove_onAdShowFailedEvent_mD3986845E0AB995A20C438C641039EC07C8AD3BD,
	IronSourceRewardedVideoEvents_add_onAdOpenedEvent_m3AC8A5AB0ACA12C2B36B26DD6EE063C6F5CFD8EA,
	IronSourceRewardedVideoEvents_remove_onAdOpenedEvent_mF02B9ED2FD8927D4EFE02B3DC61E17A3A2042B23,
	IronSourceRewardedVideoEvents_add_onAdClosedEvent_mB3AE13F218C7968A07A0162A18BC9B973CF0CD83,
	IronSourceRewardedVideoEvents_remove_onAdClosedEvent_mC73B97666AEF5E177D584AD456BA1ADD6EEA4EED,
	IronSourceRewardedVideoEvents_add_onAdRewardedEvent_m61AFF38BFDB3C1AA669B9B13D8378E083B3FB9C6,
	IronSourceRewardedVideoEvents_remove_onAdRewardedEvent_mCB1E4BEB3DDECAE2679AEE800E975F9546450F82,
	IronSourceRewardedVideoEvents_add_onAdClickedEvent_m866030CA4633267C4C4A4DD9EC95C3DCB58FAF37,
	IronSourceRewardedVideoEvents_remove_onAdClickedEvent_mD0AB46993A4494505FE2FBAE663CFED85B1AAA70,
	IronSourceRewardedVideoEvents_add_onAdAvailableEvent_mAC26BC3EFF5E9198FFDD8BE4C8F2B7AE5BFEC050,
	IronSourceRewardedVideoEvents_remove_onAdAvailableEvent_m6E4A3BE3B02D6BEBA5A6F9B6A6CC8215259F6886,
	IronSourceRewardedVideoEvents_add_onAdUnavailableEvent_mCAD1B56DE43B98C955F8680CC8AAC1913B77BAE5,
	IronSourceRewardedVideoEvents_remove_onAdUnavailableEvent_m6AC19CEB3E41D300AA29190E8F4CA97A6F9732A6,
	IronSourceRewardedVideoEvents_add_onAdLoadFailedEvent_mC182ACFFA8EB75239A333B9B8F82B12DE61C3E22,
	IronSourceRewardedVideoEvents_remove_onAdLoadFailedEvent_mF5BC2BE4F2F277CE9CE28FB4E8392BA622697CE8,
	IronSourceRewardedVideoEvents_add_onAdReadyEvent_mCCF7143E062BDA79EAC09D9EAA99C9DD39F0735C,
	IronSourceRewardedVideoEvents_remove_onAdReadyEvent_mEB0025D4FA09A4D0FF32169CCCAAACC3AD0524F3,
	IronSourceRewardedVideoEvents_Awake_mFBE07F49A80846FB989C24B4767A00CEF5C0585E,
	IronSourceRewardedVideoEvents_registerRewardedVideoEvents_mB7294E7D98DB488139FE5D10304C5F767CCC52D5,
	IronSourceRewardedVideoEvents_registerRewardedVideoManualEvents_mC0A94F107A104BD804A46EA933925C850908D766,
	IronSourceRewardedVideoEvents_getErrorFromErrorObject_m4FA92B279C933CE52A84E55AFBFA66DDBC3A1725,
	IronSourceRewardedVideoEvents_getPlacementFromObject_mE8668B98C1905716A7FA560BB5DECAF997CC4F71,
	IronSourceRewardedVideoEvents__ctor_m72AE72063578914CD70F82C93380D61B3263A6DB,
	U3CU3Ec__DisplayClass30_0__ctor_mFBC21C474FD5C70F624EECE2742D25161530943F,
	U3CU3Ec__DisplayClass30_0_U3CregisterRewardedVideoEventsU3Eb__7_m4993D860C30CADB72E3B6B0C3FA8F0865D9535CC,
	U3CU3Ec__DisplayClass30_1__ctor_mF9F05729F137CB97649E519F909769C816A8FD18,
	U3CU3Ec__DisplayClass30_1_U3CregisterRewardedVideoEventsU3Eb__8_mD906726D62D250729346AF2152872F202FC76F52,
	U3CU3Ec__DisplayClass30_2__ctor_mF98CDC964494372851E3BA7C94F205249DB160DE,
	U3CU3Ec__DisplayClass30_2_U3CregisterRewardedVideoEventsU3Eb__9_mC07B532CB6D0108B3A5224BFD6F254348BAB623F,
	U3CU3Ec__DisplayClass30_3__ctor_mBA87EDBB733E66EC70799EBDB04BACC2EF554CAC,
	U3CU3Ec__DisplayClass30_3_U3CregisterRewardedVideoEventsU3Eb__10_m4BCD17AB27A84E17435AEDB46BE6FFC99340578C,
	U3CU3Ec__DisplayClass30_4__ctor_m54A9527B75311EEAF7AD05D7A2DE8D7DDB5BE114,
	U3CU3Ec__DisplayClass30_4_U3CregisterRewardedVideoEventsU3Eb__11_m555C94F46E53A19A88BC9FA8D11181D814971FC1,
	U3CU3Ec__DisplayClass30_5__ctor_m5D484C89B4DEE58F44B6BD3EA5EE0D44BC2E1BB7,
	U3CU3Ec__DisplayClass30_5_U3CregisterRewardedVideoEventsU3Eb__12_m3052B84FF31EFF9CBF35FA49919BBF8488749CC2,
	U3CU3Ec__cctor_mF840959BC4873C406C70D034B9B544C0827BF67A,
	U3CU3Ec__ctor_mC65A730F491F84DA87427301E4BDA58B8BC59961,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_0_m0CF68F591917CB6AC3670ECE19A5DA245005F513,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_1_m1625852D533C06F5C1D08D8F08AB458F43A5550C,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_2_m401F442147E41711F752F3228D9838BC21113EA7,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_3_m4490FF2A5EE7C94791FE06EB469E1C223DB49763,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_4_m4566F98878A5E8542074406EE6C222D4D103F095,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_5_m9975EC5EF129F04A72CA532D96AC4B9CC7B4CCCB,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_6_mAB95D0FBFB93426612C08FEF77D58D861D84D799,
	U3CU3Ec_U3CregisterRewardedVideoEventsU3Eb__30_13_m059C304E334985DA544011F09F9BC195F74C778B,
	U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__31_0_mB06FC738C03209B8437222F001A024EF476B210F,
	U3CU3Ec_U3CregisterRewardedVideoManualEventsU3Eb__31_1_m5768A20A5C56190B110DFA549161AE7891AE55A3,
	U3CU3Ec__DisplayClass31_0__ctor_m26B492EA621C4D0D7B9757D47C2E831F06EADB13,
	U3CU3Ec__DisplayClass31_0_U3CregisterRewardedVideoManualEventsU3Eb__2_mB1E71AFCF6A47C24B3FCFB4DA3EB726F758E6008,
	U3CU3Ec__DisplayClass31_1__ctor_mF9B6BE240C3BD9A2BDC7A83522482D9ACAC2B4E4,
	U3CU3Ec__DisplayClass31_1_U3CregisterRewardedVideoManualEventsU3Eb__3_m2162F775D13E9AAE96A66069C51019CA251D3328,
	IronSourceRewardedVideoLevelPlayAndroid__ctor_m4944F5587E85D05F0AE352D13B6E72C2F9BD0A67,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdShowFailed_m6DCE670E53544C4588C5E13F75C3B35897BFC146,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdShowFailed_m392A4FC25E4FECDAA83692FEC842EC17498C5457,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdOpened_m70CB40E109F8EC4703E05594EBD835B919CD0EDC,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdOpened_mED20E58A386E2DB5CD80C700495FAA18DD8FF6C3,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdClosed_m2F3409F05ADA3716C87CB7DE2DF81C103E43541B,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdClosed_mF68ADB59A94E526C5E10A6B4307DC731C17EAE8D,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdRewarded_mD5234D18DB5D6D23DFE4FC8C347938B4EB2E3EFC,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdRewarded_m3F1803596C45034E5024F8C91568A609F1016FCD,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdClicked_m94E06C4FD6F474FC05D67232F6D93493D70DD576,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdClicked_mDAD55BB5D4CBAFE724B893DEE513EA242EF99CBA,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdAvailable_m22C853606ABD2D9CA0EDCD12FECAB2D3FE35DAF2,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdAvailable_m04DCE733DC3B32413BD98E8D0CFE8A4893266681,
	IronSourceRewardedVideoLevelPlayAndroid_add_OnAdUnavailable_m34453045E126AB8FD5181BFEBEDE1C479D676E27,
	IronSourceRewardedVideoLevelPlayAndroid_remove_OnAdUnavailable_m1F20670A4AEE537B444C0708D6F61A7FAE39E673,
	IronSourceRewardedVideoLevelPlayAndroid_onAdShowFailed_m26208DE352679F25C6BCD0215F047FF8EE95FB51,
	IronSourceRewardedVideoLevelPlayAndroid_onAdClosed_m401E111F9691A63443EB7B146883BE722B245594,
	IronSourceRewardedVideoLevelPlayAndroid_onAdOpened_m58A71CE17959758012157891A061CE65A4060655,
	IronSourceRewardedVideoLevelPlayAndroid_onAdRewarded_mB4C161681BA049BB9371826F95CA7281C9B723E1,
	IronSourceRewardedVideoLevelPlayAndroid_onAdClicked_m19E7F85F501BAC3A2CF192D23B2E2A2D6ACC4B3B,
	IronSourceRewardedVideoLevelPlayAndroid_onAdAvailable_m757A383FD394F69B701687397238C9906BC15A4A,
	IronSourceRewardedVideoLevelPlayAndroid_onAdUnavailable_mDF65E0E9D7C7CBDB78D024C81EA9C73A4C7B0FF7,
	U3CU3Ec__cctor_m4BCA76BB846C50D6813F1772BA4F276405CC052B,
	U3CU3Ec__ctor_m8BBE574D7632BEC6DAB09ADFF43555521E0850B4,
	U3CU3Ec_U3C_ctorU3Eb__0_0_m69AC22159789B59907CFB0B88573B8603C10B761,
	U3CU3Ec_U3C_ctorU3Eb__0_1_m06F2A4262570A51F3C08DA32ED249FC6E7F5C77B,
	U3CU3Ec_U3C_ctorU3Eb__0_2_mF0EA1F99DE7C0370AD7A57EDEC117C519B8C8BC4,
	U3CU3Ec_U3C_ctorU3Eb__0_3_m59058E12B1CB7A75A9B19CC6EDF05D1013EF6BB7,
	U3CU3Ec_U3C_ctorU3Eb__0_4_mBAEC93CE1F4D4F99F341625BA5BE1305D33909F1,
	U3CU3Ec_U3C_ctorU3Eb__0_5_m4FD2E1E0777913D815432A666B7460073F724858,
	U3CU3Ec_U3C_ctorU3Eb__0_6_m91AEC199ACB71FEE5DD2772BE553123F982A26CA,
	IronSourceRewardedVideoLevelPlayManualAndroid__ctor_mC0747B473E9E6F3094E576E4519377B1B2B5692A,
	IronSourceRewardedVideoLevelPlayManualAndroid_add_OnAdLoadFailed_m4F1427B419CB4B0DEDDCD8D2BE2489E835136D70,
	IronSourceRewardedVideoLevelPlayManualAndroid_remove_OnAdLoadFailed_mE8AB4490C63358D9E4CCCCD493A91F681DA798C1,
	IronSourceRewardedVideoLevelPlayManualAndroid_add_OnAdReady_m7585E6C4786A220DC10BE5D87EB575395F566217,
	IronSourceRewardedVideoLevelPlayManualAndroid_remove_OnAdReady_m847D347AB7CEC0E1E2A98D7F3C09208AB85E933C,
	IronSourceRewardedVideoLevelPlayManualAndroid_onAdReady_m41C2B32A734F1DB2D649D6FA8150AAB5F7AA5B1C,
	IronSourceRewardedVideoLevelPlayManualAndroid_onAdLoadFailed_m3A147970BB227DE2F9EC54A03977F11B3A016D4D,
	U3CU3Ec__cctor_m26F5DB927FF83F70BABCA3B9CFDD63E0ABD929A2,
	U3CU3Ec__ctor_mF1565A6F51942F895FE0F5C1A234CCF80EE1C534,
	U3CU3Ec_U3C_ctorU3Eb__0_0_mB8A1645FF496D78636DDDE08CBD521A63604A917,
	U3CU3Ec_U3C_ctorU3Eb__0_1_mD14802B9F28998C5321B22CDB3AF4CAC93BA4E69,
	IronSourceRewardedVideoManualAndroid__ctor_m003B82F6767C29A4D86D464EC3C2D0D07DE4188A,
	IronSourceRewardedVideoManualAndroid_add_OnRewardedVideoAdLoadFailed_m474483A76AA154D599F9F668A0C7A365689FA937,
	IronSourceRewardedVideoManualAndroid_remove_OnRewardedVideoAdLoadFailed_mF7BD36FBA613FF8C93CC3554F5ECCA43A3E732D7,
	IronSourceRewardedVideoManualAndroid_add_OnRewardedVideoAdReady_mC271CED21006C50D8B577038CD2AB447F060725E,
	IronSourceRewardedVideoManualAndroid_remove_OnRewardedVideoAdReady_mE990D22B7B5325DD077DB154B1FB7E0D71B3EF05,
	IronSourceRewardedVideoManualAndroid_onRewardedVideoAdReady_mC1FCE342802ED7EE992D8A832658E948198C663B,
	IronSourceRewardedVideoManualAndroid_onRewardedVideoAdLoadFailed_m01279D5D27939276A5C4CDAD49BB7EB297DA4CD8,
	U3CU3Ec__cctor_mC20AAB18A724C971C317EC0998769D467B1DAE48,
	U3CU3Ec__ctor_mD16C8762220D52670D033BD199729F58DBB04FD1,
	U3CU3Ec_U3C_ctorU3Eb__0_0_m3FB16B8090C97782F39815DC4668D44C62981151,
	U3CU3Ec_U3C_ctorU3Eb__0_1_m2EE2DF468DA626DB0E993E6ED6B8B73695D6F86D,
	IronSourceSegment__ctor_mE574CFEC106EDDC03C569D27C4895B5EB966605D,
	IronSourceSegment_setCustom_m0C47A244F67C01230CB5BB939E1A306B14C397BB,
	IronSourceSegment_getSegmentAsDict_mC40C238CDDE9A62366ED9A5A30D912790A94A657,
	U3CU3Ec__cctor_m6163CD352D8DEFB692DF15F7F2D192A108CB73F5,
	U3CU3Ec__ctor_mD4A8B181D66382B2A89897C054802C45FB5FF632,
	U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_0_m1B8B186D0FF7EB726E8F6CB8512A685BC03CCE3E,
	U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_1_m82BCB5EA31512718DD5B28CEBB409BA9166448C8,
	U3CU3Ec_U3CgetSegmentAsDictU3Eb__10_2_m19DC4B2EA7357C8A53DB6902607A678C1F267D6E,
	IronSourceSegmentAndroid_add_OnSegmentRecieved_mDDEA7DD5A9D62E8C08F7A047B18E21697527ABBA,
	IronSourceSegmentAndroid_remove_OnSegmentRecieved_mB3AD982751ECEF1656B46CC19FD801872A7E0063,
	IronSourceSegmentAndroid__ctor_m8CF1656B06CA2F87B1486CE6EBDFBBDE41F71459,
	IronSourceSegmentAndroid_onSegmentRecieved_m19D892C8AA7166213ACE588F06AF0F93CC13AD6E,
	U3CU3Ec__cctor_m339A561A90EFE055467EB5EE2546538402D3E76C,
	U3CU3Ec__ctor_m12C62B0AE205D489D946B88B7DBA0D7E42682DD1,
	U3CU3Ec_U3C_ctorU3Eb__3_0_m1DE5600622F06D33338F909D011FCB200A918012,
	IronSourceUtils_getErrorFromErrorObject_m4BF0C110C0643B35577B6729A7535522C960EC6B,
	IronSourceUtils_getPlacementFromObject_mD2226B29CA168FC85356404C56482F25AC7EF566,
	IronSourceUtils__ctor_m1C07CA369CEFE295D8FBBC624CD41E18FD077048,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnsupportedPlatformAgent__ctor_m1954CFC5B92BDD5D22ADC3E079C0BAE2CF1BB6C7,
	UnsupportedPlatformAgent_start_m3097EDD90F06DDCD7A4D5D41215051B4BC7D1CC0,
	UnsupportedPlatformAgent_onApplicationPause_m47FC64902BA27D678E51B757FE0B4C2E67FE1CE9,
	UnsupportedPlatformAgent_setMediationSegment_m1946D551F1860B01E50AD8D27712F5E34619E4A4,
	UnsupportedPlatformAgent_getAdvertiserId_m93E929931E6F07A4D05BF316275334EA8591975C,
	UnsupportedPlatformAgent_validateIntegration_m02E893E5DC374E2BD749E9A82D0A3F3B358C990B,
	UnsupportedPlatformAgent_shouldTrackNetworkState_mC315832CD80680374BB06E815991F991CD9DF200,
	UnsupportedPlatformAgent_setDynamicUserId_m311E373ADF1E9164CB3D3130E4927817B545F439,
	UnsupportedPlatformAgent_setAdaptersDebug_m534ED1FFE98627073258FB4462ECA5B4178CC7B0,
	UnsupportedPlatformAgent_setMetaData_mC61828B357BE984F72307C3CC9692BC793B0CD70,
	UnsupportedPlatformAgent_setMetaData_m70A8CAFBA4B6D39F971C05525A450186D08B2613,
	UnsupportedPlatformAgent_getConversionValue_mBF7AF8F77BD0591AFBD2F40D61983C4640FA33C2,
	UnsupportedPlatformAgent_setManualLoadRewardedVideo_mED1E5C92E2DDE77F79CD27CD4075FA1508C0C578,
	UnsupportedPlatformAgent_setNetworkData_m80FBB82DF70F7E74FBE23840E073E8FC4A0F970B,
	UnsupportedPlatformAgent_SetPauseGame_m59F45FF0684BC8EF16B31D135C49402E8820C1DA,
	UnsupportedPlatformAgent_setUserId_mDD0958B087FC28EC598E1A156C3D44279E60B02E,
	UnsupportedPlatformAgent_init_m8894B78C226F7E506C8D43AF6BF871E19C30B0F9,
	UnsupportedPlatformAgent_init_m827520E4CC5F44B403FA0770B6DB732A14E84381,
	UnsupportedPlatformAgent_initISDemandOnly_m3CAC9FA50476B9FB02AAF7FFD1F7849729AE3D0F,
	UnsupportedPlatformAgent_loadRewardedVideo_m37246CB7A6C36943A340898B88C31962C94F47EA,
	UnsupportedPlatformAgent_showRewardedVideo_m52F4003EBD31A5A265B4607F62A09000182847F7,
	UnsupportedPlatformAgent_showRewardedVideo_m4FE0C1511AB77E6B611244E82F1B3FFF87691A67,
	UnsupportedPlatformAgent_isRewardedVideoAvailable_mD9541F3B32491BCB5B13CFDC976B67EDDCF6851E,
	UnsupportedPlatformAgent_isRewardedVideoPlacementCapped_mE05CBA278B4DA3E9CF46405F8EDAF188269D7EBE,
	UnsupportedPlatformAgent_getPlacementInfo_m4B11B182FF44D5F74CB819A07D26A5D7859EF303,
	UnsupportedPlatformAgent_setRewardedVideoServerParams_m93FD58562D93AE669B4E01EB13A492D799F819A0,
	UnsupportedPlatformAgent_clearRewardedVideoServerParams_mC706C9C87110130861D851FBD0F12BCBC274B8F1,
	UnsupportedPlatformAgent_showISDemandOnlyRewardedVideo_m5B209B423ACEDB077BEC2E36F9CA4E5D2A8A678B,
	UnsupportedPlatformAgent_loadISDemandOnlyRewardedVideo_mC1D34F2BD041A797CD391C31DE431FCE71BEB686,
	UnsupportedPlatformAgent_isISDemandOnlyRewardedVideoAvailable_mE407C75F4CBECF7BBBF96ED3E7A93916162A5E23,
	UnsupportedPlatformAgent_loadInterstitial_mEF0107BF78E0CE8BF2882E32310C837C9A728BC4,
	UnsupportedPlatformAgent_showInterstitial_m7FC068E48CDFB16027F015BBEEB7D51C5F2395E6,
	UnsupportedPlatformAgent_showInterstitial_m786089CBD50430D389F404CBC8187DF4D9421716,
	UnsupportedPlatformAgent_isInterstitialReady_m2115D114C6DE84973CA33ACAB70BD0F1CEA0F8E4,
	UnsupportedPlatformAgent_isInterstitialPlacementCapped_mE8EB1F70953BBCEA68CF0B5152C413B6F8338426,
	UnsupportedPlatformAgent_loadISDemandOnlyInterstitial_m22EAE0BE41D7F0E2EB2BE5C2A50D5E0A0A939AA4,
	UnsupportedPlatformAgent_showISDemandOnlyInterstitial_m74D8FB5E88E9C601C2B77AB2BC8261FB583823A8,
	UnsupportedPlatformAgent_isISDemandOnlyInterstitialReady_m2C847F6F9CEC2F18890D03E9FC9CFDA33122FA69,
	UnsupportedPlatformAgent_showOfferwall_m4028E72CE1EC7465FE41B6CDEECC552EE766DCC0,
	UnsupportedPlatformAgent_showOfferwall_mB0FEBDC6CC1574E69C0F7538D777F893773E7E0E,
	UnsupportedPlatformAgent_getOfferwallCredits_m4D447F02F71CDDCB5CF3CB9B783C04EEF7EA2FBA,
	UnsupportedPlatformAgent_isOfferwallAvailable_m38CEDEE28D5111170EB084AC37AE98A13E5C3D79,
	UnsupportedPlatformAgent_loadBanner_mC85FDCA0F8CF82C110EE69EB49520D7720DEDA58,
	UnsupportedPlatformAgent_loadBanner_mC17481B70077C2DFC40718AE6B8225BA8078574E,
	UnsupportedPlatformAgent_destroyBanner_mCEE250A186ED8EAAF3BA7E0EC05B0A0A6B9E26CF,
	UnsupportedPlatformAgent_displayBanner_m2D81508D492A2F1C07AA7935925820659FE2CF78,
	UnsupportedPlatformAgent_hideBanner_mEB1B66D23137D575BBD24E874EBE6153295D7065,
	UnsupportedPlatformAgent_isBannerPlacementCapped_mB4EB76D4DC15053F8B1C8321F9F640BF59458199,
	UnsupportedPlatformAgent_setSegment_m0570F63815B7DA0E6D6A0BF8626B299BB1F80A16,
	UnsupportedPlatformAgent_setConsent_mEC44C1C8759B255B214EC1DCC342D48D0AB5944A,
	UnsupportedPlatformAgent_loadConsentViewWithType_mEAF07F7B0A86AEFD3CAFF48BC0C7374906CAD6E4,
	UnsupportedPlatformAgent_showConsentViewWithType_m6891DBEDDF9665E70BE9B17172609032AE63D5A4,
	UnsupportedPlatformAgent_setAdRevenueData_m2CB71B75BABEDD60372EB30D0FB469031C0DFF97,
	AdsControl__ctor_m3B4E137BD5032EBA13A3CB658E93FEA58E2045B2,
	AdsControl_get_Instance_m67FEBB609D578763D6D8F9EC96CC6EBB35ECF815,
	IAPManager__ctor_mA8A65B484AE753D49283D3326A929A43B2EBFE1B,
	ClickedButton_Pressed_m6672B382E1A57B446E88F788B6654ABE8434EAB6,
	ClickedButton_Released_m3779E4B3058322425B9A93AC70ABBA8219F5839C,
	ClickedButton__ctor_mBDB9C0F4CEA0330F48C2015C723D12805F30B847,
	ConstantsInGame__ctor_m715D73CB85D86E949C74FDA1AA27B302A42DB92A,
	FingerBehaviour_Start_m0993FA1DF147556CD51FE572785334E2811105E1,
	FingerBehaviour_Update_m694541BA98B18AC42CFD6E999D43C86206C45C1D,
	FingerBehaviour_CheckDrawingString_m0FAF5F2AC3170BF12D26836394E1DF3ADFE194F1,
	FingerBehaviour__ctor_mFB68AFDB7291DF3559F3F8E6950F5F8369BF8AE7,
	GameManager_get_Instance_mFF43CEFAA8DD05AC28DC954832316583DB40ED37,
	GameManager_set_Instance_mE2E0260BFD7623A07453124E2A25A64430A1602C,
	GameManager_add_OnShowResults_m61B53094395BE13547639EB13CE23504C5FFF417,
	GameManager_remove_OnShowResults_mA4542D9C64F67D9AFC95061F180786427B37B812,
	GameManager_add_OnHint_mDE03FCABEE43577C3BFF90718191F46A2F3D8A68,
	GameManager_remove_OnHint_mC6F378E3E31B3E6E6A2260BD60FFAFA4FD222387,
	GameManager_add_OnUndo_mD5F8A710FF84EE19A70DF87F571A6327F69132DB,
	GameManager_remove_OnUndo_m93EC59D1CF52393C9179672049C6717C1D36F394,
	GameManager_GetCoinValue_mF276FCEA5AC789E74B6F220E022B4637457FF267,
	GameManager_GetScoreValue_mA06870D5B2290D412061C01318BF092DC193EE66,
	GameManager_get_EndGame_m70D5BFEB9B697DF71ED6196B42C86807FD3972D8,
	GameManager_set_EndGame_mD7EAC8317EF8D2AA70AA7AED040F1998ED462E2D,
	GameManager_get_BGMStatus_m2BE6B513BD8A149E10BC2C339326B8A53A2FDFF4,
	GameManager_set_BGMStatus_m36F8639FB0D8B28A2170CBF0D3DCF3E96C3B0A86,
	GameManager_get_SFXStatus_m9F985C08C88025BD8B04AB48BA61D6D8D103DAE1,
	GameManager_set_SFXStatus_m9639AEEA3960CE0466ACDFC75DD7B39DE010624D,
	GameManager_GetGameServices_mA3523E31780C918FC16A72EDDE421A1DAF1DC28B,
	GameManager_Awake_m83273263F4E53E63CCB3A71E37AF95276A0113AF,
	GameManager_OnEnable_m728AE872D7850F32C3E1587113360B91B2DBCD1D,
	GameManager_OnDisable_m35F03FB5E9826875E864428957ABD7AB33D9B5E2,
	GameManager_Instance_AdsReward_mA05EBC0D44A2558FFE6489953F93CFA6396659FA,
	GameManager_Start_mE4356C55B4DF45C9C50BD159F7BF8E9B70A8F5F0,
	GameManager_CheckSetup_m99D209DE7A976701C835979D5ECB3C18A2255D71,
	GameManager_SettingsBtn_Onclick_mB6B47D0AECF752CD5EFD662478D3AB17DEA3F6A9,
	GameManager_TurnOffCoinShop_m374F834E206F9A23D7DC10688B7CEDAA75D28641,
	GameManager_MoreGameBtn_Onclick_m061CC449908926D9BAD920A33A1593336AA4C767,
	GameManager_ShopBtn_Onclick_m51C4CAF0D447571F24F2B9BE1324518A75EB5179,
	GameManager_EmailBtn_Onclick_m8B0EA8A7B192EEBBEC201329118FD1A321DC24EB,
	GameManager_GameCenterBtn_Onclick_m2BB2D10A23C216D344907E727E96B7EBE5C5A211,
	GameManager_BGMButton_Onclick_mCFD29C7587824FDA20095FE857A2CFE5283A698A,
	GameManager_SFXButton_Onclick_m2FC3846AFBF81466D3F039B1FBD5D9A18420B2E4,
	GameManager_BackgroundButton_Onclick_m58D156DF91B1E77B0B52EAC7883739319156381D,
	GameManager_PlayButton_Onclick_mFD750494E57EB6FD758265CA96D768653D5F5D8F,
	GameManager_SpinWheelButton_OnClick_m6D573061038EE4C9FD8721E2CDA9B1E58E1F1400,
	GameManager_BackToHomeButton_Onclick_m212EC43E26D281A8E0302DA235B8533E5CD65650,
	GameManager_BackToHomeFromSetting_m1EC30011B446387EF96F565A5B43A86558339001,
	GameManager_BackToHome_mA158A9D4EA7C52DBB65F856CC4C070D4045EA681,
	GameManager_PackageButton_Onclick_mBF9C23087A577749AF46B252BC28D24A0A29C2DD,
	GameManager_BackToPackageButton_Onclick_m091DFE8337E588B206B6E6FACB2BDFEDCACE65EC,
	GameManager_StartLevel_m62C9D4EB6D8EF6D378FD5DDA38867B7685F30A8C,
	GameManager_BackToLevelButton_Onclick_mCCB981E86601F1692445C96D5BD6EE1331825B95,
	GameManager_CheckResults_m6C42D9AC462E8BAE120E453AB15DFA87885E3814,
	GameManager_Finishing_m16FF31FC9763877CE5D997674D3DF12E6B14F8CB,
	GameManager_AddMoreStars_mE2DF1703F0975D8F643FAD4B11264C904E3A5B0A,
	GameManager_PrepareToShowResult_m401FC28CD4D73A961A3589AFA5978FFE5262402E,
	GameManager_NextBtn_Onclick_mD295CBA171805780907A81DB39BC0FF7771BC348,
	GameManager_Reshuffle_mDF494C9B1F003E50DA74F87CC2122FF80773772C,
	GameManager_UndoBtn_Onclick_m861C98379C10F7FB48E06D4C9F0244ED9532A027,
	GameManager_Update_m6690E419C0D4F81E8FC91E7CCBE5C56720AC58AC,
	GameManager_ResetBtn_Onclick_m53C825D52BF7FDC06358B38FABF7548918B62598,
	GameManager_FreeBtn_Onclick_m95E700315932DEBB8749729B01745771DFA46B09,
	GameManager_AdsCountdown_mA882DC01772EBD01ECF4AAFBDF2036648E579490,
	GameManager_HintBtn_Onclick_m1F3390180FA04818F139683A827BA36D20E31FF8,
	GameManager_MoreCoin_m96D1934B1E8386F0B999A42BCE1DD3DC30144F2D,
	GameManager_LevelBtn_Onclick_m18017CC8655D87BA1DE67815811076ADFC5D1D6D,
	GameManager_ReplayBtn_Onclick_mC8D3BEC2FF973E41BDA05DA8ECB14E1607508239,
	GameManager_NextBtn2_Onclick_m9F40CF492F61CC3F542EBEECF2F2A7D1065CF252,
	GameManager_ResetGame_m0F34F856D0E4177EDDA1D6365BC5010A00BF0B4E,
	GameManager_SetCoinValue_m7BB3E359AD998B3F29031CCFF1C378F237B57B59,
	GameManager_LoadData_m11B0D99E053EB2FDC8F1BD1D59980D99F57EFB60,
	GameManager_LoadData_mCEE5E3CA29E86482A71DF7ABF0B8282A67E7FDAC,
	GameManager__ctor_mA77D6031CDC66527C11386004CF37C8DC8D33549,
	ShowResults__ctor_mC11277F074C6464789BC1E5FCAE134CE494AD19B,
	ShowResults_Invoke_m0DAB539AAC15ED81DEF361CB4B7E3D6F9202A926,
	ShowResults_BeginInvoke_m7D9294481FB6A5FED7AAEC9BE7609E0788DB731F,
	ShowResults_EndInvoke_mEA58240266B7C7A1644563D4C71D1BDF8FCA77E6,
	Hint__ctor_m4A245A2E8CDCEC35CBC57A776A17492E51F6496C,
	Hint_Invoke_m0748B11D7813F833B169C903D973FDA7B4584F9B,
	Hint_BeginInvoke_m085C03474A6081E92E9F836D14CCAF22DF46E49A,
	Hint_EndInvoke_mB2689A0C58023A0EF28DB332B649D456B050A383,
	Undo__ctor_m13025DA3077EC422DAE0C63659823957FA9D126D,
	Undo_Invoke_mBA15DE1C9CC3E86BE1ADE4B8F9BAF11E7A366EE0,
	Undo_BeginInvoke_mA1E8CDDE7ECA6F7B5CD2E806A93985FDB4015292,
	Undo_EndInvoke_m12A4BEFF722F713F6F10C95171154F408143436D,
	U3CFinishingU3Ed__128__ctor_mC186DB5CBD25F91F512FF5EE1B5FFD35852715FA,
	U3CFinishingU3Ed__128_System_IDisposable_Dispose_mB0E7522B331A366DB1108D98D16CEF82189A4897,
	U3CFinishingU3Ed__128_MoveNext_m39ADBC8A5A373644ADB2CA44ADC369CC01687B9E,
	U3CFinishingU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B82B678020C8B97C5E10B24E4E624C74862D445,
	U3CFinishingU3Ed__128_System_Collections_IEnumerator_Reset_m8C81EF676183FE0D07A35A5C12ECDB933C3BA079,
	U3CFinishingU3Ed__128_System_Collections_IEnumerator_get_Current_m5649F9632B3F95C905EDB667872694C3E8667F2D,
	U3CAdsCountdownU3Ed__137__ctor_m78FF6A5E90FEB51C9E3AA4720642CCB6FA7F44FA,
	U3CAdsCountdownU3Ed__137_System_IDisposable_Dispose_m9C52EEACC112F106B71D7D8BEAEB3C90614CEC3B,
	U3CAdsCountdownU3Ed__137_MoveNext_mA03F1428D658B5E882D81DD1758423F75D6A8926,
	U3CAdsCountdownU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD5A5A6DA1C710CE14C726987AD2C79C92D6546A,
	U3CAdsCountdownU3Ed__137_System_Collections_IEnumerator_Reset_m5D30870097FD28FFAEB0228F03ECB4111633B87B,
	U3CAdsCountdownU3Ed__137_System_Collections_IEnumerator_get_Current_m05D27619178E258D881D7D0C01A2E4545AB1FB29,
	Packages__ctor_mACC8AAF216F0A509F5E744E0D5132FE6B3A9BE91,
	PackageLevels__ctor_m89A881DBA35D076277D41144EB7273F05A7CCAB6,
	NULL,
	NULL,
	NULL,
	NULL,
	LetterScript_get_Destination_m423DEA0555A2A0EE53714AFB9345EDAD2D5335C5,
	LetterScript_set_Destination_m89A873B230BDA58EC8C26F477BA51853E98B5508,
	LetterScript_get_OffsetDistanceOnAxis_X_m31AA0CA3F4E1F980D043351AC9A7A11B0D89C3B6,
	LetterScript_set_OffsetDistanceOnAxis_X_mE070F0865855090A609E2F5E68B0E1CC8749006D,
	LetterScript_get_IsPicking_mD2560AB8A300129340858446C473A5EB4E559D5D,
	LetterScript_set_IsPicking_m7915AF8E13737AAFEBD6B991D9DF8DB8773456E7,
	LetterScript_get_IsSelected_m34378399E161B8D75CB212DA60D6363FC30A8224,
	LetterScript_set_IsSelected_m58064D7E92E9BBEC18C2DEAF0BF1B7647607F2C8,
	LetterScript_get_IsBouncing_mD313CC4E07404C4C62F9E28E37EF3EC0ECB1D987,
	LetterScript_set_IsBouncing_m88629307A8200FF30B60EFD7C45F4A0DB0E8818C,
	LetterScript_get_Undo_m8818C62CB0B1DB731BAD454A705B056DEECDE638,
	LetterScript_set_Undo_m10AC0C21C7EAAB31255E95CB53D002047A251D96,
	LetterScript_get_IsMovingHorizontal_mA05CE2B5658B02B62995165461C036B8A34F1028,
	LetterScript_get_Alphabet_m26D0CACA0A908ABFBE90136D4EB5E98A2742D151,
	LetterScript_OnEnable_m57702334F8F5014B8B1723CDE15B3120B2FEF528,
	LetterScript_CheckTheme_m0837D7858EF55AD54BBE7600CF85857B2BC47203,
	LetterScript_CheckLetterAbove_mFC62E8320221913772231CBA1B810FCF9E7B6886,
	LetterScript_OnDisable_mBB750F0B844F853759159CEE5553062180E1F0A1,
	LetterScript_Start_m6FEA155A30C117EB84A63A2B95DCB478C92FC5FF,
	LetterScript_Update_mD94E1456E8300C7F2A038CA70E824C928CBE26A1,
	LetterScript_Appear_m9EB79BE2203F6DF24049A907598715DD8A9C30DD,
	LetterScript_Set_m5EC55B30864E9BDEAF2B6D480621228CF4CE6207,
	LetterScript_HighLightLetter_m278332F59D9D87D9E057BC3D3C4EA28BD798A9F1,
	LetterScript_NormalLetter_m62A94D5AD20A6D2C91AF137BFA21458208FE5390,
	LetterScript_ChangeState_m3D0E8E3DD3857A06240D833D9657CDCE8580D8D6,
	LetterScript_CheckValidity_m2657E051302DC77A90D1B965E3214B37E1E42921,
	LetterScript_Bound_m24EBA9AB841E509C7AC25D5EEC30CD86A98FCE70,
	LetterScript_FallingDown_mB976412474CE8075DD0E42A5C20B267FE2D6A9DA,
	LetterScript__ctor_mB03A25E28FD05A455A353C180CD0B8E589444B70,
	LevelGenerator_InitEssentialArrays_m22EE088CE2CF1F05FBE68D825D46959AC2D9133C,
	LevelGenerator_InitLevel_mFBDABC7458B586E15815E01FAA94424C93351B99,
	LevelGenerator_LoadHintData_mAEB8211D46BF771D2B183C78DFD93404998833F3,
	LevelGenerator_CreateNewWordInDictionary_mC9A97EE795223AD20B29DD6F8D141F4CC19C0545,
	LevelGenerator_ShowLetters_m12807F757DCAC730B188788C9B2D99ABBEA481DD,
	LevelGenerator_GenerateLevel_mD3C2E766C0EFD0C5FA25867AA43452D82EDF1648,
	LevelGenerator_GenerateMainSection_m931F6EDC1120FE2AE8E93F08A5EDAB224B8267B7,
	LevelGenerator_SelectAlphabetForLetter_m9C12A5EC4D26072EDEF5F1226F77697063B8F0E4,
	LevelGenerator_GenerateMiniSection_m4F7DB19E3A82FFC4F442BD76AC3CE6AB823F9738,
	LevelGenerator_ArrangeMiniSection_mFF5CACB47ED7A39E702E1AA35451591EFF6A146D,
	LevelGenerator_OnEnable_m775644E8CEEB8212C660DA8C3436B6E7559BA469,
	LevelGenerator_OnDisable_mAF8FBC5DF0ECEDB8A251C54F5FBA958C0B3F81F8,
	LevelGenerator_ShowResults_m72622F93E1E924FAD528ED32AAF6215E8D953696,
	LevelGenerator_ShowHint_mA782412416965B128ABF10101888BAB34C820F87,
	LevelGenerator_Undo_m408EEEDA4832A17E9080FCA97556AB497FB9F653,
	LevelGenerator_RemoveUndoActions_m3AEA330A2A9C072FB9E583B9AC9603EFA7343B43,
	LevelGenerator_CloseLevel_m67D7B6A8F666A2A04C9431579E59E22EF5D30839,
	LevelGenerator__ctor_mDE485869E3B183CF4708A8D6A042C3933E95A441,
	LetterGroups__ctor_mB1AFF3FA6BD6604339C2A1E1AB66D2CA77BA173E,
	WordInDictionary__ctor_m1325ECAB116AE749774AF041A72A9A27F0482D45,
	UndoAction__ctor_m968C2C1E4DB73DBA0AD378BB98AB0F4EADA5361F,
	LevelScript_Start_m755921CA4933193F9C1140E0FEE93800C1B667C3,
	LevelScript_SetUp_m82E3D91FE28C799ED30A54C40E54A0818FAB39FB,
	LevelScript_PrepareToPlay_m0A7991BDCFA83B22BD0F36D37DB1268B2AF91E14,
	LevelScript_UpdateInformation_m326D6D72603BA36B0DBBE5D410E1E59BE5AC971C,
	LevelScript__ctor_m83FD9EE4B2C9F309B2A2C0E6843C0D8AB42E46A6,
	MiniLetterScript_get_Alphabet_mAF9BACA4601F5803C349D3EFF0D830748BC4E9CE,
	MiniLetterScript_set_Alphabet_mAA1DCC62EC0C7C224E92199D117E4A584E1F62D0,
	MiniLetterScript_get_IsHint_m67301755D5D1DC612E7C4B49D1DDFE5FCB3E96EC,
	MiniLetterScript_set_IsHint_m6D32C56EDD188771042F7F34159E55A92429168B,
	MiniLetterScript_OnEnable_m22550F7A2485555A4261D7FD8A564F305DB05AEB,
	MiniLetterScript_SpriteIsNull_m6E4C41717B5D572171F1FB738895B268645ABCCD,
	MiniLetterScript_SetLetterSprite_m751FBE102A08F55611D4A18A8E3F9B3771701A34,
	MiniLetterScript_PlayHintAnimation_m67F0EC107CC023262DB182EE99DBB90762E0931F,
	MiniLetterScript_PlayResultAnimation_mFD44C034C462994341ED9F56069073DF90A49B55,
	MiniLetterScript_PlayResult_m7E43C28A0E525F0EA611F3E460E4A35CB943FF97,
	MiniLetterScript__ctor_m7C3E98B46B7AF49011F9A7A8CECD6B7C2D58371A,
	Package_Start_m05456B7C9891A342275E8A2EB5FC0890060D7F60,
	Package_CheckPackage_m9C5E60D70DE3E99CB692E2C8CEC68A22DBD13D62,
	Package_OnEnable_m0A25FFE1A7A903F91A668F7457112243134348D1,
	Package_LoadData_mB6A2264733EECE58EFBD00D7FAA974D54D3C19BC,
	Package_PackageButton_Onclick_mFAA50383AD79CFB324560A29447C310F5BE7395C,
	Package__ctor_m98B90CE4A5721AF1A174AB7A21679E787C007D6A,
	LevelStatus__ctor_mDD0D80D4EE2D69B62BFACDF2C40F743F519BA891,
	LevelContent__ctor_mED5A4FD44C9E6A0A859A178996C76D318F57101E,
	SoundManager_get_Instance_mCC7EE761B79A602131C88064AA2D4AFEAD4B91F5,
	SoundManager_set_Instance_m14C1BD8449BBCB59FFAB0AD31D4A33475BE8C899,
	SoundManager_Awake_mE86193E80847AFADE46D84BD7506A19402E50CAE,
	SoundManager__ctor_m0406710AE0BEE3838B9A026C0B122017D39F7DE2,
	WordsNumberInLevel__ctor_m4612ADD9D6D7A2B88E7ADA0AF9214DDDC1A261C3,
	OrderOfLetters__ctor_m5FA667584720B51C9B411A2615150669E511626E,
	Words__ctor_m0E6B2965FD6A1B8178FE197276F26F385F6C36F2,
	Letter_get_Alphabet_mFCDE8C49726E815ECB83D1E41C2E2F188FE5EF56,
	Letter_set_Alphabet_m7D597FCFD8F39F11BC93B32E1A4A0F47CE36DA05,
	Letter__ctor_mD2B1B65CFE4B1EDE6F162048CFB6D446FD4F6DDF,
	PickerWheel_get_IsSpinning_m8566FB6996408B59818B6A59242E03349455E7E0,
	PickerWheel_Start_mF70DA1A610F8648971E2F62D10C6F7B2E13E8F9C,
	PickerWheel_SetupAudio_m6C41A2E64D8C9E236C3BAF61BD5FAB1789DD10C7,
	PickerWheel_Generate_m0A1D2E636CD26A4CDC9F9F36883A720D381B9290,
	PickerWheel_DrawPiece_mE4D45F862EC1463EEC4E548228B9977770B5E3DB,
	PickerWheel_InstantiatePiece_mED11A91F893DD308A0A85F9C6E90E39587FFF09B,
	PickerWheel_Spin_m62C45E943DCE976B38A38E1FE9E628DEEB23FF22,
	PickerWheel_FixedUpdate_mAFCE99193C86C11AE2C4713D67EEB3D515AFE462,
	PickerWheel_OnSpinStart_m58DA7AB899C52BEC398A6C67B5F368BECE58CE5C,
	PickerWheel_OnSpinEnd_m42B4A69CB04854C24996F0C32C94A7115DE97F66,
	PickerWheel_GetRandomPieceIndex_m6FB4D76132EF2AD2B7F3004AB1A318EBC20A7112,
	PickerWheel_CalculateWeightsAndIndices_m096196FE5B0031709A884BD792911A0D79973FEE,
	PickerWheel_OnValidate_m9B2518985190068EF0FD9130C0A083F869F2CBED,
	PickerWheel__ctor_mAD463207B32A3D5498D2EED4E8B3FE9AAFC32F8F,
	U3CU3Ec__DisplayClass34_0__ctor_m311E100160BFC87F2EEF7091A159F3724A58DA2A,
	U3CU3Ec__DisplayClass34_0_U3CSpinU3Eb__0_m6D318D6918290275BB1111ED6C0B15116EBB00A7,
	U3CU3Ec__DisplayClass34_0_U3CSpinU3Eb__1_m95D3C94EC381916690AF2B0D4E672EAD4003FFDD,
	WheelPiece__ctor_m5D96EFF31440009CB109207C19E886535BBF162C,
	Json_Deserialize_m8FE82986ED46C15633CE71CEA565F9436B6474A4,
	Json_Serialize_mC01A3EE8555C42F6DA02F3BFBF75C3E8F7D2342B,
	Parser__ctor_mF6E2F8B708BC8DA7BDB80DAD1DF622355BF50536,
	Parser_Parse_m550577AC93E71023378D27EA1361383BA16504CE,
	Parser_Dispose_mC88EEDDFE3CDB836F3AF49F721D3614158DED3A1,
	Parser_ParseObject_mF3509D092287719C782B830F60B511483B9E570B,
	Parser_ParseArray_m3F732244DC13726B9DB99A9A335D4E8A9B31A575,
	Parser_ParseValue_m1BDF43E4AD2A64690AAFB6DBA800472F2A94272B,
	Parser_ParseByToken_mF0830ED1965E050AE0195353B26ADB01C87E18F1,
	Parser_ParseString_m4913878D0B3878423C25644610F690615E7724C2,
	Parser_ParseNumber_mDE9009CD828B692F4E5683708FEC450BF88D465F,
	Parser_EatWhitespace_m3D91DD801FB915B39B1A5B133CB159DA4A6D2CF1,
	Parser_get_PeekChar_m631004C7D090E106B50F8CDFA62CB254B8C5553A,
	Parser_get_NextChar_m93C329D94339309A4654DF11D6C06D4D1ABBDBDF,
	Parser_get_NextWord_m0FDD048038CF96F085F527E30FB94893225C2725,
	Parser_get_NextToken_m2CF12E0515A498D7CD696E6065BB8F5401182EB4,
	Serializer__ctor_m75AAE9DA26CF48B1C8608E850915E74AC135BD0F,
	Serializer_Serialize_m074256116E009BD598CCC76D76661DFADCA49C09,
	Serializer_SerializeValue_m402A1869F9E3A33F621B305ED4C2322D6C1B16A7,
	Serializer_SerializeObject_m76665B596B2DA337CFBAFD5374CBC170E29AE684,
	Serializer_SerializeArray_mCC39C55C650C20A797385B9B3737F6DF08ECB30D,
	Serializer_SerializeString_m5FB343C30BD22AB9DE403EB8135FF8C9A58DBF4C,
	Serializer_SerializeOther_m1ADF12DA620629116FC15C5FF3E60019B8409AA0,
	MediationExtras_get_Extras_mAB9A0A22B862FF49296A158D6763B460A6B7B9E6,
	MediationExtras_set_Extras_m35AC2360D2D08879D82F2C913F063420B918316B,
	MediationExtras__ctor_mB2F2CF61A73BEA4E6FA458627EAB360A4C7A1097,
	NULL,
	NULL,
	UnityAds_SetConsentMetaData_mCC02C38C3D0E413053F907E25CCFA2444AAFD41A,
	UnityAds__ctor_m73520DF33582A597E7C17A0EA7B736598FBAA52D,
	AdColonyAppOptions_SetPrivacyFrameworkRequired_m899246793F23C79B48400B17D1E47B071F59BF50,
	AdColonyAppOptions_GetPrivacyFrameworkRequired_m8F23B81FF0339D573B74F400D04658BF34A64226,
	AdColonyAppOptions_SetPrivacyConsentString_mB8F86BF10FFAAA8B5E1D1F8718D68DF30D3E1C00,
	AdColonyAppOptions_GetPrivacyConsentString_mBF71FB1DE0E6659D8E46F03B3310B18CEEAE13C5,
	AdColonyAppOptions_SetUserId_m6DA0BB0E74DCBDECF3EB4A95EDF909CC5A6424FD,
	AdColonyAppOptions_GetUserId_mA4FCFDE8CDEC821907053D772DC3058A8EA650EC,
	AdColonyAppOptions_SetTestMode_m4FA88F46305C59AC6A1D459BC1AF286053F9C983,
	AdColonyAppOptions_IsTestMode_m05873AC4A3DAD12FBF44ED1272604A0095F4F138,
	AdColonyAppOptions__ctor_m8CB3020802C8BDFE13F020DA3152A071B9DD475A,
	UnityAdsClientFactory_CreateUnityAdsClient_mF071B0990B9C1508D15F7549471C02941B2BB385,
	UnityAdsClientFactory__ctor_m7E9388805A0BA1D0855648555EC9BD51DFD77084,
	UnityAdsClient__ctor_m04AC52C256D7370D5755468724EE6BB5FDA0F7C1,
	UnityAdsClient_get_Instance_mFF6141BD16378E611E8BAFA7C054D08AA0ABC138,
	UnityAdsClient_SetConsentMetaData_m1FEDB70441D40525079213BEDA1BC7D7593C277E,
	UnityAdsClient__cctor_m1C34659E50F98214104455B11C61E95AE6513F2D,
	DummyClient__ctor_mF3A882B7C36D888AE9E4DAF9EC6FAE625532176C,
	DummyClient_SetConsentMetaData_mF53639A6FCA9BEFCEED398984AADA0CD65409254,
	NULL,
	UnityAds_SetConsentMetaData_m01F69568A14C744AC2A596DA7471245D1F39A650,
	UnityAds__ctor_m18403C1C51C4344B6FDA1D299A9ECD4D136717C1,
	UnityAds__cctor_mE90A66BBDC7DA9F965DA77F0A4B6D77AE584A0D2,
	AdColonyAppOptionsClientFactory_getAdColonyAppOptionsInstance_m3E6F164A142C016DE03675AA6DDC03815F1B7251,
	AdColonyAppOptionsClientFactory__ctor_m893B6AE174DA6EAC2771CEE21EE6AD32BDFCDFA7,
	AdColonyAppOptionsClient__ctor_mAB8C747F85A2778B5E13525BBEF172B98DAAFAF6,
	AdColonyAppOptionsClient_get_Instance_mD38CC038DEE25F3CDFC97B9AE006609050722DE6,
	AdColonyAppOptionsClient_SetPrivacyFrameworkRequired_m68E50A0535D274E76A8F29451660FAC595F5236C,
	AdColonyAppOptionsClient_GetPrivacyFrameworkRequired_mEC6847DA1A7946AFBFD611F60DBC625A56CDC54A,
	AdColonyAppOptionsClient_SetPrivacyConsentString_mB4A2C05636036BA640984FA38B62467DCD04A2AF,
	AdColonyAppOptionsClient_GetPrivacyConsentString_m5BDF4DCC1B8D5B7B774A2C075B955278260A5E8D,
	AdColonyAppOptionsClient_SetUserId_m8DFD4E97EE468F0A21E117EA4051B8D0BEF8EA76,
	AdColonyAppOptionsClient_GetUserId_mAA3059B8D0E55C5F57934EC8EFA8670070422062,
	AdColonyAppOptionsClient_SetTestMode_mECD4634B7EB6BFD4BD819E6B7BD4FBAF076F8ED7,
	AdColonyAppOptionsClient_IsTestMode_m78A18C36D69660DFF3EB2D577570A5DD8C320C7D,
	AdColonyAppOptionsClient_GetAdColonyPrivacyFrameworkString_mAE46A6D0AE0959E1884A79F0A9A96476DDE035B7,
	AdColonyAppOptionsClient__cctor_mAD13D6DA6D40FD1B0A04E43FF2E4BEE1CF955F99,
	DummyClient__ctor_m0BA06464ADC9CCBE47A6FAECBDEF81AB9BF2E976,
	DummyClient_SetPrivacyFrameworkRequired_mBA78A98F352B41396F96DD9C399FA95D0A61B4DB,
	DummyClient_GetPrivacyFrameworkRequired_m4EE050EEE6C8F8ED11D8D5C740C18A915C7A2356,
	DummyClient_SetPrivacyConsentString_mF59C5AEE0CA310A0AAF0815FB2D1453194A18053,
	DummyClient_GetPrivacyConsentString_mBA8D20D00910501657CBA0DA55B1BC95E0D19BCE,
	DummyClient_SetUserId_mAABCF8BEFDDE79D228A33CC292D9907295DFBFC2,
	DummyClient_GetUserId_mFF9ACE865165A5C49173BAFB02DB61B715BB2721,
	DummyClient_SetTestMode_m64C9904859BF57BA58181739D20499FABED129B9,
	DummyClient_IsTestMode_mFD4AA5547F59B6DFF81F55999FA3F40A42CDE92F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AdColonyAppOptions_GetAdColonyAppOptionsClient_m7CF66B5EE6EEFE87EC9D3588F59B572E1CCB1AB2,
	AdColonyAppOptions_SetPrivacyFrameworkRequired_m6B5427F457EE53E5659FE59BAFBB0DC689EF7523,
	AdColonyAppOptions_GetPrivacyFrameworkRequired_m4144D0D0C3430B26F08C51570ACAC7A5E4257A4C,
	AdColonyAppOptions_SetPrivacyConsentString_mD16CBD1F1215A95910DD58DE32FB58498F0BB145,
	AdColonyAppOptions_GetPrivacyConsentString_mB4B226537F135BE3738636A3DFB3E061E99B7F15,
	AdColonyAppOptions_SetUserId_mB8E9F286CC58CE277B15606CF6AD7D2CF3F8DA6F,
	AdColonyAppOptions_GetUserId_m06CFFB4C0CD4D4CFEFCAB6F448FF48F1D4F45620,
	AdColonyAppOptions_SetTestMode_mC89F24D793F99C60ADF68868B913D6DBC10A7592,
	AdColonyAppOptions_IsTestMode_m4E51DA965BC326B36668E37EB286E4AE65C7E301,
	AdColonyAppOptions__ctor_m73398EDF508F5385800D8F8FED39FDF642E365A3,
	AdColonyAppOptions__cctor_m3D42793B4281B8FBBD10752406EDB77AC704456F,
	AdColonyMediationExtras__ctor_m359AD4F47D86CDFCEB3C8C1CF91B18E2E4E0277B,
	AdColonyMediationExtras_get_AndroidMediationExtraBuilderClassName_m1A5973D015E1E620A4184D3428EFD42756AB95D8,
	AdColonyMediationExtras_get_IOSMediationExtraBuilderClassName_m1693436157396C5DD5A4E375A2DF126EAF8BCCDD,
	AdColonyMediationExtras_SetShowPrePopup_mDB6769E78764E96E668A9607AF6B870DEB2946C9,
	AdColonyMediationExtras_SetShowPostPopup_m361B18B90B76AE9014C930E30E93F420F055BC14,
	JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3,
	JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3,
	JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B,
	JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF,
	JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D,
	JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F,
	JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC,
	JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25,
	JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99,
	JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71,
	JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA,
	JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1,
	JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482,
	JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5,
	JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1,
	JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2,
	JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7,
	JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A,
	JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6,
	JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C,
	JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752,
	JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76,
	JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F,
	JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD,
	JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3,
	JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700,
	JSONNode_op_Implicit_m9105117784C6916F6B646D36A1370FB58A9C6762,
	JSONNode_op_Implicit_m8DDFBC3FCEA2F2087B7A132A483F724B1529B183,
	JSONNode_op_Equality_mF53AB65ABCF70E4C7D035DF059648FED12577634,
	JSONNode_op_Inequality_m31F67DC83671EE7A334E8A1A0445AC08CFDD9BD5,
	JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE,
	JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5,
	JSONNode_Escape_m56A8C20A29A7EE378C0A55BB6CD46467F0347950,
	JSONNode_Parse_mD6D2A7EBFFEDF36552C27F27CB53F9DBA6C9D12E,
	JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F,
	JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27,
	JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1,
	JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71,
	JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B,
	JSONNode_Deserialize_m5C0519240162A7247E99053C69876E590587B38C,
	JSONNode_LoadFromCompressedFile_mEB55B68104BA51A066485FBBC1CB6A1D16DAA504,
	JSONNode_LoadFromCompressedStream_m7368A19FFACEDA51ED6E39DE7919FDF11836F030,
	JSONNode_LoadFromCompressedBase64_mE4C615EAEC9DD685E6659BC69C77CDF7A6FE1CFB,
	JSONNode_LoadFromStream_mD78A68A7F71EE78FB1E12311DBFFFAA3E6AF8B69,
	JSONNode_LoadFromBase64_m33C55652F013772F68C8CA8B54CEE7EBE297B08C,
	JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA,
	U3Cget_ChildsU3Ed__17__ctor_mF797A836A6E3340B1BAAAF50AEEC10DFA89DAE73,
	U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m94877C8D1DA5BE6EA246389F10F4C9C7AF366A74,
	U3Cget_ChildsU3Ed__17_MoveNext_mE6F86033D5481E4036487E92E0F64B9840CB794B,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m54B690F8FA694EE02627B97D48386D9095E229E4,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m0366D0A88F5A1FBA81AE3F732F855C0CAE045788,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_m38B1B4A7F33CF7834BB44D72C8FED6220D1C75BD,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m071E822DD6D0A22882CDAA62B63D9660FF1776C0,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mA892F96D61A7E4D40CBA9B2CF48F34079F726849,
	U3Cget_DeepChildsU3Ed__19__ctor_m046016CC55A4C7B9C1FE5AE10DD72E4050826BAC,
	U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mDCF0787A78165575446D46E5C1CD1A8A10FF7018,
	U3Cget_DeepChildsU3Ed__19_MoveNext_m9F51E57FD6F93E690C029855DAA59D591E773022,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m44FF129641F4B9A288B22CC405CFDB01813AF413,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m50CD9A7591C11FE67D4AE24614BD3B979B030C7F,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB46DB5D77ECC4B15D6B4BA4ED0A03F21CE95D25D,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_m2862E593874F11BCC026302EFC96AFDE00AB5A06,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mABF1413C57FA5A3C1651355AB4A5E4C0402E27DA,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mA1AC5268F717D42575E2B41A69ED09A0C60071B0,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m481F9219BE5FB9B09E8DFC02714ED34393641102,
	JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503,
	JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5,
	JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454,
	JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE,
	JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B,
	JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B,
	JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF,
	JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A,
	JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874,
	JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635,
	JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB,
	JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B,
	JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF,
	JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E,
	U3Cget_ChildsU3Ed__13__ctor_m6474546C7E49FF6A548EFAB592D82CD4C52A3E2B,
	U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_mE5E9FC725A9C7CB1A97AE4BCF844F2F773000283,
	U3Cget_ChildsU3Ed__13_MoveNext_m93338AB8CC1520C49B28F7BA37D0216F8DFE3894,
	U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_mC2A2DBA3A3B18AB12F36800AD2129AF9CE3B4865,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_m939A44A15A892D3328145314D6F3BD3180E600F0,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m9BBCABEA39F9133BC5E7188E8E6346538077025F,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_mAAA548D9FF3C67A7AEF373A19FFAB5C9C3A97C5E,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_m3C0A4675A2FFA0A86616539694B4392F7A86B4D6,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_m208AD3C417C7CAABDDEABD4EE49670B1E6403926,
	U3CGetEnumeratorU3Ed__14__ctor_m0695479939904B620F4C38973708FCF84941755E,
	U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_mDCC768ED829C36509C1AE08EC89EDA457099995C,
	U3CGetEnumeratorU3Ed__14_MoveNext_mE03BAD590A4A72ADD88274F87F144FA61C677CD2,
	U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m4F72145277B7B879E05365A241A55E1FA2DCC478,
	U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC44ED8F1DA1967025AB91E813D3F5E8CAB273B4A,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m5FF5CA385FB75BA70CEF94B6FAE71C700ABA5A6A,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_m4BE072CF6B24FCA546848441F442FA00F8049498,
	JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93,
	JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E,
	JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551,
	JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71,
	JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C,
	JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38,
	JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680,
	JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680,
	JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877,
	JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925,
	JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD,
	JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37,
	JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027,
	JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8,
	JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446,
	U3CU3Ec__DisplayClass12_0__ctor_m54EAB9A9ED470825466B363E8EF656B51E99F746,
	U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_m919E74F0EFF672D2C2B58FE1D21F705C7A3DD3B8,
	U3Cget_ChildsU3Ed__14__ctor_m2A7CD01CA103381FBF4FB8D70B1FD26083894793,
	U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_m31D2FA345366CAF0D63CB243DCBE67745AA52783,
	U3Cget_ChildsU3Ed__14_MoveNext_m3C0CFA00C04821A51C6A3C0A5808DFDE9ECE5A5E,
	U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m9FA71F2E785602FDD64125BD5BE3FCE3561532D7,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3Ccom_adjust_sdk_JSONNodeU3E_get_Current_mB0226A6E5AC71AEBA38F19684C7758964A705388,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m65E72C29D884A8DEDBBAAD79C3035EFB6A106ACB,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m78FA739F5BE9ACDFE18DD77640515DA4B2BF4576,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3Ccom_adjust_sdk_JSONNodeU3E_GetEnumerator_mF561773E4A39039E3DAFEE4082F0FF2E39DE7F2C,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m80BE9E5871675B635E6737823E81E269BDFF7359,
	U3CGetEnumeratorU3Ed__15__ctor_m71795B1B109474BBF1C793AE265C8218EE46A317,
	U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_m92BA0C0FE94BF8EA461E8D2985DCADF217B29B3A,
	U3CGetEnumeratorU3Ed__15_MoveNext_mB78E9C334CD336419DCE6EAFEF6FFE51FD51947E,
	U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_m909727106AE5FE5AA12C727A654659A639A11F9F,
	U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D7E2674085894FC3011FFCE567CFA21DA8C30D0,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m9877E5F955C30C1CE2C2FEB43B2608137528BCE4,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_mDB2FE16291B3B81162572DC31EE732BA957BD162,
	JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425,
	JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2,
	JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5,
	JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2,
	JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE,
	JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350,
	JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F,
	JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580,
	JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD,
	JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91,
	JSONLazyCreator__ctor_mF3FA6877067F4F9086006389F0910D541AA27059,
	JSONLazyCreator__ctor_m8C48C27CBB58E5F6990AFCA554803B7E191A3D46,
	JSONLazyCreator_Set_m9FCCD3D9059234381ACD61296F3B33ABB5392630,
	JSONLazyCreator_get_Item_m8A8A9C17EDC287BFF118DDED313F4107F4072A22,
	JSONLazyCreator_set_Item_m19485C7273CDACCE9F7B1834D56FB74E19777E12,
	JSONLazyCreator_get_Item_mF8E012F6005145846304C594EACF1584CA5DDA9A,
	JSONLazyCreator_set_Item_m33B036AAA008F596ADB3F455D2021D1EF025B49A,
	JSONLazyCreator_Add_m59C51EC772277124A17D87DA5C74707E3EFD6AFB,
	JSONLazyCreator_Add_mBACDDB1A14FCE6CEA2674C9B4EDA816F21DF9E29,
	JSONLazyCreator_op_Equality_m9A8BAD40A9286520054534AE702678831750A10B,
	JSONLazyCreator_op_Inequality_mDFBBDA95E69B946B8DD0C0807083A0A34746A7C4,
	JSONLazyCreator_Equals_m586E6D9E990915F11F72E099E203D60723BEBC48,
	JSONLazyCreator_GetHashCode_m9CC90F8845B9E57BB6913BF83FB3C5D89FA61A35,
	JSONLazyCreator_ToString_m49AE6E6F07C738C9FB3F7ABC86EBBA8C0828996C,
	JSONLazyCreator_ToString_mD794E0057AC95367420ED3D100817F019BD77A91,
	JSONLazyCreator_get_AsInt_m3D1F120A6A54087B34B638943D2168F10CF58BA8,
	JSONLazyCreator_set_AsInt_m78FF9DC114F9731546230F760A5048069378B32F,
	JSONLazyCreator_get_AsFloat_m1BF100B3129E3B67B00EB12BAB1C599198E60D87,
	JSONLazyCreator_set_AsFloat_mE15D7CE3B2B38E9E5EDC079D339A8AB5FAF674D6,
	JSONLazyCreator_get_AsDouble_m40DD7A463621DD10526570D5430173F7AE7771B5,
	JSONLazyCreator_set_AsDouble_m275BB96935FAE5F2CD8A8328EAC9FE887B5A3377,
	JSONLazyCreator_get_AsBool_mAF53CC7D86CFCE9E85413B126CFD79470BCD6B30,
	JSONLazyCreator_set_AsBool_m05997EDED19E2297BFE7D45A1062CB7CA52F1ED8,
	JSONLazyCreator_get_AsArray_mAEBD138BCA37CA30F180A221CC7872FE240BFA62,
	JSONLazyCreator_get_AsObject_m52BB6F39E712E7E3ED988842537FEFF804B543C7,
	JSON_Parse_m64D44E2E2DCDC73C4FC5A08B5D13F92209F2482E,
	AdjustAndroid_Start_m1D39F038C9A56C36B88B6D1145B9F69E01C7E7C7,
	AdjustAndroid_TrackEvent_m1ACD3DE44F09700192A24C5A326E73E76673C9F2,
	AdjustAndroid_IsEnabled_m0F99D443225B532431D92E5BDDC011DFDC7AD3DF,
	AdjustAndroid_SetEnabled_m804BC54C758D85422C2D408FAFBF0D53B746423C,
	AdjustAndroid_SetOfflineMode_mAB3976F1FF8690D22BFF4A6ADEAAE8C47524B7A7,
	AdjustAndroid_SendFirstPackages_mC6C37B30223758423D5557CF2FA8674801D326CC,
	AdjustAndroid_SetDeviceToken_mF30818E7C4F0F5DD856C9E35394E9FB9ADD87E20,
	AdjustAndroid_GetAdid_m0525482D8E3F1167CD14B2A12D2E2D7E9FBD8AA5,
	AdjustAndroid_GdprForgetMe_mD1AECB214CA8BFB9783A5E3F70F5E80914F08437,
	AdjustAndroid_DisableThirdPartySharing_m55E805701C85CBBA00F7580AAE75967EA31D9061,
	AdjustAndroid_GetAttribution_mD7681342B5DDF0B2A2C9BCE18327E583189CAF9B,
	AdjustAndroid_AddSessionPartnerParameter_mFAB6A72387CDDE7C9212D10D49CF488FB26185E6,
	AdjustAndroid_AddSessionCallbackParameter_m3886354DCD87FDCFFAFA69A19C4E97E59D31C824,
	AdjustAndroid_RemoveSessionPartnerParameter_mE310C1452E7AA2779CEA5B6C8B70410104D87966,
	AdjustAndroid_RemoveSessionCallbackParameter_m7D835C9CD309FAB5C5F48ECC068EFD813B90B349,
	AdjustAndroid_ResetSessionPartnerParameters_m85FA68D4FA76A036E67BDA3371169BEFBAD46CC1,
	AdjustAndroid_ResetSessionCallbackParameters_m2319AAC0995822B275A8A731F88866F960BCF09A,
	AdjustAndroid_AppWillOpenUrl_mE323FAC42F2AE9B649BFA44CC7EC253361733AD7,
	AdjustAndroid_TrackAdRevenue_mE1FF768B0E1885910BEF4C4CB0316852B0774881,
	AdjustAndroid_TrackAdRevenue_m17E49555E6663E2C1FDB7723ED2ACF8AF7BB2523,
	AdjustAndroid_TrackPlayStoreSubscription_m0D0FD86FD0258755FB8555589C707BE28406B7BE,
	AdjustAndroid_TrackThirdPartySharing_m2F3BB31D3B60EE3D70D07494921949F090837613,
	AdjustAndroid_TrackMeasurementConsent_m9A2C735602AB9605791360DC02AFDC569CEC7672,
	AdjustAndroid_OnPause_mEAB3D744D7FA078BE7ABB84342EEA9FCAE7A32B5,
	AdjustAndroid_OnResume_m6E0115176F1BF8238B46D009A2DEC66D1DDA1C97,
	AdjustAndroid_SetReferrer_m595DF146979F63B8867E574DC6FCB4F4B9695DB0,
	AdjustAndroid_GetGoogleAdId_mDA64F7291B15B14F9B91E8812282463A73A02A40,
	AdjustAndroid_GetAmazonAdId_m7B737E67E9B95631CDBD4436109243F1AD78C5DA,
	AdjustAndroid_GetSdkVersion_m543983FDFEFD66BB6CCA9C9B5FD60799547CDA3E,
	AdjustAndroid_VerifyPlayStorePurchase_m9F1EE974114C4562DF39A8DCF93EFAB894B2A3A6,
	AdjustAndroid_SetTestOptions_m320168C98A3232EE9383FAC4A22BD11193367CC1,
	AdjustAndroid_IsAppSecretSet_m95C969EBDF9C163B3E3D665A8CD34481B4DB283D,
	AdjustAndroid__ctor_m0035BEC1E1826590313CEAF840550DF637DF1268,
	AdjustAndroid__cctor_mA53D2A12B2657A0A62B72B55988B4EAA3E7A1C1A,
	AttributionChangeListener__ctor_mA6205D1D3A3CBA8F7FD10B413B0FD68FFFA548B8,
	AttributionChangeListener_onAttributionChanged_m8EEEC19136B64A9315CCB266EF17028994504EB8,
	DeferredDeeplinkListener__ctor_m9AFF50C9019C550FE14B914675EE675B36EE0582,
	DeferredDeeplinkListener_launchReceivedDeeplink_mE6D35176E2C2AADC10C881FF8F3927AB528C5F3B,
	EventTrackingSucceededListener__ctor_m2A4ED2670A9A651245F8D5E344BDBFB5B3E3D43D,
	EventTrackingSucceededListener_onFinishedEventTrackingSucceeded_m202DFEA1323BD584F04E02C0A8323870A1959460,
	EventTrackingFailedListener__ctor_m7D5E6FA8F17AAC33B50DFC25A1ACA5183FCE6545,
	EventTrackingFailedListener_onFinishedEventTrackingFailed_m4FB36E7E27EF57535C67940BC6F148293CF366C0,
	SessionTrackingSucceededListener__ctor_mD524E8A2ACB053F1100FC8BD428D97E256EEAAF1,
	SessionTrackingSucceededListener_onFinishedSessionTrackingSucceeded_m641A6B81E4C67260BB84A35FC9803D11AA33102A,
	SessionTrackingFailedListener__ctor_mEC4AC95C995754842BC483D6D7AA0381A508BA6B,
	SessionTrackingFailedListener_onFinishedSessionTrackingFailed_m2494A8E23D515011E6014DEB2630F9F3A88F8D69,
	DeviceIdsReadListener__ctor_mDF6EF7D21335FCB0B244E975C68B5A7CC8F1E39D,
	DeviceIdsReadListener_onGoogleAdIdRead_m82DB269D37125FB1A6A3667454CF5B408AC179DD,
	DeviceIdsReadListener_onGoogleAdIdRead_m46B72AE61C8F5B9408B0FA1374F0D34557C6D16A,
	VerificationInfoListener__ctor_m6008AE03F8DF0F524F2FCBD37BACAAB1A9656F8C,
	VerificationInfoListener_onVerificationFinished_m172D0806BF4776A2B565AF3870F52F107BF1E94E,
	Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8,
	Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012,
	Adjust_start_mF24352A04B12F9A3D5314851E1F476DD4BCBF0E3,
	Adjust_trackEvent_m788CBA9B9C606FE179B1582368C6A8A171425E36,
	Adjust_setEnabled_mEF3633C6A6BBC4F91FCDB4F934A281DDE96712F1,
	Adjust_isEnabled_m254951D8A14448BE51FD7527AC80CC3950E6EE4B,
	Adjust_setOfflineMode_mDA3666A20F780FFDD8BCC07F9DB4D3215823C360,
	Adjust_setDeviceToken_mC1407160399AC998FF11E556CBFA3B38950B67A8,
	Adjust_gdprForgetMe_m7A0CFD6A9B4137418F35E5AA201B6410737359C0,
	Adjust_disableThirdPartySharing_m302224B75CA744974396F66A08AA1FE587873BE5,
	Adjust_appWillOpenUrl_m1E13932CE37598AA3C42C4509D2323491569A6F7,
	Adjust_sendFirstPackages_mCFE8665FB15B08EC04CB5BCF14C2923E5189883F,
	Adjust_addSessionPartnerParameter_m4AC3D2786FFF94A176E93DEBF049FCC6BB71B3E6,
	Adjust_addSessionCallbackParameter_mCCA2594D1EAADD151C56F4537823EBBFE3EA645F,
	Adjust_removeSessionPartnerParameter_mA50E1FF8D276CA300DFBC2D8C52E1C60194BD98C,
	Adjust_removeSessionCallbackParameter_m9D480CA9958869ACA6205BCA9CE84D235B4E45B1,
	Adjust_resetSessionPartnerParameters_m788D347452CE9472C4B4BFCB581272C33A903459,
	Adjust_resetSessionCallbackParameters_m092D40CDF59B8BE4FB165BB105F914E67E61D7B4,
	Adjust_trackAdRevenue_mE7017F85963C48E260AE4A390B6E03C1365CEC8F,
	Adjust_trackAdRevenue_m7F1739F079028F6AFA4F42629B803A152F0BB9AE,
	Adjust_trackAppStoreSubscription_m3BE586B120A00A43CD00D754363EE0548488EB5B,
	Adjust_trackPlayStoreSubscription_mB28584B6B95978DD627EF386DE383E258A55C394,
	Adjust_trackThirdPartySharing_mDBB4F949AA7D3532188DB67890C38CF006E5C3AE,
	Adjust_trackMeasurementConsent_m28091C4476B42F91B98E2864860109C8B2FF7F3C,
	Adjust_requestTrackingAuthorizationWithCompletionHandler_m0F4258A1D04183560AE8A0DB926BD1DD063330D2,
	Adjust_updateConversionValue_mB83B123737964C3CDD68E5B94588575C973EEADF,
	Adjust_updateConversionValue_m22B9DD112274E04D0CE5267196B8FC850FF32BF0,
	Adjust_updateConversionValue_m7619FC95F34018A0C8B115B114417776FB016C2A,
	Adjust_checkForNewAttStatus_mE8CA001423E1EFFB7D610102747D76F5B2DE02AF,
	Adjust_getAppTrackingAuthorizationStatus_m2CD6E53C3C56055BEF58BCA99C30417FE4231006,
	Adjust_getAdid_m2285DFA62339E5ED400D271E8E661FD0174600A6,
	Adjust_getAttribution_m3B7BBB4900BDCC0B92D6A103FE178D9E808F2188,
	Adjust_getWinAdid_m528B95DD3250ED9D5BCAB2B58C3B8CD7F6B940F6,
	Adjust_getIdfa_mB068686DAF448330241C2367A2767891EADA60CB,
	Adjust_getSdkVersion_m8F7D96C6A76363F65E34D362C4912D3D5DA34E7D,
	Adjust_setReferrer_m223FB53F552416333FC5B55D1E6F3D5DD5F9270B,
	Adjust_getGoogleAdId_m9F83C2BEA8B17987BC6BD492C1593444D427CB06,
	Adjust_getAmazonAdId_mCF6657242C0F74B3D50BD6C412C91311CFE96688,
	Adjust_getLastDeeplink_mF3BC9E789AEB1C8FB8C54BEDF98F3D04049D2D66,
	Adjust_verifyAppStorePurchase_mE3B63AAEDB2569792D5167200AEFE589E69AE2FC,
	Adjust_verifyPlayStorePurchase_m769B3D59149BABD106127A3D5E9C84F67CFA3865,
	Adjust_IsEditor_m01CB66E062E93F971F53080A97634DFA78C2A184,
	Adjust_SetTestOptions_m5313B20E29B0029F24CF33ECE44DC008CDD6F360,
	Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B,
	AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8,
	AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7,
	AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115,
	AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25,
	AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856,
	AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B,
	AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C,
	AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3,
	AdjustAppStorePurchase__ctor_mC1D24DD9ABED88BF92A169ABE33573DAB8FFF404,
	AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536,
	AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C,
	AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B,
	AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70,
	AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019,
	AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159,
	AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6,
	AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3,
	AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20,
	AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416,
	AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB,
	AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750,
	AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9,
	AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36,
	AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183,
	AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D,
	AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE,
	AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52,
	AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876,
	AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A,
	AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F,
	AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6,
	AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0,
	AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182,
	AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647,
	AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999,
	AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51,
	AdjustAttribution_get_fbInstallReferrer_m730BCBF4BD7687B6ABA49F85E1E3592944782A68,
	AdjustAttribution_set_fbInstallReferrer_m03CE43EE59FB3D653CB09AB9BD1DE86EE11D292D,
	AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307,
	AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A,
	AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9,
	AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395,
	AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7,
	AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F,
	AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724,
	AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF,
	AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F,
	AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317,
	AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982,
	AdjustConfig_setCoppaCompliantEnabled_m43149C9F256F85E6149011100CEC777326B818DF,
	AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD,
	AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6,
	AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D,
	AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA,
	AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263,
	AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE,
	AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750,
	AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574,
	AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054,
	AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E,
	AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00,
	AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE,
	AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919,
	AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73,
	AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED,
	AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1,
	AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC,
	AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66,
	AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C,
	AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1,
	AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA,
	AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E,
	AdjustConfig_setLinkMeEnabled_mC3B85AB4A602F3BB59B8B4B7FA973D9F2B8EB55E,
	AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4,
	AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23,
	AdjustConfig_setSkad4ConversionValueUpdatedDelegate_mBDC7D976A8BD22E4680DA70B9EC5EE7ECC4A45E4,
	AdjustConfig_getSkad4ConversionValueUpdatedDelegate_m791D25B57223A51EDE3A28E916F1A6AB43EC2FFF,
	AdjustConfig_setAttConsentWaitingInterval_m207E02DC9D926C771965BB2270A18A914B1B1DA3,
	AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53,
	AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C,
	AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA,
	AdjustConfig_setPreinstallFilePath_mF70F4E2F50F2E73E7EAF1DEAB6351F6AB6EB728A,
	AdjustConfig_setPlayStoreKidsAppEnabled_m6786F76DFEE24836BA51A2FA1B798FB2AEA86484,
	AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C,
	AdjustEnvironmentExtension_ToLowercaseString_mAEDC5B0CBA386D07FB258ED1BDFC83CB4394D49B,
	AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03,
	AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49,
	AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC,
	AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B,
	AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98,
	AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254,
	AdjustEvent_setProductId_mA5A287AD1813C5E813F12A097C4587AD23B2C8AA,
	AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD,
	AdjustEvent_setReceipt_m44238E44C88E2249BF57CB4775FF867EAEA76497,
	AdjustEvent_setPurchaseToken_m921AABC8958704EE95BB091E047E4DF47E4E4F3F,
	AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9,
	AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC,
	AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C,
	AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472,
	AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C,
	AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F,
	AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F,
	AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E,
	AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F,
	AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD,
	AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138,
	AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9,
	AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A,
	AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE,
	AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827,
	AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14,
	AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284,
	AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A,
	AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD,
	AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5,
	AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD,
	AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2,
	AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F,
	AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10,
	AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B,
	AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E,
	AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64,
	AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983,
	AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9,
	AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75,
	AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90,
	AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C,
	AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B,
	AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75,
	AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C,
	AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B,
	AdjustLogLevelExtension_ToLowercaseString_mEF9C47460E6774026C495F7646A4369476C53588,
	AdjustLogLevelExtension_ToUppercaseString_m457BEEAE7375DBA0C92F1180B69A432CE360A133,
	AdjustPlayStorePurchase__ctor_m7B9ABD9ED2899FDF05BDF64D3E52047C489915F2,
	AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1,
	AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4,
	AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4,
	AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4,
	AdjustPurchaseVerificationInfo_get_code_m8FB046D3E105D4581BDA3A4C60BD5FE7FE0608BF,
	AdjustPurchaseVerificationInfo_set_code_mC3B02737B3A0F2A350DFF882C39A2829D2E7A8EC,
	AdjustPurchaseVerificationInfo_get_message_mF44C745A7801EFE4681CA685163736AAD65BD093,
	AdjustPurchaseVerificationInfo_set_message_m2CF4884928E67730C04532244236225CF728AEB6,
	AdjustPurchaseVerificationInfo_get_verificationStatus_mBD654E0CF645A3B31439DFD66DEEF7A18469FF97,
	AdjustPurchaseVerificationInfo_set_verificationStatus_mAD3AC1434552BFC186F6B8A51F2116F04CE9D692,
	AdjustPurchaseVerificationInfo__ctor_mFD144FB4E4ACF2B998F0FA447CD249C2850972F1,
	AdjustPurchaseVerificationInfo__ctor_m8C6412DCE35238BF47E8E4202598F74CE085668E,
	AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986,
	AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7,
	AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071,
	AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835,
	AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9,
	AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E,
	AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7,
	AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF,
	AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337,
	AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E,
	AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577,
	AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2,
	AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F,
	AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6,
	AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351,
	AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414,
	AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573,
	AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA,
	AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99,
	AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813,
	AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA,
	AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595,
	AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC,
	AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A,
	AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D,
	AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542,
	AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE,
	AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D,
	AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82,
	AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77,
	AdjustThirdPartySharing_addPartnerSharingSetting_m46C4F5606AF8CE842EFA05FD126197ACCEC911E1,
	AdjustUrlStrategyExtension_ToLowerCaseString_mC501B171FABC8E81E217A019B01F9D079D4DC7A0,
	AdjustUtils_ConvertLogLevel_mF7D0CB4C0B08008E37686670B7361871B737A53F,
	AdjustUtils_ConvertBool_mBFC3BC841A003201C7056448C67C35625379E786,
	AdjustUtils_ConvertDouble_m328F7E087047FA52AEF1D681FCCD32D80791B749,
	AdjustUtils_ConvertInt_mE9AACF8054BA25B7605B3F8727091ED4F41CF37C,
	AdjustUtils_ConvertLong_m7B66091ED09C4DA947FB5C61D5AC40762100FAF4,
	AdjustUtils_ConvertListToJson_m0834067B90DD8AA9713B0A395933C806BDB84689,
	AdjustUtils_GetJsonResponseCompact_mB1763C6F6A17665BAA0534CE919BCFB7D7D491F6,
	AdjustUtils_GetJsonString_m7E4ABC127B656F2CF1D6D5C2973CCDC9345477A1,
	AdjustUtils_WriteJsonResponseDictionary_m45C6F803D1190D8144D7E3441A4CF870606463ED,
	AdjustUtils_TryGetValue_m3BF1818C3435B2DD8794C6BF52073DE2D50A57E9,
	AdjustUtils_GetSkad4ConversionValue_mF1B95F499F7AECC0987FA3A4DD57E10F9582741E,
	AdjustUtils_GetSkad4CoarseValue_m6A96D9597EAAD2D606A7B8730683A1870E324FCA,
	AdjustUtils_GetSkad4LockWindow_mE9E55E3A5B683CDF1BF463568133655A4BEEA39C,
	AdjustUtils_TestOptionsMap2AndroidJavaObject_m898CB8FB47E84D854197B546786A2AD9C160FCAB,
	AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2,
	AdjustUtils__cctor_m4489DD780E5669549E8C7EDAF985BDEC7AC456E1,
};
extern void U3CAwakeU3Ed__13_MoveNext_mCF7E898FE5CEEDBFBA0B2181063C660EEB7F150C_AdjustorThunk (void);
extern void U3CAwakeU3Ed__13_SetStateMachine_m87671A5980EB6DD5A90613EF13F034A439D707F1_AdjustorThunk (void);
extern void U3CChangeNameU3Ed__23_MoveNext_m715380D2F220ECA84F1FD6C7768EA280FF87F06E_AdjustorThunk (void);
extern void U3CChangeNameU3Ed__23_SetStateMachine_mEBC90E9B5632FF9FE2DC69AB44A82A0511006400_AdjustorThunk (void);
extern void U3COnPlayGamesSignedInU3Ed__28_MoveNext_m58E4F170537D3683633F8C5A21D8754E132CB4F7_AdjustorThunk (void);
extern void U3COnPlayGamesSignedInU3Ed__28_SetStateMachine_mE675A3114870098867823FD27648010FAC1EE263_AdjustorThunk (void);
extern void U3COnGetCallbacksU3Ed__29_MoveNext_m348948E06A523F4B57F9A23D4FCEC79176C2B7CB_AdjustorThunk (void);
extern void U3COnGetCallbacksU3Ed__29_SetStateMachine_m846EB23EF52841941A1983569EFA2942BE2A3999_AdjustorThunk (void);
extern void U3CSignInWithGooglePlayGamesAsyncU3Ed__30_MoveNext_m0A04B2BCA035997FD36186DBFE1BAC72993E9C68_AdjustorThunk (void);
extern void U3CSignInWithGooglePlayGamesAsyncU3Ed__30_SetStateMachine_mF2A1FA66B49F9C79DF43A2E6970D8BDD74CB9F99_AdjustorThunk (void);
extern void U3CSignInAnonymouslyAsyncU3Ed__31_MoveNext_mCA66806BE6EBDDF2ED3B120696DB58C656A15D3C_AdjustorThunk (void);
extern void U3CSignInAnonymouslyAsyncU3Ed__31_SetStateMachine_mF2072EE052257698D917DD162BF4DB7CADD8A9CB_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000072, U3CAwakeU3Ed__13_MoveNext_mCF7E898FE5CEEDBFBA0B2181063C660EEB7F150C_AdjustorThunk },
	{ 0x06000073, U3CAwakeU3Ed__13_SetStateMachine_m87671A5980EB6DD5A90613EF13F034A439D707F1_AdjustorThunk },
	{ 0x0600007D, U3CChangeNameU3Ed__23_MoveNext_m715380D2F220ECA84F1FD6C7768EA280FF87F06E_AdjustorThunk },
	{ 0x0600007E, U3CChangeNameU3Ed__23_SetStateMachine_mEBC90E9B5632FF9FE2DC69AB44A82A0511006400_AdjustorThunk },
	{ 0x06000085, U3COnPlayGamesSignedInU3Ed__28_MoveNext_m58E4F170537D3683633F8C5A21D8754E132CB4F7_AdjustorThunk },
	{ 0x06000086, U3COnPlayGamesSignedInU3Ed__28_SetStateMachine_mE675A3114870098867823FD27648010FAC1EE263_AdjustorThunk },
	{ 0x06000087, U3COnGetCallbacksU3Ed__29_MoveNext_m348948E06A523F4B57F9A23D4FCEC79176C2B7CB_AdjustorThunk },
	{ 0x06000088, U3COnGetCallbacksU3Ed__29_SetStateMachine_m846EB23EF52841941A1983569EFA2942BE2A3999_AdjustorThunk },
	{ 0x06000089, U3CSignInWithGooglePlayGamesAsyncU3Ed__30_MoveNext_m0A04B2BCA035997FD36186DBFE1BAC72993E9C68_AdjustorThunk },
	{ 0x0600008A, U3CSignInWithGooglePlayGamesAsyncU3Ed__30_SetStateMachine_mF2A1FA66B49F9C79DF43A2E6970D8BDD74CB9F99_AdjustorThunk },
	{ 0x0600008B, U3CSignInAnonymouslyAsyncU3Ed__31_MoveNext_mCA66806BE6EBDDF2ED3B120696DB58C656A15D3C_AdjustorThunk },
	{ 0x0600008C, U3CSignInAnonymouslyAsyncU3Ed__31_SetStateMachine_mF2072EE052257698D917DD162BF4DB7CADD8A9CB_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2187] = 
{
	5792,
	4713,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	8708,
	5792,
	4735,
	5667,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	4735,
	5792,
	5792,
	4735,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	5792,
	5792,
	8708,
	4735,
	2762,
	2762,
	2760,
	4735,
	1279,
	4735,
	5792,
	4735,
	5792,
	1452,
	5582,
	5792,
	5792,
	5642,
	5792,
	5792,
	5792,
	5792,
	5667,
	4713,
	5792,
	5792,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5792,
	4713,
	4646,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5667,
	5792,
	5792,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5642,
	5792,
	5667,
	5792,
	4735,
	5667,
	5792,
	5792,
	5792,
	4735,
	4713,
	4735,
	5792,
	1511,
	1278,
	1511,
	4713,
	4735,
	4209,
	5667,
	5792,
	5792,
	5792,
	4646,
	5792,
	4735,
	8708,
	5792,
	3443,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5792,
	4735,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5792,
	4735,
	5792,
	4735,
	5792,
	4735,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8708,
	5792,
	5792,
	5792,
	5792,
	5792,
	1489,
	5792,
	5792,
	5792,
	5792,
	1489,
	5792,
	5792,
	5792,
	8708,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	2547,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4209,
	5792,
	5792,
	5792,
	4206,
	5642,
	4735,
	5792,
	5792,
	5792,
	4735,
	4735,
	5792,
	3443,
	5792,
	5667,
	5792,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	5792,
	5792,
	8708,
	5792,
	5792,
	5792,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5667,
	4735,
	5792,
	3443,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4646,
	5792,
	5792,
	4735,
	4735,
	4735,
	5792,
	2762,
	2762,
	2762,
	4646,
	5792,
	4735,
	5792,
	5792,
	5792,
	4735,
	4735,
	4735,
	2762,
	4735,
	4735,
	4735,
	2762,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	2762,
	4735,
	5792,
	4735,
	5792,
	4735,
	5792,
	5792,
	5792,
	4735,
	2762,
	2762,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	4735,
	4646,
	4735,
	4735,
	5792,
	5792,
	5667,
	5792,
	4646,
	4735,
	5667,
	5792,
	4646,
	3443,
	4646,
	2762,
	2762,
	5473,
	4646,
	2762,
	4646,
	4735,
	4735,
	2762,
	2762,
	5792,
	5792,
	4735,
	5582,
	3443,
	4209,
	4735,
	5792,
	4735,
	4735,
	3443,
	5792,
	5792,
	4735,
	5582,
	3443,
	4735,
	4735,
	3443,
	5792,
	4735,
	5792,
	5582,
	2758,
	1492,
	5792,
	5792,
	5792,
	3443,
	4735,
	4646,
	4735,
	4735,
	2762,
	8708,
	5792,
	8676,
	8676,
	8676,
	8708,
	4646,
	4735,
	5667,
	5792,
	4646,
	3443,
	4646,
	2762,
	2762,
	5473,
	4646,
	2762,
	4646,
	4735,
	4735,
	2762,
	2762,
	5792,
	5792,
	5792,
	4735,
	4209,
	5582,
	3443,
	4735,
	5792,
	4735,
	4735,
	3443,
	5792,
	5792,
	4735,
	5582,
	3443,
	4735,
	4735,
	3443,
	5792,
	4735,
	5792,
	5582,
	2758,
	1492,
	5792,
	5792,
	5792,
	3443,
	4735,
	4646,
	4735,
	4735,
	2762,
	8708,
	4735,
	5667,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	8708,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	5792,
	5792,
	4209,
	4209,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8708,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8708,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8676,
	5792,
	4735,
	4646,
	4735,
	4735,
	8708,
	5642,
	5667,
	5642,
	2568,
	5667,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8564,
	8564,
	8564,
	8564,
	4735,
	8564,
	8564,
	8564,
	8564,
	4735,
	8564,
	8564,
	8564,
	8564,
	4735,
	8564,
	8564,
	8564,
	8564,
	4735,
	8564,
	8564,
	8564,
	8564,
	4735,
	8564,
	8564,
	8564,
	8564,
	4735,
	4209,
	4209,
	7948,
	5792,
	8708,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	5792,
	5792,
	4735,
	4735,
	4735,
	4735,
	2762,
	2762,
	5792,
	5792,
	4735,
	5792,
	5792,
	4646,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	2762,
	2762,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4646,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8564,
	5792,
	5792,
	8708,
	8655,
	5792,
	5792,
	5792,
	8708,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	8676,
	8676,
	8676,
	8676,
	8676,
	5792,
	2547,
	4735,
	4646,
	5582,
	5667,
	5642,
	5642,
	8708,
	4735,
	5667,
	4735,
	4735,
	4735,
	4735,
	5792,
	4735,
	4735,
	8708,
	5792,
	4735,
	4735,
	4735,
	4735,
	5792,
	5792,
	8708,
	5792,
	5792,
	8708,
	5792,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8708,
	5792,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	4735,
	2762,
	4735,
	2762,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	5792,
	5792,
	4209,
	4209,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8708,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	2762,
	4735,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	2762,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8708,
	5792,
	2762,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	8708,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	4735,
	5792,
	4735,
	4735,
	4735,
	8708,
	5792,
	4735,
	5792,
	5792,
	4735,
	4735,
	4646,
	1511,
	5667,
	5642,
	5667,
	5667,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8708,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	4646,
	4735,
	4735,
	4735,
	4735,
	2762,
	4735,
	2762,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	8564,
	5792,
	5792,
	5792,
	4209,
	4209,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8708,
	5792,
	2762,
	2762,
	4735,
	4735,
	2762,
	4735,
	5792,
	5792,
	4735,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	2762,
	4735,
	4735,
	2762,
	2762,
	4735,
	5792,
	8708,
	5792,
	2762,
	4735,
	4735,
	2762,
	2762,
	4735,
	5792,
	5792,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	8708,
	5792,
	4735,
	4735,
	5792,
	4735,
	4735,
	4735,
	4735,
	5792,
	4735,
	8708,
	5792,
	4735,
	5792,
	5792,
	2762,
	5667,
	8708,
	5792,
	4181,
	4209,
	4209,
	4735,
	4735,
	5792,
	4735,
	8708,
	5792,
	4735,
	8325,
	8325,
	5792,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	5792,
	5792,
	4646,
	4735,
	5667,
	5792,
	4646,
	3443,
	4646,
	2762,
	2762,
	5473,
	4646,
	2762,
	4646,
	4735,
	4735,
	2762,
	2762,
	5792,
	5792,
	4735,
	5582,
	3443,
	4209,
	4735,
	5792,
	4735,
	4735,
	3443,
	5792,
	5792,
	4735,
	5582,
	3443,
	4735,
	4735,
	3443,
	5792,
	4735,
	5792,
	5582,
	2758,
	1492,
	5792,
	5792,
	5792,
	3443,
	4735,
	4646,
	4735,
	4735,
	2762,
	5792,
	8676,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8676,
	8564,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5642,
	5642,
	5582,
	4646,
	5642,
	4713,
	5642,
	4713,
	5667,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	5792,
	5792,
	5792,
	4646,
	5792,
	5792,
	5792,
	5792,
	4713,
	5792,
	5792,
	5792,
	5792,
	5792,
	1513,
	5792,
	945,
	5792,
	4735,
	5667,
	4713,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5667,
	5792,
	4713,
	5792,
	5792,
	5792,
	5792,
	4713,
	4735,
	4735,
	5792,
	2760,
	4735,
	1279,
	4735,
	2760,
	5792,
	2162,
	4735,
	2760,
	5582,
	2162,
	3443,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5792,
	5792,
	0,
	0,
	0,
	0,
	5784,
	4839,
	5717,
	4779,
	5582,
	4646,
	5582,
	4646,
	5582,
	4646,
	5582,
	4646,
	5582,
	5667,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4735,
	5792,
	5792,
	4646,
	3443,
	5792,
	5792,
	5792,
	5792,
	1453,
	4735,
	5792,
	4735,
	482,
	931,
	2130,
	287,
	192,
	5792,
	5792,
	4735,
	5792,
	5582,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	935,
	4713,
	2547,
	5792,
	5778,
	4833,
	5582,
	4646,
	5792,
	5792,
	4735,
	5792,
	4779,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	4713,
	5792,
	8676,
	8564,
	5792,
	5792,
	5792,
	5792,
	5792,
	5667,
	4735,
	5792,
	5582,
	5792,
	5792,
	5792,
	4713,
	5667,
	5792,
	5792,
	4735,
	4735,
	5642,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	5792,
	8325,
	8325,
	4735,
	8325,
	5792,
	5667,
	5667,
	5667,
	4206,
	5667,
	5667,
	5792,
	5778,
	5778,
	5667,
	5642,
	5792,
	8325,
	4735,
	4735,
	4735,
	4735,
	4735,
	5667,
	4735,
	5792,
	0,
	0,
	7938,
	5792,
	7904,
	8112,
	7907,
	8321,
	8564,
	8676,
	8556,
	8655,
	5792,
	8676,
	5792,
	5792,
	8676,
	2747,
	8708,
	5792,
	2747,
	0,
	7938,
	5792,
	8708,
	8676,
	5792,
	5792,
	8676,
	2509,
	3419,
	2568,
	4206,
	4735,
	5667,
	4646,
	5582,
	4206,
	8708,
	5792,
	2509,
	3419,
	2568,
	4206,
	4735,
	5667,
	4646,
	5582,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	8676,
	7904,
	8112,
	7907,
	8321,
	8564,
	8676,
	8556,
	8655,
	5792,
	8708,
	5792,
	5667,
	5667,
	4646,
	4646,
	2762,
	4206,
	2568,
	4209,
	2762,
	5667,
	4735,
	5642,
	4735,
	4209,
	4206,
	4209,
	5667,
	5667,
	5667,
	4209,
	5642,
	4713,
	5717,
	4779,
	5607,
	4678,
	5582,
	4646,
	5667,
	5667,
	8325,
	8325,
	7505,
	7505,
	3443,
	5642,
	8325,
	8325,
	4735,
	4735,
	4735,
	4735,
	5667,
	8325,
	8325,
	8325,
	8325,
	8325,
	8325,
	5792,
	4713,
	5792,
	5582,
	5667,
	5792,
	5667,
	5667,
	5667,
	4713,
	5792,
	5582,
	5792,
	5792,
	5667,
	5792,
	5667,
	5667,
	5667,
	4206,
	2568,
	4209,
	2762,
	5642,
	2762,
	4206,
	4209,
	5667,
	5667,
	5667,
	4209,
	4735,
	5792,
	4713,
	5792,
	5582,
	5792,
	5667,
	5792,
	5667,
	5667,
	5667,
	4713,
	5792,
	5582,
	5792,
	5667,
	5792,
	5667,
	4209,
	2762,
	4206,
	2568,
	5642,
	2762,
	4209,
	4206,
	4209,
	5667,
	5667,
	5667,
	4209,
	4735,
	5792,
	5792,
	3212,
	4713,
	5792,
	5582,
	5792,
	5667,
	5792,
	5667,
	5667,
	5667,
	4713,
	5792,
	5582,
	5792,
	5667,
	5792,
	5667,
	5667,
	4735,
	4735,
	4779,
	4678,
	4646,
	4713,
	5667,
	4209,
	4735,
	4735,
	2762,
	4735,
	4206,
	2568,
	4209,
	2762,
	4735,
	2762,
	7505,
	7505,
	3443,
	5642,
	5667,
	4209,
	5642,
	4713,
	5717,
	4779,
	5607,
	4678,
	5582,
	4646,
	5667,
	5667,
	8325,
	8564,
	8564,
	8655,
	8556,
	8556,
	8708,
	8564,
	8676,
	8708,
	8708,
	8676,
	7948,
	7948,
	8564,
	8564,
	8708,
	8708,
	8564,
	7948,
	8564,
	8564,
	8564,
	8556,
	8708,
	8708,
	8564,
	8564,
	8676,
	8676,
	7948,
	8564,
	8115,
	5792,
	8708,
	4735,
	4735,
	4735,
	3443,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	4735,
	5792,
	4646,
	8564,
	8564,
	8556,
	8655,
	8556,
	8564,
	8708,
	8708,
	8564,
	8708,
	7948,
	7948,
	8564,
	8564,
	8708,
	8708,
	7948,
	8564,
	8564,
	8564,
	8564,
	8556,
	7948,
	8560,
	7315,
	6416,
	8708,
	8669,
	8676,
	8676,
	8676,
	8676,
	8676,
	8564,
	8564,
	8676,
	8676,
	7369,
	7948,
	8655,
	8564,
	5792,
	4735,
	2344,
	4713,
	4735,
	4735,
	4735,
	2762,
	2762,
	1513,
	1032,
	4735,
	4735,
	2762,
	2762,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5471,
	4541,
	5667,
	4735,
	5667,
	4735,
	5792,
	4735,
	4735,
	2758,
	1486,
	4713,
	4735,
	4735,
	4646,
	4646,
	4646,
	4646,
	4646,
	4678,
	4735,
	4646,
	4735,
	500,
	2762,
	5667,
	2762,
	5667,
	2762,
	5667,
	2762,
	5667,
	2762,
	5667,
	2762,
	5667,
	4646,
	4646,
	4646,
	5792,
	4646,
	2762,
	5667,
	2762,
	5667,
	4713,
	4735,
	4646,
	4646,
	4735,
	4646,
	4735,
	8321,
	4735,
	2344,
	2762,
	2762,
	4735,
	4735,
	4735,
	2762,
	4735,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5582,
	4646,
	5667,
	4735,
	5792,
	4735,
	4735,
	4735,
	5667,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5792,
	4735,
	4735,
	4735,
	5667,
	8321,
	8321,
	2762,
	304,
	4735,
	2762,
	2762,
	5642,
	4713,
	5667,
	4735,
	5667,
	4735,
	5792,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5582,
	4646,
	5667,
	4735,
	5792,
	4735,
	4735,
	4735,
	5667,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5667,
	4735,
	5792,
	4735,
	4735,
	4735,
	5667,
	4533,
	1513,
	1507,
	8321,
	8206,
	8207,
	8167,
	8209,
	8234,
	8325,
	8325,
	7722,
	7948,
	7722,
	8226,
	8325,
	8115,
	7722,
	5792,
	8708,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x060005FC, { 0, 1 } },
	{ 0x060005FD, { 1, 2 } },
	{ 0x060005FE, { 3, 2 } },
};
extern const uint32_t g_rgctx_JsonUtility_FromJson_TisWrapper_1_t041A2BB4B9ED45A72B4F2E1E3CB80F97F5EEB84D_m7DFB304D62185B621D8D27AB2A4791F7342908D1;
extern const uint32_t g_rgctx_Wrapper_1_t16A4385C27BC23777A38E85F9BFE1F83574188FD;
extern const uint32_t g_rgctx_Wrapper_1__ctor_mC92B1D4D45A39DBA9951C409EA58644F03C9B194;
extern const uint32_t g_rgctx_Wrapper_1_t1193216C1BE750C9DB86914D5302BA434A7DB97D;
extern const uint32_t g_rgctx_Wrapper_1__ctor_mC482CCC6BFB4D0BA7D5AFC0FF9E0E3E47609A373;
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonUtility_FromJson_TisWrapper_1_t041A2BB4B9ED45A72B4F2E1E3CB80F97F5EEB84D_m7DFB304D62185B621D8D27AB2A4791F7342908D1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Wrapper_1_t16A4385C27BC23777A38E85F9BFE1F83574188FD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Wrapper_1__ctor_mC92B1D4D45A39DBA9951C409EA58644F03C9B194 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Wrapper_1_t1193216C1BE750C9DB86914D5302BA434A7DB97D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Wrapper_1__ctor_mC482CCC6BFB4D0BA7D5AFC0FF9E0E3E47609A373 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2187,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
