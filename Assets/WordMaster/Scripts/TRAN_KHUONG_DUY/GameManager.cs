﻿using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TRAN_KHUONG_DUY
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        public delegate void ShowResults(string str);
        public delegate void Hint();
        public delegate bool Undo();

        public event ShowResults OnShowResults;
        public event Hint OnHint;
        public event Undo OnUndo;

        [Header("UI")]
        public GameObject homeUI;
        public GameObject spinWheelUI;
        public GameObject settingsUI;
        public GameObject packageUI;
        public GameObject packageLevelsUI;
        public GameObject lowestTierInGameUI;
        public GameObject highestTierInGameUI;
        public GameObject fadeImgUI;
        public GameObject resultBoardUI;
        public GameObject coinShopUI;
        public GameObject panel_tutorial_UI;
        public GameObject ads_layer_UI;

        public TMP_Text homeCointText;
        public TMP_Text inGameCointText;
        public TMP_Text packageLable;
        public TMP_Text levelsLabel;
        public TMP_Text inGameLabel;

        public GameObject title;

        [Space]
        public GameObject playButton1;
        public GameObject playButton2;
        public GameObject bg1Check;
        public GameObject bg2Check;

        [Space]
        public Animation[] victoryNotification;
        private Animation lastVictoryNotification;

        [Space]
        public LevelScript[] levels;

        [Space]
        public GameObject menuBackground;
        public GameObject inGameBackground;
        public SpriteRenderer menuBackgroundSpr;
        public Sprite homeTile_1;
        public Sprite homeTile_2;

        [Space]
        public Image BGMIcon;
        public GameObject BGMCheck;
        public Sprite BGMOn;
        public Sprite BGMOff;
        public Image SFXIcon;
        public GameObject SFXCheck;
        public Sprite SFXOn;
        public Sprite SFXOff;

        [Header("In Game")]
        public CURRENTGROUP currentGroup = CURRENTGROUP.G_3;
        public string currentContent;
        public LevelGenerator levelGenerator;

        [Space(10)]
        public TMP_Text drawingStringTxt;
        public TMP_Text qualityTxt;
        public GameObject emptyStar1;
        public GameObject emptyStar2;
        //public GameObject bigEmptyStar2;
        public GameObject emptyStar3;
        public GameObject resultStar1;
        public GameObject resultStar2;
        //public GameObject resultBigStar2;
        public GameObject resultStar3;
        public TMP_Text moveTxt;
        public TMP_Text previousRecordTxt;

        [Header("Packages")]
        public Packages[] packagesArray;

        private string[] contentParagraphs;
        private int moveNumber;
        private int resultNumber;

        //public int levelNumber;
        
        //[SerializeField] private LevelStatus[] levelStatus;
        [SerializeField] private LevelContent[] levelContents;
        private bool isCalculating = false;

        [SerializeField] private GameObject widget_coinKurang;
        [SerializeField] private GameObject widget_hintHabis;

        private int coin = 0, star = 0, coinPerKata = 0;
        private bool isFromMenu = true;
        public int GetCoinValue() { return coinPerKata; }
        public int GetScoreValue() { return currentPackage.packages[currentPackage.totalWord - 1].status.scoreNumber; }

        private packageManager.PackageLevel currentPackage;

        public bool EndGame { get; set; }

        public int BGMStatus { get; private set; }
        public int SFXStatus { get; private set; }

        private GameServices gameServices;
        private string currentPackageName;
        private int currentHint;

        //for button ads hint
        [SerializeField] private GameObject[] adsContents;
        [SerializeField] private TMP_Text label_countdown;

        [SerializeField] private int countdown = 60;
        [SerializeField] private Button b_freeAds;
        [SerializeField] private bool isAdsWaiting;

        [SerializeField] private bool spinLogin;
        public bool IsSpinLogin() { return spinLogin; }
        public void ChangeSpinState(bool value) { spinLogin = value; }

        
        public GameServices GetGameServices()
        {
            gameServices = gameObject.GetComponent<GameServices>();
            return gameServices;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            if (PlayerPrefs.GetInt("FirstGame") == 0)
            {
                PlayerPrefs.SetInt(ConstantsInGame.Coin, 200);
                homeCointText.text = inGameCointText.text = coin + "";
                PlayerPrefs.SetInt("FirstGame", 1);
            }

            gameServices = GetComponent<GameServices>();
        }

        private void OnEnable()
        {
            //AdsManager.Instance.OnAdRewardSuccess += Instance_AdsReward;
            ISManager.Instance.OnAdRewardSuccess += OnISAdRewardSuccess;
        }

        public void SendAdjustEvent(string _token)
        {
            AdjustEvent adjustEvent = new AdjustEvent(_token);
            Adjust.trackEvent(adjustEvent);
        }

        private void OnDisable()
        {
            //AdsManager.Instance.OnAdRewardSuccess -= Instance_AdsReward;
            ISManager.Instance.OnAdRewardSuccess -= OnISAdRewardSuccess;
        }

        private void Instance_AdsReward(string _token)
        {
            if (_token == AdjustToken.RewardedAdsFreeHintToken)
            {
                if (OnHint != null)
                {
                    OnHint();
                }

                ads_layer_UI.SetActive(false);
            }
        }
        private void OnISAdRewardSuccess(string _token)
        {
            if (_token == AdjustToken.RewardedAdsFreeHintToken)
            {
                if (OnHint != null)
                {
                    OnHint();
                }

                ads_layer_UI.SetActive(false);
            }
        }


        private void Start()
        {
            //PlayerPrefs.DeleteAll();
            Application.targetFrameRate = 60;
            CheckSetup();
            levelGenerator.InitEssentialArrays();
        }

        public void CheckSetup()
        {
            // Check coin
            coin = PlayerPrefs.GetInt(ConstantsInGame.Coin, 0);
            homeCointText.text = inGameCointText.text = coin + "";

            // Check star
            if(PlayerPrefs.HasKey("STARS"))
            {
                star = PlayerPrefs.GetInt("STARS");
            }

            // Check background type
            if (PlayerPrefs.GetInt(ConstantsInGame.Background, 1) == 1)
            {
                playButton1.SetActive(true);
                bg1Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_1;
            }
            else
            {
                playButton2.SetActive(true);
                bg2Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_2;
            }

            // Check music in game
            if (PlayerPrefs.GetInt(ConstantsInGame.BGM, 1) == 1)
            {
                BGMIcon.sprite = BGMOn;
                BGMCheck.SetActive(true);
                BGMStatus = 1;
                SoundManager.Instance.menuBG.Play();
            }
            else
            {
                BGMIcon.sprite = BGMOff;
                BGMCheck.SetActive(false);
                BGMStatus = 0;
            }

            // Check sound in game
            if (PlayerPrefs.GetInt(ConstantsInGame.SFX, 1) == 1)
            {
                SFXIcon.sprite = SFXOn;
                SFXCheck.SetActive(true);
                SFXStatus = 1;
            }
            else
            {
                SFXIcon.sprite = SFXOff;
                SFXCheck.SetActive(false);
                SFXStatus = 0;
            }
        }

        // Settings button is clicked
        public void SettingsBtn_Onclick()
        {
            homeUI.SetActive(false);
            settingsUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Turn off coin shop button is clicked
        public void TurnOffCoinShop()
        {
            if(isFromMenu)
            {
                homeUI.SetActive(true);

                fadeImgUI.SetActive(false);
                coinShopUI.SetActive(false);
            }
            else
            {
                int n = 0;

                if (PlayerPrefs.HasKey("LEVELSAATINI"))
                {
                    n = PlayerPrefs.GetInt("LEVELSAATINI");
                }

                if (n < packageManager.Instance.GetMaxPackageList())
                {
                    string findPackageName = packageManager.Instance.GetPackageName(n);
                    LoadData(packageManager.Instance.GetPackageLevel(findPackageName));
                }

                fadeImgUI.SetActive(false);
                coinShopUI.SetActive(false);
            }
        }

        // More game button is clicked
        public void MoreGameBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Shop button is clicked
        public void ShopBtn_Onclick(bool fromMenu)
        {
            if(isCalculating)
            {
                return;
            }
            isFromMenu = fromMenu;

            homeUI.SetActive(false);

            fadeImgUI.SetActive(true);
            coinShopUI.SetActive(true);
            levelGenerator.CloseLevel();
            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);

            menuBackground.SetActive(true);
            inGameBackground.SetActive(false);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Email button is clicked
        public void EmailBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Game center button is clicked
        public void GameCenterBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // BGM button is clicked
        public void BGMButton_Onclick()
        {
            if (BGMCheck.activeInHierarchy)
            {
                PlayerPrefs.SetInt(ConstantsInGame.BGM, 0);
                BGMIcon.sprite = BGMOff;
                BGMCheck.SetActive(false);
                BGMStatus = 0;
                SoundManager.Instance.menuBG.Stop();
            }
            else
            {
                PlayerPrefs.SetInt(ConstantsInGame.BGM, 1);
                BGMIcon.sprite = BGMOn;
                BGMCheck.SetActive(true);
                BGMStatus = 1;
                SoundManager.Instance.menuBG.Play();
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // SFX button is clicked
        public void SFXButton_Onclick()
        {
            if (SFXCheck.activeInHierarchy)
            {
                PlayerPrefs.SetInt(ConstantsInGame.SFX, 0);
                SFXIcon.sprite = SFXOff;
                SFXCheck.SetActive(false);
                SFXStatus = 0;
            }
            else
            {
                PlayerPrefs.SetInt(ConstantsInGame.SFX, 1);
                SFXIcon.sprite = SFXOn;
                SFXCheck.SetActive(true);
                SFXStatus = 1;
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Background button is clicked
        public void BackgroundButton_Onclick(int type)
        {
            PlayerPrefs.SetInt(ConstantsInGame.Background, type);
            if (type == 1)
            {
                playButton1.SetActive(true);
                playButton2.SetActive(false);
                bg1Check.SetActive(true);
                bg2Check.SetActive(false);
                menuBackgroundSpr.sprite = homeTile_1;
            }
            else
            {
                playButton1.SetActive(false);
                playButton2.SetActive(true);
                bg1Check.SetActive(false);
                bg2Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_2;
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Play button on Home is clicked
        public void PlayButton_Onclick()
        {
            homeUI.SetActive(false);
            title.SetActive(false);
            packageUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnPlay.Play();
            }
        }

        // Spin Wheel button on Home is clicked
        public void SpinWheelButton_OnClick()
        {
            homeUI.SetActive(false);
            title.SetActive(false);
            spinWheelUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnPlay.Play();
            }
        }

        // Back button on Package is clicked
        public void BackToHomeButton_Onclick()
        {
            homeUI.SetActive(true);
            title.SetActive(true);

            settingsUI.SetActive(false);
            packageUI.SetActive(false);
            spinWheelUI.SetActive(false);
            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);

            menuBackground.SetActive(true);
            inGameBackground.SetActive(false);

            levelGenerator.CloseLevel();

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void BackToHomeFromSetting()
        {
            homeUI.SetActive(true);
            title.SetActive(true);

            settingsUI.SetActive(false);
            packageUI.SetActive(false);
            spinWheelUI.SetActive(false);
            lowestTierInGameUI.SetActive(false);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void BackToHome()
        {
            BackToHomeButton_Onclick();

            int r = Mathf.FloorToInt(UnityEngine.Random.value * 100f);
            if (r <= 10)
            {
                ads_layer_UI.SetActive(true);
               // AdsManager.Instance.RequestInterstitial(AdjustToken.InterstitialAdsShowBackToHomeToken);
                
                ads_layer_UI.SetActive(false);
            }
        }
        //
        public void PackageButton_Onclick(string packageLabel, string levelsLabel, LevelContent[] levelContents)
        {
            packageUI.SetActive(false);
            packageLevelsUI.SetActive(true);

            this.packageLable.text = packageLabel;
            this.levelsLabel.text = levelsLabel;

            // Setup levels
            int length = levelContents.Length;
            for (int i = 0; i < length; i++)
            {
                this.levels[i].SetUp(levelContents[i].status.unlock,
                                     levelContents[i].status.starNumber,
                                     levelContents[i].group,
                                     levelContents[i].content);
            }

            // Cache these variables to use later
            this.levelContents = levelContents;

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Back button on Package level 2 is clicked
        public void BackToPackageButton_Onclick()
        {
            if(isCalculating)
            {
                return;
            }

            int r = Mathf.FloorToInt(UnityEngine.Random.value * 100f);
            if (r <= 10)
            {
                ads_layer_UI.SetActive(true);
                //AdsManager.Instance.RequestInterstitial(AdjustToken.InterstitialAdsShowBackToPackageToken);
                ads_layer_UI.SetActive(false);
            }

            homeUI.SetActive(false);
            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);
            packageUI.SetActive(true);

            menuBackground.SetActive(true);
            inGameBackground.SetActive(false);

            levelGenerator.CloseLevel();

            if (BGMStatus == 1)
            {
                SoundManager.Instance.menuBG.Play();
                SoundManager.Instance.ingameBG.Stop();
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void StartLevel(CURRENTGROUP group, string content, int level , string packageName)
        {
            packageLevelsUI.SetActive(false);
            lowestTierInGameUI.SetActive(true);
            highestTierInGameUI.SetActive(true);

            menuBackground.SetActive(false);
            inGameBackground.SetActive(true);

            packageUI.SetActive(false);

            currentPackageName = packageName;
            inGameLabel.text = packageName;

            currentPackage.totalWord = level;
            PlayerPrefs.SetInt("LevelNumber", level);
            currentGroup = group;
            currentContent = content;
            contentParagraphs = currentContent.Split(' ');
            levelGenerator.InitLevel(currentGroup, currentContent, contentParagraphs);

            currentPackageName = packageName;

            if (BGMStatus == 1)
            {
                SoundManager.Instance.menuBG.Stop();
                SoundManager.Instance.ingameBG.Play();
            }
            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }

            if(isAdsWaiting)
            {
                b_freeAds.interactable = false;

                adsContents[1].SetActive(true);
                adsContents[0].SetActive(false);
            }
            else
            {
                b_freeAds.interactable = true;

                adsContents[0].SetActive(true);
                adsContents[1].SetActive(false);
            }

            inGameLabel.text = packageName;
        }

        // Back button on inGame is clicked
        public void BackToLevelButton_Onclick()
        {
            if (!EndGame)
            {
                lowestTierInGameUI.SetActive(false);
                highestTierInGameUI.SetActive(false);
                packageLevelsUI.SetActive(true);
                inGameBackground.SetActive(false);
                menuBackground.SetActive(true);
                panel_tutorial_UI.SetActive(false);

                ResetGame();
                levelGenerator.CloseLevel();

                // Update information of the level
                PackageButton_Onclick(packageLable.text, levelsLabel.text, levelContents);
            }
        }

        public void CheckResults(string drawingString)
        {
            if (drawingString.Length >= 3)
            {
                moveNumber++;
            }

            bool rightAnswer = false;

            int stringLength = contentParagraphs.Length;
            for (int i = stringLength - 1; i >= 0; i--)
            {
                if (drawingString == contentParagraphs[i])
                {
                    if (OnShowResults != null)
                    {
                        OnShowResults(drawingString);
                        resultNumber++;
                        rightAnswer = true;
                    }
                    break;
                }
            }

            if (!rightAnswer)
            {
                if (SFXStatus == 1)
                {
                    SoundManager.Instance.sfxWrongAnswer.Play();
                }
            }
            else
            {
                if (SFXStatus == 1)
                {
                    SoundManager.Instance.sfxWordlistComplete.Play();
                    panel_tutorial_UI.SetActive(false);
                }

                PlayerPrefs.SetString("PEMAINBARU", "new player");
                GetComponent<GameServices>().SetHighscoreButton();

                AdjustEvent adjustEvent = new AdjustEvent("ty29rz");
                Adjust.trackEvent(adjustEvent);
            }

            if (resultNumber == stringLength)
            {
                EndGame = true;
                StartCoroutine(Finishing());
            }
        }

        public IEnumerator Finishing()
        {
            isCalculating = true;

            yield return new WaitForSeconds(0.5f);
            int length = victoryNotification.Length;
            int random = UnityEngine.Random.Range(0, length);

            lastVictoryNotification = victoryNotification[random];
            lastVictoryNotification.gameObject.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.sfxFontDown.Play();
            }

            yield return new WaitForSeconds(1.0f);
            lastVictoryNotification.Play("victory_disappear");

            yield return new WaitForSeconds(1.0f);
            lastVictoryNotification.gameObject.SetActive(false);

            fadeImgUI.SetActive(true);
            resultBoardUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.sfxGradeStar.Play();
            }

            // Show current move record used to complete the level
            moveTxt.text = moveNumber.ToString();

            // Show previous move record used to complete the level
            previousRecordTxt.text = "Previous Record : " + currentPackage.packages[currentPackage.totalWord].status.moveRecord;

            // Save current move record
            currentPackage.packages[currentPackage.totalWord].status.moveRecord = moveNumber;

            emptyStar1.SetActive(true);
            emptyStar2.SetActive(true);
            emptyStar3.SetActive(true);

            resultStar1.SetActive(false);
            resultStar2.SetActive(false);
            resultStar3.SetActive(false);

            PrepareToShowResult();

            // Show result
            //int starNumber = 1;
            yield return new WaitForSeconds(0.5f);

            resultStar1.SetActive(true);

           

            if (moveNumber <= 2)
            {
                //starNumber = 3;     // Special - 3 stars with a big star

                if (currentPackage.packages[currentPackage.totalWord].status.scoreNumber != 10)
                {
                    coinPerKata = 10;
                    MoreCoin(10);
                }
                else
                {
                    coinPerKata = 0;
                }
                currentPackage.packages[currentPackage.totalWord].status.scoreNumber = 10;


                yield return new WaitForSeconds(0.5f);
                resultStar2.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                resultStar3.SetActive(true);

                if(currentPackage.packages[currentPackage.totalWord].status.starNumber < 3)
                {
                    AddMoreStars(3);
                }
            }
            if (moveNumber <= 5 && moveNumber > 2)
            {
                if (currentPackage.packages[currentPackage.totalWord].status.scoreNumber != 6)
                {
                    coinPerKata = 6;
                    MoreCoin(6);
                }
                else
                {
                    coinPerKata = 0;
                }
                currentPackage.packages[currentPackage.totalWord].status.scoreNumber = 6;


                yield return new WaitForSeconds(0.5f);
                resultStar2.SetActive(true);
                if (currentPackage.packages[currentPackage.totalWord].status.starNumber < 3)
                {
                    AddMoreStars(3);
                }
            }
            if (moveNumber > 5)
            {
                if (currentPackage.packages[currentPackage.totalWord].status.scoreNumber != 2)
                {
                    coinPerKata = 2;
                    MoreCoin(2);
                }
                else
                {
                    coinPerKata = 0;
                }
                currentPackage.packages[currentPackage.totalWord].status.scoreNumber = 2;

                if (currentPackage.packages[currentPackage.totalWord].status.starNumber < 2)
                {
                    AddMoreStars(2);
                }
            }


            if (currentPackage.totalWord < 10)
            {
                // We have 5 levels from 1 to 5, and indexes begin from 0 to 4
                currentPackage.packages[currentPackage.totalWord].status.unlock = 1;    // Unlock the next level
            }
            else
            {
                // Notify that we have completed this level - full 5 level statuses
                int packagesArrayLength = packagesArray.Length;
                for (int i = 0; i < packagesArrayLength; i++)
                {
                    if (packageLable.text == packagesArray[i].packageLabel)
                    {
                        for (int j = 0; j < packagesArray[i].packageLevels.Length; j++)
                        {
                            if (levelsLabel.text == packagesArray[i].packageLevels[j].name)
                            {
                                packagesArray[i].packageLevels[j].completeStatus = 1;
                                break;
                            }
                        }
                        break;
                    }
                }

                // Check if we have completed this package or not
                int n = 0;
                for (int i = 0; i < packagesArrayLength; i++)
                {
                    if (packageLable.text == packagesArray[i].packageLabel)
                    {
                        for (int j = 0; j < packagesArray[i].packageLevels.Length; j++)
                        {
                            n += packagesArray[i].packageLevels[j].completeStatus;
                        }

                        if (n >= packagesArray[i].packageLevels.Length)
                        {
                            // unlock the next package
                            PlayerPrefs.SetInt(packagesArray[i + 1].packageLabel, 1);
                            SendAdjustEvent(AdjustToken.TotalPackagesCompletedToken);
                        }
                        break;
                    }
                }
            }

            currentPackage.totalWord++;
            packageManager.Instance.SaveData();

            isCalculating = false;
        }

        private void AddMoreStars(int v)
        {
            star += v;
            PlayerPrefs.SetInt("STARS" , star);

            GetComponent<GameServices>().SetNewLeaderboard(star);
        }

        private void PrepareToShowResult()
        {
            if (moveNumber == resultNumber)
            {
                qualityTxt.text = "Perfect!";
            }
            else if (moveNumber == (resultNumber + 1))
            {
                qualityTxt.text = "Great!";
            }
            else
            {
                qualityTxt.text = "Good!";
            }
#if ADS_PLUGIN
            AdsControl.Instance.showAds();
#endif
        }

        // Next btn is clicked
        public void NextBtn_Onclick()
        {
            if (EndGame)
            {
                return;
            }

            if (currentPackage.totalWord < 10)
            {
                // If the next level was unlocked
                if (currentPackage.packages[currentPackage.totalWord].status.unlock == 0)
                {
                    ResetGame();
                    // Update information of the current level
                    levels[currentPackage.totalWord].UpdateInformation(currentPackage.packages[currentPackage.totalWord].status.unlock, currentPackage.packages[currentPackage.totalWord].status.starNumber);
                        
                    //AdjustEvent adjustEvent = new AdjustEvent("s874yt");
                    //Adjust.trackEvent(adjustEvent);

                    Reshuffle();
                    StartLevel(currentPackage.packages[currentPackage.totalWord].group, currentPackage.packages[currentPackage.totalWord].content, currentPackage.totalWord, currentPackageName);
                }
                else
                {
                    LevelBtn_Onclick();
                }
            }
            else
            {
                // Update information of the current level
                //levels[currentPackage.totalWord].UpdateInformation(currentPackage.packages[currentPackage.totalWord].status.unlock, currentPackage.packages[currentPackage.totalWord].status.starNumber);

                ResetGame();

                int n = 0;
                if(PlayerPrefs.HasKey("LEVELSAATINI"))
                {
                    n = PlayerPrefs.GetInt("LEVELSAATINI");
                }
                n++;

                PlayerPrefs.SetInt("LEVELSAATINI", n);

                //check next package
                if (n < packageManager.Instance.GetMaxPackageList())
                {
                    string findPackageName = packageManager.Instance.GetPackageName(n);
                    currentPackage = packageManager.Instance.GetPackageLevel(findPackageName);

                    currentPackage.totalWord = 0;

                    levelContents = currentPackage.packages;

                    StartLevel(currentPackage.packages[currentPackage.totalWord].group, currentPackage.packages[currentPackage.totalWord].content, currentPackage.totalWord, currentPackage.packageName);
                    inGameLabel.text = currentPackage.packageName;

                    ads_layer_UI.SetActive(true);
                    //AdsManager.Instance.RequestInterstitial(AdjustToken.InterstitialAdsShowNextLevelToken);
                    GameManager.Instance.SendAdjustEvent(AdjustToken.TotalPackagesCompletedToken);
                        
                    ads_layer_UI.SetActive(false);
                }
                else
                {
                    BackToPackageButton_Onclick();
                }       
            }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
                currentHint = 0;
            
        }

        private void Reshuffle()
        {
            // Knuth shuffle algorithm :: courtesy of Wikipedia :)
            for (int t = 0; t < 9; t++)
            {
                if(t > currentPackage.totalWord - 1)
                {
                    LevelContent tmp = levelContents[t];
                    int r = UnityEngine.Random.Range(t, levelContents.Length);
                    levelContents[t] = levelContents[r];
                    levelContents[r] = tmp;
                }
            }

            int[] key = new int[levelContents.Length];

            for (int i = 0; i < levelContents.Length; i++)
            {
                switch (levelContents[i].group)
                {
                    case CURRENTGROUP.G_3:
                        key[i] = 3;
                        break;
                    case CURRENTGROUP.G_4:
                        key[i] = 4;
                        break;
                    case CURRENTGROUP.G_5:
                        key[i] = 5;
                        break;
                    case CURRENTGROUP.G_7:
                        key[i] = 7;
                        break;
                }
            }

            Array.Sort(key , levelContents);
        }

        // Undo btn is clicked
        public void UndoBtn_Onclick()
        {
            if (!EndGame)
            {
                if (OnUndo != null)
                {
                    if (OnUndo())
                    {
                        resultNumber--;
                    }
                }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.C))
            {
                MoreCoin(5000);
            }
        }

        // Reset btn is clicked
        public void ResetBtn_Onclick()
        {
            if (!EndGame)
            {
                ResetGame();
                contentParagraphs = currentContent.Split(' ');
                levelGenerator.InitLevel(currentGroup, currentContent, contentParagraphs);

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        // Free btn is clicked
        public void FreeBtn_Onclick()
        {
            if(isAdsWaiting)
            {
                return;
            }

            if (!EndGame)
            {
                if(levelGenerator.hintLength >= currentPackage.packages[currentPackage.totalWord].content.Length - 1)
                {
                    widget_hintHabis.SetActive(true);
                    return;
                }

                b_freeAds.interactable = false;
                isAdsWaiting = true;

                adsContents[0].SetActive(false);
                adsContents[1].SetActive(true);
                StartCoroutine(AdsCountdown());

                ads_layer_UI.SetActive(true);
                //AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsFreeHintToken , OnGetReward);
                ads_layer_UI.SetActive(false);

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        private void OnGetReward(string obj)
        {

        }

        IEnumerator AdsCountdown()
        {
            countdown = 60;
            for (int i = 0; i < countdown; i++)
            {
                label_countdown.text = $"00:{countdown}";
                yield return new WaitForSeconds(1f);
                countdown--;
            }
            b_freeAds.interactable = true;
            isAdsWaiting = false;

            adsContents[0].SetActive(true);
            adsContents[1].SetActive(false);
        }

        // Hint btn is clicked
        public void HintBtn_Onclick()
        {
            if (!EndGame)
            {
                if (levelGenerator.hintLength >= currentPackage.packages[currentPackage.totalWord].content.Length - 1)
                {
                    widget_hintHabis.SetActive(true);
                    return;
                }

                if (OnHint != null)
                {
                    if (PlayerPrefs.GetInt(ConstantsInGame.Coin) >= 25)
                    {
                        OnHint();
                        coin -= 25;
                        homeCointText.text = inGameCointText.text = coin + "";
                        PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
                        currentHint++;
                    }
                    else
                    {
                        widget_coinKurang.SetActive(true);
                    }
                }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnHint.Play();
                }
            }
        }

        public void MoreCoin(int _value)
        {
            coin += _value;
            homeCointText.text = inGameCointText.text = coin + "";
            PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
        }

        // Level btn is clicked
        public void LevelBtn_Onclick()
        {
            if (!isCalculating)
            {
                ResetGame();
                BackToPackageButton_Onclick();

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        // Replay btn is clicked
        public void ReplayBtn_Onclick()
        {
            if (!isCalculating)
            {
                ResetGame();
                ResetBtn_Onclick();
            }
        }

        // Next btn in result board is clicked
        public void NextBtn2_Onclick()
        {
            if (!isCalculating)
            {
                EndGame = false;
                NextBtn_Onclick();
            }
        }

        private void ResetGame()
        {
            fadeImgUI.SetActive(false);
            resultBoardUI.SetActive(false);
            isCalculating = false;
            EndGame = false;
            resultNumber = moveNumber = 0;
            levelGenerator.RemoveUndoActions();
        }

        public void SetCoinValue(int value)
        {
            coin = value;
            PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
            homeCointText.text = inGameCointText.text = coin + "";
        }

        public void LoadData(packageManager.PackageLevel newCurrentPackage)
        {
            levelContents = newCurrentPackage.packages;
            currentPackage = newCurrentPackage;

            if(currentPackage.totalWord > 0)
            {
                Reshuffle();
            }

            if(currentPackage.totalWord == 10)
            {
                currentPackage.totalWord = 0;
            }

            StartLevel(levelContents[currentPackage.totalWord].group, levelContents[currentPackage.totalWord].content, currentPackage.totalWord, newCurrentPackage.packageName);
            inGameLabel.text = newCurrentPackage.packageName;

            this.currentPackage = newCurrentPackage;
            currentPackageName = newCurrentPackage.packageName;

            //Debug.Log(currentPackage.packageName + " , " + currentPackage.packages[levelNumber - 1].content);
            //check if first time playing show tutorial

            if(newCurrentPackage.packageName == "Musics" && newCurrentPackage.packages[currentPackage.totalWord].content == "MIC")
            {
                if (!PlayerPrefs.HasKey("PEMAINBARU"))
                {
                    panel_tutorial_UI.SetActive(true);
                    packageManager.Instance.HapusData();
                }
            }
        }

        public void LoadData(string levelsLabel)
        {
            // Load package data
            string levelDataJson = PlayerPrefs.GetString(levelsLabel, "");

            // If there is no data exists then we'll initialize
           /* if (levelDataJson == "")
            {
                levelStatus = new LevelStatus[10];      // We have 10 levels
                levelStatus[0] = new LevelStatus(1);   // Always unlock the first level      
                levelStatus[1] = new LevelStatus(0);
                levelStatus[2] = new LevelStatus(0);
                levelStatus[3] = new LevelStatus(0);
                levelStatus[4] = new LevelStatus(0);
                levelStatus[5] = new LevelStatus(0);
                levelStatus[6] = new LevelStatus(0);
                levelStatus[7] = new LevelStatus(0);
                levelStatus[8] = new LevelStatus(0);
                levelStatus[9] = new LevelStatus(0);
            }
            else
            {
                levelStatus = JsonHelper.FromJson<LevelStatus>(levelDataJson);
                // We have 4 stars if we reach a big star, but in fact just accept 3 stars per level
                int n = (levelStatus[0].starNumber == 4 ? 3 : levelStatus[0].starNumber) +
                        (levelStatus[1].starNumber == 4 ? 3 : levelStatus[1].starNumber) +
                        (levelStatus[2].starNumber == 4 ? 3 : levelStatus[2].starNumber) +
                        (levelStatus[3].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[4].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[5].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[6].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[7].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[8].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[9].starNumber == 4 ? 3 : levelStatus[4].starNumber);
            }*/
        }
    }

    [System.Serializable]
    public class Packages
    {
        public string packageLabel;
        public PackageLevels[] packageLevels;
    }

    [System.Serializable]
    public class PackageLevels
    {
        public string name;
        [HideInInspector]
        public int completeStatus = 0;      // 0 - not complete, 1 - complete
    }
}
