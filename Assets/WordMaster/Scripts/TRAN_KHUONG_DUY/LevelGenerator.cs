﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField]
        private LetterGroups[] letterGroups;

        private string currentContent;
        private string[] contentParagraphs;
        private CURRENTGROUP currentGroup;

        // The last words group is activated - Ex: 3 words, 4 words, 7 words...
        private GameObject lastWordsGroup;

        // The arrays of letters correspond to the words groups
        [SerializeField] private LetterScript[] bigSizeLetter;       // Big size - 3 words   
        [SerializeField] private LetterScript[] normalSizeLetter;     // Normal size - 4 words
        [SerializeField] private LetterScript[] tinySizeLetter;     // Normal size - 4 words
        [SerializeField] private LetterScript[] smallSizeLetter;     // Small size - 7 words

        // The arrays of empty letter block at beneath corresponding to the words groups
        private SpriteRenderer[] bigSizeMiniEmptyLetterBlock;
        private SpriteRenderer[] normalSizeMiniEmptyLetterBlock;
        private SpriteRenderer[] tinySizeMiniEmptyLetterBlock;
        private SpriteRenderer[] smallSizeMiniEmptyLetterBlock;

        // The arrays of letters in answer section correspond to the words groups
        private MiniLetterScript[] bigSizeMiniLetter;
        private MiniLetterScript[] normalSizeMiniLetter;
        private MiniLetterScript[] tinySizeMiniLetter;
        private MiniLetterScript[] smallSizeMiniLetter;

        // Variables are used to check. If words group was created so we would use it. Otherwise, we will create a new instance. 
        private bool group_3_lettersHaveAlreadyExisted = false;
        private bool group_4_lettersHaveAlreadyExisted = false;
        private bool group_5_lettersHaveAlreadyExisted = false;
        private bool group_7_lettersHaveAlreadyExisted = false;

        private List<WordInDictionary> dictionary;
        private WordInDictionary thisWord;
        string hintDataJson = "";
        public int hintLength;

        private List<UndoAction> undoActions;

        public void InitEssentialArrays()
        {
            bigSizeLetter = new LetterScript[3];
            bigSizeMiniEmptyLetterBlock = new SpriteRenderer[3];
            bigSizeMiniLetter = new MiniLetterScript[3];

            normalSizeLetter = new LetterScript[4];
            normalSizeMiniEmptyLetterBlock = new SpriteRenderer[4];
            normalSizeMiniLetter = new MiniLetterScript[4];

            tinySizeLetter = new LetterScript[5];
            tinySizeMiniEmptyLetterBlock = new SpriteRenderer[5];
            tinySizeMiniLetter = new MiniLetterScript[5];

            smallSizeLetter = new LetterScript[7];
            smallSizeMiniEmptyLetterBlock = new SpriteRenderer[7];
            smallSizeMiniLetter = new MiniLetterScript[7];

            undoActions = new List<UndoAction>();
        }

        public void InitLevel(CURRENTGROUP group, string content, string[] contentParagraphs)
        {
            currentGroup = group;
            currentContent = content;
            this.contentParagraphs = contentParagraphs;

            switch (currentGroup)
            {
                case CURRENTGROUP.G_3:
                    GenerateLevel(CURRENTGROUP.G_3, ref group_3_lettersHaveAlreadyExisted, ConstantsInGame.Tag_0,
                        bigSizeMiniEmptyLetterBlock, bigSizeMiniLetter);
                    // Make sure all letters are shown at the beginning
                    ShowLetters(bigSizeLetter);
                    LoadHintData(bigSizeMiniLetter);
                    break;
                case CURRENTGROUP.G_4:
                    GenerateLevel(CURRENTGROUP.G_4, ref group_4_lettersHaveAlreadyExisted, ConstantsInGame.Tag_1,
                        normalSizeMiniEmptyLetterBlock, normalSizeMiniLetter);
                    // Make sure all letters are shown at the beginning
                    ShowLetters(normalSizeLetter);
                    LoadHintData(normalSizeMiniLetter);
                    break;
                case CURRENTGROUP.G_5:
                    GenerateLevel(CURRENTGROUP.G_5, ref group_5_lettersHaveAlreadyExisted, ConstantsInGame.Tag_2,
                        tinySizeMiniEmptyLetterBlock, tinySizeMiniLetter);
                    // Make sure all letters are shown at the beginning
                    ShowLetters(tinySizeLetter);
                    LoadHintData(tinySizeMiniLetter);
                    break;
                case CURRENTGROUP.G_7:
                    GenerateLevel(CURRENTGROUP.G_7, ref group_7_lettersHaveAlreadyExisted, ConstantsInGame.Tag_3,
                        smallSizeMiniEmptyLetterBlock, smallSizeMiniLetter);
                    // Make sure all letters are shown at the beginning
                    ShowLetters(smallSizeLetter);
                    LoadHintData(smallSizeMiniLetter);
                    break;
            }
        }

        private void LoadHintData(MiniLetterScript[] miniLetters)
        {
            hintLength = 0;

            // Make sure all results hide when level is started
            int lengh = miniLetters.Length;
            for (int i = lengh - 1; i >= 0; i--)
            {
                if (miniLetters[i].gameObject.activeInHierarchy)
                {
                    miniLetters[i].gameObject.SetActive(false);
                }
            }

            hintDataJson = PlayerPrefs.GetString(ConstantsInGame.Dictionary_Json, "");

            // If there is no dictionary exists then we'll create a one
            if (hintDataJson == "")
            {
                dictionary = new List<WordInDictionary>();
                CreateNewWordInDictionary();
            }
            else
            {
                WordInDictionary[] tmp = JsonHelper.FromJson<WordInDictionary>(hintDataJson);
                dictionary = tmp.OfType<WordInDictionary>().ToList();
                int length = dictionary.Count;

                for (int i = length - 1; i >= 0; i--)
                {
                    // Search this word in the dictionary
                    if (dictionary[i].content == currentContent)
                    {
                        thisWord = dictionary[i];
                        int thisWordLengh = thisWord.hintList.Length;
                        for (int j = thisWordLengh - 1; j >= 0; j--)
                        {
                            if (thisWord.hintList[j] == 1)
                            {
                                hintLength++;
                                miniLetters[j].gameObject.SetActive(true);
                                miniLetters[j].IsHint = true;
                                miniLetters[j].PlayHintAnimation();
                            }
                        }
                        return;
                    }
                }

                // If there is no this word in the dictionary
                CreateNewWordInDictionary();
            }
        }

        private void CreateNewWordInDictionary()
        {
            // This word is going to be the first word
            thisWord = new WordInDictionary
            {
                content = currentContent
            };

            int sum = 0;
            int length = contentParagraphs.Length;
            for (int i = length - 1; i >= 0; i--)
            {
                sum += contentParagraphs[i].Length;
            }
            thisWord.hintList = new byte[sum];
            for (int i = sum - 1; i >= 0; i--)
            {
                thisWord.hintList[i] = 0;
            }

            dictionary.Add(thisWord);
        }

        private void ShowLetters(LetterScript[] letters)
        {
            int length = letters.Length;
            for (int i = length - 1; i >= 0; i--)
            {
                if (!letters[i].gameObject.activeInHierarchy)
                {
                    letters[i].gameObject.SetActive(true);
                }
            }
        }

        private void GenerateLevel(CURRENTGROUP group, ref bool groupHaveAlreadyExisted, string _tag, SpriteRenderer[] miniEmptyLetterBlockList, MiniLetterScript[] miniLetterList)
        {
            int index = 0;
            if (!groupHaveAlreadyExisted)
            {
                int length = letterGroups.Length;
                for (int i = length - 1; i >= 0; i--)
                {
                    // Check to get the needed group
                    if (letterGroups[i].mainSectionTransform.CompareTag(_tag))
                    {
                        index = i;
                        if (lastWordsGroup != null)
                        {
                            // Inactivate last words group
                            lastWordsGroup.SetActive(false);
                        }
                        // Get and activate new words group
                        lastWordsGroup = letterGroups[index].mainSectionTransform.parent.gameObject;
                        lastWordsGroup.SetActive(true);

                        int length_2 = letterGroups[index].wordsNumberInLevel.orderOfLetters.Length;
                        float offsetDistanceOnAxis_X = 0.0f;
                        for (int j = 0; j < length_2; j++)
                        {
                            // Generate the letters for the game
                            GenerateMainSection(index, group, j, ref offsetDistanceOnAxis_X);
                            // Generate the letters for the answer part
                            GenerateMiniSection(miniEmptyLetterBlockList, miniLetterList, j, length_2 - 1, 0, index);
                        }
                        groupHaveAlreadyExisted = true;

                        break;
                    }
                }
            }
            else
            {
                int length = letterGroups.Length;
                for (int i = length - 1; i >= 0; i--)
                {
                    if (letterGroups[i].mainSectionTransform.CompareTag(_tag))
                    {
                        index = i;
                        // Inactivate last words group
                        lastWordsGroup.SetActive(false);
                        // Get and activate new words group
                        lastWordsGroup = letterGroups[index].mainSectionTransform.parent.gameObject;
                        lastWordsGroup.SetActive(true);
                        break;
                    }
                }
            }

            // Generate alphabet for the letters and arrange the order of the letters for the answer part
            switch (group)
            {
                case CURRENTGROUP.G_3:
                    Letter[] letters_G3 = SelectAlphabetForLetter(index, ref bigSizeLetter);
                    ArrangeMiniSection(letters_G3,
                                        miniEmptyLetterBlockList, bigSizeMiniLetter,
                                        letterGroups[index].wordsNumberInLevel.miniOutsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniInsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniEmptyBlockDistance,
                                        letterGroups[index].miniSectionTransform);
                    break;
                case CURRENTGROUP.G_4:
                    Letter[] letters_G4 = SelectAlphabetForLetter(index, ref normalSizeLetter);
                    ArrangeMiniSection(letters_G4,
                                        miniEmptyLetterBlockList, normalSizeMiniLetter,
                                        letterGroups[index].wordsNumberInLevel.miniOutsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniInsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniEmptyBlockDistance,
                                        letterGroups[index].miniSectionTransform);
                    break;
                case CURRENTGROUP.G_5:
                    Letter[] letters_G5 = SelectAlphabetForLetter(index, ref tinySizeLetter);
                    ArrangeMiniSection(letters_G5,
                                        miniEmptyLetterBlockList, tinySizeMiniLetter,
                                        letterGroups[index].wordsNumberInLevel.miniOutsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniInsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniEmptyBlockDistance,
                                        letterGroups[index].miniSectionTransform);
                    break;
                case CURRENTGROUP.G_7:
                    Letter[] letters_G7 = SelectAlphabetForLetter(index, ref smallSizeLetter);
                    ArrangeMiniSection(letters_G7,
                                        miniEmptyLetterBlockList, smallSizeMiniLetter,
                                        letterGroups[index].wordsNumberInLevel.miniOutsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniInsideSprite,
                                        letterGroups[index].wordsNumberInLevel.miniEmptyBlockDistance,
                                        letterGroups[index].miniSectionTransform);
                    break;
            }
        }

        private void GenerateMainSection(int index, CURRENTGROUP group, int index2, ref float offsetDistanceOnAxis_X)
        {
            GameObject emptyLetterBlock = Instantiate(letterGroups[index].wordsNumberInLevel.emptyBlockLetterPrefab,
                                                                letterGroups[index].mainSectionTransform);

            emptyLetterBlock.transform.localPosition =
                new Vector3(letterGroups[index].wordsNumberInLevel.orderOfLetters[index2].coordinate_X *
                            letterGroups[index].wordsNumberInLevel.horizontalAxisDistance,
                            letterGroups[index].wordsNumberInLevel.orderOfLetters[index2].coordinate_Y *
                            letterGroups[index].wordsNumberInLevel.verticalAxisDistance,
                            0.0f);

            LetterScript letterBlock = Instantiate(letterGroups[index].wordsNumberInLevel.letterBlockPrefab,
                                                letterGroups[index].mainSectionTransform).GetComponent<LetterScript>();

            // Setup the destination of this letter to make appear effect
            letterBlock.Destination = new Vector3(emptyLetterBlock.transform.localPosition.x,
                                                         emptyLetterBlock.transform.localPosition.y + 0.05f,
                                                         emptyLetterBlock.transform.localPosition.z);

            // Make a little bit gap between the letters
            letterBlock.OffsetDistanceOnAxis_X = offsetDistanceOnAxis_X;
            letterBlock.Appear();

            // Get letter into the array
            switch (group)
            {
                case CURRENTGROUP.G_3:
                    bigSizeLetter[index2] = letterBlock;
                    break;
                case CURRENTGROUP.G_4:
                    normalSizeLetter[index2] = letterBlock;
                    break;
                case CURRENTGROUP.G_5:
                    tinySizeLetter[index2] = letterBlock;
                    break;
                case CURRENTGROUP.G_7:
                    smallSizeLetter[index2] = letterBlock;
                    break;
            }

            offsetDistanceOnAxis_X += 1.5f;
        }

        private Letter[] SelectAlphabetForLetter(int index, ref LetterScript[] letterArray)
        {
            int wordsListLength = letterGroups[index].wordsNumberInLevel.listOfWords.Length;
            for (int i = wordsListLength - 1; i >= 0; i--)
            {
                if (letterGroups[index].wordsNumberInLevel.listOfWords[i].content == currentContent)
                {
                    int length = letterArray.Length;
                    for (int j = length - 1; j >= 0; j--)
                    {
                        letterArray[j].Set(letterGroups[index].wordsNumberInLevel.listOfWords[i].letters[j]);
                    }
                    return (letterGroups[index].wordsNumberInLevel.listOfWords[i].letters);
                }
            }
            return null;
        }

        private void GenerateMiniSection(SpriteRenderer[] emptyLetterBlockList, MiniLetterScript[] letterList, int n, int value1, int value2, int index)
        {
            if (n == value1 || n == value2)
            {
                SpriteRenderer miniOutsideEmptyLetterBlock = Instantiate(
                                                letterGroups[index].wordsNumberInLevel.miniEmptyBlockPrefab,
                                                letterGroups[index].miniSectionTransform).GetComponent<SpriteRenderer>();

                emptyLetterBlockList[n] = miniOutsideEmptyLetterBlock;
                emptyLetterBlockList[n].sprite = letterGroups[index].wordsNumberInLevel.miniOutsideSprite;

                // Save the mini letter to the array
                letterList[n] = miniOutsideEmptyLetterBlock.GetComponentInChildren<MiniLetterScript>(true);
                letterList[n].gameObject.SetActive(false);

                if (n == value2)
                {
                    miniOutsideEmptyLetterBlock.GetComponent<SpriteRenderer>().flipY = true;
                }
            }
            else
            {
                SpriteRenderer miniInsideEmptyLetterBlock = Instantiate(
                                                    letterGroups[index].wordsNumberInLevel.miniEmptyBlockPrefab,
                                                    letterGroups[index].miniSectionTransform).GetComponent<SpriteRenderer>();

                emptyLetterBlockList[n] = miniInsideEmptyLetterBlock;
                emptyLetterBlockList[n].sprite = letterGroups[index].wordsNumberInLevel.miniInsideSprite;

                // Save the mini letter to the array
                letterList[n] = miniInsideEmptyLetterBlock.GetComponentInChildren<MiniLetterScript>(true);
                letterList[n].gameObject.SetActive(false);
            }
        }

        private void ArrangeMiniSection(Letter[] lettersList, SpriteRenderer[] emptyLetterBlockList, MiniLetterScript[] letterSprites,
                                        Sprite outside, Sprite inside, float width, Transform parent)
        {
            float pos = 0.0f;
            int n = -1;
            int stringLength = currentContent.Length;
            for (int i = 0; i < stringLength; i++)
            {
                if (currentContent[i] == ' ')
                {
                    // Take out a span
                    pos += 0.15f;
                }
                else
                {
                    n++;
                    // Set up the position for the empty letter block
                    emptyLetterBlockList[n].transform.localPosition = new Vector3(pos, 0.0f, 0.0f);
                    pos += width;

                    if (i + 1 < stringLength)
                    {
                        if (i == 0 || currentContent[i + 1] == ' ')
                        {
                            emptyLetterBlockList[n].sprite = outside;
                            // Edit a bit to look better
                            if (i == 0)
                            {
                                letterSprites[n].transform.localPosition = new Vector3(0.0f, -0.013f, 0.0f);
                            }
                            else
                            {
                                letterSprites[n].transform.localPosition = new Vector3(0.0f, 0.013f, 0.0f);
                            }
                        }
                        else
                        {
                            if (currentContent[i - 1] == ' ')
                            {
                                emptyLetterBlockList[n].sprite = outside;
                                emptyLetterBlockList[n].flipY = true;
                                // Edit a bit to look better 
                                letterSprites[n].transform.localPosition = new Vector3(0.0f, -0.013f, 0.0f);
                            }
                            else
                            {
                                emptyLetterBlockList[n].sprite = inside;
                                emptyLetterBlockList[n].flipY = false;
                            }
                        }
                    }
                    else
                    {
                        // The last letter of the row
                        // Edit a bit to look better
                        letterSprites[n].transform.localPosition = new Vector3(0.0f, 0.013f, 0.0f);
                    }

                    if (lettersList != null)
                    {
                        // Set alphabet for the mini letter
                        int lettersLength = lettersList.Length;
                        for (int j = lettersLength - 1; j >= 0; j--)
                        {
                            // Loop through the array of main letters to get sprites for mini letters
                            if (lettersList[j].Alphabet == currentContent[i] + "")
                            {
                                letterSprites[n].SetLetterSprite(lettersList[j].sprite);
                                letterSprites[n].Alphabet = currentContent[i];
                                break;
                            }
                        }
                    }
                }
            }

            // Align the answer section into the center
            parent.localPosition = new Vector3(emptyLetterBlockList[n].transform.localPosition.x / -2.0f, parent.localPosition.y, parent.localPosition.z);
        }

        private void OnEnable()
        {
            GameManager.Instance.OnShowResults += ShowResults;
            GameManager.Instance.OnHint += ShowHint;
            GameManager.Instance.OnUndo += Undo;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnShowResults -= ShowResults;
            GameManager.Instance.OnHint -= ShowHint;
            GameManager.Instance.OnUndo -= Undo;
        }

        private void ShowResults(string result)
        {
            LetterScript[] letter = null;
            MiniLetterScript[] miniLetter = null;
            switch (currentGroup)
            {
                case CURRENTGROUP.G_3:
                    letter = bigSizeLetter;
                    miniLetter = bigSizeMiniLetter;
                    break;
                case CURRENTGROUP.G_4:
                    letter = normalSizeLetter;
                    miniLetter = normalSizeMiniLetter;
                    break;
                case CURRENTGROUP.G_5:
                    letter = tinySizeLetter;
                    miniLetter = tinySizeMiniLetter;
                    break;
                case CURRENTGROUP.G_7:
                    letter = smallSizeLetter;
                    miniLetter = smallSizeMiniLetter;
                    break;
            }

            // Create a new action
            UndoAction action = new UndoAction();
            action.miniLetters = new List<MiniLetterScript>();
            action.letters = new List<LetterScript>();
            int actionIndex = 0;

            float delayTime = 0.0f;
            int resultLength = result.Length;

            // Show results
            int miniLetterLength = miniLetter.Length;
            for (int j = 0; j < miniLetterLength; j++)
            {
                if (miniLetter[j].Alphabet == result[0])
                {
                    for (int k = j; k < j + resultLength; k++)
                    {
                        miniLetter[k].gameObject.SetActive(true);
                        miniLetter[k].PlayResultAnimation(delayTime);
                        delayTime += 0.1f;
                        // Save action to undo later
                        action.miniLetters.Add(miniLetter[k]);
                    }
                    break;
                }
            }

            for (int i = 0; i < resultLength; i++)
            {
                // Run bound effect on the swiped letters
                int letterLength = letter.Length;
                for (int j = letterLength - 1; j >= 0; j--)
                {
                    if (letter[j].Alphabet == result[i] + "" && !letter[j].IsSelected && letter[j].IsPicking)
                    {
                        letter[j].IsSelected = true;
                        letter[j].Bound();
                        // Save action to undo later
                        action.letters.Add(letter[j]);
                        break;
                    }
                }
                actionIndex++;
            }

            // Add a new action to list to undo later
            undoActions.Add(action);
        }

        private void ShowHint()
        {
            MiniLetterScript[] tmp = null;
            switch (currentGroup)
            {
                case CURRENTGROUP.G_3:
                    tmp = bigSizeMiniLetter;
                    break;
                case CURRENTGROUP.G_4:
                    tmp = normalSizeMiniLetter;
                    break;
                case CURRENTGROUP.G_5:
                    tmp = tinySizeMiniLetter;
                    break;
                case CURRENTGROUP.G_7:
                    tmp = smallSizeMiniLetter;
                    break;
            }

            int i = 0;
            int k = i;
            int length = contentParagraphs.Length;

            while (true)
            {
                // Loop through each specific paragraph of current content
                for (int j = 0; j < length; j++)
                {
                    if (i < contentParagraphs[j].Length)
                    {
                        // If this character does not suggest yet then we will suggest it
                        if (thisWord.hintList[k] == 0 && !tmp[k].gameObject.activeInHierarchy)
                        {
                            thisWord.hintList[k] = 1;
                            tmp[k].gameObject.SetActive(true);
                            tmp[k].PlayHintAnimation();

                            // Save dictionary
                            hintDataJson = JsonHelper.ToJson(dictionary.ToArray());
                            PlayerPrefs.SetString(ConstantsInGame.Dictionary_Json, hintDataJson);
                            return;
                        }
                    }
                    // The index is switched to next paragraph
                    k = i + contentParagraphs[j].Length;
                }
                i++;
                k = i;
                hintLength++;

                // End if whole the characters were suggested already
                if (k == thisWord.hintList.Length - 1)
                {
                    return;
                }
            }
        }

        private bool Undo()
        {
            if (undoActions.Count > 0)
            {
                UndoAction action = undoActions[undoActions.Count - 1];
                // Get back the letters
                int letterLength = action.letters.Count;
                for (int i = letterLength - 1; i >= 0; i--)
                {
                    if (action.letters[i].gameObject.activeInHierarchy)
                    {
                        action.letters[i].gameObject.SetActive(false);
                    }
                    action.letters[i].Undo = true;
                    action.letters[i].gameObject.SetActive(true);
                }

                // Activate the hint of the mini letters
                int miniLetterLength = action.miniLetters.Count;
                for (int i = miniLetterLength - 1; i >= 0; i--)
                {
                    if (action.miniLetters[i].IsHint)
                    {
                        action.miniLetters[i].PlayHintAnimation();
                    }
                    else
                    {
                        action.miniLetters[i].gameObject.SetActive(false);
                    }
                }

                // Remove this action
                undoActions.Remove(action);
                return true;
            }
            return false;
        }

        public void RemoveUndoActions()
        {
            undoActions.Clear();
        }

        public void CloseLevel()
        {
            if(lastWordsGroup == null)
            {
                return;
            }

            if (lastWordsGroup.activeInHierarchy)
            {
                lastWordsGroup.SetActive(false);
            }
        }
    }

    [System.Serializable]
    public class LetterGroups
    {
        public WordsNumberInLevel wordsNumberInLevel;
        public Transform mainSectionTransform;
        public Transform miniSectionTransform;
    }

    [System.Serializable]
    public class WordInDictionary
    {
        public string content;      // Content of word. Ex: DOG, CAT, LION, PUMPKIN...etc.
        public byte[] hintList;     // Used to determine the order of character and state of it, 0 - not hint,  1 - hint
    }

    public class UndoAction
    {
        public List<LetterScript> letters;
        public List<MiniLetterScript> miniLetters;
    }
}
