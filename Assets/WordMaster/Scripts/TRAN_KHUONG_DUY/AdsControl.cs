﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SocialPlatforms;
#if ADS_PLUGIN
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
#endif
using System;
using TRAN_KHUONG_DUY;

public class AdsControl : MonoBehaviour
{


    protected AdsControl()
    {
    }

    private static AdsControl _instance;
    #if ADS_PLUGIN
    InterstitialAd interstitial;
    RewardBasedVideoAd rewardBasedVideo;
    BannerView bannerView;
    ShowOptions options;
#endif
    public string AdmobID_Android, AdmobID_IOS, BannerID_Android, BannerID_IOS;
    public string UnityID_Android, UnityID_IOS, UnityZoneID, yourStoreLink;


    public static AdsControl Instance { get { return _instance; } }
    #if ADS_PLUGIN
    void Awake()
    {
        if (FindObjectsOfType(typeof(AdsControl)).Length > 1)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        MakeNewInterstial();
        RequestBanner();
        if (PlayerPrefs.GetInt("RemoveAds") == 0)
            ShowBanner();
        else
            HideBanner();
        if (Advertisement.isSupported)
        { // If the platform is supported,
#if UNITY_IOS
			Advertisement.Initialize (UnityID_IOS); // initialize Unity Ads.
#endif

#if UNITY_ANDROID
            Advertisement.Initialize(UnityID_Android); // initialize Unity Ads.
#endif
        }
        options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        DontDestroyOnLoad(gameObject); //Already done by CBManager


    }


    public void HandleInterstialAdClosed(object sender, EventArgs args)
    {

        if (interstitial != null)
            interstitial.Destroy();
        MakeNewInterstial();



    }

    void MakeNewInterstial()
    {


#if UNITY_ANDROID
        interstitial = new InterstitialAd(AdmobID_Android);
#endif
#if UNITY_IPHONE
		interstitial = new InterstitialAd (AdmobID_IOS);
#endif
        interstitial.OnAdClosed += HandleInterstialAdClosed;
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);


    }


    public void showAds()
    {
        if (PlayerPrefs.GetInt("RemoveAds") == 0)
        {
            if (interstitial.IsLoaded())
                interstitial.Show();
            else if (Advertisement.IsReady())
                Advertisement.Show();

        }
    }


    public bool GetRewardAvailable()
    {
        bool avaiable = false;
        avaiable = Advertisement.IsReady();
        return avaiable;
    }

    public void ShowRewardVideo()
    {

        Advertisement.Show(UnityZoneID, options);


    }


    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = BannerID_Android;
#elif UNITY_IPHONE
		string adUnitId = BannerID_IOS;
#else
		string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);

    }

    public void ShowBanner()
    {
        if (PlayerPrefs.GetInt("RemoveAds") == 0)
            bannerView.Show();
    }

    public void HideBanner()
    {
        bannerView.Hide();
    }



    public void ShowFB()
    {
        Application.OpenURL("https://www.facebook.com/PonyStudio2507/?ref=settings");
    }

    public void RateMyGame()
    {

        if (PlayerPrefs.GetInt("Rate") == 0)
        {
            Application.OpenURL(yourStoreLink);
            GameManager.Instance.MoreCoin(100);
            PlayerPrefs.SetInt("Rate", 1);
    }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                GameManager.Instance.MoreCoin(50);
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }
    #endif
}

