﻿using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    [CreateAssetMenu(fileName = "New Words Group", menuName = "Words Group")]
    public class WordsNumberInLevel : ScriptableObject
    {
        public GameObject emptyBlockLetterPrefab;
        public GameObject letterBlockPrefab;

        public float verticalAxisDistance = 1.54f;
        public float horizontalAxisDistance = 1.42f;

        public GameObject miniEmptyBlockPrefab;
        public float miniEmptyBlockDistance = 0.58f;

        public Sprite miniOutsideSprite;
        public Sprite miniInsideSprite;

        public OrderOfLetters[] orderOfLetters;
        public Words[] listOfWords;
    }

    [System.Serializable]
    public class OrderOfLetters
    {
        public float coordinate_X;
        public float coordinate_Y;
    }

    [System.Serializable]
    public class Words
    {
        public string content;
        public Letter[] letters;
    }

    [System.Serializable]
    public class Letter
    {
        public LETTER alphabet;
        public Sprite sprite;

        // Switch enum value to a string for the later easier processing
        public string Alphabet
        {
            get { return alphabet + ""; }
            private set { }
        }
    }
}
