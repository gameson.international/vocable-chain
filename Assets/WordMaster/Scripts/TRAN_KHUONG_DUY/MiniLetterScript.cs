﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    public class MiniLetterScript : MonoBehaviour
    {
        [SerializeField]
        private Animation anim;

        public char Alphabet { get; set; }

        private SpriteRenderer letterBlockSprite;
        private SpriteRenderer letterSprite;

        public bool IsHint { get; set; }

        private void OnEnable()
        {
            IsHint = false;
        }

        // Check if sprites is null
        private void SpriteIsNull()
        {
            if (letterSprite == null)
            {
                letterSprite = GetComponentsInChildren<SpriteRenderer>()[1];
            }
            if (letterBlockSprite == null)
            {
                letterBlockSprite = GetComponent<SpriteRenderer>();
            }
        }

        public void SetLetterSprite(Sprite sprite)
        {
            SpriteIsNull();
            letterSprite.sprite = sprite;
        }

        public void PlayHintAnimation()
        {
            anim.Play(ConstantsInGame.Hint_Anim_Big);
            anim.Play(ConstantsInGame.Hint_Anim_Normal);
            anim.Play(ConstantsInGame.Hint_Anim_Small);
            anim.Play(ConstantsInGame.Hint_Anim_Tiny);
        }

        public void PlayResultAnimation(float delayTime)
        {
            if (!IsHint)
            {
                // Turn off sprites to make delay effect
                SpriteIsNull();
                letterBlockSprite.enabled = false;
                letterSprite.enabled = false;
            }

            Invoke("PlayResult", delayTime);
        }

        private void PlayResult()
        {
            if (!IsHint)
            {
                letterBlockSprite.enabled = true;
                letterSprite.enabled = true;
            }

            AnimationClip b = anim.GetClip(ConstantsInGame.Result_Anim_Big);
            if(b != null)
            {
                anim.Play(b.name);
            }
            AnimationClip n = anim.GetClip(ConstantsInGame.Result_Anim_Normal);
            if (n != null)
            {
                anim.Play(n.name);
            }
            AnimationClip s = anim.GetClip(ConstantsInGame.Result_Anim_Small);
            if (s != null)
            {
                anim.Play(s.name);
            }
            AnimationClip t = anim.GetClip(ConstantsInGame.Result_Anim_Tiny);
            if (t != null)
            {
                anim.Play(t.name);
            }

            /*anim.Play(ConstantsInGame.Result_Anim_Big);
            anim.Play(ConstantsInGame.Result_Anim_Normal);
            anim.Play(ConstantsInGame.Result_Anim_Small);
            anim.Play(ConstantsInGame.Result_Anim_Tiny);*/
        }
    }
}
