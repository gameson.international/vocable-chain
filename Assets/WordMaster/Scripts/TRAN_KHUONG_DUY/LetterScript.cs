﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace TRAN_KHUONG_DUY
{
    public class LetterScript : MonoBehaviour
    {
        [SerializeField]
        private LETTER alphabet;

        [SerializeField]
        private GameObject normal;      // The letter at the normal state
        [SerializeField]
        private GameObject highLight;   // The letter at highlight state

        [Space(10)]
        [SerializeField]
        private float movingHorizontalVelocity = 25.0f;
        private float startTime;
        private float timeToMove;
        [SerializeField]
        private float firstAxisXValueOnRightSide;
        private Vector3 startPos;
        private bool isMovingHorizontal;
        private bool setUpEverything = false;

        [SerializeField]
        private float rotatingVelocity = 1.0f;
        private bool isRotating = false;

        [Space(10)]
        [SerializeField]
        private Rigidbody2D rigid2D;
        [SerializeField]
        private float maxVelocityUp = 1.0f;
        [SerializeField]
        private float minVelocityUp = 1.0f;
        [SerializeField]
        private float velocityRight = 1.0f;

        [SerializeField] private SpriteRenderer normalLetterRender;
        [SerializeField] private SpriteRenderer highLightLetterRender;

        private CircleCollider2D circleCollider2D;     // Collider is used to scan around the letter to find out the closest other letter
        private int layerMaskLetter;

        public Vector3 Destination { get; set; }
        public float OffsetDistanceOnAxis_X { get; set; }

        private SortingGroup[] sortingGroups;
        private int sortingGroupOrder;

        public bool IsPicking { get; set; }
        public bool IsSelected { get; set; }

        private LetterScript letterAbove = null;
        private Vector3 positionBelow;
        public bool IsBouncing { get; private set; }

        [Space(10)]
        [SerializeField]
        private float movingDownVelocity = 10.0f;
        private Vector3 beforeFalling;
        private Vector3 afterFalling;
        private bool isMovingDown;

        public bool Undo { get; set; }
        public bool IsMovingHorizontal
        {
            get { return isMovingHorizontal; }
        }

        // Switch enum value to a string for the later easier processing
        public string Alphabet
        {
            get { return alphabet + ""; }
        }

        private void OnEnable()
        {
            // Disable gravity
            rigid2D.gravityScale = 0.0f;

            ChangeState(false);

            if (Undo)
            {
                Undo = false;
                // If 'letterAbove' != null then make it back the prevous position
                if (letterAbove != null)
                {
                    if (!letterAbove.IsBouncing && !letterAbove.IsMovingHorizontal)
                    {
                        letterAbove.transform.localPosition = letterAbove.Destination;
                        letterAbove.CheckLetterAbove();
                    }
                }
            }

            // Prevent error if we not ready for moving letter yet
            if (setUpEverything)
            {
                Appear();
            }

            CheckTheme();
        }

        private void CheckTheme()
        {
            normal.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.GetComponent<ThemeManager>().GetTheme().WordButton;
            highLight.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.GetComponent<ThemeManager>().GetTheme().Highlight;
        }

        private void CheckLetterAbove()
        {
            if (letterAbove != null)
            {
                letterAbove.transform.localPosition = letterAbove.Destination;
                letterAbove.CheckLetterAbove();
            }
        }

        private void OnDisable()
        {
            transform.localPosition = startPos;
            transform.localRotation = Quaternion.identity;
            isRotating = false;
            IsPicking = false;
            IsSelected = false;
            Undo = false;
        }

        private void Start()
        {
            circleCollider2D = GetComponent<CircleCollider2D>();
            layerMaskLetter = LayerMask.GetMask("Letter");
        }

        private void Update()
        {
            // Appear
            if (isMovingHorizontal)
            {
                float percentage = (Time.time - startTime) / timeToMove;
                transform.localPosition = Vector3.Lerp(startPos, Destination, percentage);

                if (percentage >= 1.0f)
                {
                    isMovingHorizontal = false;

                    // Set order in layer of sorting Groups to default 
                    int length = sortingGroups.Length;
                    for (int i = length - 1; i >= 0; i--)
                    {
                        sortingGroups[i].sortingOrder = sortingGroupOrder;
                    }
                }
            }
            else
            {
                // Inactivate if the letter become invisible
                Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
                if (!GeometryUtility.TestPlanesAABB(planes, normalLetterRender.bounds))
                {
                    gameObject.SetActive(false);
                }
            }

            // Bounce
            if (isRotating)
            {
                transform.Rotate(0.0f, 0.0f, rotatingVelocity);
            }

            // Falling Down
            if (isMovingDown)
            {
                float percentage = (Time.time - startTime) / timeToMove;
                transform.position = Vector3.Lerp(beforeFalling, afterFalling, percentage);

                if (percentage >= 1.0f)
                {
                    isMovingDown = false;
                }
            }
        }

        public void Appear()
        {
            if (sortingGroups == null)
            {
                // Cache sorting groups
                sortingGroups = GetComponentsInChildren<SortingGroup>(true);
                sortingGroupOrder = sortingGroups[0].sortingOrder;
            }

            // Set order in layer of sorting Groups higher 
            int length = sortingGroups.Length;
            for (int i = length - 1; i >= 0; i--)
            {
                sortingGroups[i].sortingOrder = sortingGroupOrder + 1;
            }

            if (Destination.x > 0)
            {
                startPos = new Vector3(firstAxisXValueOnRightSide + OffsetDistanceOnAxis_X, Destination.y, Destination.z);
            }
            else
            {
                startPos = new Vector3(-firstAxisXValueOnRightSide - OffsetDistanceOnAxis_X, Destination.y, Destination.z);
            }

            startTime = Time.time;
            timeToMove = Vector3.Distance(startPos, Destination) / movingHorizontalVelocity;
            isMovingHorizontal = true;

            // Now we can move letter whenever it appears
            if (!setUpEverything)
            {
                setUpEverything = true;
            }

            IsBouncing = false;
        }

        public void Set(Letter _letter)
        {
            alphabet = _letter.alphabet;

            if (normalLetterRender == null)
            {
                normalLetterRender = normal.GetComponentsInChildren<SpriteRenderer>(true)[1];
            }
            if (highLightLetterRender == null)
            {
                highLightLetterRender = highLight.GetComponentsInChildren<SpriteRenderer>(true)[1];
            }
            normalLetterRender.sprite = highLightLetterRender.sprite = _letter.sprite;
        }

        public void HighLightLetter()
        {
            ChangeState(true);
        }

        public void NormalLetter()
        {
            ChangeState(false);
        }

        private void ChangeState(bool highLightState)
        {
            normal.SetActive(!highLightState);
            highLight.SetActive(highLightState);
        }

        public bool CheckValidity(Collider2D other)
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, circleCollider2D.radius, layerMaskLetter);

            // Get the letter above this letter
            letterAbove = null;
            for (int i = col.Length - 1; i >= 0; i--)
            {
                if (col[i].transform.position.y > transform.position.y && col[i].transform.position.x == transform.position.x)
                {
                    letterAbove = col[i].GetComponent<LetterScript>();
                    break;
                }
            }

            // Check out the validity of the letters around this letter 
            for (int i = col.Length - 1; i >= 0; i--)
            {
                if (col[i] == other)
                {
                    return true;
                }
            }

            return false;
        }

        public void Bound()
        {
            // Increase order in layer of sorting Groups
            int length = sortingGroups.Length;
            for (int i = length - 1; i >= 0; i--)
            {
                sortingGroups[i].sortingOrder = sortingGroupOrder + 1;
            }

            // Enable gravity
            rigid2D.gravityScale = 3.5f;
            rigid2D.velocity = new Vector2(Random.Range(-velocityRight, velocityRight),
                                            Random.Range(minVelocityUp, maxVelocityUp));

            IsBouncing = true;
            isRotating = true;
            isMovingDown = false;

            // Notify for the letter above falling down
            if (letterAbove != null)
            {
                // The falling position of the letter above is this letter position
                letterAbove.positionBelow = transform.position;
                letterAbove.FallingDown();
            }
        }

        public void FallingDown()
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(transform.position, circleCollider2D.radius, layerMaskLetter);

            // Get the letter above this letter
            letterAbove = null;
            for (int i = col.Length - 1; i >= 0; i--)
            {
                if (col[i].transform.position.y > transform.position.y && col[i].transform.position.x == transform.position.x)
                {
                    letterAbove = col[i].GetComponent<LetterScript>();
                    break;
                }
            }

            // If this letter does not bounce, we are going to make it moves down
            if (!IsBouncing)
            {
                beforeFalling = transform.position;
                afterFalling = positionBelow;
                startTime = Time.time;
                timeToMove = Vector3.Distance(beforeFalling, afterFalling) / movingDownVelocity;
                isMovingDown = true;

                //
                if (letterAbove != null)
                {
                    // The falling position of the letter above
                    letterAbove.positionBelow = positionBelow + (letterAbove.transform.position - beforeFalling);
                    letterAbove.FallingDown();
                }
            }
            else
            {
                if (letterAbove != null)
                {
                    // The falling position of the letter above is the position where this letter assume to fall down
                    letterAbove.positionBelow = positionBelow;
                    letterAbove.FallingDown();
                }
            }
        }
    }
}
