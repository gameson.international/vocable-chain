﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TRAN_KHUONG_DUY
{
    public class Package : MonoBehaviour
    {
        private GameManager gameManager;

        public bool packageIsUnlocked = false;

        [Header("UI")]
        public Button button;
        public GameObject scoreBg;
        public GameObject lockIcon;
        public TMP_Text scoreTxt;
        public string packageLabel;
        public string levelsLabel;

        [Header("Setup Level")]
        public LevelContent[] levelContents;

        private LevelStatus[] levelStatus;

        private void Start()
        {
            gameManager = GameManager.Instance;
            CheckPackage();
        }

        private void CheckPackage()
        {

        }

        private void OnEnable()
        {
            // Check if the package is unclocked or not
            if (!packageIsUnlocked)
            {
                if (PlayerPrefs.GetInt(packageLabel, 0) == 0)
                {
                    button.interactable = false;
                    scoreBg.SetActive(false);
                    lockIcon.SetActive(true);
                }
                else
                {
                    button.interactable = true;
                    scoreBg.SetActive(true);
                    lockIcon.SetActive(false);

                    LoadData();
                }
            }
            else
            {
                button.interactable = true;
                scoreBg.SetActive(true);
                lockIcon.SetActive(false);

                LoadData();
            }
        }

        private void LoadData()
        {
            // Load package data
            string levelDataJson = PlayerPrefs.GetString(levelsLabel, "");

            // If there is no data exists then we'll initialize
            if (levelDataJson == "")
            {
                levelStatus = new LevelStatus[10];      // We have 10 levels
                levelStatus[0] = new LevelStatus(1);   // Always unlock the first level      
                levelStatus[1] = new LevelStatus(0);
                levelStatus[2] = new LevelStatus(0);
                levelStatus[3] = new LevelStatus(0);
                levelStatus[4] = new LevelStatus(0);
                levelStatus[5] = new LevelStatus(0);
                levelStatus[6] = new LevelStatus(0);
                levelStatus[7] = new LevelStatus(0);
                levelStatus[8] = new LevelStatus(0);
                levelStatus[9] = new LevelStatus(0);

                scoreTxt.text = "0/30";
            }
            else
            {
                levelStatus = JsonHelper.FromJson<LevelStatus>(levelDataJson);
                // We have 4 stars if we reach a big star, but in fact just accept 3 stars per level
                int n = (levelStatus[0].starNumber == 4 ? 3 : levelStatus[0].starNumber) +
                        (levelStatus[1].starNumber == 4 ? 3 : levelStatus[1].starNumber) +
                        (levelStatus[2].starNumber == 4 ? 3 : levelStatus[2].starNumber) +
                        (levelStatus[3].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[4].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[5].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[6].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[7].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[8].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[9].starNumber == 4 ? 3 : levelStatus[4].starNumber);
                scoreTxt.text = n + "/30";
            }
        }

        // Package button on Package is clicked 
        public void PackageButton_Onclick()
        {
            gameManager.PackageButton_Onclick(packageLabel, levelsLabel, levelContents);
        }
    }

    [System.Serializable]
    public class LevelStatus
    {
        // 0 - locked, 1 - unlocked
        public int unlock = 0;
        // 0 - zero, 1 - one, 2 - two, 3 - three, 4 - three stars and have big one
        public int starNumber = 0;
        public int scoreNumber = 0;
        // Move records is used to complete the level
        public int moveRecord = 0;

        public LevelStatus(int unlock)
        {
            this.unlock = unlock;
        }
    }

    [System.Serializable]
    public class LevelContent
    {
        public CURRENTGROUP group;
        public string content;
        public LevelStatus status;
    }
}

