﻿
namespace TRAN_KHUONG_DUY
{
    public enum LETTER
    {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
    }

    public enum CURRENTGROUP
    {
        G_3,
        G_4,
        G_5,
        G_7,
    }
}
