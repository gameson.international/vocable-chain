﻿
namespace TRAN_KHUONG_DUY
{
    public class ConstantsInGame
    {
        #region TAGS

        public const string Tag_0 = "3 Words";
        public const string Tag_1 = "4 Words";
        public const string Tag_2 = "5 Words";
        public const string Tag_3 = "7 Words";

        #endregion

        #region Keys of PlayerPrefs

        public const string Dictionary_Json = "DictionaryJson";
        public const string Coin = "Coin";
        public const string Background = "Background";
        public const string BGM = "BGM";
        public const string SFX = "SFX";
        public const string Chest = "chest";

        #endregion

        #region Animation names

        public const string Hint_Anim_Big = "hint_appear_Big";
        public const string Result_Anim_Big = "result_appear_Big";

        public const string Hint_Anim_Normal = "hint_appear_Normal";
        public const string Result_Anim_Normal = "result_appear_Normal";

        public const string Hint_Anim_Small = "hint_appear_Small";
        public const string Result_Anim_Small = "result_appear_Small";

        public const string Hint_Anim_Tiny = "hint_appear_Tiny";
        public const string Result_Anim_Tiny = "result_appear_Tiny";
        #endregion
    }
}
