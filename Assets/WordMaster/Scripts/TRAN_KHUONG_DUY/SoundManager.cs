﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Instance { get; private set; }

        public AudioSource menuBG;
        public AudioSource ingameBG;
        public AudioSource btnClick;
        public AudioSource btnPlay;
        public AudioSource btnHint;
        public AudioSource sfxBlockDrag;
        public AudioSource sfxWrongAnswer;
        public AudioSource sfxWordlistComplete;
        public AudioSource sfxFontDown;
        public AudioSource sfxGradeStar;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
