﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace TRAN_KHUONG_DUY
{
    [CustomEditor(typeof(WordsNumberInLevel))]
    public class WordsNumberInLevelEditor : Editor
    {
        private WordsNumberInLevel wordsNumberInLevel;
        private SerializedProperty orderOfLetters;
        private SerializedProperty listOfWords;

        private bool cache = false;
        private bool expandOrderOfLetters = true;
        private bool expandOrderListOfWords = true;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (!cache)
            {
                cache = true;
                wordsNumberInLevel = (WordsNumberInLevel)target;
                orderOfLetters = serializedObject.FindProperty("orderOfLetters");
                listOfWords = serializedObject.FindProperty("listOfWords");
            }

            wordsNumberInLevel.emptyBlockLetterPrefab = (GameObject)EditorGUILayout.ObjectField("Empty Letter Block",
                                                    wordsNumberInLevel.emptyBlockLetterPrefab, typeof(GameObject), true);

            wordsNumberInLevel.letterBlockPrefab = (GameObject)EditorGUILayout.ObjectField("Letter Block",
                                                    wordsNumberInLevel.letterBlockPrefab, typeof(GameObject), true);

            GUILayout.Space(10.0f);
            wordsNumberInLevel.verticalAxisDistance = EditorGUILayout.FloatField(new GUIContent("Vertical Distance",
                                                        "The distance between letters on the vertical axis"),
                                                        wordsNumberInLevel.verticalAxisDistance);

            wordsNumberInLevel.horizontalAxisDistance = EditorGUILayout.FloatField(new GUIContent("Horizontal Distance",
                                                        "The distance between letters on the horizontal axis"),
                                                        wordsNumberInLevel.horizontalAxisDistance);

            GUILayout.Space(10.0f);
            wordsNumberInLevel.miniEmptyBlockPrefab = (GameObject)EditorGUILayout.ObjectField("Mini Empty Letter Block",
                                                        wordsNumberInLevel.miniEmptyBlockPrefab, typeof(GameObject), true);

            wordsNumberInLevel.miniEmptyBlockDistance = EditorGUILayout.FloatField(new GUIContent("Mini Empty Block Distance",
                                                        "The distance between mini empty blocks"),
                                                        wordsNumberInLevel.miniEmptyBlockDistance);

            GUILayout.Space(10.0f);
            wordsNumberInLevel.miniOutsideSprite = (Sprite)EditorGUILayout.ObjectField("Mini Outside Sprite",
                                                        wordsNumberInLevel.miniOutsideSprite, typeof(Sprite), true);

            wordsNumberInLevel.miniInsideSprite = (Sprite)EditorGUILayout.ObjectField("Mini Inside Sprite",
                                                        wordsNumberInLevel.miniInsideSprite, typeof(Sprite), true);

            #region Order Of Letters

            GUILayout.Space(20.0f);

            GUIStyle orderOfLettersLabelStyle = new GUIStyle();
            orderOfLettersLabelStyle.richText = true;
            GUILayout.Label("<i>ORDER OF LETTERS</i>", orderOfLettersLabelStyle);

            GUILayout.BeginVertical(EditorStyles.helpBox);

            GUIStyle style = new GUIStyle(GUI.skin.button);
            style.margin.left = 30;
            style.margin.top = 10;
            style.margin.bottom = 10;
            style.richText = true;
            if (!expandOrderOfLetters)
            {
                if (GUILayout.Button(new GUIContent("➥ <b>Expand</b>"), style, GUILayout.Width(80.0f)))
                {
                    expandOrderOfLetters = true;
                }
            }
            else
            {
                if (GUILayout.Button(new GUIContent("➦ <b>Collapse</b>"), style, GUILayout.Width(80.0f)))
                {
                    expandOrderOfLetters = false;
                }
            }
            ShowArrayProperty(orderOfLetters, expandOrderOfLetters, "Letter");
            GUILayout.EndVertical();

            #endregion

            #region List Of Words

            GUILayout.Space(20.0f);

            GUIStyle listOfWordsLabelStyle = new GUIStyle();
            listOfWordsLabelStyle.richText = true;
            GUILayout.Label("<i>LIST OF WORDS</i>", listOfWordsLabelStyle);

            GUILayout.BeginVertical(EditorStyles.helpBox);

            GUIStyle style2 = new GUIStyle(GUI.skin.button);
            style2.margin.left = 30;
            style2.margin.top = 10;
            style2.margin.bottom = 10;
            style2.richText = true;
            if (!expandOrderListOfWords)
            {
                if (GUILayout.Button(new GUIContent("➥ <b>Expand</b>"), style2, GUILayout.Width(80.0f)))
                {
                    expandOrderListOfWords = true;
                }
            }
            else
            {
                if (GUILayout.Button(new GUIContent("➦ <b>Collapse</b>"), style, GUILayout.Width(80.0f)))
                {
                    expandOrderListOfWords = false;
                }
            }
            ShowArrayProperty(listOfWords, expandOrderListOfWords, "Words");

            GUILayout.EndVertical();

            #endregion

            serializedObject.ApplyModifiedProperties();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(wordsNumberInLevel);
            }
        }

        public void ShowArrayProperty(SerializedProperty list, bool toggle, string label = "Element ")
        {
            EditorGUI.indentLevel += 1;
            if (toggle)
            {
                //EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                for (int i = 0; i < list.arraySize; i++)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i),
                    new GUIContent(label + " " + (i + 1).ToString()), true);

                    GUIStyle style = new GUIStyle(GUI.skin.button);
                    style.margin.top = 0;
                    if (GUILayout.Button(new GUIContent("-", "Delete this element"), style, GUILayout.Width(20.0f)))
                    {
                        list.DeleteArrayElementAtIndex(i);
                    }

                    EditorGUILayout.EndHorizontal();

                    if (list == listOfWords)
                    {
                        GUILayout.Space(5.0f);
                    }
                }

                GUILayout.Space(10.0f);
                GUI.backgroundColor = Color.grey;
                GUIStyle style2 = new GUIStyle(GUI.skin.button);
                style2.richText = true;
                if (GUILayout.Button(new GUIContent("<b>Add new</b>"), style2))
                {
                    list.arraySize += 1;
                }
                GUI.backgroundColor = Color.white;

                EditorGUI.indentLevel -= 1;
                if (list.arraySize == 0)
                {
                    EditorGUILayout.HelpBox("List is empty!", MessageType.Error, true);
                }
                else
                {
                    EditorGUILayout.HelpBox("The list contains " + list.arraySize + " elements!", MessageType.Info, true);
                }
                EditorGUI.indentLevel += 1;
            }
            EditorGUI.indentLevel -= 1;
        }
    }
}
