﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    [CustomPropertyDrawer(typeof(OrderOfLetters))]
    public class OrderOfLettersDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            Rect contentPosition = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var indent = EditorGUI.indentLevel;

            float half = contentPosition.width / 2;

            EditorGUIUtility.labelWidth = 16f;
            contentPosition.width *= 0.5f;
            contentPosition.width -= 5.0f;
            EditorGUI.indentLevel = 0;

            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("coordinate_X"),
                                    new GUIContent("X", "Coordinate of X Axis"));

            contentPosition.x += half;

            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("coordinate_Y"),
                                    new GUIContent("Y", "Coordinate of Y Axis"));

            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}
