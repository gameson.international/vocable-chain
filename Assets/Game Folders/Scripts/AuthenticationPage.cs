using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using System;
using TMPro;
using UnityEngine.UI;
using TRAN_KHUONG_DUY;

public class AuthenticationPage : MonoBehaviour
{
    [SerializeField] private TMP_Text label_loading;
    [SerializeField] GameObject panel_loading;
    [SerializeField] private Image loading_bar;
    [SerializeField] private GameObject panel_fake_loading;

    private float timer = 0f;


    private void Update()
    {
        timer += Time.deltaTime /5f;
        loading_bar.fillAmount = timer;

        if(timer >=1f)
        {
            GameManager.Instance.BackToHomeButton_Onclick();
            gameObject.SetActive(false);
        }
    }

    public void SetLabel(string txt)
    {
        label_loading.text = txt;
    }
}
