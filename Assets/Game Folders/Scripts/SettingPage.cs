using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class SettingPage : MonoBehaviour
{
    [SerializeField] private Button b_reset;

    [SerializeField] private GameObject panel_confirmation;
    [SerializeField] private Button b_yes;
    [SerializeField] private Button b_no;

    [SerializeField] private TMP_Text label_version;
    //[SerializeField] private TMP_Text adInfo;

    private void Start()
    {
        b_reset.onClick.AddListener(() => panel_confirmation.SetActive(true));

        b_no.onClick.AddListener(() => panel_confirmation.SetActive(false));
        b_yes.onClick.AddListener(ResetGameData);

        label_version.text = $"Version : {Application.version}";
    }
    private void OnEnable()
    {
        //adInfo.text = $"Admob : {(AdsManager.Instance.GetRewardedAdInfo().CanShowAd() ? "Ready" : "Not Ready")}/n Ironsource : {(ISManager.Instance.GetCanShowAd()?"Ready":"Not Ready")}";
    }

    private void ResetGameData()
    {
        panel_confirmation.SetActive(false);
        PlayerPrefs.DeleteAll();

        packageManager.Instance.HapusData();
        GameManager.Instance.BackToHomeFromSetting();

        Application.Quit();
    }
}