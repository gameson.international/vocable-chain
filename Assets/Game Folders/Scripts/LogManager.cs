using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogManager : MonoBehaviour
{
    public static LogManager Instance;

    [SerializeField] private TMP_Text label_log;
    [SerializeField] private Transform content;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void AddLog(string value)
    {
        TMP_Text newLog = Instantiate(label_log, content);
        newLog.text = value;
    }
}
