using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Themes" , menuName = "New/Themes")]
public class Tema : ScriptableObject
{
    public string nama;

    [Header("Background")]
    public Sprite BG;

    [Header("Menu Button")]
    public Sprite ButtonPlay;
    public Sprite ButtonSetting;
    public Sprite ButtonShop;
    public Sprite ButtonLink;
    public Sprite ButtonLeaderboard;

    [Header("In Game Button")]
    public Sprite ButtonPlus;
    public Sprite ButtonAds;
    public Sprite ButtonHint;
    public Sprite ButtonBack;

    [Header("Panel Result")]
    public Sprite Panel;
    public Sprite StarsHolder;
    public Sprite Star;
    public Sprite ButtonReplay;
    public Sprite ButtonPackage;
    public Sprite ButtonNext;

    [Header("Prefab Games")]
    public Sprite WordButton;
    public Sprite WordPlace;
    public Sprite Highlight;
}
