using com.adjust.sdk;
using OldScript;
using System;
using System.Collections;
using System.Collections.Generic;
using TRAN_KHUONG_DUY;
using UnityEngine;
public class ISManager : MonoBehaviour
{
    public static ISManager Instance;

    //Banner
    [SerializeField] private IronSourceBannerSize bannerSize;
    [SerializeField] private IronSourceBannerPosition bannerPosition;

    public bool GetCanShowAd() { return IronSource.Agent.isRewardedVideoAvailable(); }

    //static string
    public static string FreeHint = "Hint";
    public static string DailySpin = "Spin";
    public static string ChangeName = "Name";
    public static string NextLevel = "Next Level";
    public static string RewardedAdID = "DefaultRewardedVideo";

    public delegate void AdsRewardSuccessDelegate(string name);
    public event AdsRewardSuccessDelegate OnAdRewardSuccess;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

#if UNITY_ANDROID
        string appKey = "19e606f05";
#else
        string appKey = "1b092713d";
#endif
        IronSource.Agent.validateIntegration();
        IronSource.Agent.init(appKey);



        GameManager.Instance.GetGameServices().onGetDataServer += ISManager_onGetDataServer;
    }

    private void OnEnable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent += IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedVideoOnAdRewardedEvent;
        IronSourceRewardedVideoEvents.onAdClickedEvent += IronSourceRewardedVideoEvents_onAdClickedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent += IronSourceInterstitialEvents_onAdClosedEvent;
        IronSourceInterstitialEvents.onAdLoadFailedEvent += IronSourceInterstitialEvents_onAdLoadFailedEvent;
        IronSourceInterstitialEvents.onAdClickedEvent += IronSourceInterstitialEvents_onAdClickedEvent;

        IronSourceEvents.onRewardedVideoAdLoadFailedEvent += IronSourceEvents_onRewardedVideoAdLoadFailedEvent;

    }
    private void ISManager_onGetDataServer(DataServer dataServer)
    {
        RewardedAdID = dataServer.AdUnitIdIronsourceRewardedID;
        IronSource.Agent.loadRewardedVideo();
        IronSource.Agent.loadInterstitial();
    }

    private void IronSourceEvents_onRewardedVideoAdLoadFailedEvent(IronSourceError obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsShowFailedToken);
    }

    private void OnDisable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent -= IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent -= RewardedVideoOnAdRewardedEvent;
        IronSourceRewardedVideoEvents.onAdClickedEvent -= IronSourceRewardedVideoEvents_onAdClickedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent -= IronSourceInterstitialEvents_onAdClosedEvent;
        IronSourceInterstitialEvents.onAdLoadFailedEvent -= IronSourceInterstitialEvents_onAdLoadFailedEvent;
        IronSourceInterstitialEvents.onAdClickedEvent -= IronSourceInterstitialEvents_onAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdLoadFailedEvent -= IronSourceEvents_onRewardedVideoAdLoadFailedEvent;
        GameManager.Instance.GetGameServices().onGetDataServer -= ISManager_onGetDataServer;
    }

    private void IronSourceEvents_onSdkInitializationCompletedEvent()
    {
        LoadAds();

        //IronSource.Agent.loadBanner(IronSourceBannerSize.SMART, bannerPosition);
        //IronSource.Agent.displayBanner();
    }
    public void LoadAds()
    {
        IronSource.Agent.loadRewardedVideo();
        IronSource.Agent.loadInterstitial();
    }
    private void RewardedVideoOnAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
    {
        //OnAdRewardSuccess?.Invoke(placement.getPlacementName());
        IronSource.Agent.loadRewardedVideo();
        

        //if (placement.getPlacementName() == DailySpin)
        //{
        //    AdjustEvent adjustEvent = new AdjustEvent("q1uodg");
        //    Adjust.trackEvent(adjustEvent);
        //}
        //if (placement.getPlacementName() == FreeHint)
        //{
        //    AdjustEvent adjustEvent = new AdjustEvent("z2vlvc");
        //    Adjust.trackEvent(adjustEvent);
        //}
        //if (placement.getPlacementName() == ChangeName)
        //{
        //    AdjustEvent adjustEvent = new AdjustEvent("g6lc8j");
        //    Adjust.trackEvent(adjustEvent);
        //}
    }

    private void IronSourceRewardedVideoEvents_onAdClickedEvent(IronSourcePlacement arg1, IronSourceAdInfo arg2)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsClickedToken);
    }

    private void IronSourceInterstitialEvents_onAdClosedEvent(IronSourceAdInfo info)
    {
        IronSource.Agent.loadInterstitial();
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsShowToken);
    }

    private void IronSourceInterstitialEvents_onAdClickedEvent(IronSourceAdInfo obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsClickedToken);
    }

    private void IronSourceInterstitialEvents_onAdLoadFailedEvent(IronSourceError obj)
    {
        IronSource.Agent.loadInterstitial();

        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsShowFailedToken);
    }

    public void RequestRewardedAds(string adjustAdsToken)
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            GameManager.Instance.HintBtn_Onclick();
            return;
        }

        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo();
            OnAdRewardSuccess?.Invoke(adjustAdsToken);
            if (!AdsManager.Instance.GetRewardedAdInfo().CanShowAd())
            {
                AdsManager.Instance.LoadRewardedAds();

            }
            GameManager.Instance.SendAdjustEvent(adjustAdsToken);
        }
        else
        {
            AdsManager.Instance.RequestRewardedAds(adjustAdsToken , OnGetReward);
        }
    }

    private void OnGetReward(string obj)
    {

    }

    public void RequestInterstitial()
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

        if (IronSource.Agent.isInterstitialReady())
        {
            IronSource.Agent.showInterstitial();
        }
        else
        {
            IronSource.Agent.loadInterstitial();
        }
    }
}
