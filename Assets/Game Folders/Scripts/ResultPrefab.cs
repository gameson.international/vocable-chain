using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;

public class ResultPrefab : MonoBehaviour
{
    [SerializeField] private TMP_Text label_poin;
    [SerializeField] private TMP_Text label_coin;
    private void OnEnable()
    {
        StartCoroutine(SetLabel());
    }

    IEnumerator SetLabel()
    {
        label_poin.gameObject.SetActive(false);
        label_coin.gameObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        label_coin.text = $"{GameManager.Instance.GetCoinValue()}";
        label_poin.text = $"{GameManager.Instance.GetScoreValue()}";
        label_poin.gameObject.SetActive(true);
        label_coin.gameObject.SetActive(true);
    }
}
