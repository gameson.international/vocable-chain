using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeManager : MonoBehaviour
{
    [SerializeField] private Tema[] allThemes;
    [SerializeField] private Tema currentTheme;
    [SerializeField] string selectedTheme = "Wood";

    [Header("Sprite Holder")]
    [SerializeField] private ThemeHolder holder;

    private void Awake()
    {
        if(PlayerPrefs.HasKey("THEMES"))
        {
            selectedTheme = PlayerPrefs.GetString("THEMES");
        }
        else
        {
            PlayerPrefs.SetString("THEMES", "Wood");
        }

        SetTheme(selectedTheme);
    }

    public Tema GetTheme()
    {
        return currentTheme;
    }

    public void SetTheme(string themeName)
    {
        if(!PlayerPrefs.HasKey("Wood"))
        {
            PlayerPrefs.SetString("Wood", "Wood");
        }
        selectedTheme = themeName;
        currentTheme = Array.Find(allThemes, t => t.nama == selectedTheme);

        holder.BGGame.sprite = currentTheme.BG;
        holder.ButtonAds.sprite = currentTheme.ButtonAds;
        holder.ButtonHint.sprite = currentTheme.ButtonHint;
        holder.ButtonPlus.sprite = currentTheme.ButtonPlus;
        holder.ButtonBack.sprite = currentTheme.ButtonBack;
        holder.Panel.sprite = currentTheme.Panel;
        holder.StarsHolder.sprite = currentTheme.StarsHolder;
        holder.ButtonReplay.sprite = currentTheme.ButtonReplay;
        holder.ButtonPackage.sprite = currentTheme.ButtonPackage;
        holder.ButtonNext.sprite = currentTheme.ButtonNext;
        foreach (var item in holder.Stars)
        {
            item.sprite = currentTheme.Star;
        }
    }
}

[System.Serializable]
public class ThemeHolder
{
    [Header("background")]
    public Image BGAwal;
    public SpriteRenderer BGGame;

    [Header("Menu Button")]
    public Image ButtonPlay;
    public Image ButtonSetting;
    public Image ButtonLeaderboard;
    public Image ButtonLink;
    public Image ButtonShop;

    [Header("In Game UI")]
    public Image ButtonAds;
    public Image ButtonHint;
    public Image ButtonPlus;
    public Image ButtonBack;

    [Header("Panel Result")]
    public Image Panel;
    public Image StarsHolder;
    public Image[] Stars;
    public Image ButtonReplay;
    public Image ButtonPackage;
    public Image ButtonNext;
}
