using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class ThemeCard : MonoBehaviour
{
    public string themeName;

    [SerializeField] private Button b_use;
    [SerializeField] private int price;

    private void Start()
    {
        if(PlayerPrefs.HasKey(themeName))
        {
            b_use.GetComponentInChildren<TMP_Text>().text = "Use";
        }
        else
        {
            b_use.GetComponentInChildren<TMP_Text>().text = "Buy";
        }

        b_use.onClick.AddListener(() =>
        {
            if (PlayerPrefs.HasKey(themeName))
            {
                //use this theme
                ApplyTheme();
            }
            else
            {
                //buy these theme
                if (GameManager.Instance.GetCoinValue() > price)
                {
                    PlayerPrefs.SetString(themeName, themeName);
                    GameManager.Instance.MoreCoin(-price);
                    //use this theme
                    ApplyTheme();
                }
            }
        });
    }

    private void ApplyTheme()
    {
        b_use.GetComponentInChildren<TMP_Text>().text = "Active";
        
        GameManager.Instance.GetComponent<ThemeManager>().SetTheme(themeName);
        PlayerPrefs.SetString("THEMES", themeName);
    }
}
