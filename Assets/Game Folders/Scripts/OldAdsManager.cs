using com.adjust.sdk;
using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using TRAN_KHUONG_DUY;
using UnityEngine;

public class OldAdsManager : MonoBehaviour
{
    public static OldAdsManager Instance;

    private BannerView banner;
    private InterstitialAd interstitial;
    private RewardedAd rewarded;
    public RewardedAd GetRewardedAdInfo() { return rewarded; }

#if UNITY_ANDROID
    private string banner_id = "ca-app-pub-3949237800163961/6373572398";
    private string interstitial_id = "ca-app-pub-3949237800163961/9494559756";
    private string rewarded_id = "ca-app-pub-3949237800163961/8179663997";
    //static string
    public static string FreeHint = "Hint";
    public static string DailySpin = "Spin";
    public static string ChangeName = "Name";
    public static string NextLevel = "Next Level";
    public static string SpeedUp = "Speed Up";
#elif IOS //jika IOS
    private string banner_id = "ca-app-pub-3949237800163961/3568048297";
    private string interstitial_id = "ca-app-pub-3949237800163961/8628803283";
    private string rewarded_id = "ca-app-pub-3949237800163961/1218857174";
    //static string
    public static string FreeHint = "ca-app-pub-3949237800163961/6002639940";
    public static string DailySpin = "ca-app-pub-3949237800163961/4804629411";
    public static string ChangeName = "ca-app-pub-3949237800163961/9841048269";
    public static string NextLevel = "Next Level";
    public static string SpeedUp = "ca-app-pub-3949237800163961/7239221068";
#endif

    public delegate void AdsRewardSuccessDelegate(string name);
    public event AdsRewardSuccessDelegate OnAdRewardSuccess;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey("ADS"))
        {
            return;
        }
        MobileAds.RaiseAdEventsOnUnityMainThread = true;

        MobileAds.Initialize((initStatus) =>
        {
            Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
            foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
            {
                string className = keyValuePair.Key;
                AdapterStatus status = keyValuePair.Value;

                switch (status.InitializationState)
                {
                    case AdapterState.NotReady:
                        break;
                    case AdapterState.Ready:
                        break;
                }
            }

            LoadRewardedAds();
            LoadInterstitialAds();
            LoadBannerAds();
        });

        GameManager.Instance.GetGameServices().onGetDataServer += AdsManager_onGetDataServer;

    }

    private void AdsManager_onGetDataServer(DataServer dataServer)
    {
        banner_id = dataServer.AdUnitIdAdmobBannerID;
        interstitial_id = dataServer.AdUnitIdAdmobInterstitialID;
        rewarded_id = dataServer.AdUnitIdAdmobRewardedID;
        LoadRewardedAds();
        LoadInterstitialAds();
        LoadBannerAds();
    }

    private void SetInterstitialEvents()
    {
        interstitial.OnAdClicked += Interstitial_OnAdClicked;
        interstitial.OnAdFullScreenContentClosed += Interstitial_OnAdFullScreenContentClosed;
        interstitial.OnAdFullScreenContentFailed += Interstitial_OnAdFullScreenContentFailed;
        interstitial.OnAdFullScreenContentOpened += Interstitial_OnAdFullScreenContentOpened;
        interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
        interstitial.OnAdPaid += Interstitial_OnAdPaid;
    }

    private void SetRewardedEvents()
    {
        rewarded.OnAdClicked += Rewarded_OnAdClicked;
        rewarded.OnAdFullScreenContentClosed += Rewarded_OnAdFullScreenContentClosed;
        rewarded.OnAdFullScreenContentFailed += Rewarded_OnAdFullScreenContentFailed;
        rewarded.OnAdFullScreenContentOpened += Rewarded_OnAdFullScreenContentOpened;
        rewarded.OnAdImpressionRecorded += Rewarded_OnAdImpressionRecorded;
        rewarded.OnAdPaid += Rewarded_OnAdPaid;
    }

    private void OnDisable()
    {
        interstitial.OnAdClicked -= Interstitial_OnAdClicked;
        interstitial.OnAdFullScreenContentClosed -= Interstitial_OnAdFullScreenContentClosed;
        interstitial.OnAdFullScreenContentFailed -= Interstitial_OnAdFullScreenContentFailed;
        interstitial.OnAdFullScreenContentOpened -= Interstitial_OnAdFullScreenContentOpened;
        interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
        interstitial.OnAdPaid -= Interstitial_OnAdPaid;

        rewarded.OnAdClicked -= Rewarded_OnAdClicked;
        rewarded.OnAdFullScreenContentClosed -= Rewarded_OnAdFullScreenContentClosed;
        rewarded.OnAdFullScreenContentFailed -= Rewarded_OnAdFullScreenContentFailed;
        rewarded.OnAdFullScreenContentOpened -= Rewarded_OnAdFullScreenContentOpened;
        rewarded.OnAdImpressionRecorded -= Rewarded_OnAdImpressionRecorded;
        rewarded.OnAdPaid -= Rewarded_OnAdPaid;

        GameManager.Instance.GetGameServices().onGetDataServer -= AdsManager_onGetDataServer;
    }

    #region EVENT CALLBACKS
    private void Interstitial_OnAdPaid(AdValue obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsOnPaidToken);
    }

    private void Interstitial_OnAdImpressionRecorded()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsImpessionRecordToken);
    }

    private void Interstitial_OnAdFullScreenContentOpened()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsOnOpenedToken);
    }

    private void Interstitial_OnAdFullScreenContentFailed(AdError obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsShowFailedToken);
    }

    private void Interstitial_OnAdFullScreenContentClosed()
    {
        LoadInterstitialAds();
    }

    private void Interstitial_OnAdClicked()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.InterstitialAdsClickedToken);
    }

    private void Rewarded_OnAdPaid(AdValue obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsOnPaidToken);
    }

    private void Rewarded_OnAdImpressionRecorded()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsImpressionRecordToken);
    }

    private void Rewarded_OnAdFullScreenContentOpened()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsOnOpenedToken);
    }

    private void Rewarded_OnAdFullScreenContentFailed(AdError obj)
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsShowFailedToken);
    }

    private void Rewarded_OnAdFullScreenContentClosed()
    {
        LoadRewardedAds();
    }

    private void Rewarded_OnAdClicked()
    {
        GameManager.Instance.SendAdjustEvent(AdjustToken.RewardedAdsClickedToken);
    }
    #endregion

    public void LoadBannerAds()
    {
        if (banner != null)
        {
            banner.Destroy();
            banner = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        banner = new BannerView(banner_id, AdSize.Banner, AdPosition.Bottom);

        banner.LoadAd(adRequest);
    }

    public void LoadInterstitialAds()
    {
        if(interstitial != null)
        {
            interstitial.Destroy();
            interstitial = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        InterstitialAd.Load(interstitial_id, adRequest,
            (InterstitialAd ad, LoadAdError error) =>
            {
              if (error != null || ad == null)
                {
                    Debug.LogError("interstitial ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Interstitial ad loaded with response : "
                          + ad.GetResponseInfo());

                interstitial = ad;
                
                SetInterstitialEvents();
            });

    }

    public void LoadRewardedAds()
    {
        if (rewarded != null)
        {
            rewarded.Destroy();
            rewarded = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        RewardedAd.Load(rewarded_id, adRequest,
            (RewardedAd ad, LoadAdError error) =>
            {
              if (error != null || ad == null)
                {
                    Debug.LogError("Rewarded ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Rewarded ad loaded with response : "
                          + ad.GetResponseInfo());

                rewarded = ad;

                SetRewardedEvents();
            });
    }

    public void RequestRewardedAds(string adjustAdsToken , Action<string> RewardType)
    {
        if (PlayerPrefs.HasKey(Gameson.GameManager.Ads))
        {
            GameManager.Instance.HintBtn_Onclick();
            return;
        }

        if(rewarded.CanShowAd())
        {
            rewarded.Show((Reward reward) => 
            {
                RewardType(adjustAdsToken);
            });

            if(!ISManager.Instance.GetCanShowAd())
                ISManager.Instance.LoadAds();
            OnAdRewardSuccess?.Invoke(adjustAdsToken);

            if(!string.IsNullOrEmpty(adjustAdsToken))
                GameManager.Instance.SendAdjustEvent(adjustAdsToken);
        }
        else
        {
            ISManager.Instance.RequestRewardedAds(adjustAdsToken);
        }
    }

    public void RequestInterstitial()
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

        if(interstitial.CanShowAd())
        {
            interstitial.Show();
        }
    }
    public void RequestInterstitial(string adjustToken)
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

        if (interstitial.CanShowAd())
        {
            interstitial.Show();
            GameManager.Instance.SendAdjustEvent(adjustToken);
        }
    }

    public void RequestBanner()
    {
        if(PlayerPrefs.HasKey(Gameson.GameManager.Ads))
        {
            return;
        }
        if (banner != null)
        {
            banner.Show();
        }
    }
}
