using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class leaderboardCard : MonoBehaviour
{
    [SerializeField] private TMP_Text label_nama;
    [SerializeField] private TMP_Text label_rank;
    [SerializeField] private TMP_Text label_score;

    public void SetLeaderbordData(string nama , int rank , int score)
    {
        label_nama.text = nama;
        label_rank.text = $"{rank}";
        label_score.text = $"{score}";
    }
}
