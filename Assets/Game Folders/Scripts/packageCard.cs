using Gameson;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class packageCard : MonoBehaviour
{
    [SerializeField] private string namaPackage;

    [SerializeField] private GameObject[] allPanels;

    [SerializeField] private Image hexaImage;
    [SerializeField] private Sprite[] allSprites;

    [SerializeField] private GameObject[] allStars;

    [SerializeField] private TMP_Text label_score;
    [SerializeField] private TMP_Text label_packageName;
    [SerializeField] private TMP_Text label_packageName_lock;

    [SerializeField] private TMP_Text label_word;
    [SerializeField] private TMP_Text label_totalScore;
    [SerializeField] private TMP_Text label_stars;

    [Header("Button Play")]
    [SerializeField] private Sprite[] allButtonPlays;

    [SerializeField] private bool isBought = true , isDefault;
    [SerializeField] private int price;
    [SerializeField] private TMP_Text label_price;

    [SerializeField] private Button b_buy;
    [SerializeField] private Button b_play;

    public void InitiateCard(string nama ,int word, int score)
    {
        namaPackage = nama;
    
        label_packageName.text = nama;
        label_packageName_lock.text = nama;

        b_buy.onClick.AddListener(BuyPackage);

        if (PlayerPrefs.HasKey(namaPackage) || isDefault)
        {
            isBought = true;
            //packageManager.Instance.AddNewPackage(namaPackage);

            b_play.onClick.AddListener(() =>
            {
                LevelManager.Instance.PlayPackage(nama);
            });

            allPanels[0].SetActive(true);
            allPanels[1].SetActive(false);

            GetComponent<RectTransform>().sizeDelta = new Vector2(590, 200);
        }
        else
        {
            isBought = false;
            label_price.text = price.ToString();

            allPanels[1].SetActive(true);
            allPanels[0].SetActive(false);

            GetComponent<RectTransform>().sizeDelta = new Vector2(590, 100);
        }

        if(isDefault)
        {
            //label_score.text = temp.totalScore.ToString();

            Button thisButton = gameObject.GetComponent<Button>();
            thisButton.onClick.AddListener(() =>
            {
                /*packageManager.PackageLevel levelThisCard = packageManager.Instance.GetPackageLevel(gameObject.name);
                GameManager.Instance.LoadData(levelThisCard);*/
            });
        }

        SetCard(word , score);
    }

    public void SetCard(int totalWord , int totalScore)
    {
        label_score.text = totalScore.ToString();
        if (isBought || isDefault)
        {
            b_play.onClick.AddListener(() =>
            {
                //GameManager.Instance.LoadData(packageManager.Instance.GetPackageLevel(namaPackage));
            });

            label_word.text = $"Words <br> <size=45><color=green>{totalWord}/30</size></color>";
            label_totalScore.text = $"Total Score <br> <size=45><color=green>{totalScore}</size></color>";
            label_stars.text = $"Star Points <br> <size=45><color=green>{totalScore}</size></color>";
        }

        if (totalWord > 0)
        {
            hexaImage.sprite = allSprites[1];
            
        }
        else
        {
            hexaImage.sprite = allSprites[0];
            
        }
        if (totalScore == 100)
        {
            b_play.image.sprite = allButtonPlays[1];
            hexaImage.sprite = allSprites[2];
            foreach (var item in allStars)
            {
                item.SetActive(true);
            }
        }
        else
        {
            b_play.image.sprite = allButtonPlays[0];
        }
    }

    public void BuyPackage()
    {
        int coin = PlayerPrefs.GetInt(ConstantsInGame.Coin);

        if(coin < price)
        {
            return;
        }

        isBought = true;
        PlayerPrefs.SetString(namaPackage, "beli");
        packageManager.Instance.AddNewPackage(namaPackage);
        Gameson.GameManager.Instance.AddCoin(-price);
    }
}