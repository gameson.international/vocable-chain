using System.Collections;
using System.Collections.Generic;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class WidgetKurangCoin : MonoBehaviour
{
    [SerializeField] private Button b_close;
    [SerializeField] private Button b_buy;

    private void Start()
    {
        b_close.onClick.AddListener(() => gameObject.SetActive(false));
        b_buy.onClick.AddListener(() =>
        {
            GameManager.Instance.ShopBtn_Onclick(false);
            gameObject.SetActive(false);
        });
    }
}
