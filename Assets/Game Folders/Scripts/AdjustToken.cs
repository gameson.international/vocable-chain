
public static class AdjustToken
{
    public static string Buy1000CoinToken = "7w40ar";
    public static string Buy500CoinToken = "jw5ddy";
    public static string BuyFailedToken = "n9ar7l";
    public static string BuyFreeAdsToken = "hhqezc";
    public static string InterstitialAdsClickedToken = "eq9o92";
    public static string InterstitialAdsImpessionRecordToken = "pcay1l";
    public static string InterstitialAdsOnPaidToken = "2hbrob";
    public static string InterstitialAdsShowToken = "inwh5t";
    public static string InterstitialAdsShowFailedToken = "btfnat";
    public static string InterstitialAdsShowBackToHomeToken = "mjm8xz";
    public static string InterstitialAdsShowBackToPackageToken = "ba9z58";
    public static string InterstitialAdsShowNextLevelToken = "pywgzv";
    public static string PlayGameToken = "ty29rz";
    public static string RewardedAdsChangeNameToken = "g6lc8j";
    public static string RewardedAdsClickedToken = "z4wv2g";
    public static string RewardedAdsDailyRewardSpinToken = "q1uodg";
    public static string RewardedAdsFreeHintToken = "mqj4zx";
    public static string RewardedAdsImpressionRecordToken = "x6akb3";
    public static string RewardedAdsMysteryChestToken = "eqnqta";
    public static string RewardedAdsOnPaidToken = "4kgjrk";
    public static string RewardedAdsOnOpenedToken = "w5h49g";
    public static string RewardedAdsShowFailedToken = "q1hhqu";
    public static string RewardedAdsSpeedUpToken = "xdcpsc";
    public static string TotalPackagesCompletedToken = "s874yt";
    public static string ExitGameToken = "sa8jag";
}
