using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SplashscreenPage : MonoBehaviour
{
    [SerializeField] private string namaScene;

    [SerializeField] VideoPlayer video;

    private void Start()
    {
        video = GetComponent<VideoPlayer>();

        video.loopPointReached += Video_loopPointReached;
        video.Play();
    }

    private void Video_loopPointReached(VideoPlayer source)
    {
        PindahScene(namaScene);
    }

    public void PindahScene(string nama)
    {
        SceneManager.LoadScene(nama);
    }
}
