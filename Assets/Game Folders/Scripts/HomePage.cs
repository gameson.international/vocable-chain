using OldScript;
using System;
using System.Collections;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class HomePage : MonoBehaviour
{
    [SerializeField] private Button b_link;
    [SerializeField] private Button b_misteryChest;
    [SerializeField] private Button b_playGame;
    [SerializeField] private Button b_exit;
    [SerializeField] private Button b_shop;
    [SerializeField] private Button b_setting;

    [SerializeField] private TMP_Text label_chest;

    [SerializeField] private string uri;

    [SerializeField] private string tanggal;
    [SerializeField] private DateTime saveTime;

    private bool dailyBonusIsActive = true;
    [SerializeField] private Sprite[] chests;

    [SerializeField] private GameObject panel_speedUpChest;
    [SerializeField] private TMP_Text label_countdown;
    [SerializeField] private Button b_close, b_speedUp;

    [SerializeField] private bool speedUpDone = false;
    private void OnEnable()
    {
        CheckHourly();

        if (GameManager.Instance != null)
        {
            GameManager.Instance.CheckSetup();
        }
        AdsManager.Instance.OnAdRewardSuccess += Instance_OnAdRewardSuccess;
        ISManager.Instance.OnAdRewardSuccess += Ironsource_OnAdRewardSuccess;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdRewardSuccess -= Instance_OnAdRewardSuccess;
        ISManager.Instance.OnAdRewardSuccess -= Ironsource_OnAdRewardSuccess;
    }

    private void Start()
    {
        b_playGame.onClick.AddListener(() =>
        {
            int n = 0;
            if (PlayerPrefs.HasKey("LEVELSAATINI"))
            {
                n = PlayerPrefs.GetInt("LEVELSAATINI");
            }

            if (n < packageManager.Instance.GetMaxPackageList())
            {
                string findPackageName = packageManager.Instance.GetPackageName(n);
                GameManager.Instance.LoadData(packageManager.Instance.GetPackageLevel(findPackageName));
                gameObject.SetActive(false);
            }
            else
            {
                GameManager.Instance.BackToPackageButton_Onclick();
            }
            GameManager.Instance.SendAdjustEvent(AdjustToken.PlayGameToken);
        });

        b_link.onClick.AddListener(() =>
        {
            Application.OpenURL(uri);
        });

        CheckHourly();
        GameManager.Instance.CheckSetup();

        b_misteryChest.onClick.AddListener(() =>
        {
            //AdsManager.Instance.RequestRewardedAds(AdsManager.DailySpin);
            if (dailyBonusIsActive)
            {
                GameManager.Instance.ChangeSpinState(false);
                GameManager.Instance.SpinWheelButton_OnClick();
                CheckHourly();
            }
            else
            {
                panel_speedUpChest.SetActive(true);
            }

        });

        b_exit.onClick.AddListener(() => {
            GameManager.Instance.SendAdjustEvent(AdjustToken.ExitGameToken);
            Application.Quit();
            });
        b_shop.onClick.AddListener(() => GameManager.Instance.ShopBtn_Onclick(true));
        b_setting.onClick.AddListener(() => GameManager.Instance.SettingsBtn_Onclick());

        b_close.onClick.AddListener(() => panel_speedUpChest.SetActive(false));

        speedUpDone = PlayerPrefs.GetInt("SpeedUpDone") == 1 ? true : false;
        b_speedUp.onClick.AddListener(() => {
            AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsSpeedUpToken , OnGetReward);
            speedUpDone = true;
            PlayerPrefs.SetInt("SpeedUpDone", 1);
            b_speedUp.gameObject.SetActive(false);
            });
        b_speedUp.gameObject.SetActive(!speedUpDone);
    }

    private void OnGetReward(string obj)
    {
        
    }
    #region HandleSpeedUp

    private bool singleClick = false;
    private int clickCount = 0;
    private float clickDelay = 0.3f;
    private void HandleSpeedUp()
    {
        clickCount++;

        if (!singleClick)
        {
            singleClick = true;
            StartCoroutine(DelayedClickAction());
        }
    }

    private IEnumerator DelayedClickAction()
    {
        DataServer findGame = Array.Find(GameManager.Instance.GetGameServices().GetGamesonData().gameson, g => g.DataName == "VocableChain");
        yield return new WaitForSeconds(clickDelay);

        if (clickCount == 1)
        {
            if (findGame.FirstSpin == "Admob")
            {
                AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsSpeedUpToken , OnGetReward);
            }
            else if (findGame.FirstSpin == "Ironsource")
            {
                ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsSpeedUpToken);
            }
        }
        else if(clickCount>1)
        {
            if (findGame.SecondSpin == "Admob")
            {
                AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsSpeedUpToken , OnGetReward);
            }
            else if (findGame.SecondSpin == "Ironsource")
            {
                ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsSpeedUpToken);
            }
        }

        singleClick = false;
        clickCount = 0;
    }
    #endregion
    private void Ironsource_OnAdRewardSuccess(string _token)
    {
        if (_token == AdjustToken.RewardedAdsSpeedUpToken)
        {
            panel_speedUpChest.SetActive(false);
            saveTime = DateTime.Parse(tanggal);

            saveTime = saveTime.AddMinutes(-30);

            tanggal = saveTime.ToString();
            PlayerPrefs.SetString(ConstantsInGame.Chest, tanggal);
            CheckHourly();
        }
    }

    private void Instance_OnAdRewardSuccess(string _token)
    {
        if (_token == AdjustToken.RewardedAdsSpeedUpToken)
        {
            panel_speedUpChest.SetActive(false);
            saveTime = DateTime.Parse(tanggal);

            saveTime = saveTime.AddMinutes(-30);

            tanggal = saveTime.ToString();
            PlayerPrefs.SetString(ConstantsInGame.Chest, tanggal);
            CheckHourly();
        }
    }

    public void CheckHourly()
    {
        if (GameManager.Instance.IsSpinLogin())
            return;
        saveTime = DateTime.Now.AddHours(-2);

        if (PlayerPrefs.HasKey(ConstantsInGame.Chest))
        {
            tanggal = PlayerPrefs.GetString(ConstantsInGame.Chest);
            saveTime = DateTime.Parse(tanggal);
        }

        DateTime nextDate = saveTime.AddHours(2);
        DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

        TimeSpan span = nextDate - sekarang;
        dailyBonusIsActive = span.Hours <= 0 && span.Minutes <= 0;

        if (dailyBonusIsActive)
        {
            b_misteryChest.image.sprite = chests[0];
            b_misteryChest.GetComponent<Animator>().SetBool("isActive", true);
            label_chest.text = "Claim Reward !";
        }
        else
        {
            b_misteryChest.image.sprite = chests[1];
            b_misteryChest.GetComponent<Animator>().SetBool("isActive", false);
        }
        //b_misteryChest.interactable = dailyBonusIsActive;
    }

    private void Update()
    {
        if (!dailyBonusIsActive)
        {
            saveTime = DateTime.Parse(tanggal);
            DateTime nextDate = saveTime.AddHours(2);

            DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

            TimeSpan span = nextDate - sekarang;
            label_chest.text = span.ToString();
            label_countdown.text = span.ToString();
        }
    }
}