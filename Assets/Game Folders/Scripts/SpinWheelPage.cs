using System;
using System.Collections;
using System.Collections.Generic;
using EasyUI.PickerWheelUI;
using Gameson;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class SpinWheelPage : MonoBehaviour
{
    [SerializeField] private Button b_spin;
    [SerializeField] private Button b_back;
    [SerializeField] private PickerWheel pickerWheel;

    [SerializeField] private GameObject panel_result;
    [SerializeField] private TMP_Text label_result;

    [SerializeField] private Animator buttonAnim;

    //[SerializeField] private bool isAndroid = false;
    private void Start()
    {
        b_back.onClick.AddListener(() => {
            panel_result.SetActive(false);
            TRAN_KHUONG_DUY.GameManager.Instance.BackToHomeFromSetting();
            });
        panel_result.SetActive(false);
        buttonAnim = b_spin.GetComponent<Animator>();
        buttonAnim.SetBool("isActive", true);

        b_spin.onClick.AddListener(() =>
        {
            if (TRAN_KHUONG_DUY.GameManager.Instance.IsSpinLogin())
                AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsDailyRewardSpinToken , OnGetReward);
            else
                AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsMysteryChestToken , OnGetReward);
            b_spin.interactable = false;
        });

        
        label_result.gameObject.SetActive(false);
    }

    private void OnGetReward(string obj)
    {
        throw new NotImplementedException();
    }

    private void OnEnable()
    {

        AdsManager.Instance.OnAdRewardSuccess += Instance_OnAdRewardSuccess;
        ISManager.Instance.OnAdRewardSuccess += Instance_OnAdRewardSuccess1;
        pickerWheel.OnSpinStart(OnSpinStart);
        pickerWheel.OnSpinEnd(OnEndSpin);
    }

    private void Instance_OnAdRewardSuccess1(string name)
    {
        if (name == AdjustToken.RewardedAdsDailyRewardSpinToken || name == AdjustToken.RewardedAdsMysteryChestToken)
            pickerWheel.Spin();
    }

    private void Instance_OnAdRewardSuccess(string name)
    {
        if (name == AdjustToken.RewardedAdsDailyRewardSpinToken || name == AdjustToken.RewardedAdsMysteryChestToken)
            pickerWheel.Spin();
    }

    private void OnDisable()
    {

    }
    private void OnSpinStart()
    {
        
    }

    private void OnEndSpin(WheelPiece pieces)
    {
        //ShowResult($"<size=50>Congratulation!</size> <br> You Get <size=35><color=green>{pieces.Amount}</color> Coin!!");
        b_spin.interactable = true;

        int coin = PlayerPrefs.GetInt(ConstantsInGame.Coin, 0);
        coin += pieces.Amount;

        PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
        PlayerPrefs.SetString(ConstantsInGame.Chest, DateTime.Now.ToString());

        panel_result.SetActive(true);
        label_result.gameObject.SetActive(true);
        label_result.text = pieces.Amount.ToString();

        TRAN_KHUONG_DUY.GameManager.Instance.CheckSetup();
        TRAN_KHUONG_DUY.GameManager.Instance.GetComponent<GameServices>().RequestNotification("Check Your Game!" , "Dont forget to claim your reward today!" , 2);
    }

    private void ShowResult(string txt)
    {
        b_spin.gameObject.SetActive(false);
        label_result.gameObject.SetActive(true);
        label_result.text = txt;
    }
    #region HandleSpin

    private bool singleClick = false;
    private int clickCount = 0;
    private float clickDelay = 0.3f;
    private void HandleSpin()
    {
        buttonAnim.SetBool("isActive", false);
        clickCount++;

        if (!singleClick)
        {
            singleClick = true;
            StartCoroutine(DelayedClickAction());
        }
    }

    private IEnumerator DelayedClickAction()
    {
        DataServer findGame = Array.Find(TRAN_KHUONG_DUY.GameManager.Instance.GetGameServices().GetGamesonData().gameson, g => g.DataName == "VocableChain");
        yield return new WaitForSeconds(clickDelay);
        b_spin.interactable = false;
        if (clickCount == 1)
        {
            if (findGame.FirstSpin == "Admob")
            {
                if (TRAN_KHUONG_DUY.GameManager.Instance.IsSpinLogin())
                {
                    AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsDailyRewardSpinToken , OnGetReward);
                }
                else
                {
                    AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsMysteryChestToken , OnGetReward);
                }
            }
            else if (findGame.FirstSpin == "Ironsource")
            {
                if (TRAN_KHUONG_DUY.GameManager.Instance.IsSpinLogin())
                {
                    ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsDailyRewardSpinToken);
                }
                else
                {
                    ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsMysteryChestToken);
                }
            }
        }
        else if(clickCount > 1)
        {
            if (findGame.SecondSpin == "Admob")
            {
                if (TRAN_KHUONG_DUY.GameManager.Instance.IsSpinLogin())
                {
                    AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsDailyRewardSpinToken , OnGetReward);
                }
                else
                {
                    AdsManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsMysteryChestToken , OnGetReward);
                }
            }
            else if (findGame.SecondSpin == "Ironsource")
            {
                if (TRAN_KHUONG_DUY.GameManager.Instance.IsSpinLogin())
                {
                    ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsDailyRewardSpinToken);
                }
                else
                {
                    ISManager.Instance.RequestRewardedAds(AdjustToken.RewardedAdsMysteryChestToken);
                }
            }
        }
        singleClick = false;
        clickCount = 0;
    }
    #endregion
}
