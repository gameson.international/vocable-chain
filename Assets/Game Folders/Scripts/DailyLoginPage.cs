using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class DailyLoginPage : MonoBehaviour
{
    

    [SerializeField] private RewardData[] allRewards;
    [SerializeField] private BonusCard[] allcards;
    [SerializeField] List<RewardData> listReward = new List<RewardData>();

    [SerializeField] private Button b_claim , b_close , b_openPanelReward;

    [SerializeField] private GameObject panelReward;
    [SerializeField] private Image iconReward;
    [SerializeField] private TMP_Text labelReward;

    private void Start()
    {
        string saveAwal = DateTime.Now.AddDays(-1).ToShortDateString();

        if (PlayerPrefs.HasKey("LOGINAWAL"))
        {
            foreach (var item in allRewards)
            {
                listReward.Add(item);
            }
            int n = listReward.Count-1;
            for (int i = 0; i < n; i++)
            {
                int r = PlayerPrefs.GetInt($"DailyLoginDay{i}");
                allRewards[i] = listReward[r];
            }

            saveAwal = PlayerPrefs.GetString("LOGINAWAL");
        }
        else
        {
            int n = allRewards.Length-1;
            for (int i = 0; i < n; i++)
            {
                int r = i + Random.Range(0, n - i);
                allRewards[i] = allRewards[r];
                PlayerPrefs.SetInt($"DailyLoginDay{i}",r);
            }
            PlayerPrefs.SetString("LOGINAWAL", saveAwal);
        }

        b_close.onClick.AddListener(() => gameObject.SetActive(false));

        //jarak dari awal main sampai hari ini
        DateTime waktuAwal = DateTime.Parse(saveAwal);

        //int temp = waktuAwal.Day - DateTime.Now.Day;
        TimeSpan span = DateTime.Now - waktuAwal;
        int jarak = span.Days;
        if(jarak > 6)
        {
            jarak = 1;
        }

        Init(jarak);

        b_openPanelReward.onClick.AddListener(() =>
        {
            panelReward.SetActive(true);
            iconReward.sprite = allRewards[jarak - 1].icon;

            switch (allRewards[jarak - 1].tipe)
            {
                case RewardEnum.Coin:
                    labelReward.text = $"{allRewards[jarak - 1].amount} Coin.";
                    if(allcards[jarak - 1].isLastBonus)
                    {
                        labelReward.text = $"{allcards[jarak - 1].GetBonusCoin()} Coin.";
                    }
                    break;
                case RewardEnum.SpinWheel:
                    labelReward.text = $"Free Spin Wheels!";
                    PlayerPrefs.SetInt("DailyLogin", 1);
                    break;
            }
        });
       
        b_claim.onClick.AddListener(() =>
        {
            allcards[jarak - 1].Claim();
            gameObject.SetActive(false);
        });

        if (allcards[jarak - 1].isBonusClaimed())
        {
            gameObject.SetActive(false);
        }
    }

    private void Init(int jarakWaktu)
    {
        for (int i = 0; i < allRewards.Length; i++)
        {
            allcards[i].InitCard(allRewards[i].tipe , allRewards[i].icon, allRewards[i].amount);
        }

        if (jarakWaktu > allcards.Length)
        {
            jarakWaktu = allcards.Length;
        }

        for (int i = 0; i < jarakWaktu; i++)
        {
            allcards[i].ReadyToClaim();
        }
    }

    public void CheckClaimButton(bool aktif)
    {
        b_openPanelReward.interactable = aktif;
    }
}

[System.Serializable]
public class RewardData
{
    public RewardEnum tipe = RewardEnum.Coin;
    public Sprite icon;
    public int amount;
}

public enum RewardEnum
{
    Coin,
    SpinWheel
}
