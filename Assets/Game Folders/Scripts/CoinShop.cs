using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class CoinShop : MonoBehaviour
{
    [SerializeField] private GameObject panel_loading;
    [SerializeField] private TMP_Text label_coin;

    private void Start()
    {
        SetCoinLabel();
    }

    private void SetCoinLabel()
    {
        label_coin.text = PlayerPrefs.GetInt(ConstantsInGame.Coin).ToString();
    }

    public void Buy()
    {
        panel_loading.SetActive(true);

        StartCoroutine(Loading());
    }

    IEnumerator Loading()
    {
        yield return new WaitForSeconds(3f);
        panel_loading.SetActive(false);
    }

    public void OnSuccessPurchasing(int n)
    {
        if (n == 50)
        {
            GameManager.Instance.MoreCoin(500);
            SetCoinLabel();
            AdjustEvent adjustEvent = new AdjustEvent("jw5ddy");
            Adjust.trackEvent(adjustEvent);
        }
        if (n == 100)
        {
            GameManager.Instance.MoreCoin(1000);
            SetCoinLabel();
            AdjustEvent adjustEvent = new AdjustEvent("7w40ar");
            Adjust.trackEvent(adjustEvent);
        }
        if (n == 1000)
        {
            //remove all ads
            PlayerPrefs.SetString("ADS", "non active");
            AdjustEvent adjustEvent = new AdjustEvent("hhqezc");
            Adjust.trackEvent(adjustEvent);
        }
    }

    public void OnfailedPurchasing()
    {
        AdjustEvent adjustEvent = new AdjustEvent("n9ar7l");
        Adjust.trackEvent(adjustEvent);
    }
}
