using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class Widget : MonoBehaviour
    {
        public WidgetName nama;

        protected virtual void Start()
        {
            Button[] allButton = GetComponentsInChildren<Button>();
            foreach (var item in allButton)
            {
                item.onClick.AddListener(() => AudioManager.Instance.PlayAudio(SoundName.Click));
            }
        }

        protected void CloseWidget()
        {
            gameObject.SetActive(false);
        }
    }

    public enum WidgetName
    {
        dailyBonus,
        Exit,
        Reward,
        forceUpdate,
        NoInternet,
        Result,
        Agreement,
        TimesUp
    }
}
