using Gameson;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MiniBlockGroup : MonoBehaviour
{
    [SerializeField] private MiniBlock[] allBlocks;
    private int letterOpen = 0;
    private int currentGroup;
    [SerializeField] private bool isFadeIn = false;


    public MiniBlock[] allActiveBlock;
    public MiniBlock[] availableBlock;

    private void OnEnable()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }
    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                letterOpen = 0;
                break;
            case GameState.Game:
                letterOpen = 0;
                break;
            case GameState.Package:
                letterOpen = 0;
                break;
            case GameState.Result:
                letterOpen = 0;
                break;

        }
    }

    public void InitBlock(GroupWord groupWord)
    {
        isFadeIn = false;
        foreach (var b in allBlocks)
        {
            b.gameObject.SetActive(false);
        }

        switch (groupWord.thisGrup)
        {
            case Group.Group3:
                for (int i = 0; i < 3; i++)
                {
                    allBlocks[i].gameObject.SetActive(true);
                    allBlocks[i].InitBlock(groupWord.code[i].ToString());
                }
                currentGroup = 3;
                break;
            case Group.Group4:
                for (int i = 0; i < 4; i++)
                {
                    allBlocks[i].gameObject.SetActive(true);
                    allBlocks[i].InitBlock(groupWord.code[i].ToString());
                }
                currentGroup = 4;
                break;
            case Group.Group5:
                for (int i = 0; i < 5; i++)
                {
                    allBlocks[i].gameObject.SetActive(true);
                    allBlocks[i].InitBlock(groupWord.code[i].ToString());
                }
                currentGroup = 5;
                break;
            case Group.Group7:
                for (int i = 0; i < 7; i++)
                {
                    allBlocks[i].gameObject.SetActive(true);
                    allBlocks[i].InitBlock(groupWord.code[i].ToString());
                }
                currentGroup = 7;
                break;
        }
    }

    public void CurrentActive(int n)
    {
        foreach (var b in allBlocks)
        {
            b.ActiveBlock();
        }
    }

    public void Solved()
    {
        foreach (var b in allBlocks)
        {
            b.Revealed();
        }
    }

    public void ResetBlock()
    {
        isFadeIn = false;
        foreach (var b in allBlocks)
        {
            b.GetComponent<Button>().interactable = false;
        }
    }

    public void Reset()
    {
        
    }

    public void Hide()
    {
        foreach (var item in allBlocks)
        {
            item.Hide();
        }
    }

    public void OnGetHint()
    {
        allBlocks[letterOpen].Revealed();
        letterOpen++;
    }

    public void OnGetRocket()
    {
        
        allActiveBlock = GetComponentsInChildren<MiniBlock>();
        availableBlock = Array.FindAll(allActiveBlock, b => b.isRevealed == false);
        if(availableBlock.Length <= 1)
        {
            return;
        }
        int r = UnityEngine.Random.Range(0, availableBlock.Length);
        availableBlock[r].Revealed();
    }

    public void OnGetArrow()
    {
        isFadeIn = true;
        foreach (var item in allBlocks)
        {
            item.SetArrowTarget();
        }
    }

    private void Update()
    {
        Image temp = GetComponent<Image>();
        if (isFadeIn)
        {
            temp.CrossFadeAlpha(1, 0.2f, false);
        }
        else
        {
            temp.CrossFadeAlpha(0, 0.2f, false);
        }
    }
}