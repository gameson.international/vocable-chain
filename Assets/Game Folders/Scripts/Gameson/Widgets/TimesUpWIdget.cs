using Gameson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimesUpWIdget : Widget
{
    [SerializeField] private Button homeButton;

    private void Start()
    {
        homeButton.onClick.AddListener(() =>
        {
            GameManager.Instance.ChangeState(GameState.Menu);
            CloseWidget();
        });
    }
}