using Gameson;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class forceUpdateWidget : Widget
{
    [SerializeField, TextArea(3, 3)] private string uri;

    [SerializeField] private TMP_Text label_newVersion, label_oldVersion;

    [SerializeField] private Button b_update;

    private void Start()
    {
        b_update.onClick.AddListener(() =>
        {
            Application.OpenURL(uri);
            Application.Quit();
        });

        label_oldVersion.text = $"Your current game Version {Application.version} is behind !";
        label_newVersion.text = $"Update to version : {GameService.Instance.GetGameCurrentVersion()}";
    }
}
