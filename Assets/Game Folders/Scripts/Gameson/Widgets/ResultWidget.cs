using Gameson;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResultWidget : Widget
{
    [SerializeField] private TMP_Text labelTotalWord;
    [SerializeField] private TMP_Text labelTotalScore;

    [SerializeField] private GameObject[] allStars;

    [SerializeField] private Button b_doubleBonus;
    [SerializeField] private Button b_package;
    [SerializeField] private Button b_next;

    [SerializeField] private Button infoButton;
    [SerializeField] private GameObject infoPanel;

    [SerializeField] private ItemHadiah[] allHadiah;

    private int stars;
    private int coins;

    private bool isInfoShow = false;

    private void OnEnable()
    {
        labelTotalScore.text = $"{LevelManager.Instance.GetTotalScore()}";
        labelTotalWord.text = $"{LevelManager.Instance.GetTotalWordSolved()}";
        b_doubleBonus.interactable = true;

        foreach (var item in allStars)
        {
            item.SetActive(false);
        }
        foreach (var item in allHadiah)
        {
            item.gameObject.SetActive(false);
        }

        stars = LevelManager.Instance.GetStarsValue();
        StartCoroutine(InitiateStars(stars));
        DatabaseManager.Instance.SaveJsonDatabase();

        infoButton.onClick.AddListener(() =>
        {
            infoPanel.SetActive(!isInfoShow);
            isInfoShow = !isInfoShow;
        });
    }

    IEnumerator InitiateStars(int star)
    {
        SetupButton(false);
        for (int i = 0; i < star; i++)
        {
            allStars[i].SetActive(true);
            AudioManager.Instance.PlayAudio(SoundName.Star);
            yield return new WaitForSeconds(0.5f);
        }

        yield return new WaitForSeconds(1f);

        bool isGetBonus = UnityEngine.Random.Range(0, 5) == 3;

        if (isGetBonus)
        {
            int r = UnityEngine.Random.Range(1, star);
            for (int i = 0; i < r; i++)
            {
                if (i == 0)
                {
                    allHadiah[0].gameObject.SetActive(true);

                    coins = UnityEngine.Random.Range(2, 10);
                    allHadiah[0].Init(0, coins);

                    GameManager.Instance.AddCoin(coins);
                }
                else
                {
                    allHadiah[i].gameObject.SetActive(true);
                    allHadiah[i].Init(i, 1);
                }
            }
            yield return new WaitForSeconds(0.5f);
        }
        else
        {
            allHadiah[0].gameObject.SetActive(true);

            coins = UnityEngine.Random.Range(2, 10);
            allHadiah[0].Init(0 , coins);

            GameManager.Instance.AddCoin(coins);
        }
        yield return new WaitForSeconds(1f);
        SetupButton(true);
    }

    protected override void Start()
    {
        base.Start();

        b_doubleBonus.onClick.AddListener(DoubleBonus);
        b_next.onClick.AddListener(Next);
        b_package.onClick.AddListener(() =>
        {
            GameManager.Instance.ChangeState(GameState.Package);
            CloseWidget();
        });
    }

    private void Next()
    {
        GameManager.Instance.ChangeState(GameState.Game);
        GameManager.Instance.SaveProgress();
        gameObject.SetActive(false);
    }

    private void DoubleBonus()
    {
        AdsManager.Instance.RequestRewardedAds("Double Bonus" , OnGetReward);
    }

    private void OnGetReward(string obj)
    {
        Debug.Log($"Reward Ads for : {obj}");
        coins *= 2;

        allHadiah[0].Init(0, coins);

        GameManager.Instance.AddCoin(coins / 2);
        b_doubleBonus.interactable = false;
    }

    private void SetupButton(bool active)
    {
        b_doubleBonus.interactable = active;
        b_package.interactable = active;
        b_next.interactable = active;
    }
}
