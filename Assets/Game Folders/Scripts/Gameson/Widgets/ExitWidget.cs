using Gameson;
using UnityEngine;
using UnityEngine.UI;

public class ExitWidget : Widget
{
    [SerializeField] private Button b_yes;
    [SerializeField] private Button b_no;

    private void Start()
    {
        b_yes.onClick.AddListener(() => Application.Quit());
        b_no.onClick.AddListener(() => 
        { 
            GameManager.Instance.ChangeState(GameState.Menu);
            this.gameObject.SetActive(false); 
        });
    }
}
