using Gameson;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AgreementWidget : Widget
{
    [SerializeField] private Button b_agree;

    private void Start()
    {
        b_agree.onClick.AddListener(() =>
        {
            PlayerPrefs.SetString(GameManager.NewPlayer, DateTime.Now.ToString());
            GameManager.Instance.ChangeState(GameState.Menu);
            CloseWidget();
        });
    }
}
