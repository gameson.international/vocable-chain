using com.adjust.sdk;
using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameson
{
    public class AdsManager : MonoBehaviour
    {
        public static AdsManager Instance;

        private BannerView banner;
        private InterstitialAd interstitial;
        private RewardedAd rewarded;

        [SerializeField] private string banner_id = "ca-app-pub-3949237800163961/6373572398";
        [SerializeField] private string interstitial_id = "ca-app-pub-3949237800163961/9494559756";
        [SerializeField] private string rewarded_id = "ca-app-pub-3949237800163961/8179663997";

        public delegate void AdsRewardSuccessDelegate(string name);
        public event AdsRewardSuccessDelegate OnAdRewardSuccess;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            if (PlayerPrefs.HasKey(GameManager.Ads))
            {
                return;
            }

            MobileAds.RaiseAdEventsOnUnityMainThread = true;

            MobileAds.Initialize((initStatus) =>
            {
                Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
                foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
                {
                    string className = keyValuePair.Key;
                    AdapterStatus status = keyValuePair.Value;

                    switch (status.InitializationState)
                    {
                        case AdapterState.NotReady:
                            break;
                        case AdapterState.Ready:
                            break;
                    }
                }

                LoadRewardedAds();
                LoadInterstitialAds();
                LoadBannerAds();
            });
        }

        private void SetInterstitialEvents()
        {
            interstitial.OnAdClicked += Interstitial_OnAdClicked;
            interstitial.OnAdFullScreenContentClosed += Interstitial_OnAdFullScreenContentClosed;
            interstitial.OnAdFullScreenContentFailed += Interstitial_OnAdFullScreenContentFailed;
            interstitial.OnAdFullScreenContentOpened += Interstitial_OnAdFullScreenContentOpened;
            interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
            interstitial.OnAdPaid += Interstitial_OnAdPaid;
        }

        private void SetRewardedEvents()
        {
            rewarded.OnAdClicked += Rewarded_OnAdClicked;
            rewarded.OnAdFullScreenContentClosed += Rewarded_OnAdFullScreenContentClosed;
            rewarded.OnAdFullScreenContentFailed += Rewarded_OnAdFullScreenContentFailed;
            rewarded.OnAdFullScreenContentOpened += Rewarded_OnAdFullScreenContentOpened;
            rewarded.OnAdImpressionRecorded += Rewarded_OnAdImpressionRecorded;
            rewarded.OnAdPaid += Rewarded_OnAdPaid;
        }

        private void OnDisable()
        {
            interstitial.OnAdClicked += Interstitial_OnAdClicked;
            interstitial.OnAdFullScreenContentClosed += Interstitial_OnAdFullScreenContentClosed;
            interstitial.OnAdFullScreenContentFailed += Interstitial_OnAdFullScreenContentFailed;
            interstitial.OnAdFullScreenContentOpened += Interstitial_OnAdFullScreenContentOpened;
            interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
            interstitial.OnAdPaid += Interstitial_OnAdPaid;

            rewarded.OnAdClicked += Rewarded_OnAdClicked;
            rewarded.OnAdFullScreenContentClosed += Rewarded_OnAdFullScreenContentClosed;
            rewarded.OnAdFullScreenContentFailed += Rewarded_OnAdFullScreenContentFailed;
            rewarded.OnAdFullScreenContentOpened += Rewarded_OnAdFullScreenContentOpened;
            rewarded.OnAdImpressionRecorded += Rewarded_OnAdImpressionRecorded;
            rewarded.OnAdPaid += Rewarded_OnAdPaid;
        }

        #region EVENT CALLBACKS
        private void Interstitial_OnAdPaid(AdValue obj)
        {
            AdjustEvent adjustEvent = new AdjustEvent("2hbrob");
            Adjust.trackEvent(adjustEvent);
        }

        private void Interstitial_OnAdImpressionRecorded()
        {
            AdjustEvent adjustEvent = new AdjustEvent("pcay1l");
            Adjust.trackEvent(adjustEvent);
        }

        private void Interstitial_OnAdFullScreenContentOpened()
        {
            AdjustEvent adjustEvent = new AdjustEvent("inwh5t");
            Adjust.trackEvent(adjustEvent);
        }

        private void Interstitial_OnAdFullScreenContentFailed(AdError obj)
        {
            AdjustEvent adjustEvent = new AdjustEvent("btfnat");
            Adjust.trackEvent(adjustEvent);
        }

        private void Interstitial_OnAdFullScreenContentClosed()
        {
            LoadInterstitialAds();
        }

        private void Interstitial_OnAdClicked()
        {
            AdjustEvent adjustEvent = new AdjustEvent("eq9o92");
            Adjust.trackEvent(adjustEvent);
        }

        private void Rewarded_OnAdPaid(AdValue obj)
        {
            AdjustEvent adjustEvent = new AdjustEvent("4kgjrk");
            Adjust.trackEvent(adjustEvent);
        }

        private void Rewarded_OnAdImpressionRecorded()
        {
            AdjustEvent adjustEvent = new AdjustEvent("x6akb3");
            Adjust.trackEvent(adjustEvent);
        }

        private void Rewarded_OnAdFullScreenContentOpened()
        {
            AdjustEvent adjustEvent = new AdjustEvent("w5h49g");
            Adjust.trackEvent(adjustEvent);
        }

        private void Rewarded_OnAdFullScreenContentFailed(AdError obj)
        {
            AdjustEvent adjustEvent = new AdjustEvent("q1hhqu");
            Adjust.trackEvent(adjustEvent);
        }

        private void Rewarded_OnAdFullScreenContentClosed()
        {
            LoadRewardedAds();
        }

        private void Rewarded_OnAdClicked()
        {
            AdjustEvent adjustEvent = new AdjustEvent("z4wv2g");
            Adjust.trackEvent(adjustEvent);
        }
        #endregion

        public void LoadBannerAds()
        {
            if (banner != null)
            {
                banner.Destroy();
                banner = null;
            }

            var adRequest = new AdRequest();
            adRequest.Keywords.Add("my-admob-ads");

            banner = new BannerView(banner_id, AdSize.Banner, AdPosition.Bottom);

            banner.LoadAd(adRequest);
        }

        public void LoadInterstitialAds()
        {
            if (interstitial != null)
            {
                interstitial.Destroy();
                interstitial = null;
            }

            var adRequest = new AdRequest();
            adRequest.Keywords.Add("my-admob-ads");

            InterstitialAd.Load(interstitial_id, adRequest,
                (InterstitialAd ad, LoadAdError error) =>
                {
                    if (error != null || ad == null)
                    {
                        Debug.LogError("interstitial ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    Debug.Log("Interstitial ad loaded with response : "
                              + ad.GetResponseInfo());

                    interstitial = ad;

                    SetInterstitialEvents();
                });
        }

        public void LoadRewardedAds()
        {
            if (rewarded != null)
            {
                rewarded.Destroy();
                rewarded = null;
            }

            var adRequest = new AdRequest();
            adRequest.Keywords.Add("my-admob-ads");

            RewardedAd.Load(rewarded_id, adRequest,
                (RewardedAd ad, LoadAdError error) =>
                {
                    if (error != null || ad == null)
                    {
                        Debug.LogError("Rewarded ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    Debug.Log("Rewarded ad loaded with response : "
                              + ad.GetResponseInfo());

                    rewarded = ad;

                    SetRewardedEvents();
                });
        }

        public void RequestRewardedAds(string placement , Action<string> OnGetReward)
        {
            if (PlayerPrefs.HasKey(GameManager.Ads))
            {
                //GameManager.Instance.HintBtn_Onclick();
                return;
            }

            if (rewarded.CanShowAd())
            {
                rewarded.Show((Reward reward) => 
                {
                    OnAdRewardSuccess?.Invoke(placement);
                    OnGetReward.Invoke(placement);
                });
            }
        }

        public void RequestInterstitial()
        {
            if (PlayerPrefs.HasKey(GameManager.Ads))
            {
                return;
            }

            if (interstitial.CanShowAd())
            {
                interstitial.Show();
            }
        }

        public void RequestBanner()
        {
            if (PlayerPrefs.HasKey(GameManager.Ads))
            {
                return;
            }

            if (banner != null)
            {
                banner.Show();
            }
        }
    }
}
