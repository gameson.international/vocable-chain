using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson 
{
    public class Notif_PowerUp : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text label_desc;

        private Animator anim;

        private void OnEnable()
        {
            TryGetComponent<Animator>(out anim);

            StartCoroutine(HideNotif());
        }

        IEnumerator HideNotif()
        {
            yield return new WaitForSeconds(1f);
            anim.Play("Hide");
        }

        public void Setup(Sprite thisIcon, PowerUp thisPoweUp)
        {
            icon.sprite = thisIcon;
            label_desc.text = $"use x 1 {thisPoweUp.ToString()}";
        }

        public void Show(PowerUp newPower)
        {
            this.gameObject.SetActive(true);
            switch (newPower)
            {
                case PowerUp.Hint:
                    Setup(GameManager.Instance.GetPlayerData().powerUps[0].icon, GameManager.Instance.GetPlayerData().powerUps[0].nama);
                    break;
                case PowerUp.Rocket:
                    Setup(GameManager.Instance.GetPlayerData().powerUps[1].icon, GameManager.Instance.GetPlayerData().powerUps[1].nama);
                    break;
                case PowerUp.Arrow:
                    Setup(GameManager.Instance.GetPlayerData().powerUps[2].icon, GameManager.Instance.GetPlayerData().powerUps[2].nama);
                    break;
            }
        }

        public void DeactiveNotif()
        {
            this.gameObject.SetActive(false);
        }
    } 



    public enum PowerUp
    {
        Hint,
        Rocket,
        Arrow
    }
}
