﻿using Gameson;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ItemHadiah : MonoBehaviour
{
    [SerializeField] Image icon;
    [SerializeField] TMP_Text labelValue;

    public void Init(int i , int amount)
    {
        if(i == 0)
        {
            labelValue.text = $"{amount}";
            AudioManager.Instance.PlayAudio(SoundName.Star);
        }
        else
        {
            int h = UnityEngine.Random.Range(0, 3);
            icon.sprite = GameManager.Instance.GetPlayerData().powerUps[h].icon;
            
            labelValue.text = $"{amount}";
            GameManager.Instance.AddPowerUp(GameManager.Instance.GetPlayerData().powerUps[h].nama, 1);
            AudioManager.Instance.PlayAudio(SoundName.Star);
        }
    }
}
