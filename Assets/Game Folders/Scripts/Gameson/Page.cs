using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class Page : MonoBehaviour
    {
        public PageName nama = PageName.Menu;

        public static string animHide = "page_hide";

        protected bool isAnimReady , isUsingEndAnimation = true;
        protected Animator anim;

        protected virtual void OnEnable()
        {
            isAnimReady = TryGetComponent<Animator>(out anim);
            Button[] allButtons = GetComponentsInChildren<Button>(true);
            foreach (var item in allButtons)
            {
                item.onClick.AddListener(() => AudioManager.Instance.PlayAudio(SoundName.Click));
            }
        }

        protected IEnumerator PageChange(GameState state)
        {
            if (isAnimReady && isUsingEndAnimation)
            {
                anim.Play(animHide);
                yield return new WaitForSeconds(0.5f);
                GameManager.Instance.ChangeState(state);
            }
            else
            {
                GameManager.Instance.ChangeState(state);
                yield return null;
            }
        }
    }


    public enum PageName
    {
        Authentication,
        Menu,
        Game,
        Setting,
        Shop,
        DailyBonus,
        SpinWheel,
        Package,
        Bonus,
        Promo
    }
}
