﻿namespace Gameson
{
    [System.Serializable]
    public class WordPackage
    {
        public string namaPackage;
        public int wordSolved;
        public int scorePackage;
        public GroupWord[] contentPackage;

        public WordPackage(string nama , int word,int score, GroupWord[] allWord)
        {
            namaPackage = nama;
            wordSolved = word;
            scorePackage = score;
            contentPackage = allWord;
        }
    }
}