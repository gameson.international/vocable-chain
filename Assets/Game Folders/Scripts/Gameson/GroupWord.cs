﻿namespace Gameson
{
    [System.Serializable]
    public class GroupWord
    {
        public Group thisGrup = Group.Group3;
        public string code;
        public bool isSolved;

        public GroupWord(string kode, int grup, bool isSolve = false)
        {
            code = kode;
            if(grup == 3)
            {
                thisGrup = Group.Group3;
            }
            if (grup == 4)
            {
                thisGrup = Group.Group4;
            }
            if (grup == 5)
            {
                thisGrup = Group.Group5;
            }
            if (grup == 7)
            {
                thisGrup = Group.Group7;
            }
            this.isSolved = isSolve;
        }
    }
}