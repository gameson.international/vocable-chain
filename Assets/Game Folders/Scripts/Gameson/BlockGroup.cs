using Gameson;
using System.Collections.Generic;
using UnityEngine;

public class BlockGroup : MonoBehaviour
{
    public Group thisGroup = Group.Group3;

    [SerializeField] private HexaBlock[] allBlocks;

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void InitBlockGroup(string code)
    {
        for (int t = 0; t < allBlocks.Length; t++)
        {
            HexaBlock tmp = allBlocks[t];
            int r = Random.Range(t, allBlocks.Length);
            allBlocks[t] = allBlocks[r];
            allBlocks[r] = tmp;
        }

        for (int i = 0; i < allBlocks.Length; i++)
        {
            allBlocks[i].InitBlock(code[i].ToString());
        }
    }

    public void Solved()
    {
        //anim.Play("solved");
    }
}