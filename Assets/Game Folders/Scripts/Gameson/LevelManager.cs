using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameson
{
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager Instance;

        [SerializeField] private BlockGroup[] allGroups;
        [SerializeField] private LetterContainer[] allLetters;

        [SerializeField] private GroupWord[] currentWord;

        private string currentCode = "";
        private int activePackage = 0;
        private int activeWord = 0;

        private int totalSolved = 0;
        private int attempt = 0;
        private int star = 3;

        public bool isSolving;

        public List<string> trySolving = new List<string>();

        private DatabaseManager database;

        #region EVENT
        public delegate void TryToSolveDelegate(string newText);
        public event TryToSolveDelegate OnTryToSolved;

        public delegate void SolveCodeDelegate(int currentWord);
        public event SolveCodeDelegate OnSolved;

        public delegate void StartGroupWordDelegate(GroupWord[] newWord, int activeWords);
        public event StartGroupWordDelegate OnGroupInitiated;

        public delegate void PowerUpDelegate(PowerUp power);
        public event PowerUpDelegate OnPowerUpInitiated;
        #endregion

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            database = GetComponent<DatabaseManager>();
        }

        private void Start()
        {
            activePackage = PlayerPrefs.GetInt(GameManager.Level, 0);
            GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
        }

        public void PlayPackage(string nama)
        {
            activePackage = GetAllPackage().FindIndex(p => p.namaPackage == nama);
            GameManager.Instance.ChangeState(GameState.Game);
        }

        private void OnDisable()
        {
            GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
        }

        private void Instance_OnStateChanged(GameState newState)
        {
            switch (newState)
            {
                case GameState.Menu:
                    attempt = 0;
                    foreach (var g in allGroups)
                    {
                        g.gameObject.SetActive(false);
                    }
                    break;
                case GameState.Game:
                    StartCoroutine(StartLevel());
                    break;
                case GameState.Result:
                    ResetLevel();
                    break;
                case GameState.Package:
                    foreach (var g in allGroups)
                    {
                        g.gameObject.SetActive(false);
                    }
                    break;
            }
        }

        private void ResetLevel()
        {
            attempt = 0;
            foreach (var b in allGroups)
            {
                b.gameObject.SetActive(false);
            }
        }

        IEnumerator StartLevel()
        {
            yield return new WaitForSeconds(0.5f);
            CreateNewGroup();
            InitGroupWord();
        }

        public void Shuffle()
        {
            //check if grup word is all solved
            if(currentWord[0].isSolved && currentWord[1].isSolved && currentWord[2].isSolved)
            {
                CalculateReward();
                
                return;
            }

            activeWord++;
            if(activeWord > 2)
            {
                activeWord = 0;
            }

            if (currentWord[activeWord].isSolved)
            {
                Shuffle();
                return;
            }

            InitGroupWord();
        }

        private void CalculateReward()
        {
            totalSolved += 3;
            database.GetAllPackages()[activePackage].wordSolved = totalSolved;
            if(totalSolved >= 30)
            {
                activePackage++;
                PlayerPrefs.SetInt(GameManager.Level, activePackage);
            }

            if(attempt < 5)
            {
                star = 3;
                GameManager.Instance.GetPlayerData().score += 10;
                database.GetAllPackages()[activePackage].scorePackage += 10;
            }
            if(attempt >= 5 && attempt < 8)
            {
                star = 2;
                GameManager.Instance.GetPlayerData().score += 7;
                database.GetAllPackages()[activePackage].scorePackage += 7;
            }
            if(attempt > 7)
            {
                star = 1;
                GameManager.Instance.GetPlayerData().score += 3;
                database.GetAllPackages()[activePackage].scorePackage += 3;
            }

            GameManager.Instance.ChangeState(GameState.Result);
        }

        public int GetActiveGroup()
        {
            return activeWord;
        }

        public int GetActivePackage()
        {
            return activePackage;
        }

        private void CreateNewGroup()
        {
            totalSolved = database.GetAllPackages()[activePackage].wordSolved;
            currentWord = database.GetNewCurrentWord(activePackage);
        }

        private void InitGroupWord()
        {
            foreach (var b in allGroups)
            {
                b.gameObject.SetActive(false);
            }
            switch (currentWord[activeWord].thisGrup)
            {
                case Group.Group3:
                    allGroups[0].gameObject.SetActive(true);
                    allGroups[0].InitBlockGroup(currentWord[activeWord].code);
                    break;
                case Group.Group4:
                    allGroups[1].gameObject.SetActive(true);
                    allGroups[1].InitBlockGroup(currentWord[activeWord].code);
                    break;
                case Group.Group5:
                    allGroups[2].gameObject.SetActive(true);
                    allGroups[2].InitBlockGroup(currentWord[activeWord].code);
                    break;
                case Group.Group7:
                    allGroups[3].gameObject.SetActive(true);
                    allGroups[3].InitBlockGroup(currentWord[activeWord].code);
                    break;
            }

            OnGroupInitiated?.Invoke(currentWord, activeWord);
        }

        public void StartSolving(string newCode)
        {
            isSolving = true;
            trySolving.Add(newCode);

            OnTryToSolved.Invoke(newCode);
        }

        public void AddSolveCode(string newCode)
        {
            trySolving.Add(newCode);
            currentCode = "";
            for (int i = 0; i < trySolving.Count; i++)
            {
                currentCode += trySolving[i];
            }
            CheckResult();

            OnTryToSolved.Invoke(currentCode);
        }

        private void CheckResult()
        {
            switch (currentWord[activeWord].thisGrup)
            {
                case Group.Group3:
                    if(trySolving.Count == 3)
                    {
                        attempt++;
                        if (currentCode == currentWord[activeWord].code)
                        {
                            SolveCode(activeWord);
                        }
                    }
                    break;
                case Group.Group4:
                    if (trySolving.Count == 4)
                    {
                        attempt++;
                        if (currentCode == currentWord[activeWord].code)
                        {
                            SolveCode(activeWord);
                        }
                    }
                    break;
                case Group.Group5:
                    if (trySolving.Count == 5)
                    {
                        attempt++;
                        if (currentCode == currentWord[activeWord].code)
                        {
                            SolveCode(activeWord);
                        }
                    }
                    break;
                case Group.Group7:
                    if (trySolving.Count == 7)
                    {
                        attempt++;
                        if (currentCode == currentWord[activeWord].code)
                        {
                            SolveCode(activeWord);
                        }
                    }
                    break;
            }
            if (attempt >= 9)
            {
                GameManager.Instance.ChangeState(GameState.TimesUp);
            }
        }

        private void SolveCode(int number)
        {
            StartCoroutine(SolveCodeCoroutine(number));
        }

        IEnumerator SolveCodeCoroutine(int number)
        {
            yield return new WaitForSeconds(0.5f);
            currentWord[number].isSolved = true;
            allGroups[number].Solved();
            isSolving = false;

            AudioManager.Instance.PlayAudio(SoundName.Victory);
            OnSolved?.Invoke(activeWord);
            yield return new WaitForSeconds(1.5f);
            Shuffle();
        }

        public void ResetSolving()
        {
            isSolving = false;
            trySolving.Clear();
            trySolving = new List<string>();
            currentCode = "";

            OnTryToSolved.Invoke("");
        }

        public Sprite GetLetter(string findLetter)
        {
            LetterContainer l = Array.Find(allLetters, find => find.nama == findLetter);
            return l.gambar;
        }

        public void InitiatePowerUp(PowerUp newPowerUp)
        {
            OnPowerUpInitiated?.Invoke(newPowerUp);
        }

        public void GetReward()
        {

        }

        public int GetTotalWordSolved()
        {
            return totalSolved;
        }
        public int GetTotalScore()
        {
            return database.GetAllPackages()[activePackage].scorePackage;
        }
        public int GetStarsValue()
        {
            return star;
        }

        public List<WordPackage> GetAllPackage()
        {
            return database.GetAllPackages();
        }
    }


    public enum Group
    {
        Group3,
        Group4,
        Group5,
        Group7
    }


    [System.Serializable]
    public class LetterContainer
    {
        public string nama;
        public Sprite gambar;
    } 
}
