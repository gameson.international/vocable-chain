using Gameson;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Transaction : MonoBehaviour
{
    [SerializeField] private TMP_Text labelResult;
    [SerializeField] private TMP_Text labelValue;
    [SerializeField] private TMP_Text labelItem;
    [SerializeField] private Button closeButton;

    private void Start()
    {
        closeButton.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlayAudio(SoundName.Click);
            gameObject.SetActive(false);
        });
    }

    public void InitializeTransaction(bool success , int amount , PowerUp item)
    {
        if (success)
        {
            labelResult.text = $"SUCCESS!";
        }
        else
        {
            labelResult.text = $"FAILED!";
        }
        labelValue.text = $"{amount}";
        labelItem.text = $"{item}";
    }
}
