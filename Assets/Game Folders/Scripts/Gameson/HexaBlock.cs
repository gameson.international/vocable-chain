using Gameson;
using UnityEngine;

public class HexaBlock : MonoBehaviour
{
    [SerializeField] private string currentLetter;

    [SerializeField] private GameObject hover;
    [SerializeField] private SpriteRenderer letter;
    [SerializeField] private TrailRenderer line;

    private bool isUse;

    public void InitBlock(string newLetter)
    {
        currentLetter = newLetter;
        letter.sprite = LevelManager.Instance.GetLetter(currentLetter);
    }

    private void OnMouseDown()
    {
        hover.SetActive(true);
        line.gameObject.SetActive(true);
        AudioManager.Instance.PlayAudio(SoundName.Drag);

        if (!isUse)
        {
            LevelManager.Instance.StartSolving(currentLetter);
            isUse = true;
        }
    }

    private void OnMouseUp()
    {
        hover.SetActive(false);
        line.gameObject.SetActive(false);
        LevelManager.Instance.ResetSolving();

        isUse = false;
    }

    private void OnMouseOver()
    {
        hover.SetActive(LevelManager.Instance.isSolving);

        if (LevelManager.Instance.isSolving && !isUse)
        {
            LevelManager.Instance.AddSolveCode(currentLetter);
            AudioManager.Instance.PlayAudio(SoundName.Drag);
            isUse = true;
        }
    }

    private void OnMouseExit()
    {
        if (!LevelManager.Instance.isSolving)
        {
            hover.SetActive(false);
        }
    }

    private void Update()
    {
        if(!LevelManager.Instance.isSolving)
        {
            hover.SetActive(false);
            isUse = false;
        }

        if(line.gameObject.activeInHierarchy)
        {
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPos.z = 0f;
            line.transform.position = newPos;
        }
    }
}
