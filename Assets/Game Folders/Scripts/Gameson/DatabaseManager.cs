using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Gameson
{
    public partial class DatabaseManager : MonoBehaviour
    {
        public static DatabaseManager Instance;

        [SerializeField] List<WordPackage> allPackage = new List<WordPackage>();

        [SerializeField] private TextAsset csvFile;
        [SerializeField] private LiveDatabase[] database;

        private string[] data;
        private string path;

        private List<string> words = new List<string>();

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            path = Application.persistentDataPath + "/FileData.json";

            if (File.Exists(path))
            {
                LoadJsonDatabase();
                return;
            }

            ReadCsv();
        }

        private void ReadCsv()
        {
            data = csvFile.text.Split(new string[] { ",", "\n" }, StringSplitOptions.None);

            int jumlahPackage = data.Length / 42;

            List<LiveDatabase> liveData = new List<LiveDatabase>();
            
            for (int i = 0; i < jumlahPackage; i++)
            {
                for (int x = i * 42 + 1; x < i * 42 + 42; x++)
                {
                    words.Add(data[x]);
                }

                LiveDatabase newData = new LiveDatabase(data[i * 42], words.ToArray());
                liveData.Add(newData);
                words.Clear();
            }

            database = liveData.ToArray();

            for (int i = 0; i < jumlahPackage; i++)
            {
                string nama = database[i].namaPackage;
                List<GroupWord> allWord = new List<GroupWord>();

                for (int x = 0; x < database[i].allWords.Length - 1; x++)
                {
                    GroupWord newGroup = new GroupWord(database[i].allWords[x], database[i].allWords[x].Length);
                    allWord.Add(newGroup);
                }

                WordPackage newWordPackage = new WordPackage(nama,0 ,0 , allWord.ToArray());
                allPackage.Add(newWordPackage);
            }
            SaveJsonDatabase();
        }

        public GroupWord[] GetNewCurrentWord(int activePackage)
        {
            GroupWord[] newGrup = new GroupWord[3];

            List<GroupWord> grupAvailable = new List<GroupWord>();

            grupAvailable = Array.FindAll(allPackage[activePackage].contentPackage, g => g.isSolved == false).ToList();

            for (int i = 0; i < newGrup.Length; i++)
            {
                int index = UnityEngine.Random.Range(0, grupAvailable.Count);
                newGrup[i] = grupAvailable[index];
                grupAvailable.RemoveAt(index);
            }

            return newGrup;
        }

        public void SaveJsonDatabase()
        {
            WordPackageDatabase newDatabase = new WordPackageDatabase(allPackage);
            string json = JsonUtility.ToJson(newDatabase);
            File.WriteAllText(path, json);
        }

        private void LoadJsonDatabase()
        {
            string json = File.ReadAllText(path);
            WordPackageDatabase temp = JsonUtility.FromJson<WordPackageDatabase>(json);
            for (int i = 0; i < temp.allpackages.Length; i++)
            {
                allPackage.Add(temp.allpackages[i]);
            }
        }

        public void HapusData()
        {
            string path = Application.persistentDataPath;
            DirectoryInfo directoryInfo = new DirectoryInfo(path);

            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                file.Delete();

            }
        }

        public List<WordPackage> GetAllPackages()
        {
            return allPackage;
        }
    }
}