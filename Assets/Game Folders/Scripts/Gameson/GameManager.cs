using System;
using UnityEngine;

namespace Gameson
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public GameState currentState = GameState.Authentication;

        [SerializeField] private PlayerData playerData;

        public static string Coin = "coin";
        public static string Ads = "ads";
        public static string Chest = "chest";
        public static string Bonus = "daily bonus";
        public static string Hint = "hint";
        public static string Arrow = "arrow";
        public static string Rocket = "rocket";
        public static string Level = "level";

        public static string Spin = "spin";
        public static string NewPlayer = "new user";

        public delegate void ChangeStateDelegate(GameState newState);
        public event ChangeStateDelegate OnStateChanged;

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            Application.targetFrameRate = 45;

            playerData.coin = PlayerPrefs.GetInt(Coin, 100);
            playerData.powerUps[0].jumlah = PlayerPrefs.GetInt(Hint, 3);
            playerData.powerUps[1].jumlah = PlayerPrefs.GetInt(Rocket, 0);
            playerData.powerUps[2].jumlah = PlayerPrefs.GetInt(Arrow, 0);
        }

        public void ChangeState(GameState newState)
        {
            if(newState == currentState)
            {
                return;
            }

            Debug.Log($"<color=white>State Change to : {newState}! </color>");

            currentState = newState;
            OnStateChanged?.Invoke(currentState);
        }

        public PlayerData GetPlayerData()
        {
            return playerData;
        }

        public void SaveProgress()
        {
            DatabaseManager.Instance.SaveJsonDatabase();
        }

        public void AddCoin(int r)
        {
            playerData.coin += r;
            PlayerPrefs.SetInt(Coin, playerData.coin);
        }

        public void AddPowerUp(PowerUp power, int amount)
        {
            switch (power)
            {
                case PowerUp.Hint:
                    playerData.powerUps[0].jumlah += amount;
                    PlayerPrefs.SetInt(Hint, playerData.powerUps[0].jumlah);
                    break;
                case PowerUp.Rocket:
                    playerData.powerUps[1].jumlah += amount;
                    PlayerPrefs.SetInt(Rocket, playerData.powerUps[1].jumlah);
                    break;
                case PowerUp.Arrow:
                    playerData.powerUps[2].jumlah += amount;
                    PlayerPrefs.SetInt(Arrow, playerData.powerUps[2].jumlah);
                    break;
            }
        }

        public void UsePowerUp(PowerUp power , int amount)
        {
            switch (power)
            {
                case PowerUp.Hint:
                    playerData.powerUps[0].jumlah -= amount;
                    break;
                case PowerUp.Rocket:
                    playerData.powerUps[1].jumlah -= amount;
                    break;
                case PowerUp.Arrow:
                    playerData.powerUps[2].jumlah -= amount;
                    break;
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.F6))
            {
                AddCoin(500);
            }
        }
    }

    public enum GameState
    {
        Authentication,
        DailyBonus,
        Menu,
        Game,
        Level,
        Setting,
        Package,
        Shop,
        SpinWheels,
        ForceUpdate,
        NoInternet,
        Result,
        Bonus,
        Exit,
        Promo,
        Agreement,
        TimesUp
    }

    [System.Serializable]
    public class PlayerData
    {
        public string playerName;
        public int coin;
        public int score;
        public int spin;

        public PowerUps[] powerUps;
        //public Chest
    }


    [System.Serializable]
    public struct PowerUps
    {
        public PowerUp nama;
        public Sprite icon;
        public int jumlah;
    }
    
}