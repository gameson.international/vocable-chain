using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    private SoundName currentBgm = SoundName.Game;

    [SerializeField] private Sound[] allSounds;

    [SerializeField, Range(0, 1)] private float BgmVolume = 0.4f;
    [SerializeField, Range(0, 1)] private float SfxVolume = 0.67f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        InitiateSound();
    }

    public void PlayAudio(SoundName findName)
    {
        if(findName == currentBgm)
        {
            return;
        }

        Sound temp = Array.Find(allSounds, s => s.soundName == findName);
        switch (temp.tipe)
        {
            case SoundType.BGM:
                foreach (var item in allSounds)
                {
                    item.source.Stop();
                }
                currentBgm = findName;
                temp.source.Play();
                break;
            case SoundType.SFX:
                temp.source.Play();
                break;
        }
    }

    private void InitiateSound()
    {
        foreach (Sound s in allSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.klip;
            switch (s.tipe)
            {
                case SoundType.BGM:
                    s.source.volume = BgmVolume;
                    break;
                case SoundType.SFX:
                    s.source.volume = SfxVolume;
                    break;
            }
        }
    }

    public void Mute(SoundType tipe , bool isOn)
    {
        foreach (var s in allSounds)
        {
            switch (tipe)
            {
                case SoundType.BGM:
                    if (s.tipe == tipe)
                    {
                        if (isOn)
                        {
                            s.source.volume = BgmVolume;
                        }
                        else
                        {
                            s.source.volume = 0f;
                        }
                    }
                    break;
                case SoundType.SFX:
                    if (s.tipe == tipe)
                    {
                        if (isOn)
                        {
                            s.source.volume = SfxVolume;
                        }
                        else
                        {
                            s.source.volume = 0f;
                        }
                    }
                    break;
            }
        }
    }
}

public enum SoundName
{
    Menu,
    Game,
    Wrong,
    Click,
    Hint,
    Victory,
    Star,
    Drag
}

public enum SoundType
{
    BGM,
    SFX
}

[System.Serializable]
public class Sound
{
    public SoundName soundName;
    public AudioClip klip;
    public SoundType tipe;
    public bool isLooping;

    [HideInInspector]
    public AudioSource source;
}
