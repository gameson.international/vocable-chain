using System;
using UnityEngine;

namespace Gameson
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] private Page[] allPages;
        [SerializeField] private Widget[] allWidgets;

        private void Start()
        {
            allPages = GetComponentsInChildren<Page>(true);
            allWidgets = GetComponentsInChildren<Widget>(true);

            GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
            AudioManager.Instance.PlayAudio(SoundName.Menu);
        }

        private void OnDisable()
        {
            GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
        }

        private void Instance_OnStateChanged(GameState newState)
        {
            switch (newState)
            {
                case GameState.Authentication:
                    SetPage(PageName.Authentication);
                    break;
                case GameState.DailyBonus:
                    SetPage(PageName.DailyBonus);
                    break;
                case GameState.Menu:
                    SetPage(PageName.Menu);
                    AudioManager.Instance.PlayAudio(SoundName.Menu);
                    break;
                case GameState.Game:
                    SetPage(PageName.Game);
                    AudioManager.Instance.PlayAudio(SoundName.Game);
                    break;
                case GameState.Setting:
                    SetPage(PageName.Setting);
                    break;
                case GameState.Package:
                    SetPage(PageName.Package);
                    AudioManager.Instance.PlayAudio(SoundName.Menu);
                    break;
                case GameState.Shop:
                    SetPage(PageName.Shop);
                    break;
                case GameState.Level:
                    break;
                case GameState.SpinWheels:
                    SetPage(PageName.SpinWheel);
                    break;
                case GameState.ForceUpdate:
                    ShowWidget(WidgetName.forceUpdate);
                    break;
                case GameState.NoInternet:
                    //ShowWidget(WidgetName.NoInternet);
                    break;
                case GameState.Result:
                    ShowWidget(WidgetName.Result);
                    AudioManager.Instance.PlayAudio(SoundName.Victory);
                    foreach (var pages in allPages)
                    {
                        pages.gameObject.SetActive(false);
                    }
                    break;
                case GameState.Bonus:
                    SetPage(PageName.Bonus);
                    break;
                case GameState.Exit:
                    ShowWidget(WidgetName.Exit);
                    break;
                case GameState.Promo:
                    SetPage(PageName.Promo);
                    break;
                case GameState.Agreement:
                    ShowWidget(WidgetName.Agreement);
                    break;
                case GameState.TimesUp:
                    ShowWidget(WidgetName.TimesUp);
                    break;
            }
        }

        public void SetPage(PageName namaPage)
        {
            foreach (var pages in allPages)
            {
                pages.gameObject.SetActive(false);
            }

            Page findPage = Array.Find(allPages, p => p.nama == namaPage);
            findPage?.gameObject.SetActive(true);
        }

        public void ShowWidget(WidgetName namaWidget)
        {
            Debug.Log($"<color=white>Show Widget {namaWidget}!</color>");
            Widget findWidget = Array.Find(allWidgets, w => w.nama == namaWidget);
            findWidget?.gameObject.SetActive(true);
        }
    }
}
