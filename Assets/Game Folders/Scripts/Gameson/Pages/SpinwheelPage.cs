using EasyUI.PickerWheelUI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class SpinwheelPage : Page
    {
        [SerializeField] private Button b_home;
        [SerializeField] private Button b_spin;
        [SerializeField] private Button b_back;
        [SerializeField] private PickerWheel pickerWheel;

        [SerializeField] private GameObject panel_result;
        [SerializeField] private TMP_Text label_result;

        [SerializeField] private Animator buttonAnim;

        private void Start()
        {
            b_home.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Menu)));
            b_spin.onClick.AddListener(HandleSpin);

            b_back.onClick.AddListener(() => {
                panel_result.SetActive(false);
                StartCoroutine(PageChange(GameState.Menu));
            });

            panel_result.SetActive(false);

            buttonAnim = b_spin.GetComponent<Animator>();
            buttonAnim.SetBool("isActive", true);

            label_result.gameObject.SetActive(false);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            Actions.OnRewardedAdLoadSuccess += OnRewardedAdLoadSuccess;

            pickerWheel.OnSpinStart(OnSpinStart);
            pickerWheel.OnSpinEnd(OnEndSpin);
        }

        private void OnDisable()
        {
            Actions.OnRewardedAdLoadSuccess -= OnRewardedAdLoadSuccess;
        }

        private void HandleSpin()
        {
            AdsManager.Instance.RequestRewardedAds("Daily Spin" , OnGetReward);
        }

        private void OnGetReward(string obj)
        {
            pickerWheel.Spin();
        }

        private void OnRewardedAdLoadSuccess()
        {
            b_spin.interactable = true;
        }

        private void OnSpinStart()
        {
            buttonAnim.SetBool("isActive", false);
            b_spin.interactable = false;
        }

        private void OnEndSpin(WheelPiece pieces)
        {
            //ShowResult($"<size=50>Congratulation!</size> <br> You Get <size=35><color=green>{pieces.Amount}</color> Coin!!");
            b_spin.interactable = true;

            int coin = PlayerPrefs.GetInt(GameManager.Coin, 0);
            coin += pieces.Amount;

            PlayerPrefs.SetInt(GameManager.Coin, coin);
            PlayerPrefs.SetString(GameManager.Chest, DateTime.Now.ToString());

            panel_result.SetActive(true);
            label_result.gameObject.SetActive(true);
            label_result.text = pieces.Amount.ToString();

            //GameManager.Instance.CheckSetup();
            GameManager.Instance.GetComponent<GameServices>().RequestNotification("Check Your Game!", "Dont forget to claim your reward today!", 2);
        }
    }
}
