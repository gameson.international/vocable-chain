using Gameson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PackagePage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private packageCard itemCard;
    [SerializeField] private Transform content;

    protected override void OnEnable()
    {
        base.OnEnable();
        foreach  (Transform t in content)
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < LevelManager.Instance.GetAllPackage().Count; i++)
        {
            packageCard item = Instantiate(itemCard, content);
            item.InitiateCard(LevelManager.Instance.GetAllPackage()[i].namaPackage, LevelManager.Instance.GetAllPackage()[i].wordSolved, LevelManager.Instance.GetAllPackage()[i].scorePackage);
        }
    }

    private void Start()
    {
        b_home.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Menu)));
    }
}
