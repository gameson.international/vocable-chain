using Gameson;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private Button b_package;
    [SerializeField] private Button b_shop;

    //button power ups
    [SerializeField] private Button b_hint;
    [SerializeField] private Button b_rocket;
    [SerializeField] private Button b_shuffle;
    [SerializeField] private Button b_arrow;

    [SerializeField] private TMP_Text label_coin;
    [SerializeField] private TMP_Text label_package;

    [SerializeField] private TMP_Text label_solve;

    [SerializeField] private TMP_Text label_jumlahHint;
    [SerializeField] private TMP_Text label_jumlahRocket;
    [SerializeField] private TMP_Text label_jumlahArrow;

    [SerializeField] private Notif_PowerUp notifier;

    [SerializeField] private MiniBlockGroup[] allGroups;

    private void Start()
    {
        allGroups = GetComponentsInChildren<MiniBlockGroup>();

        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        b_package.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Package));
        b_shop.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Shop));

        b_hint.onClick.AddListener(GetHint);
        b_rocket.onClick.AddListener(GetRocket);
        b_arrow.onClick.AddListener(GetArrow);

        b_shuffle.onClick.AddListener(() => LevelManager.Instance.Shuffle());
    }
    private void SetupPowerUps()
    {
        label_jumlahHint.text = $"{GameManager.Instance.GetPlayerData().powerUps[0].jumlah}";
        label_jumlahRocket.text = $"{GameManager.Instance.GetPlayerData().powerUps[1].jumlah}";
        label_jumlahArrow.text = $"{GameManager.Instance.GetPlayerData().powerUps[2].jumlah}";
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        LevelManager.Instance.OnTryToSolved += Instance_OnTryToSolved;
        LevelManager.Instance.OnGroupInitiated += Instance_OnGroupInitiated;
        LevelManager.Instance.OnSolved += Instance_OnSolved;
        LevelManager.Instance.OnPowerUpInitiated += Instance_OnPowerUpInitiated;

        SetupPowerUps();

        label_coin.text = $"{GameManager.Instance.GetPlayerData().coin}";
        label_package.text = $"{LevelManager.Instance.GetAllPackage()[LevelManager.Instance.GetActivePackage()].namaPackage}";
    }

    

    private void OnDisable()
    {
        LevelManager.Instance.OnTryToSolved -= Instance_OnTryToSolved;
        LevelManager.Instance.OnGroupInitiated -= Instance_OnGroupInitiated; 
        LevelManager.Instance.OnSolved -= Instance_OnSolved;
        LevelManager.Instance.OnPowerUpInitiated -= Instance_OnPowerUpInitiated;

        foreach (var item in allGroups)
        {
            item.Hide();
        }
    }

    private void Instance_OnGroupInitiated(GroupWord[] newWord , int activeWord)
    {
        for (int i = 0; i < allGroups.Length; i++)
        {
            allGroups[i].InitBlock(newWord[i]);
        }
        allGroups[activeWord].CurrentActive(activeWord);
    }

    private void Instance_OnTryToSolved(string newText)
    {
        label_solve.text = newText;
    }

    private void Instance_OnSolved(int currentWord)
    {
        allGroups[currentWord].Solved();
    }

    private void Instance_OnPowerUpInitiated(PowerUp power)
    {
        int n = LevelManager.Instance.GetActiveGroup();

        switch (power)
        {
            case PowerUp.Hint:
                allGroups[n].OnGetHint();
                break;
            case PowerUp.Rocket:
                foreach (var item in allGroups)
                {
                    item.OnGetRocket();
                }
                break;
            case PowerUp.Arrow:
                allGroups[n].OnGetArrow();
                break;
        }
    }

    

    private void GetArrow()
    {
        int n = GameManager.Instance.GetPlayerData().powerUps[2].jumlah;
        if(n > 0)
        {
            GameManager.Instance.GetPlayerData().powerUps[2].jumlah--;
            notifier.Show(GameManager.Instance.GetPlayerData().powerUps[2].nama);
            LevelManager.Instance.InitiatePowerUp(PowerUp.Arrow);
        }
        SetupPowerUps();
    }

    private void GetRocket()
    {
        int n = GameManager.Instance.GetPlayerData().powerUps[1].jumlah;
        if (n > 0)
        {
            GameManager.Instance.GetPlayerData().powerUps[1].jumlah--;
            notifier.Show(GameManager.Instance.GetPlayerData().powerUps[1].nama);
            LevelManager.Instance.InitiatePowerUp(PowerUp.Rocket);
        }
        SetupPowerUps();
    }

    private void GetHint()
    {
        int n = GameManager.Instance.GetPlayerData().powerUps[0].jumlah;
        if (n > 0)
        {
            GameManager.Instance.UsePowerUp(PowerUp.Hint, 1);

            notifier.Show(GameManager.Instance.GetPlayerData().powerUps[0].nama);
            LevelManager.Instance.InitiatePowerUp(PowerUp.Hint);
        }
        SetupPowerUps();
    }
}