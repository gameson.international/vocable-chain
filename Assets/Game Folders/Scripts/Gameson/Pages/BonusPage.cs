using Gameson;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BonusPage : Page
{
    [SerializeField] private Button b_home;

    [Header("DAILY LOGIN")]
    [SerializeField] private ItemBonusDaily[] itemBonuses;
    [SerializeField] private Button b_collect;

    [Header("FREE SPIN")]
    [SerializeField] private Animator panelFreeSpinAnimator;
    [SerializeField] private Button b_getSpin;
    [SerializeField] private TMP_Text labelTimer;

    private int activeDay = 0;
    private DateTime playerSignInDate;
    private string SignInDate;
    
    private bool isFreeSpinActive;
    private DateTime saveTime;
    private string tanggal;


    protected override void OnEnable()
    {
        base.OnEnable();

        CheckDailyBonus();
        CheckFreeBonus();
    }

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        b_collect.onClick.AddListener(CollectDailyBonus);
        b_getSpin.onClick.AddListener(CollectFreeSpin);
    }

    private void CheckDailyBonus()
    {
        SignInDate = PlayerPrefs.GetString(GameManager.NewPlayer);
        playerSignInDate = DateTime.Parse(SignInDate);

        DateTime sekarang = DateTime.Now;

        TimeSpan span = sekarang - playerSignInDate;
        activeDay = span.Days;

        for (int i = 0; i < itemBonuses.Length; i++)
        {
            itemBonuses[i].Init(i);

            if (i == activeDay)
            {
                itemBonuses[i].ReadyToClaim();
            }

            if (i < activeDay)
            {
                itemBonuses[i].GetCollected();
            }
        }

        b_collect.interactable = !itemBonuses[activeDay].isCollected;
    }

    private void CheckFreeBonus()
    {
        if (PlayerPrefs.HasKey(GameManager.Bonus))
        {
            tanggal = PlayerPrefs.GetString(GameManager.Bonus);
            saveTime = DateTime.Parse(tanggal);

            DateTime nextDate = saveTime.AddHours(2);
            DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

            TimeSpan span = nextDate - sekarang;
            isFreeSpinActive = span.Hours <= 0 && span.Minutes <= 0;

            if(!isFreeSpinActive)
            {
                b_getSpin.gameObject.SetActive(false);
                isFreeSpinActive = false;
                panelFreeSpinAnimator.Play("idle");
            }
        }
        else
        {
            isFreeSpinActive = true;
            panelFreeSpinAnimator.Play("active");
        }
    }

    private void CollectFreeSpin()
    {
        if (isFreeSpinActive)
        {
            b_getSpin.gameObject.SetActive(false);
            PlayerPrefs.SetString(GameManager.Bonus, DateTime.Now.ToString());
            isFreeSpinActive = false;

            GameManager.Instance.ChangeState(GameState.SpinWheels);
        }
    }

    private void CollectDailyBonus()
    {
        if(itemBonuses[activeDay].isCollected)
        {
            return;
        }
        GameManager.Instance.AddCoin(itemBonuses[activeDay].GetBonusAmount());
        itemBonuses[activeDay].GetCollected();
        b_collect.interactable = false;
    }

    private void Update()
    {
        if (!isFreeSpinActive)
        {
            saveTime = DateTime.Parse(tanggal);
            DateTime nextDate = saveTime.AddHours(2);

            DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

            TimeSpan span = nextDate - sekarang;
            labelTimer.text = span.ToString();
        }
    }
}


public enum ChestType
{
    Basic,
    Rare,
    Legend
}

[System.Serializable]
public class ItemBonusDaily
{
    [SerializeField] private int amount;
    [SerializeField] private GameObject objToday;
    [SerializeField] private GameObject objCheckmark;

    [SerializeField] private TMP_Text labelAmount;

    [SerializeField] public bool isReady, isCollected;

    private int dayNumber;

    public void Init(int n)
    {
        dayNumber = n;
        labelAmount.text = $" + {amount}";
        isCollected = PlayerPrefs.HasKey($"Bonus{dayNumber}");
    }

    public int GetBonusAmount()
    {
        return amount;
    }

    public void GetCollected()
    {
        isCollected = true;
        objCheckmark.SetActive(true);
        objToday.SetActive(false);
        PlayerPrefs.SetInt($"Bonus{dayNumber}" , dayNumber);
    }

    public void ReadyToClaim()
    {
        isReady = true;
        objCheckmark.SetActive(false);
        objToday.SetActive(true);
        if(isCollected)
        {
            GetCollected();
        }
    }
}
