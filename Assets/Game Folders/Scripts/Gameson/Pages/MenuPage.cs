using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class MenuPage : Page
    {
        [SerializeField] private Button b_link;
        [SerializeField] private Button b_misteryChest;
        [SerializeField] private Button b_leaderboard;
        [SerializeField] private Button b_playGame;
        [SerializeField] private Button b_exit;
        [SerializeField] private Button[] b_shop;
        [SerializeField] private Button b_setting;

        [SerializeField , TextArea(2,2)] private string link;

        [SerializeField] private TMP_Text label_coin;

        protected override void OnEnable()
        {
            base.OnEnable();

            label_coin.text = $"{GameManager.Instance.GetPlayerData().coin}";

            if(!PlayerPrefs.HasKey(GameManager.NewPlayer))
            {
                GameManager.Instance.ChangeState(GameState.Agreement);
            }
        }

        private void Start()
        {
            b_exit.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Exit));
            b_link.onClick.AddListener(() => OpenUrl(link));
            b_playGame.onClick.AddListener(PlayGame);
            b_setting.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Setting)));
            b_misteryChest.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Bonus));
            b_leaderboard.onClick.AddListener(() => GameService.Instance.ShowLeaderboard());

            foreach (var b in b_shop)
            {
                b.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Shop)));
            }
            AdsManager.Instance.RequestBanner();
        }
        private void PlayGame()
        {
            int n = 0;

            if (PlayerPrefs.HasKey("LEVELSAATINI"))
            {
                n = PlayerPrefs.GetInt("LEVELSAATINI");
            }
            GameManager.Instance.ChangeState(GameState.Game);
        }

        private void OpenUrl(string link)
        {
            Application.OpenURL(link);
        }
    }
}
