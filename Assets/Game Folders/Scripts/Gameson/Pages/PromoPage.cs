using Gameson;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PromoPage : Page
{
    [SerializeField] private Button b_knightJumper;   
    [SerializeField] private Button b_kartToon;   
    [SerializeField] private Button b_close;

    [SerializeField] private string uri_knight;
    [SerializeField] private string uri_kart;

    [SerializeField] private GameObject[] panelPromos;
    private int promoIndex = 0;

    private Vector2 startTouchPosition;
    private Vector2 endTouchPosition;

    private void Start()
    {
        b_knightJumper.onClick.AddListener(() => OpenApp(uri_knight));
        b_kartToon.onClick.AddListener(() => OpenApp(uri_kart));

        b_close.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
    }

    private void OpenApp(string uri)
    {
        Application.OpenURL(uri);
    }

    private void Update()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouchPosition = Input.GetTouch(0).position;

            if(endTouchPosition.x > startTouchPosition.x)
            {
                NextPage();
            }
            if(endTouchPosition.x < startTouchPosition.x)
            {
                NextPage();
            }
        }
    }

    private void NextPage()
    {
        promoIndex++;
        if(promoIndex > panelPromos.Length - 1)
        {
            promoIndex = 0;
        }
        foreach (var p in panelPromos)
        {
            p.SetActive(false);
        }

        panelPromos[promoIndex].SetActive(true);
    }
}
