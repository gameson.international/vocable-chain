using UnityEngine;

namespace Gameson
{
    public class AuthenticationPage : Page
    {
        private void Start()
        {
            isUsingEndAnimation = false;
        }

        public void EndAnimation()
        {
            StartCoroutine(PageChange(GameState.Promo));
        }
    }
}
