using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class ShopPage : Page
    {
        [SerializeField] private Button b_home;
        [SerializeField] private GameObject panel_loading;
        [SerializeField] private TMP_Text label_coin;

        [SerializeField] private Button hintButton;
        [SerializeField] private Button arrowButton;
        [SerializeField] private Button rocketButton;

        [SerializeField] private Transaction transaction;

        private void Start()
        {
            SetCoinLabel();

            b_home.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Menu)));

            hintButton.onClick.AddListener(() => BuyPower(PowerUp.Hint));
            arrowButton.onClick.AddListener(() => BuyPower(PowerUp.Arrow));
            rocketButton.onClick.AddListener(() => BuyPower(PowerUp.Rocket));
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SetCoinLabel();
        }

        private void BuyPower(PowerUp item)
        {
            transaction.gameObject.SetActive(true);
            switch (item)
            {
                case PowerUp.Hint:
                    if (GameManager.Instance.GetPlayerData().coin < 250)
                    {
                        transaction.InitializeTransaction(false, 10, PowerUp.Hint);
                        return;
                    }
                    GameManager.Instance.AddPowerUp(PowerUp.Hint, 10);
                    GameManager.Instance.AddCoin(-250);
                    transaction.InitializeTransaction(true, 10, PowerUp.Hint);
                    break;
                case PowerUp.Rocket:
                    if (GameManager.Instance.GetPlayerData().coin < 300)
                    {
                        transaction.InitializeTransaction(false, 10, PowerUp.Rocket);
                        return;
                    }
                    GameManager.Instance.AddPowerUp(PowerUp.Rocket, 10);
                    GameManager.Instance.AddCoin(-300);
                    transaction.InitializeTransaction(true, 10, PowerUp.Rocket);
                    break;
                case PowerUp.Arrow:
                    if (GameManager.Instance.GetPlayerData().coin < 275)
                    {
                        transaction.InitializeTransaction(false, 10, PowerUp.Arrow);
                        return;
                    }
                    GameManager.Instance.AddPowerUp(PowerUp.Arrow, 10);
                    GameManager.Instance.AddCoin(-275);
                    transaction.InitializeTransaction(true, 10, PowerUp.Arrow);
                    break;
            }

        }

        private void SetCoinLabel()
        {
            label_coin.text = $"{GameManager.Instance.GetPlayerData().coin}";
        }

        public void Buy()
        {
            panel_loading.SetActive(true);

            StartCoroutine(Loading());
        }

        

        IEnumerator Loading()
        {
            yield return new WaitForSeconds(1.5f);
            panel_loading.SetActive(false);
            SetCoinLabel();
        }

        public void OnSuccessPurchasing(int n)
        {
            if (n == 50)
            {
                //GameManager.Instance.MoreCoin(500);
                SetCoinLabel();
                AdjustEvent adjustEvent = new AdjustEvent("jw5ddy");
                Adjust.trackEvent(adjustEvent);
            }
            if (n == 100)
            {
                //GameManager.Instance.MoreCoin(1000);
                SetCoinLabel();
                AdjustEvent adjustEvent = new AdjustEvent("7w40ar");
                Adjust.trackEvent(adjustEvent);
            }
            if (n == 1000)
            {
                //remove all ads
                PlayerPrefs.SetString(GameManager.Ads, "non active");
                AdjustEvent adjustEvent = new AdjustEvent("hhqezc");
                Adjust.trackEvent(adjustEvent);
            }
        }

        public void OnfailedPurchasing()
        {
            AdjustEvent adjustEvent = new AdjustEvent("n9ar7l");
            Adjust.trackEvent(adjustEvent);
        }
    }


}
