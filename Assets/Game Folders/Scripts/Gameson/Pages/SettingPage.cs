using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class SettingPage : Page
    {
        [SerializeField] private Button b_home;
        [SerializeField] private Button b_reset;
        [SerializeField] private Button b_privacyPolicy;

        [SerializeField] private Toggle toggleBgm;
        [SerializeField] private Toggle toggleSfx;

        [SerializeField] private TMP_Dropdown dropdownLanguage;
        [SerializeField] private TMP_Text labelDropdown;

        [SerializeField] private GameObject panel_confirmation;
        [SerializeField] private Button b_yes;
        [SerializeField] private Button b_no;

        [SerializeField, TextArea(3, 3)] private string uri;

        [SerializeField] private TMP_Text label_version;

        private void Start()
        {
            b_home.onClick.AddListener(() => StartCoroutine(PageChange(GameState.Menu)));
            b_reset.onClick.AddListener(() => panel_confirmation.SetActive(true));
            b_privacyPolicy.onClick.AddListener(() => Application.OpenURL(uri));

            b_no.onClick.AddListener(() => panel_confirmation.SetActive(false));
            b_yes.onClick.AddListener(ResetGameData);

            label_version.text = $"Current version : {Application.version}";
            dropdownLanguage.onValueChanged.AddListener((int n) =>
            {
                if(n == 0)
                {
                    Lean.Localization.LeanLocalization.SetCurrentLanguageAll("English");
                }
                if(n == 1)
                {
                    Lean.Localization.LeanLocalization.SetCurrentLanguageAll("Indonesian");
                }
            });
            if(Lean.Localization.LeanLocalization.GetFirstCurrentLanguage() =="English")
            {
                dropdownLanguage.value = 0;
            }
            else
            {
                dropdownLanguage.value = 1;
            }

            labelDropdown.text = Lean.Localization.LeanLocalization.GetFirstCurrentLanguage();

            toggleBgm.onValueChanged.AddListener(HandleBGM);
            toggleSfx.onValueChanged.AddListener(HandleSFX);
        }

        private void HandleBGM(bool isOn)
        {
            AudioManager.Instance.Mute(SoundType.BGM , isOn);
        }

        private void HandleSFX(bool isOn)
        {
            AudioManager.Instance.Mute(SoundType.SFX, isOn);
        }

        private void ResetGameData()
        {
            panel_confirmation.SetActive(false);
            PlayerPrefs.DeleteAll();

            DatabaseManager.Instance.HapusData();
            StartCoroutine(PageChange(GameState.Menu));
            Application.Quit();
        }

        private void Update()
        {
            
        }
    }
}
