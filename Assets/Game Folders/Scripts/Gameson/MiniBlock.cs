using Gameson;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MiniBlock : MonoBehaviour
{
    [SerializeField] private Image icon_letter;
    [SerializeField] private string code;

    public bool isRevealed;

    private Button b_reveal;
    private Animator anim;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        b_reveal = GetComponent<Button>();
          
        anim.Play("idle");
    }

    private void Start()
    {
        b_reveal.onClick.AddListener(Revealed);
    }

    public void InitBlock(string newCode)
    {
        code = newCode;
        icon_letter.sprite = LevelManager.Instance.GetLetter(code);
        isRevealed = false;
    }

    public void ActiveBlock()
    {
        if (gameObject.activeInHierarchy)
        {
            anim.Play("active");
        }
    }

    public void Revealed()
    {
        if (gameObject.activeInHierarchy)
        {
            anim.Play("revealed");
            icon_letter.gameObject.SetActive(true);

            MiniBlockGroup group = GetComponentInParent<MiniBlockGroup>();
            group.ResetBlock();
            isRevealed = true;
        }
    }

    public void Hide()
    {
        icon_letter.gameObject.SetActive(false);
    }

    public void OnGetRocket()
    {

    }

    internal void SetArrowTarget()
    {
        b_reveal.interactable = true;
    }
}
