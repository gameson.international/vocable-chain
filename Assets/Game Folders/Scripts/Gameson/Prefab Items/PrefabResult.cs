using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PrefabResult : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TMP_Text label_value;

    public void SetItemReward(Sprite gambar , int total)
    {
        icon.sprite = gambar;
        label_value.text = $"+ {total}";

    }
}