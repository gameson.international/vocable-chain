﻿namespace Gameson
{
    [System.Serializable]
    public class LiveDatabase
    {
        public string namaPackage;
        public string[] allWords;

        public LiveDatabase(string nama, string[] words)
        {
            namaPackage = nama;
            allWords = words;
        }
    }
}