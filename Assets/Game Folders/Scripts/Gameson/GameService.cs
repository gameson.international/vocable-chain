using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Core;
using UnityEngine;
using UnityEngine.Networking;

namespace Gameson
{
    public class GameService : MonoBehaviour
    {
        public static GameService Instance;

        [SerializeField] private string GamesonServerUri = "http://gameson.r3ste.mobi/";
        [SerializeField] private string leaderboardId;

        private GamesonData gamesonServer;
        [SerializeField] private DataServer findGameServerData;

        private async void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            await UnityServices.InitializeAsync();

            if (AuthenticationService.Instance.IsSignedIn)
            {
                return;
            }
#if UNITY_ANDROID
            PlayGamesPlatform.Activate();
            PlayGamesPlatform.Instance.Authenticate(OnPlayGamesSignedIn);

            AuthenticationService.Instance.SignedIn += Instance_SignedIn;
            AuthenticationService.Instance.SignInFailed += Instance_SignInFailed;
#endif

            StartCoroutine(CheckAppVersion());
        }

        private void Instance_SignedIn()
        {
        }


        private void Instance_SignInFailed(RequestFailedException obj)
        {
        }

        IEnumerator CheckAppVersion()
        {
            UnityWebRequest request = UnityWebRequest.Get(GamesonServerUri);
            yield return request.SendWebRequest();

            switch (request.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    string json = request.downloadHandler.text;
                    gamesonServer = JsonUtility.FromJson<GamesonData>(json);
                    
                    break;
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                default:
                    break;
            }
        }
        public string GetGameCurrentVersion()
        {
            return findGameServerData.DataVersion;
        }

        private void Update()
        {
            switch (Application.internetReachability)
            {
                case NetworkReachability.NotReachable:
                    //GameManager.Instance.ChangeState(GameState.NoInternet);
                    break;
                case NetworkReachability.ReachableViaCarrierDataNetwork:
                    break;
                case NetworkReachability.ReachableViaLocalAreaNetwork:
                    break;
            }
        }

        public void SetNewLeaderboard(int newScore)
        {
            PlayGamesPlatform.Instance.ReportScore(newScore, leaderboardId, (bool success) => {  });
        }

        public void ShowLeaderboard()
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboardId);
        }

        private async void OnPlayGamesSignedIn(SignInStatus status)
        {
            switch (status)
            {
                case SignInStatus.Success:
                    PlayGamesPlatform.Instance.RequestServerSideAccess(true, OnGetCallbacks);
                    //b_highScore.gameObject.SetActive(true);
                    break;
                case SignInStatus.InternalError:
                    await SignInAnonymouslyAsync();
                    //b_highScore.gameObject.SetActive(false);
                    break;
                case SignInStatus.Canceled:
                    await SignInAnonymouslyAsync();
                    //b_highScore.gameObject.SetActive(false);
                    break;
            }
        }

        private async void OnGetCallbacks(string token)
        {
            await SignInWithGooglePlayGamesAsync(token);
        }

        async Task SignInWithGooglePlayGamesAsync(string authCode)
        {
            try
            {
                await AuthenticationService.Instance.SignInWithGooglePlayGamesAsync(authCode);
            }
            catch (AuthenticationException ex)
            {
                // Compare error code to AuthenticationErrorCodes
                // Notify the player with the proper error message
                Debug.LogException(ex);
            }
            catch (RequestFailedException ex)
            {
                // Compare error code to CommonErrorCodes
                // Notify the player with the proper error message
                Debug.LogException(ex);
            }
        }

        async Task SignInAnonymouslyAsync()
        {
            try
            {
                await AuthenticationService.Instance.SignInAnonymouslyAsync();

                // Shows how to get the playerID
                Debug.Log($"PlayerID: {AuthenticationService.Instance.PlayerId}");

            }
            catch (AuthenticationException ex)
            {
                // Compare error code to AuthenticationErrorCodes
                // Notify the player with the proper error message
                Debug.LogException(ex);
            }
            catch (RequestFailedException ex)
            {
                // Compare error code to CommonErrorCodes
                // Notify the player with the proper error message
                Debug.LogException(ex);
            }
        }
    }
}