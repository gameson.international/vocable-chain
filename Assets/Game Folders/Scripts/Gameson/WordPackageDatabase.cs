﻿using System.Collections.Generic;

namespace Gameson
{
    [System.Serializable]
    public class WordPackageDatabase
    {
        public WordPackage[] allpackages;

        public WordPackageDatabase(List<WordPackage> packages)
        {
            allpackages = packages.ToArray();
        }
    }
}